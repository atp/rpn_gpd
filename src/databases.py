"""Module for databases.
"""
import os
from abc import ABC, abstractmethod
from functools import partial

import numpy as np
import tables as tab

from ground_truth import findGroundTruthPositives

os.environ["BLOSC_NTHREADS"] = str(os.cpu_count())

# High compression/decompression speed
# filters = tab.Filters(complevel=9, complib="blosc:lz4")

# Slow compression, fast decompression, less space usage than lz4 above
filters = tab.Filters(complevel=5, complib="blosc:lz4hc")

# Use a chunk cache size of 2GB.
chunk_cache_size = 2 * 2 ** 30


class Database(ABC):
    """Abstract base class for databases.

    A database contains meta data and grasp data. The meta data describes the object
    categories, object instances, camera viewpoints and sample points. The grasp data
    depends on the algorithm used to generate the grasps. Typically, it consists of
    images and labels.

    Attributes:
        filepath (str): the path to the database file.
        file (tables.File): the file handle for the database.
        meta (dict): the meta data.
        data (dict): the grasp data.
    """

    def __init__(self, path, rows):
        """Construct a database with nodes used by all subclasses.

        Args:
            path (str): path to database file.
            rows (int): maximum number of rows in the database.
        """
        self.filepath = path
        self.file = tab.open_file(path, "w", chunk_cache_size=chunk_cache_size)
        self.file.create_group("/", "meta")
        self.meta = {
            "categories": self.file.create_carray(
                "/meta", "categories", tab.StringAtom(50), shape=(rows,)
            ),
            "instances": self.file.create_carray(
                "/meta", "instances", tab.StringAtom(50), shape=(rows,)
            ),
            "viewpoints": self.file.create_carray(
                "/meta", "viewpoints", tab.Float32Atom(), shape=(rows, 3)
            ),
            "samples": self.file.create_carray(
                "/meta", "samples", tab.Float32Atom(), shape=(rows, 3)
            ),
        }
        self.data = {}
        self.actual_rows = 0
        self.max_rows = rows

    def __str__(self):
        s = "Database:\n"
        s += str(self.file.list_nodes("/"))
        return s

    def addInstanceData(self, data, category, instance):
        """Add data collected for an object instance.

        <data> is a list of dictionaries of the form {graspdata, samples, viewpoint}
        (see DataGenerator->createInstanceData).

        Args:
            data (list of dicts): the instance data (see above).
            category_idx (str): the name of the object category.
            instance (str): the name of the object instance.
        """
        start = self.actual_rows
        for i in range(len(data)):
            n = len(data[i]["samples"])
            stop = start + n
            for k, v in data[i]["graspdata"].items():
                if k in self.data:
                    self.data[k][start:stop] = v
            self.meta["categories"][start:stop] = [category] * n
            self.meta["instances"][start:stop] = [instance] * n
            self.meta["viewpoints"][start:stop] = data[i]["viewpoint"]
            self.meta["samples"][start:stop] = data[i]["samples"]
            start = stop
        self.actual_rows = stop

    def addData(self, data):
        """Add data to the datbase.

        Args:
            data (dict): the data to be added.
        """
        start = self.actual_rows
        stop = start + len(data["meta"]["categories"])
        for k in self.meta:
            self.meta[k][start:stop] = data["meta"][k]
        for k in self.data:
            self.data[k][start:stop] = data[k]
        self.actual_rows = stop

    def shrink(self):
        """Shrink the database by removing empty rows from its arrays."""
        print(f"Database has {self.actual_rows} rows (max: {self.max_rows}).")
        if self.actual_rows < self.max_rows:
            self.file.close()
            shrinkDatabase(self.filepath, self.actual_rows)

    def deriveBinaryLabels(self):
        """Derive binary labels for this database."""
        deriveBinaryLabels(self.filepath)


class ImagesDatabase(Database):
    """A database containing labels and images."""

    def __init__(self, path, labels_shape, img_shape, chunkrows=50):
        """Construct a database for storing labels and images.

        Args:
            path (str): the path to the database file.
            labels_shape (tuple): the expected shape of the labels array.
            img_shape (tuple): the expected shape of the images array.
        """
        super().__init__(path, labels_shape[0])
        self.data = {}
        self.data["labels"] = self.file.create_carray(
            "/", "labels", tab.Float32Atom(), shape=labels_shape, filters=filters
        )
        if len(labels_shape) == 2:
            self.data["rot_idxs"] = self.file.create_carray(
                "/",
                "rot_idxs",
                tab.Int32Atom(),
                (labels_shape[0],),
                filters=filters,
            )
        imgs_shape = (labels_shape[0],) + img_shape
        imgs_chunkshape = (chunkrows,) + img_shape
        self.data["images"] = self.file.create_carray(
            "/",
            "images",
            tab.UInt8Atom(),
            shape=imgs_shape,
            filters=filters,
            chunkshape=imgs_chunkshape,
        )


class AnchorsDatabase(Database):
    """A database containing labels, proposal images, and grasp images."""

    def __init__(
        self,
        path,
        labels_shape,
        prop_img_shape,
        grasp_img_shape,
        chunkrows=50,
    ):
        """Construct a database for storing labels, proposal images and grasp images.

        Args:
            path (str): the path to the database file.
            labels_shape (tuple): the expected shape of the labels array.
            prop_img_shape (tuple): the expected shape of the proposal images array.
            grasp_img_shape (tuple): the expected shape of the grasp images array.
            chunkrows (int, optional): the number of rows for database chunking.
        """
        rows, cols = labels_shape[0], labels_shape[1]
        super().__init__(path, rows)
        self.data["labels"] = self.file.create_carray(
            "/", "labels", tab.Float32Atom(), shape=labels_shape, filters=filters
        )

        func = partial(
            self.file.create_carray, where="/", atom=tab.UInt8Atom(), filters=filters
        )
        self.data["prop_imgs"] = func(
            name="prop_imgs",
            shape=(rows,) + prop_img_shape,
            chunkshape=(chunkrows,) + prop_img_shape,
        )
        self.data["grasp_imgs"] = func(
            name="grasp_imgs",
            shape=(rows, cols) + grasp_img_shape,
            chunkshape=(chunkrows, cols) + grasp_img_shape,
        )


class CombinedDatabase(Database):
    """A database for both MA and GPD."""

    def __init__(
        self,
        path,
        rows,
        gpd_cfgs,
        ma_cfgs,
        chunkrows=50,
    ):
        """Construct a database for both MA and GPD.

        Args:
            path (str): the path to the database file.
            rows (int): the number of rows expected to be stored in the database.
            gpd_cfgs (dict): configuration for GPD arrays in the database.
            ma_cfgs (dict): configuration for MA arrays in the database.
            chunkrows (int, optional): the number of rows for database chunking.
        """
        super().__init__(path, rows)

        self.file.create_group("/", "gpd")
        self.data["gpd"] = {}
        for k, v in gpd_cfgs.items():
            self.data["gpd"][k] = self.file.create_carray(
                "/gpd",
                k,
                v["dtype"],
                (rows,) + v["shape"],
                filters=filters,
                chunkshape=(chunkrows,) + v["shape"],
            )

        self.file.create_group("/", "ma")
        self.data["ma"] = {}
        for k, v in ma_cfgs.items():
            self.data["ma"][k] = self.file.create_carray(
                "/ma",
                k,
                v["dtype"],
                (rows,) + v["shape"],
                filters=filters,
                chunkshape=(chunkrows,) + v["shape"],
            )

    def addInstanceData(self, data, category_idx, instance):
        """Add data collected for an object instance.

        <data> is a list of dictionaries of the form {gpd, ma}, where each dictionary
        is of the form {graspdata, samples, viewpoint}.

        Args:
            data (list of dicts): the instance data (see above).
            category_idx (int): the index of the object category.
            instance (str): the name of the object instance.
        """
        start = self.actual_rows
        for i in range(len(data)):
            n = len(data[i]["samples"])
            stop = start + n
            for k, v in data[i]["graspdata"]["gpd"].items():
                if k in self.data["gpd"]:
                    self.data["gpd"][k][start:stop] = v
                else:
                    print(f"Warning: {k} does not exist in group <gpd>")
                    continue
            for k, v in data[i]["graspdata"]["ma"].items():
                if k in self.data["ma"]:
                    self.data["ma"][k][start:stop] = v
                else:
                    print(f"Warning: {k} does not exist in group <ma>")
                    continue
            self.meta["categories"][start:stop] = [category_idx] * n
            self.meta["instances"][start:stop] = [instance] * n
            self.meta["viewpoints"][start:stop] = data[i]["viewpoint"]
            self.meta["samples"][start:stop] = data[i]["samples"]
            start = stop
        self.actual_rows = stop

    def __str__(self):
        s = "======== GPD =======\n"
        s += f"{self.file.list_nodes('/gpd')}\n"
        s += "====================\n"
        s += "======== MA ========\n"
        s += f"{self.file.list_nodes('/ma')}\n"
        s += "====================\n"
        return s

    def deriveBinaryLabels(self):
        deriveBinaryLabels(self.filepath, "/gpd/labels")
        deriveBinaryLabels(self.filepath, "/ma/labels")


def shrinkDatabase(filepath, stop, max_in_memory=10000):
    """Shrink all arrays in a database to a given number of rows.

    Args:
        filepath (str): the path to the database file.
        stop (int): the number of rows to shrink the database to.
        max_in_memory (int, optional): max number of rows in memory for array copy.
    """
    print("Removing empty elements from database:", filepath)

    with tab.open_file(filepath, "a") as f:
        for g in f.walk_groups():
            nodes = [x for x in f.list_nodes(g) if type(x) == tab.carray.CArray]
            print(f"Nodes in this group: {[x.name for x in nodes]}")
            for n in nodes:
                node_name, old_size = n._v_pathname, n.shape[0]
                node = replaceNode(n._v_pathname, f, stop)
                print(f"Shrinked node {node_name}: {old_size} --> {node.shape[0]}")
    print("================================================================")


def deriveBinaryLabels(file_out, name="/labels"):
    """Add binary labels to a given database.

    The binary labels are derived from the grasp geometry information by evaluating a
    number of geometric conditions for a ground truth grasp.

    Args:
        file_out (str): the path to the database file.
    """
    with tab.open_file(file_out, "a") as f:
        labels = f.get_node(name)
        # Select collision-free and antipodal labels.
        if labels.ndim == 2:
            mask = (labels[:, 0] == 1) & (labels[:, 1] == 1)
        elif labels.ndim == 3:
            mask = (labels[:, :, 0] == 1) & (labels[:, :, 1] == 1)
        positives = np.nonzero(mask)
        if labels.ndim == 2:
            labels_out = np.zeros(len(labels))
            labels_out[positives[0]] = 1
        elif labels.ndim == 3:
            labels_out = np.zeros(labels.shape[:2])
            labels_out[positives[0], positives[1]] = 1
        idx = name.rfind("/")
        if idx == 0:
            f.rename_node(name, name[1:] + "_raw")
            where = "/"
        elif idx > 0:
            f.rename_node(name, name[idx + 1 :] + "_raw")
            where = name[:idx]
        dset = f.create_carray(where, "labels", obj=labels_out, filters=filters)
        print(f"Added dataset {dset} to database {file_out}")


def copyBlockwise(node, new_name, fout, stop, max_in_memory=10000):
    """Copy a node blockwise to a database.

    Args:
        node (tables.node): the node to be copied.
        new_name (str): the name of the new node.
        fout (tables.file): the file handle of the database.
        stop (int): the row index at which to stop the copy.
        max_in_memory (int, optional): the max number of rows allowed in system memory.

    Returns:
        tables.CArray: the array contained in the new node.
    """
    blocks = list(np.arange(0, stop, max_in_memory)) + [stop]

    new_shape = (stop,) + node.shape[1:]
    dset = fout.create_carray(
        node._v_pathname[: node._v_pathname.rfind("/") + 1],
        new_name,
        tab.UInt8Atom(),
        shape=new_shape,
        chunkshape=node.chunkshape,
        filters=filters,
    )
    print(f"Created dataset: {dset}, {dset.dtype}, {node.dtype}")

    for i in range(1, len(blocks)):
        print(
            f"({i}/{len(blocks)-1}) Copying block: " + f"[{blocks[i-1]}, {blocks[i]}]"
        )
        dset[blocks[i - 1] : blocks[i]] = node[blocks[i - 1] : blocks[i]]

    return dset


def replaceNode(where, filehandle, stop):
    """Replace a node in a database with a new node whose array has less rows.

    Args:
        where (str): the path to the node at which the array is stored.
        filehandle (tables.File): the filehandle of the database.
        stop (int): the row index at which to stop the copy.

    Returns:
        tables.Node: the new node.
    """
    node = filehandle.get_node(where)
    nodename = node.name
    new_name = nodename + "new"
    idx = where.rfind("/")
    path = where[: idx + 1]

    # Check if the node fits into memory.
    if len(node.shape) < 4:
        filehandle.create_carray(path, new_name, obj=node[:stop], filters=filters)
    else:
        copyBlockwise(node, new_name, filehandle, stop)
    new_node = filehandle.get_node(path + new_name)

    # Replace the old node with the new node.
    filehandle.remove_node(where)
    filehandle.rename_node(path, nodename, new_name)

    return filehandle.get_node(path + nodename)

"""Merge multiple HDF5 databases into a single database.

Usage: python merge_databases.py DB1 DB2 ... OUT

The databases, DB1, DB2, and so on, are merged in the given order and stored in OUT.

"""

import os
import sys

import tables as tab

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=5, complib="blosc:lz4hc")


def mergeNode(pathname, files, out_file):
    print("Merging node", pathname)
    rows = 0
    for f in files:
        rows += f.get_node(pathname).shape[0]
    node = files[0].get_node(pathname)
    path = pathname[: pathname.rfind("/") + 1]
    print("path:", path)
    out_shape = (rows,) + node.shape[1:]
    out = out_file.create_carray(
        path, node.name, atom=node.atom, shape=out_shape, filters=filters
    )
    r = 0
    for f in in_files:
        inp = f.get_node(pathname)
        out[r : r + inp.shape[0]] = inp[:]
        r += inp.shape[0]
        print(f"  #rows processed: {r}")


if len(sys.argv) < 3:
    print("Error: not enough input arguments!")
    print("Usage: python merge_databases.py IN1 IN2 ... OUT")
    exit(-1)

# Read in the databases to be merged.
files = [x for x in sys.argv[1:-1]]
print("Files to be merged:", files)
out_file = tab.open_file(sys.argv[-1], "w")
print("Output file:", out_file)
in_files = [tab.open_file(x, "r") for x in files]
nodes = in_files[0].list_nodes("/")
print("Going to merge the following nodes:", nodes)

# Merge the databases.
for node in nodes:
    if type(node) == tab.group.Group:
        out_file.create_group("/", node._v_name)
        for k in node._v_children:
            mergeNode(node[k]._v_pathname, in_files, out_file)
    else:
        mergeNode(node._v_pathname, in_files, out_file)

# Close all open files.
out_file.close()
for f in in_files:
    f.close()

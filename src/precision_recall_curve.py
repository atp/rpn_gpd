"""Plot a precision-recall curve for files in a given folder.

The given folder is expected to contain files called *PATTERN*.npy and 
*precisions.npy.

"""

import os
import sys
from copy import deepcopy
from datetime import datetime
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from utils.analysis import paretoFront

# average runtime per image on 8-core CPU
time_per_grasp_image = 0.0005

# average runtime per forward pass for a single batch with 64 elements
# on a Nvidia 1080 GPU
time_forward = {}
time_forward["lenet"] = 0.0026
time_forward["resnet"] = 0.0865

batch_size = 64


def labelsAndLegend(xlabel, ylabel, outdir, suffix=""):
    """Add axis labels and a legend to a plot, and show the plot."""
    # plt.legend(loc='lower center') # gcin
    # plt.legend()
    # plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    title = f"{ylabel}_{xlabel}_{suffix}"
    plt.title(title)
    # plt.savefig(outdir + title + '.png')
    plt.show()


def precisionPlot(
    data,
    keys,
    metric,
    draw_scatter=False,
    draw_pareto=False,
    strict_pareto=False,
    xlim=[],
    ylim=[-0.05, 1.05],
):
    """Plot precision against a given metric."""
    suffix = ""
    if draw_pareto:
        suffix = "pareto"
        marker = "-"
    if draw_scatter:
        suffix += "scatter"
        marker = "x"

    print(f"keys: {keys}, metric: {metric}")
    linestyles = ("dashed", "dotted", "solid")
    plt.figure()

    for i, k in enumerate(keys):
        x, y = deepcopy(data[k])[metric], deepcopy(data[k])["precisions"]
        if draw_pareto:
            print(f"before: {[np.min(x), np.max(x)]}, {[np.min(y), np.max(y)]}")
            # x = np.around(x, -1)
            pareto = paretoFront(np.vstack((x, y)).T, strict_pareto)
            x, y = pareto[:, 0], pareto[:, 1]
            print(f"after: {[np.min(x), np.max(x)]}, {[np.min(y), np.max(y)]}")

        if draw_scatter:
            plt.plot(x, y, marker, label=k)
        else:
            # plt.plot(x,y, 'o')
            # idxs = np.argsort(x)
            # stop = np.where(x[idxs] == np.max(x))[0][0]
            # idxs = idxs[:stop+3]
            # x, y = x[idxs], y[idxs]
            # prune = np.where(x == np.max(x))[0]
            # x = np.delete(x, prune[:-1])
            # y = np.delete(y, prune[:-1])
            # idxs = np.where(x == np.max(x))[0]
            # x = np.hstack((x[:idxs[0]], x[idxs]))
            # y = np.hstack((y[:idxs[0]], y[idxs]))
            # pts = np.vstack((x, y))

            # u, idxs, cnt = np.unique(y, return_counts=True, return_index=True)

            # idxs = np.argsort(x)
            idxs = np.lexsort((-y, x))
            x, y = x[idxs], y[idxs]

            # plt.plot(x, y, marker, label=k)
            # plt.plot(x, y, 'o', label=k)
            plt.plot(x, y, linestyle=linestyles[i], label=k, alpha=1.0 - 0.2 * i)
            # if len(xlim) == 1:
            # plt.xlim(right=xlim[0])
            # elif len(xlim) == 2:
            # plt.xlim(xlim)
            # plt.ylim(ylim)

    labelsAndLegend(metric, "precision", outdir, suffix)


# Read command line arguments.
if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print(
        "Usage: python precision_recall_curve.py INPUT_DIR [PATTERN] [METRIC] [METHOD]"
    )
    print("PATTERN is the filename pattern to be matched")
    print("METRIC is the metric to be used for the x-data, default: recall")
    print("METHOD is the name of the method to be plotted, e.g.: cls->gc")
    exit(-1)
input_dir = sys.argv[1]

if len(sys.argv) > 2:
    pattern = sys.argv[2]
else:
    pattern = "confmats"

if len(sys.argv) > 3:
    metric_key = sys.argv[3]
else:
    metric_key = "recall"
print("metric_key:", metric_key)

if len(sys.argv) > 4:
    method_key = sys.argv[4]
else:
    method_key = ""

# TODO make this a command line argument
network = "lenet"

# Create a new directory in which to store the plots.
outdir = "/home/andreas/Pictures/rpn_gpd/" + str(datetime.now()) + "/"
os.mkdir(outdir)

# Read data files. Order of values: tp, fp, tn, fn, k.
files = glob(input_dir + "*" + pattern + "*.npy")
data = {}
for f in files:
    k = f[f.rfind("/") + 1 : f.rfind("_")]
    if len(method_key) > 0 and method_key != k:
        continue
    print("Found matching method:", k)
    data[k] = np.load(f)
    # print(k)
    # print(data[k])

metrics = {}
for k in data:
    X = data[k]
    X = X[:, -5:]
    metrics[k] = {}
    metrics[k]["precisions"] = X[:, 0] / (X[:, 0] + X[:, 1])  # tp / (tp + fp)
    metrics[k]["recalls"] = X[:, 0] / (X[:, 0] + X[:, 3])  # tp / (tp + fn)
    metrics[k]["fprs"] = X[:, 1] / (X[:, 1] + X[:, 2])  # fp / (fp + tn)
    metrics[k]["gcins"] = X[:, -1]  # number of GC inputs
    metrics[k]["detections"] = X[:, 0] + X[:, 1]  # tp + fp

    # To avoid division by zero.
    mask = (X[:, 0] + X[:, 1] > 0) & (X[:, 0] + X[:, 3] > 0)
    for l in metrics[k]:
        metrics[k][l] = metrics[k][l][mask]

    # Calculate detections per second.
    time_images = metrics[k]["gcins"] * time_per_grasp_image
    num_batches = metrics[k]["gcins"] / batch_size
    time_forwards = num_batches * time_forward[network]
    total_times = time_images + time_forwards
    metrics[k]["dps"] = metrics[k]["detections"] / total_times

# Sort keys such that shortest is first.
keys = sorted(data.keys(), key=lambda x: len(x))

plots = {
    "scatter": [True, False],
    "paretoscatter": [True, True],
    "pareto": [False, True],
}
# strict_pareto = False if metric_key == 'recall' else True
# strict_pareto = True
strict_pareto = False
metric_key = metric_key + "s"
xlim = [-5000, 100000] if metric_key == "detections" else []  # zoom for detections
xlim = [-5000, 100000] if metric_key == "gcin" else []  # zoom for detections

for l in plots:
    precisionPlot(
        metrics, keys, metric_key, plots[l][0], plots[l][1], strict_pareto, xlim
    )

print("Stored plots at:", outdir)
exit()

"""Datasets for neural network learning.

LearningDataset and CompleteDataset both create an in-memory dataset by
only reading into memory those images and labels which are indexed by an index
map in the given database. This allows to, e.g., load a balanced dataset into
memory.

"""

import os

import numpy as np
import tables
import torch
import torchvision.transforms.functional as TF
from torch.utils.data import Dataset

from ground_truth import findGroundTruthPositives

os.environ["BLOSC_NTHREADS"] = "4"


class BasicDataset(Dataset):
    """Dataset for images and labels."""

    def __init__(self, file_path, num_classes=2, transform=None):
        super().__init__()
        self.transform = transform

        with tables.open_file(file_path, "r") as f:
            print(f"Loading labels ...")
            if num_classes == 2:
                self.labels = torch.from_numpy(np.array(f.root.labels)).long()
                # required for focal loss
                # self.labels = torch.from_numpy(np.array(f.root.labels)).float()
            else:
                node_names = [n._v_pathname for n in f.list_nodes(f.root)]
                if "labels_table" in node_names:
                    labels = f.root.labels_table
                else:
                    labels = f.root.labels
                self.labels = torch.from_numpy(np.array(labels)).float()

            print(f"Loading images ...")
            self.images = torch.from_numpy(np.array(f.root.images))

        print("Loaded data from:", file_path)
        print(f" - labels: {tensorShapeTypeSizeInfo(self.labels)}")
        print(f" - images: {tensorShapeTypeSizeInfo(self.images)}")

    def __getitem__(self, index):
        image = self.images[index].float()
        image = image.permute(2, 0, 1) / 255.0
        label = self.labels[index]
        # print("image:", image.shape)

        if self.transform:
            # image = TF.to_pil_image(image)
            # image = self.transform(image)
            # print("image:", image.mode)
            # image = TF.to_tensor(image)
            image = self.transform(image)
            image = image.squeeze(0)

        return (image, label)

    def __len__(self):
        return self.labels.shape[0]


class ProposalDataset(Dataset):
    """Dataset for grasp proposal images and their labels."""

    def __init__(self, file_path):
        super().__init__()

        with tables.open_file(file_path, "r") as f:
            idxs = np.array(f.root.gen_index_map)
            print(f"Using index map: {f.root.gen_index_map} ...")

            labels = np.array(f.root.labels)[idxs, :]
            print(f"Creating {len(idxs)} labels from: {f.root.labels} ...")
            pos = np.nonzero(np.sum(labels, 1))[0]
            self.labels = np.zeros(len(idxs))
            self.labels[pos] = 1
            num_pos = len(np.nonzero(labels == 1)[0])
            num_neg = len(np.nonzero(labels == 0)[0])
            print(f"  positives: {num_pos}, negatives: {num_neg}")

            images = f.root.gen_images
            print(f"Loading {len(idxs)} images from: {images} ...")
            self.images = [images[i] for i in idxs]
            self.images = torch.from_numpy(np.array(self.images))

            print("Loaded data from:", file_path)
            print(f" - labels: {tensorShapeTypeSizeInfo(self.labels)}")
            print(f" - images: {tensorShapeTypeSizeInfo(self.images)}")

    def __getitem__(self, index):
        image = self.images[index].float()
        image = image.permute(2, 0, 1) / 255.0
        label = self.labels[index].int()

        return (image, label)

    def __len__(self):
        return self.labels.shape[0]


class GraspClassifierDataset(Dataset):
    """Dataset for grasp classifier images and their labels."""

    def __init__(self, file_path):
        super().__init__()

        with tables.open_file(file_path, "r") as f:
            idxs = np.array(f.root.gc_index_map)
            print(f"Using index map: {f.root.gc_index_map} ...")

            labels = np.array(f.root.labels)
            print(f"Loading {len(idxs)} labels from: {f.root.labels} ...")
            self.labels = torch.from_numpy(labels[idxs[:, 0], idxs[:, 1]])

            images = f.root.gc_images
            print(f"Loading {len(idxs)} images from: {images} ...")
            self.images = [images[i, j] for i, j in zip(idxs[:, 0], idxs[:, 1])]
            self.images = torch.from_numpy(np.array(self.images))

            print("Loaded data from:", file_path)
            print(f" - labels: {tensorShapeTypeSizeInfo(self.labels)}")
            print(f" - images: {tensorShapeTypeSizeInfo(self.images)}")

    def __getitem__(self, index):
        image = self.images[index].float()
        image = image.permute(2, 0, 1) / 255.0
        label = self.labels[index].int()

        return (image, label)

    def __len__(self):
        return self.labels.shape[0]


class LearningDataset(Dataset):
    def __init__(self, file_path, image_array_name, num_classes):
        super().__init__()
        with tables.open_file(file_path, "r") as f:
            nodes = f.list_nodes(f.root)
            images = [n for n in nodes if n.name == image_array_name]
            if len(images) == 0:
                print(f"Error: image array <{image_array_name}> not found.")
                exit(-1)
            images = images[0]

            idxs = np.array(f.root.index_map)
            idxs = idxs[np.argsort(idxs[:, 0])]

            # Load the indexed labels into memory.
            print("Loading labels ...")
            labels = np.array(f.root.labels)
            if num_classes == 2:
                self.labels = torch.from_numpy(labels[idxs[:, 0], idxs[:, 1]])
            else:
                rows = np.unique(idxs[:, 0])
                self.labels = torch.from_numpy(labels[rows, :])

            # Load the indexed images into memory.
            print(f"Loading {len(idxs)} images from: {images} ...")
            if images.name == "gen_images":
                images = [images[i] for i in idxs[:, 0]]
            elif images.name == "cls_images":
                images = [images[i, j] for i, j in zip(idxs[:, 0], idxs[:, 1])]

            self.images = torch.from_numpy(np.array(images))

            print("Loaded data from:", file_path)
            print(f" - labels: {tensorShapeTypeSizeInfo(self.labels)}")
            print(f" - images: {tensorShapeTypeSizeInfo(self.images)}")

    def __getitem__(self, index):
        image = self.images[index].float()
        image = image.permute(2, 0, 1) / 255.0
        label = self.labels[index].int()

        return (image, label)

    def __len__(self):
        return self.labels.shape[0]


class IndexedDataset(Dataset):
    def __init__(self, file_path):
        super().__init__()
        f = tables.open_file(file_path, "r")
        gen_images = f.root.gen_images
        gc_images = f.root.gc_images
        labels = f.root.labels
        idxs = np.array(f.root.gc_index_map)
        idxs = idxs[np.argsort(idxs[:, 0])]
        rows = np.unique(idxs[:, 0])
        self.labels = torch.from_numpy(labels[rows, :])

        # Note: pytables does not support duplicate indices.
        print(f"Loading {len(idxs)} GEN images ...")
        self.gen_images = [gen_images[i] for i in idxs[:, 0]]
        self.gen_images = torch.from_numpy(np.array(self.gen_images))

        print(f"Loading {len(idxs)} GC images ...")
        #        self.gc_images = [gc_images[i,j] for i,j in zip(idxs[:,0], idxs[:,1])]
        self.gc_images = [gc_images[i, :] for i in idxs[:, 0]]
        self.gc_images = torch.from_numpy(np.array(self.gc_images))

        print("Loaded data from:", file_path)
        print(f" - labels: {tensorShapeTypeSizeInfo(self.labels)}")
        print(f" - gen_images: {tensorShapeTypeSizeInfo(self.gen_images)}")
        print(f" - gc_images: {tensorShapeTypeSizeInfo(self.gc_images)}")

    def __getitem__(self, index):
        row = int(np.floor(index / self.labels.shape[-1]))
        gen_image = self.gen_images[row].float()
        gen_image = gen_image.permute(2, 0, 1) / 255.0
        gc_images = self.gc_images[row, :].float()
        gc_images = gc_images.permute(0, 3, 1, 2) / 255.0
        labels = self.labels[row, :].int()

        return (gen_image, gc_images, labels)

    def __len__(self):
        return self.labels.shape[0] * self.labels.shape[1]


class MatrixDataset(Dataset):
    def __init__(self, file_path, where="/"):
        super().__init__()
        f = tables.open_file(file_path, "r")
        self.gen_images = f.get_node(where, "prop_imgs")
        self.gc_images = f.get_node(where, "grasp_imgs")
        self.labels = np.array(f.get_node(where, "labels"))
        print("Loaded data from:", file_path)
        print("  gen_images:", self.gen_images.shape, self.gen_images.dtype)
        print("  gc_images:", self.gc_images.shape, self.gc_images.dtype)
        print("  labels:", self.labels.shape, self.labels.dtype)
        print(
            f"  positives: {np.count_nonzero(self.labels)}, "
            + f"negatives: {np.count_nonzero(self.labels==0)}"
        )

    def __getitem__(self, index):
        gen_image = torch.from_numpy(self.gen_images[index]).float()
        gen_image = gen_image.permute(2, 0, 1) / 255.0
        gc_images = torch.from_numpy(self.gc_images[index]).float()
        gc_images = gc_images.permute(0, 3, 1, 2) / 255.0
        labels = torch.from_numpy(self.labels[index, :]).int()

        return (gen_image, gc_images, labels)

    def __len__(self):
        return self.labels.shape[0]


class GpdDataset(Dataset):
    def __init__(self, file_path, where="/"):
        super().__init__()
        f = tables.open_file(file_path, "r")
        self.proposals = np.array(f.get_node(where, "proposals"))
        self.images = f.get_node(where, "images")
        self.labels = np.array(f.get_node(where, "labels"))
        print("Loaded GPD data from:", file_path)
        print("  proposals:", self.proposals.shape, self.proposals.dtype)
        print("  images:", self.images.shape, self.images.dtype)
        print("  labels:", self.labels.shape, self.labels.dtype)
        print(
            f"  positives: {np.count_nonzero(self.labels)}, "
            + f"negatives: {np.count_nonzero(self.labels==0)}"
        )

    def __getitem__(self, index):
        proposals = torch.from_numpy(self.proposals[index, :]).int()
        images = torch.from_numpy(self.images[index]).float()
        images = images.permute(0, 3, 1, 2) / 255.0
        labels = torch.from_numpy(self.labels[index, :]).int()

        return (proposals, images, labels)

    def __len__(self):
        return self.labels.shape[0]


def tensorShapeTypeSizeInfo(x):
    s = f"shape: {x.shape}, dtype: {x.dtype}, "
    size = x.element_size() * x.numel()
    if size > 2 ** 30:
        s += f"size: {size/(2**30):.3f}Gb"
    else:
        s += f"size: {size/(2**20):.3f}Mb"
    return s


def convertLabelsToBinary(dataset):
    Y = dataset.labels
    mask = findGroundTruthPositives(Y)
    positives = np.nonzero(mask)
    labels_out = np.zeros(Y.shape[:2])
    labels_out[positives[0], positives[1]] = 1
    dataset.labels = labels_out
    return dataset

"""Generate grasps for a mesh file.
"""

import sys
from argparse import ArgumentParser
from copy import deepcopy
from time import time

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from grasp_scene_data import GraspData, SceneData, storeGrasps
from ground_truth import findGroundTruthPositives
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from learning import Metrics
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import preprocessPointCloud
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes
from utils.plot import plotColoredHands

cube_size = 1.0

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Generate grasp poses for an object mesh file")
    parser.add_argument("file", help="path to mesh file")
    parser.add_argument("scale", help="scaling factor", type=float)
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument("num_samples", help="number of samples", type=int)
    parser.add_argument("--plot", help="turn plotting on", action="store_true")
    parser.add_argument("--store", help="store grasp poses", action="store_true")
    return parser


def createHandsGenerator(method, hand_geom, antip_prms):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        gen = QdHandsGenerator(hand_geom, antip_prms)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def main() -> None:
    draw_geoms = o3d.visualization.draw_geometries

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # data = createPointCloudFromMesh(args.file, args.scale)
    scene = SingleObjectScene(args.file)
    scene.resetToScale(args.scale)
    mesh, views_cloud = scene.createCompletePointCloud()
    mesh_tree = o3d.geometry.KDTreeFlann(mesh)
    if args.plot:
        draw_geoms(
            [mesh, views_cloud], "Multiview point cloud and view points", 640, 480
        )
        draw_geoms([mesh], "Surface normals", 640, 480)

    # Create a point cloud from a random viewpoint on a sphere.
    camera_distance = 0.5
    look_at = np.zeros(3)
    x = np.random.normal(size=3)
    x = x / np.linalg.norm(x) * camera_distance
    camera_pose = calcLookAtPose(x, look_at)
    cloud = scene.createProcessedPointCloudFromView(camera_pose)

    # Draw sample points from the point cloud.
    samples = sampleFromCube(cloud, cube_size, args.num_samples)
    if args.plot:
        plotSamplesAsBoxes(cloud, samples, "Samples")

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    gen = createHandsGenerator(args.method, hand_geom, antip_prms)

    # Generate the ground truth grasp poses.
    t = time()
    camera_pose = scene.scene.get_pose(scene.camera_node)
    hands, frames = gen.generateHands(cloud, samples, True, camera_pose=camera_pose)
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")
    # idxs = np.array(np.nonzero(findGroundTruthPositives(hands)))
    proposals = hands[:, :, 0] == 1
    gt_hands, gt_frames = gen.generateHands(mesh, samples, True, camera_pose=camera_pose)
    positive_grasps = (gt_hands[:, :, 0] == 1) & (gt_hands[:, :, 1] == 1)
    print(proposals.shape, positive_grasps.shape)
    tps = (proposals) & (positive_grasps)
    fps = (proposals) & (~positive_grasps)
    tp = np.count_nonzero(tps)
    fp = np.count_nonzero(fps)
    print("tp:", tp, "fp:", fp)
    print("precision:", tp / (tp + fp))
    labels = positive_grasps.astype(int).flatten()
    predictions = proposals.astype(int).flatten()
    metrics = Metrics(labels, predictions)
    print(metrics)

    # Partial view.
    hands, frames = gen.generateHands(cloud, samples, True, camera_pose=camera_pose)
    props = np.zeros(hands.shape[:2])
    if args.method == "gpd":
        props[hands[:, :, 0] == 1] = 1
    elif args.method == "qd":
        props[:, :] = 1
    # print(f"Hands in point cloud:\n{hands}")
    # print(f"Grasp proposals:\n{props}")
    # Check how well the proposal strategy matches the ground truth.
    ground_truth = np.logical_and(gt_hands[:, :, 0], gt_hands[:, :, 1])
    metrics = Metrics(ground_truth.flatten(), props.flatten())
    num_proposals = np.sum(props.flatten())
    max_grasps = np.prod(props.shape)
    print("How well do the proposals match the ground truth?")
    print(metrics)
    print(f"#proposals: {num_proposals}, max #grasps: {max_grasps}")
    if args.plot:
        plt.matshow(ground_truth)
        plt.xlabel("poses")
        plt.ylabel("samples")
        plt.title("Ground truth grasps")
        plt.draw()
        plt.matshow(props)
        plt.xlabel("poses")
        plt.ylabel("samples")
        plt.title("Grasp proposals")
        plt.show()

    matches = ground_truth == props
    # plt.matshow(matches, cmap="Greys", interpolation="nearest")
    if args.plot:
        plt.matshow(matches, cmap="Spectral_r", interpolation="nearest")
        plt.xlabel("poses")
        plt.ylabel("samples")
        plt.title("Proposals matching ground truth")
        plt.show()

    # exit()

    # Visualize the grasp poses.
    m = gen.num_hand_orientations
    idxs = np.vstack((np.zeros(m), np.arange(m))).astype(int)
    print(idxs)
    input("?")
    tree = o3d.geometry.KDTreeFlann(cloud)
    poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )
    if args.plot:
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [1.0, 0.0, 0.0])
        p.plotHands(poses, [1.0, 1.0, 0.0], scale=0.1)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("grasp poses")
        p.show()

    # Store the grasps.
    if args.store:
        category = args.file
        instance = category[category.rfind("/") + 1 :]
        category = category[: category.rfind("/")]
        category = category[category.rfind("/") + 1 :]
        scene_data = SceneData(
            category, instance, args.scale, camera_pose, np.asarray(cloud.points)
        )
        grasp_data = GraspData(poses, np.ones(len(poses)), args.method)
        instance = instance[: instance.rfind(".")]
        fpath = f"./ground_truth_{args.method}_{category}_{instance}"
        storeGrasps(scene_data, grasp_data, fpath)

    # Visualize all hand orientations for a single grasp position.
    # Used to check algorithms and make figures.
    print("\nEvaluating a single grasp position ...")
    idxs = np.vstack((np.zeros(m), np.arange(m))).astype(np.int)
    poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )

    if args.plot:
        # poses = [poses[0]]
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        p.plotApproaches(poses, [0.0, 1.0, 1.0], scale=[0.1, 1.0, 0.1])
        p.plotHands(poses, [0.0, 1.0, 0.0], scale=0.1)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("approach vectors (single position)")
        p.show()

    if args.plot:
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        p.plotHands(poses, [0.0, 1.0, 0.0], scale=0.1)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("grasp poses (single position)")
        p.show()
        if args.method == "qd":
            hand_colors = np.random.rand(int(len(poses) / gen.num_approaches), 3)
            hand_colors = np.tile(hand_colors, (1, gen.num_approaches))
            hand_colors = hand_colors.reshape((len(poses), 3))
        else:
            hand_colors = np.zeros((len(poses), 3), dtype=np.float)
            hand_colors[:, 0] = np.linspace(0.1, 1.0, len(poses))
            hand_colors = np.random.rand(len(poses), 3)
        plotColoredHands(
            [cloud],
            poses,
            hand_colors,
            hand_params,
            "grasp poses (single position)",
            plot_base_frame=True,
        )
        exit()

    # Label the grasp poses.
    print("Labelling grasp poses using mesh ...")
    hand_eval = HandEval(hand_geom, antip_prms)
    infos = hand_eval.evalPoses(mesh, poses)
    labels_dict = {
        "collision-free": infos[:, 0] == 1,
        "antipodal": infos[:, 1] == 1,
        "collision_free and antipodal": infos[:, 0] * infos[:, 1] == 1,
    }
    idxs_checker = np.array(np.nonzero(labels_dict["collision_free and antipodal"]))
    print(
        "infos:",
        infos.shape,
        [(k, np.count_nonzero(labels_dict[k])) for k in labels_dict],
    )

    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [1.0, 0.0, 1.0])
    p.plotHands(
        poses[labels_dict["collision-free"]], [0.0, 0.1, 0.0], opacity=0.1, scale=0.1
    )
    p.plotHands(poses[labels_dict["antipodal"]], [0.0, 1.0, 0.0], scale=0.1)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose)
    p.show()

    # Ensure that the antipodal labels are the same, for a single sample.
    if args.num_samples == 1:
        if args.method == "gpd":
            print("Error: this check is not reliable for GPD (reason: proposals).")
            exit(-1)
        mask = idxs_generator[1] == idxs_checker
        all_equal = np.all(mask)
        assert all_equal
        print(f"Are the labels calculated by the hands generator correct?")
        print(f"  Answer: {all_equal}")


if __name__ == "__main__":
    main()

"""Add balanced index maps to a given database.

Create an index map for each of the image arrays in the database. The index 
map for the proposal images is balanced with respect to the rows of the 
<labels> array (N). The second map is balanced with respect to all elements of 
the <labels> array (N x M).

Given a database, find the indices that would correspond to a balanced version
of the database. Create an index map that maps an index i to an index (r,c)
where (r,c) corresponds to row r and column c in the balanced database.

"""

import os
import sys

import numpy as np
import psutil
import tables as tab

from utils.db_utils import calcMaxInMemory

keys = ["labels", "gen_images", "gc_images"]
os.environ["BLOSC_NTHREADS"] = "4"
filters = tab.Filters(complevel=5, complib="blosc")

if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python create_balanced_db.py PATH_TO_DB [SIZE]")

# Read command line parameters.
fname = sys.argv[1]

idx_map_size = -1
if len(sys.argv) == 3:
    idx_map_size = int(sys.argv[2])

with tab.open_file(fname, "a") as f:
    labels = np.asarray(f.root.labels, np.int)
    pos = np.asarray(np.nonzero(labels == 1), np.int32)
    neg = np.asarray(np.where(labels == 0), np.int32)
    total = 2 * pos.shape[1]
    print(
        f"Found {pos.shape[1]} positives and {neg.shape[1]} negatives in "
        + f"database {fname}"
    )
    print(f"Balanced database has {total} elements")

    pos_rows = np.unique(pos[0, :])
    neg_rows = np.asarray(np.where(np.sum(labels, 1) == 0)[0], np.int32)
    print(f"Rows with >= 1 positive: {len(pos_rows)}")
    print(f"Rows with only negatives: {len(neg_rows)}")
    assert len(pos_rows) + len(neg_rows) == labels.shape[0]

    gen_idxs = np.hstack((pos_rows, neg_rows))
    if idx_map_size == -1 or len(gen_idxs) <= idx_map_size:
        print("Shuffling indices for GEN index map ...")
        np.random.shuffle(gen_idxs)
    else:
        subset = np.random.choice(
            np.arange(labels.shape[0]), idx_map_size, replace=False
        )
        gen_idxs = gen_idxs[subset]
        print(f"Randomly subsampled {gen_idxs.shape[0]} indices")

    node_names = [n.name for n in f.list_nodes(f.root)]
    print("Existing nodes in DB:", node_names)

    # Store the index map for the proposal images in the database.
    if "gen_index_map" in node_names:
        f.remove_node("/", "gen_index_map")
    gen_idx_map = f.create_carray("/", "gen_index_map", obj=gen_idxs, filters=filters)
    print(f"Added array {gen_idx_map}\n")

    # Create the index map for the grasp images.
    neg_idxs = np.random.choice(np.arange(neg.shape[1]), pos.shape[1], replace=False)
    gc_idxs = np.hstack((pos, neg[:, neg_idxs])).T

    # Randomly shuffle all indices or choose a random subset.
    if idx_map_size == -1:
        gc_idxs = np.random.shuffle(gc_idxs)
    else:
        subset = np.random.choice(np.arange(total), idx_map_size, replace=False)
        gc_idxs = gc_idxs[subset]
        print(f"Randomly subsampled {gc_idxs.shape[0]} indices")

    # Store the index map for the grasp images in the database.
    if "gc_index_map" in node_names:
        f.remove_node("/", "gc_index_map")
    gc_idx_map = f.create_carray("/", "gc_index_map", obj=gc_idxs, filters=filters)
    print(f"Added array {gc_idx_map}")

    # Verify that the index map is correct: the number of positives equals the
    # number of negatives in the balanced version of the database .
    labels_balanced = np.asarray(f.root.labels, np.int)[gc_idxs[:, 0], gc_idxs[:, 1]]
    num_pos_out = np.sum(labels_balanced == 1)
    num_neg_out = np.sum(labels_balanced == 0)
    print(f"labels_balanced: {labels_balanced.shape}")
    print(f"num_pos_out: {num_pos_out}")
    print(f"num_neg_out: {num_neg_out}")
    if idx_map_size == -1:
        assert num_pos_out == num_neg_out
        assert num_pos_out == pos.shape[1]

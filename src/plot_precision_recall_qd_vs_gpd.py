"""Plot precision-recall for GPD and QD:CLS->ROT.

Usage: python plot_precision_recall.py PATH

PATH is the path to a NPY file that contains the metrics for the CLS and ROT 
components of QD. This NPY file is produced by <test_cls_and_rot_models.py>.

Precision and recall for GPD were calculated by <eval_proposal_generator.py>.

"""

import sys
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np


def paretoFrontier(pts: np.array) -> np.array:
    """Calculate the Pareto frontier for a set of points.

    Args:
        pts (np.array): 2 x n the set of points.

    Returns:
        np.array: 2 x n the points on the Pareto frontier.
    """
    pareto = np.ones(pts.shape[1], dtype=bool)
    for i in range(len(pareto)):
        b = np.all(pts.T > pts[:, i], 1)
        if np.any(b):
            pareto[i] = 0

    return pts[:, pareto]


def finalizePlot(
    xlabel: str = "Recall",
    ylabel: str = "Precision",
    xlim: str = [-0.05, 1.05],
    ylim: str = [-0.05, 1.05],
    out_path: str = "/tmp/",
):
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.legend()
    plt.savefig(out_path, format="png")
    print("Stored precision-recall plot at: " + out_path)
    plt.show()


def main() -> None:
    # Read command line arguments.
    filepath = sys.argv[1]

    # Read the metrics for QD.
    qd_metrics = np.load(filepath, allow_pickle=True, encoding="latin1").item()
    qd_recall, qd_precision = [], []
    for k, v in qd_metrics.items():
        qd_recall += [x.recall() for x in v]
        qd_precision += [x.precision() for x in v]
    print("thresholds:", qd_metrics.keys())

    # Calculate the Pareto frontier for QD.
    qd_pareto = paretoFrontier(np.vstack((qd_recall, qd_precision)))
    idxs = np.argsort(qd_pareto[0], axis=0)
    qd_pareto = qd_pareto[:, idxs]

    # These values were calculated by <eval_proposal_generator.py>.
    gpd_precision = 0.219
    gpd_recall = 0.603

    #  Plot precision against recall (raw).
    plt.figure()
    plt.plot(qd_recall, qd_precision, "o", label="qd: cls and rot")
    plt.plot(gpd_recall, gpd_precision, "x", label="gpd: proposal generator")
    out_path = f"/tmp/raw-precision-recall-{datetime.now()}.png"
    finalizePlot(out_path=out_path)

    # Plot precision against recall (Pareto).
    plt.figure()
    plt.plot(qd_pareto[0], qd_pareto[1], "-o", label="qd: cls and rot")
    plt.plot(gpd_recall, gpd_precision, "x", label="gpd: proposal generator")
    out_path = f"/tmp/pareto-precision-recall-{datetime.now()}.png"
    finalizePlot(out_path=out_path)


if __name__ == "__main__":
    main()

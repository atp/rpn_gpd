import sys

import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.utils.data as torchdata
from h5_dataset import H5Dataset
from sklearn.metrics import auc

from learning import Metrics, evalMultiLabel
from networks.network import LenetWithSigmoid

# Network parameters.
input_channels = 3
units = [32, 64, 500]
batch_size = 64

# Index of the class for which the curves are plotted.
class_idx = 1


def addLabelsAndLimits(xlabel, ylabel, fname):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.legend()
    plt.savefig("/home/andreas/Pictures/rpn_gpd2/" + fname)
    plt.show()


def evalBinary(
    model, loader, device, thresh, class_idx=1, print_confusion_matrix=False
):
    #    print('Testing the network on the test data ...')
    model.eval()
    correct = 0
    total = 0
    balanced_accuracy = 0
    batches = 0
    avg_metrics = Metrics()

    with torch.no_grad():
        for data in loader:
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            outputs = model(inputs)
            y = torch.where(
                outputs[:, class_idx] > thresh,
                torch.ones(outputs.shape[0]).to(device),
                torch.zeros(outputs.shape[0]).to(device),
            )
            _, predicted = torch.max(outputs.data, 1)
            correct += (y.long() == labels.long()).sum().item()
            total += labels.size(0)

            act = np.asarray(labels.cpu()).flatten()
            pred = np.asarray(y.cpu()).flatten()
            batch_metrics = Metrics(act, pred)
            balanced_accuracy += batch_metrics.balancedAccuracy()
            avg_metrics.concat(batch_metrics)
            batches += 1

    accuracy = 100 * correct / float(total)
    balanced_accuracy = 100.0 * balanced_accuracy / float(batches)

    #    print(f'Test set accuracy: {accuracy:.3f}% ({correct}/{total}), '\
    #          + f'balanced accuracy: {balanced_accuracy:.3f}%')
    #    print('-------------------------------------------------')
    #    print('Average Evaluation Metrics')
    #    print('accuracy, precision, recall:', avg_metrics.toArray())
    #    print('-------------------------------------------------')
    if print_confusion_matrix:
        avg_metrics.printConfusionMatrix()

    return accuracy, avg_metrics


def evalModel(model_path, data_path):
    # Load the data.
    dataset = H5Dataset(data_path, 10000)
    loader = torchdata.DataLoader(dataset, batch_size=batch_size, shuffle=True)
    if dataset.labels.ndim == 1:
        num_classes = 2
    else:
        num_classes = len(dataset.labels[0])
    print(f"num_classes: {num_classes}")

    # Setup the classification model.
    net = LenetWithSigmoid(input_channels, num_classes, units)
    net.load_state_dict(torch.load(model_path))
    net.eval()
    print("Copying network to GPU ...")
    net.to(device)
    print(net)

    return calcMetrics(net, loader, num_classes)


def calcMetrics(model, loader, num_classes, num_threshs=100):
    threshs = np.linspace(0.0, 1.0, num_threshs)
    precisions = np.zeros(len(threshs))
    recalls = np.zeros(len(threshs))
    fprs = np.zeros(len(threshs))
    print(f"classifier thresholds: {threshs}")

    # Calculate precision, recall, and false positive rate for each threshold.
    for i in range(len(threshs)):
        if num_classes == 2:
            _, m = evalBinary(model, loader, device, threshs[i])
        else:
            _, m = evalMultiLabel(
                model, loader, device, threshs[i], apply_sigmoid=False
            )
        precisions[i] = m.precision()
        recalls[i] = m.recall()
        fprs[i] = m.fpr()
        s = f"({i}) threshold: {threshs[i]:.3f}, accuracy: {m.accuracy():.3f},"
        s += f" precision: {m.precision():.3f} ({m.tp}/({m.tp} + {m.fp})), "
        s += f"recall: {m.recall():.3f}, FPR: {m.fpr():.3f}"
        print(s)

    return {"recalls": recalls, "precisions": precisions, "fprs": fprs}


def findFullName(model_path):
    desc = ""

    # Find name of object.
    if model_path.find("battery") >= 0:
        desc = "BATTERY_"
    elif model_path.find("detergent") >= 0:
        desc = "DETERGENT_"
    elif model_path.find("mug") >= 0:
        desc = "MUG_"
    elif model_path.find("stapler") >= 0:
        desc = "STAPLER_"
    elif model_path.find("tetra_pak") >= 0:
        desc = "TETRA_PAK_"

    # Find name of method.
    if model_path.find("ma7") >= 0:
        desc += "MA7_"
    elif model_path.find("sa") >= 0:
        desc += "SA_"
    elif model_path.find("no_trans_gpd") >= 0:
        desc += "NO_TRANSLATION_GPD_"
    elif model_path.find("gpd") >= 0:
        desc += "GPD_"

    # Find name of component.
    if model_path.find("gc") >= 0:
        desc += "GC"
    elif model_path.find("rot") >= 0:
        desc += "ROT"
    elif model_path.find("cls") >= 0:
        desc += "CLS"

    return desc


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

results = {}
num_models = int(np.floor(len(sys.argv) - 1) / 2)
print("num_models:", num_models)
for i in range(num_models):
    model_path = sys.argv[2 * i + 1]
    data_path = sys.argv[2 * i + 2]
    model_name = findFullName(model_path)
    print(f"Evaluating model <{model_name}> on data <{data_path}> ...")
    results[model_name] = evalModel(model_path, data_path)

print(results)

fname = "pr"
for k, v in results.items():
    plt.plot(v["recalls"], v["precisions"], label=k)
    fname += "_" + k
addLabelsAndLimits("recall", "precision", fname)

fname = "roc_"
for k, v in results.items():
    a = auc(v["fprs"], v["recalls"])
    label = k + " (area = " + "{:.3f}".format(a) + ")"
    plt.plot(v["fprs"], v["recalls"], label=label)
    fname += "_" + k
plt.plot([0, 1], [0, 1], "b--")
addLabelsAndLimits("false positive rate", "recall", fname)

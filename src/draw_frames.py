"""Visualize the frames used in our method.
"""

import sys
from argparse import ArgumentParser
from copy import deepcopy
from functools import partial
from time import time

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from grasp_image import GraspImageGenerator, volume_dims
from grasp_scene_data import GraspData, SceneData, storeGrasps
from ground_truth import findGroundTruthPositives
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from proposal import ProposalImageGenerator, sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import numpyToCloud, orientNormalsTowardsMean
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes
from utils.plot import plotColoredHands

cube_size = 1.0
grasp_img_shape = (60, 60, 4)

grey = [0.4, 0.4, 0.4]
cyan = [0.0, 1.0, 0.0]
orange = [1.0, 0.6, 0.0]
yellow = [1.0, 1.0, 0.0]
green = [0.0, 1.0, 0.0]
red = [1.0, 0.0, 0.0]
blue = [0.0, 0.0, 1.0]
black = [0.0, 0.0, 0.0]
purple = [0.6, 0.15, 0.9]

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Visualize the frames used in our method")
    parser.add_argument("file", help="path to mesh file")
    parser.add_argument("scale", help="scaling factor", type=float)
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument("num_samples", help="number of samples", type=int)

    return parser


def createHandsGenerator(method, hand_geom, antip_prms):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, num_approaches=1)
        gen = QdHandsGenerator(hand_geom, antip_prms)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def createRect(top_left, size, ax1, ax2):
    line = np.linspace(0, size, 100)
    pts = [ax1 * x + top_left for x in line]
    top_right = pts[-1]
    pts += [ax2 * x + top_left for x in line]
    bottom_left = pts[-1]
    pts += [ax2 * x + top_right for x in line]
    pts += [ax1 * x + bottom_left for x in line]

    return pts


def plotProposalDescriptor(
    mesh, cloud, samples, camera_pose, gen, camera_frame=None, camera_cloud=None
):
    samples = np.zeros((1, 3))
    center = cloud.get_center()
    samples[0] = center
    samples[0, 0] = cloud.get_max_bound()[0]

    Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0]))
    camera_frame = deepcopy(camera_pose)
    camera_frame[:3, :3] = np.dot(camera_frame[:3, :3], Rx.as_matrix())

    prep_cloud, prep_samples = gen.preprocess(cloud, samples, camera_frame)

    prop_params = {"image_size": 60, "image_type": "depth"}
    prop_img_gen = ProposalImageGenerator(**prop_params)
    prop_imgs = prop_img_gen.createImages(prep_cloud, prep_samples)
    print("prop_imgs:", prop_imgs.shape)
    """
    for i in range(prop_imgs[0].shape[-1]):
        plt.figure()
        plt.imshow(prop_imgs[0][:, :, i], cmap="gray")
        plt.axis("off")
        plt.draw()
    plt.show()
    """

    mesh_color = red
    cloud_color = blue

    line = np.linspace(0, 1.0, 25)
    axes = np.array(
        [
            [0.25 - samples[0, 0], 0.0, 0.0],
            [0.0, -0.25 - samples[0, 2], 0.0],
            [0.0, 0.0, 0.25 - samples[0, 2]],
        ]
    )
    pts = [np.stack([ax * x + samples[0] for x in line]) for ax in axes]

    left = np.array([1.0, 0.0, 0.0])
    down = np.array([0.0, 0.0, -1.0])
    x_axis = np.array([1.0, 0.0, 0.0])
    y_axis = np.array([0.0, 1.0, 0.0])
    rects = [createRect(pts[1][-1] + np.array([-0.1, 0.0, 0.1]), 0.2, left, down)]
    rects += [createRect(pts[0][-1] + np.array([0.0, -0.1, 0.1]), 0.2, y_axis, down)]
    rects += [createRect(pts[2][-1] + np.array([-0.1, -0.1, 0.0]), 0.2, y_axis, x_axis)]

    offsets = np.array([[0.5, 0.0, -0.2], [0.5, 0.0, -0.25], [0.5, 0.0, -0.3]])
    # img_rects = [rects[-1] + x for x in offsets]

    # Plot the descriptor volume for the proposal image.
    origin = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
    # draw_geoms([prep_cloud, origin], "prep_cloud")
    p = HandsPlot(gen.hand_geom)
    p.plotCloud(mesh, mesh_color)
    p.plotCloud(cloud, cloud_color)
    p.plotCloud(numpyToCloud(samples), yellow)
    for x in pts:
        p.plotCloud(numpyToCloud(x), black)
    for x in rects:
        p.plotCloud(numpyToCloud(x), black)
    # for x in img_rects:
    # p.plotCloud(numpyToCloud(x), black)
    for x in offsets:
        X = np.eye(4)
        X[:3, 3] = x + pts[2][-1]
        p.plotCube(X, 0.2, 0.2, 0.002, grey, 0.7)
    box_pose = np.eye(4)
    # box_pose[:3, 3] = samples[0]
    box_pose[:3, :3] = np.dot(camera_frame[:3, :3], box_pose[:3, :3])
    box_dims = np.array([0.5, 0.5, 0.5])
    if camera_frame is not None:
        p.plotAxes(camera_frame)
    p.plotCube(box_pose, box_dims[0], box_dims[1], box_dims[2], grey, 0.1)
    # p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    # p.setCameraPose([-0.5, -0.5, 0.5], [0, 0, 1], [0, 0, 0])
    p.setTitle("Descriptor volume: proposal image")
    p.show()


def main() -> None:
    draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # data = createPointCloudFromMesh(args.file, args.scale)
    scene = SingleObjectScene(args.file)
    scene.resetToScale(args.scale)
    mesh, views_cloud = scene.createCompletePointCloud()

    camera_pose = scene.scene.get_pose(scene.camera_node)
    # camera_pose[:3, 3] -= 0.02 * camera_pose[:3, 2]
    camera_pose = calcLookAtPose([1.0, 0.0, 0.0], np.zeros(3))
    # cloud = scene.createPointCloudFromView(camera_pose)
    cloud = scene.createProcessedPointCloudFromView(camera_pose)
    tree = o3d.geometry.KDTreeFlann(cloud)

    # draw_geoms([mesh, views_cloud],
    # "Multiview point cloud and view points", 640, 480)
    # draw_geoms([mesh], "Surface normals", 640, 480)

    # Draw sample points from the point cloud.
    samples = sampleFromCube(cloud, cube_size, args.num_samples)
    # samples[0, 2] += 0.02
    pts = np.asarray(cloud.points)
    mins, maxs = np.min(pts, 0), np.max(pts, 0)
    print(mins, maxs)
    # samples[0, 0] = mins[0] + 0.5 * (maxs[0] - mins[0])
    samples[0] = mins + 0.5 * (maxs - mins)
    # print(f"samples: {samples}")
    # plotSamplesAsBoxes(mesh, samples, "Samples")
    plotSamplesAsBoxes(cloud, samples, "Samples")
    print(f"samples[0]: {samples[0]}")

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antipodal_params = {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": True,
    }
    antip_prms = AntipodalParams(**antipodal_params)
    gen = createHandsGenerator(args.method, hand_geom, antip_prms)

    # Plot the proposal descriptor.
    plotProposalDescriptor(mesh, cloud, samples, camera_pose, gen)
    # exit()

    # Generate the grasp poses.
    t = time()
    camera_pose = scene.scene.get_pose(scene.camera_node)
    hands, frames = gen.generateHands(cloud, samples, camera_pose=camera_pose)
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")
    # idxs = np.array(np.nonzero(findGroundTruthPositives(hands)))
    collision_free = hands[:, :, 0] == 1
    antipodal = hands[:, :, 1] == 1

    # Plot only poses corresponding to ground truth positives.
    idxs = np.array(np.nonzero(collision_free & antipodal))

    # Plot all poses for a single sample.
    idxs = np.vstack(
        (np.zeros(gen.num_hand_orientations), np.arange(gen.num_hand_orientations))
    ).astype(int)

    total = hands.shape[0] * hands.shape[1]
    print(f"collision_free: {np.sum(collision_free)}/{total}")
    print(f"antipodal: {np.sum(antipodal)}/{total}")

    # camera_mesh = o3d.io.read_triangle_mesh(
    # "/home/andreas/data/camera_simple.ply")
    camera_body = o3d.geometry.TriangleMesh.create_box(depth=2.0)
    camera_head = o3d.geometry.TriangleMesh.create_cone(radius=0.5, height=1.0)
    camera_body = camera_body.translate([-0.5, -0.5, 0.4])
    camera_mesh = camera_body + camera_head
    # camera_mesh = camera_mesh.scale(0.02, camera_mesh.get_center())
    camera_mesh = camera_mesh.transform(camera_pose)
    # camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.86 * camera_pose[:3, 2])
    camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.875 * camera_pose[:3, 2])
    # camera_cloud = camera_mesh.sample_points_poisson_disk(1000)
    camera_cloud = camera_mesh.sample_points_uniformly(10000)

    Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0]))
    camera_frame = deepcopy(camera_pose)
    camera_frame[:3, :3] = np.dot(camera_frame[:3, :3], Rx.as_matrix())
    print(camera_frame)

    # Calculate the grasp poses.
    poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )

    samples_cloud = numpyToCloud(samples)

    # idxs = np.array(np.nonzero(collision_free))
    idxs = np.array(np.nonzero(collision_free & antipodal))
    free_poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )

    # Plot a sample.
    # p = HandsPlot(hand_geom)
    # p.plotCloud(mesh, [0.5, 0.0, 0.0])
    # p.plotCloud(cloud, [0.0, 0.0, 1.0])
    # p.plotCloud(samples_cloud, [1.0, 1.0, 0.0], 10.0)
    # p.show()

    # Find the mid pose.
    poses_plot = free_poses
    mid = int(np.floor(len(poses_plot) / 2))

    print("")
    pose_mid = gen.indicesToPoses(
        cloud,
        tree,
        samples,
        {"indices": idxs[:, mid][..., np.newaxis], "frames": frames},
    )

    # Calculate grasp images.
    prep_cloud, prep_samples = gen.preprocess(cloud, samples, camera_pose)
    grasp_img_gen = GraspImageGenerator(grasp_img_shape, gen)
    rots = gen.calcRotations(idxs[1])
    rots = rots[mid][np.newaxis, ...]
    idxs = idxs[:, mid][..., np.newaxis]
    grasp_imgs = grasp_img_gen.calcImagesAtIndices(
        prep_cloud, prep_samples, idxs, rots, []
    )
    # grasp_imgs = grasp_img_gen.calcImagesAtIndices(cloud, samples, idxs, rots, [])
    # print(f"grasp_imgs: {grasp_imgs.shape}")
    # grasp_img_mid = grasp_imgs[0]
    # grasp_img_mid = grasp_imgs[mid]
    # print(f"grasp_img_mid: {grasp_img_mid.shape}")

    prep_tree = o3d.geometry.KDTreeFlann(prep_cloud)
    outs = grasp_img_gen.calcImageAtIndex(
        idxs[:, 0], prep_samples, prep_cloud, prep_tree, rots[0], [], True
    )
    grasp_img_mid, mask, bounds = outs[0], outs[1], outs[2]
    # print(len(outs), [type(x) for x in outs])
    print(f"box: {bounds}")
    # print(f"mask: {np.count_nonzero(mask)}, cloud: {len(cloud.points)}")

    normals_channels = grasp_img_mid[:, :, :3]
    depth_channel = grasp_img_mid[:, :, -1]
    normals_channels = normals_channels.transpose((1, 0, 2))
    depth_channel = depth_channel.transpose((1, 0))
    # depth_channel = np.zeros_like(depth_channel)
    # depth_channel[0,1] = 1.0
    plt.figure()
    plt.imshow(normals_channels)
    plt.axis("off")
    plt.draw()
    plt.figure()
    plt.imshow(depth_channel, cmap="gray")
    plt.axis("off")
    plt.show()

    descriptor_pts = np.array(cloud.points)[mask]
    descriptor_cloud = numpyToCloud(descriptor_pts)
    poses_plot = [poses_plot[mid]]
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [0.5, 0.0, 0.0])
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    # p.plotCloud(descriptor_cloud, [1.0, 0.0, 1.0])
    p.plotHands(poses_plot, [1.0, 1.0, 0.0])
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    p.plotAxes(camera_frame)
    hand_frame = poses_plot[0]
    # p.plotAxes(hand_frame, [0.15, 0.15, 0.15])
    # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setCameraPose([-0.5, -0.5, 0.5], [0, 0, 1], [0, 0, 0])
    p.setTitle("Grasp")
    p.show()

    # Plot the descriptor volume for the grasp image.
    box_dims = np.array(
        [bounds[1] - bounds[0], bounds[3] - bounds[2], bounds[5] - bounds[4]]
    )
    # box_center = np.array([bounds[0], bounds[2], bounds[4]]) + 0.5 * box_dims
    # box_center += poses_plot[0][:3, 3]
    # box_dims = np.array([volume_dims[k] for k in ["depth", "width", "height"]])
    # box_dims = np.array([volume_dims[k] for k in ["width", "depth", "height"]])
    # box_dims = [box_dims[1], box_dims[0], box_dims[2]]
    # box_dims[2] = 0.02
    box_center = deepcopy(poses_plot[0][:3, 3])
    # box_center += 0.5 * box_dims[1] * poses_plot[0][:3, 0]
    box_center += 0.5 * box_dims[0] * poses_plot[0][:3, 0]
    box_pose = np.eye(4)
    box_pose[:3, 3] = box_center
    box_pose[:3, :3] = poses_plot[0][:3, :3]
    print(f"(descriptor volume) center: {box_center}, dims: {box_dims}")
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    # p.plotCloud(descriptor_cloud, [1.0, 0.0, 1.0])
    p.plotHands(poses_plot, yellow, 1.0)
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    p.plotCube(box_pose, box_dims[0], box_dims[1], box_dims[2], purple, 0.4)
    p.plotAxes(camera_frame)
    # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setCameraPose([-0.5, -0.5, 0.5], [0, 0, 1], [0, 0, 0])
    p.setTitle("Descriptor volume: grasp image")
    p.show()

    prop_params = {"image_size": 60, "image_type": "depth"}
    prop_img_gen = ProposalImageGenerator(**prop_params)
    prop_imgs = prop_img_gen.createImages(prep_cloud, prep_samples)
    print("prop_imgs:", prop_imgs.shape)
    for i in range(prop_imgs[0].shape[-1]):
        plt.figure()
        plt.imshow(prop_imgs[0][:, :, i], cmap="gray")
        plt.axis("off")
        plt.draw()
    plt.show()

    # Plot the descriptor volume for the proposal image.
    origin = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
    # draw_geoms([prep_cloud, origin], "prep_cloud")
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    box_pose = np.eye(4)
    box_pose[:3, 3] = samples[0]
    box_pose[:3, :3] = np.dot(camera_frame[:3, :3], box_pose[:3, :3])
    box_dims = np.array([0.1, 0.1, 0.1])
    p.plotAxes(camera_frame)
    p.plotCube(box_pose, box_dims[0], box_dims[1], box_dims[2], purple, 0.4)
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    p.setCameraPose([-0.5, -0.5, 0.5], [0, 0, 1], [0, 0, 0])
    p.setTitle("Descriptor volume: proposal image")
    p.show()
    # exit()

    # box = o3d.geometry.TriangleMesh.create_box(box_dims[0], box_dims[1], box_dims[2])
    # box.translate(box_center)
    # cloud.paint_uniform_color([0, 0, 1.0])
    # draw_geoms([cloud, box])

    # Plot some grasps.
    # poses_plot = [poses_plot[mid]]
    # poses_plot = [poses_plot[mid - 2], poses_plot[mid], poses_plot[mid + 2]]
    # rand_idxs = np.random.choice(np.arange(len(free_poses)), 6)
    # rand_idxs = rand_idxs[2:4]
    # poses_plot = poses_plot[rand_idxs]
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [0.5, 0.0, 0.0])
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    p.plotHands(poses_plot, [1.0, 1.0, 0.0])
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    p.plotAxes(camera_frame)
    hand_frame = poses_plot[0]
    p.plotAxes(hand_frame, [0.15, 0.15, 0.15])
    p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setTitle("Ground truth positives")
    p.show()

    normals_channels = grasp_img_mid[:, :, :3]
    depth_channel = grasp_img_mid[:, :, -1]
    normals_channels = normals_channels.transpose((1, 0, 2))
    depth_channel = depth_channel.transpose((1, 0))
    plt.figure()
    plt.imshow(normals_channels)
    plt.draw()
    plt.figure()
    plt.imshow(depth_channel, cmap="gray")
    plt.show()

    # exit()

    # Plot the frames for a single grasp pose.
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [0.5, 0.0, 0.0])
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    mid = int(np.floor(len(poses) / 2))
    print(f"mid: {mid}, poses: {len(poses)}")
    mid = mid - 2
    p.plotHands([poses[mid]], [1.0, 1.0, 0.0])
    hand_frame = poses[mid]
    p.plotAxes(camera_frame)
    p.plotAxes(hand_frame, [0.15, 0.15, 0.15])
    p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setTitle("single grasp pose")
    p.show()

    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [0.5, 0.0, 0.0])
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    mid = int(np.floor(len(poses) / 2))
    print(f"mid: {mid}, poses: {len(poses)}")
    mid = mid - 2
    l = poses[mid:mid+4]
    print("l:", len(l), mid, mid+4)
    # p.plotHands([poses[mid:mid+4]], [1.0, 1.0, 0.0])
    p.plotHands(l, [1.0, 1.0, 0.0])
    # p.plotAxes(camera_frame)
    # p.plotAxes(hand_frame, [0.15, 0.15, 0.15])
    # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setTitle("rotations about approach axis")
    p.show()
    exit()

    sphere_pts = np.zeros((len(poses), 3))
    sphere_normals = np.zeros((len(poses), 3))
    for i, X in enumerate(poses):
        # sphere_pts[i] = samples[0] - 0.15 * X[:3, 0]
        # sphere_normals[i] = X[:3, 0]
        sphere_pts[i] = samples[0]
        sphere_normals[i] = -X[:3, 0]
    print(sphere_normals)
    sphere_cloud = numpyToCloud(sphere_pts, sphere_normals)
    print(f"sphere_normals: {len(sphere_normals)}")
    draw_geoms(geometry_list=[sphere_cloud], window_name="sphere cloud")

    # p = HandsPlot(hand_geom)
    # p.plotNormals(sphere_pts, sphere_normals)
    # p.plotSphere(samples[0], 0.1, [0.0, 1.0, 0.0], 0.5)
    # p.show()

    # Plot all possible grasp poses for a single sample position.
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [0.5, 0.0, 0.0])
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    # p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    # p.plotHands(poses, [1.0, 1.0, 0.0], scale=0.1)
    # p.plotCloud(sphere_cloud, [0.2, 0.2, 0.2])
    p.plotNormals(sphere_pts, sphere_normals)
    # p.plotSphere(samples[0], 0.1, [0.0, 1.0, 0.0], 0.5)
    # p.plotArrow(
        # sphere_pts[mid], sphere_pts[mid] + 0.25 * sphere_normals[mid], color=red
    # )
    # p.plotArrow(
        # sphere_pts[mid + 3],
        # sphere_pts[mid + 3] + 0.25 * sphere_normals[mid + 3],
        # color=green,
    # )
    # p.plotAxes(camera_frame)
    # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setTitle("all grasp poses at single sample position")
    # p.setCameraPose([0, 0, 1.0], [0, 0, 1], [0, 0, 0])
    p.setCameraPose([0.25, -0.25, 1.0], [0, 0, 1], [0, 0, 0])
    p.show()

    print(f"samples[0]: {samples[0]}")
    print("DONE!")


if __name__ == "__main__":
    main()

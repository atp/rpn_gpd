import glob
import os
import sys

folder = sys.argv[1]
min_instances = int(sys.argv[2])

subfolders = []
for f in os.listdir(folder):
    if os.path.isdir(folder + f):
        subfolders.append(folder + f)

subfolders = sorted(subfolders)
categories = []

for s in subfolders:
    category = s[s.rfind("/") + 1 :]
    ply_files = glob.glob(s + "/*.ply")
    if len(ply_files) >= min_instances:
        categories.append(category)
    print("Category:", category, "#instances:", len(ply_files))

print(
    "\nFound %d categories with at least %d instances:"
    % (len(categories), min_instances)
)
for c in categories:
    print(c)

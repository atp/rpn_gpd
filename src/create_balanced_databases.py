"""Create balanced database from a given database.

Usage: python create_balanced_databases.py PATH_TO_DB KEY SIZE

Take an HDF5 database as input and produces a balanced HDF5 database as output. The 
output database contains an array of images and array of labels that are derived from 
the input database. The balancing is achieved by finding the indices that would 
correspond to a balanced version of the database. Calculate index maps for proposal 
images and grasp images. Those maps are then used to create a new database that is 
balanced.

"""

import os
import sys

import numpy as np
import psutil
import tables as tab

from utils.db_utils import calcMaxInMemory

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=9, complib="blosc:lz4")


def createIndexMap(labels):
    """Create a balanced index map for a labels array.

    Args:
        labels (?): the labels array.

    Returns:
        np.array: the balanced index map, with equally many positives and negatives.
    """
    pos = np.asarray(np.nonzero(labels == 1), np.int32)
    neg = np.asarray(np.where(labels == 0), np.int32)
    # if labels.ndim >= 2:
    if neg.shape[1] > pos.shape[1]:
        neg_idxs = np.random.choice(
            np.arange(neg.shape[1]), pos.shape[1], replace=False
        )
        gc_idxs = np.hstack((pos, neg[:, neg_idxs])).T
    else:
        pos_idxs = np.random.choice(
            np.arange(pos.shape[1]), neg.shape[1], replace=False
        )
        gc_idxs = np.hstack((pos[:, pos_idxs], neg)).T

    print(f"Found {pos.shape[1]} positives and {neg.shape[1]} negatives.")

    return gc_idxs, pos


def createIndexMapFromRows(labels, pos):
    """Create a balanced index map from the rows of a labels array.

    Args:
        labels (?): the labels array.
        pos (np.array): the indices for positive elements of `labels`.

    Returns:
        np.array: the balanced index map, with equally many positives and negatives.
    """
    gen_idxs = []
    pos_rows = np.unique(pos[0, :])
    neg_rows = np.asarray(np.where(np.sum(labels, 1) == 0)[0], np.int32)
    print("Before balancing: ")
    print(f"  Rows with >= 1 positive: {len(pos_rows)}")
    print(f"  Rows with only negatives: {len(neg_rows)}")
    if len(neg_rows) > len(pos_rows) and len(pos_rows) > 0:
        neg_rows = np.random.choice(neg_rows, len(pos_rows), replace=False)
    elif len(pos_rows) > len(neg_rows) and len(neg_rows) > 0:
        pos_rows = np.random.choice(pos_rows, len(neg_rows), replace=False)
    print("After balancing:")
    print(f"  Rows with >= 1 positive: {len(pos_rows)}")
    print(f"  Rows with only negatives: {len(neg_rows)}")
    #    assert(len(pos_rows) + len(neg_rows) == labels.shape[0])
    gen_idxs = np.hstack((pos_rows, neg_rows))

    return gen_idxs


def createImageArray(node, new_name, fout, idxs, max_in_memory=10000):
    """Copy a node blockwise to a database.

    The `node` from which to copy is assumed to be an array of images, with the last
    three dimensions being the dimensions of the image (e.g., height, width, channels).

    Args:
        node (tables.node): the node to be copied.
        new_name (str): the name of the new node.
        fout (tables.file): the file handle of the database.
        idxs (np.array): the indices of the elements to be copied.
        max_in_memory (int, optional): the max number of rows allowed in system memory.

    Returns:
        tables.CArray: the array contained in the new node.
    """
    blocks = list(np.arange(0, idxs.shape[1], max_in_memory)) + [idxs.shape[1]]

    shape = (idxs.shape[1],) + node.shape[-3:]
    dset = fout.create_carray(
        "/",
        new_name,
        tab.UInt8Atom(),
        shape=shape,
        filters=filters,
    )
    print(f"Created dataset: {dset}, {dset.dtype}, {node.dtype}")

    for i in range(1, len(blocks)):
        print(
            f"({i}/{len(blocks)-1}) Copying block: " + f"[{blocks[i-1]}, {blocks[i]}]"
        )
        imgs = np.array(node[blocks[i - 1] : blocks[i]])
        block_idxs = idxs[(idxs[:, 0] >= blocks[i - 1]) & (idxs[:, 0] < blocks[i])]
        dset[blocks[i - 1] : blocks[i]] = imgs[block_idxs]
        print(
            f"  block_idxs: {block_idxs.shape}, {[block_idxs.min(), block_idxs.max()]}"
        )

    return dset


def createDatabase(fin, idxs_list, key, fpath_out):
    """Create a database by copying the elements from an array given by an index list.

    Args:
        fin (tab.File): the database from which to read the array.
        idxs_list (np.array): the indices of the array elements to be copied.
        key (str): the name of the array from which to copy.
        fpath_out (str): the path to the output database file.
    """
    labels = np.array(fin.get_node("/labels"))
    imgs_in = fin.get_node("/", key)
    idxs = idxs_list[key]
    labels_table = []
    print("key:", key, "imgs_in:", type(imgs_in), imgs_in.shape, "idxs:", idxs.shape)
    print("Extracting images ...")

    if key == "images" and labels.ndim == 1:
        imgs_out = [np.squeeze(imgs_in[i, :]) for i in idxs]
        labels_out = [np.squeeze(labels[i]) for i in idxs]
    elif key == "prop_imgs" or (key == "images" and labels.ndim == 2):
        imgs_out = [imgs_in[i] for i in idxs]
        print("Extracting labels ...")
        neg_idx = np.nonzero(np.sum(labels[idxs, :], 1) == 0)[0]
        labels_out = np.ones(len(idxs))
        labels_out[neg_idx] = 0
        print(
            f"positives: {np.count_nonzero(labels_out == 1)}, "
            + f"negatives: {np.count_nonzero(labels_out == 0)}"
        )
        labels_table = labels[idxs, :]
    elif key == "grasp_imgs":
        imgs_out = [imgs_in[i, j] for i, j in zip(idxs[:, 0], idxs[:, 1])]
        labels_out = [labels[i, j] for i, j in zip(idxs[:, 0], idxs[:, 1])]

    imgs_out = np.array(imgs_out)
    print(f"imgs_in: {imgs_in}, imgs_out: {imgs_out.shape}, {imgs_out.dtype}")
    print(f"Storing new database at: {fpath_out}")

    with tab.open_file(fpath_out, "w") as fout:
        imgs_dset = fout.create_carray(
            "/", "images", tab.UInt8Atom(), obj=imgs_out, filters=filters
        )
        print(f"Created dataset: {imgs_dset}")
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(f"Created dataset: {labels_dset}")
        if len(labels_table) > 0:
            labels_table_dset = fout.create_carray(
                "/", "labels_table", obj=labels_table, filters=filters
            )
            print(f"Created dataset: {labels_table_dset}")


def main():
    if len(sys.argv) < 2:
        print("Error: not enough input arguments!")
        print("Usage: python create_balanced_databases.py PATH_TO_DB [KEY] [SIZE]")
        print(
            "KEY is the location of the array in the database to be accessed "
            + "(e.g., images, prop_imgs, grasp_imgs). If unspecified, will try "
            + "to copy /prop_imgs and /grasp_imgs."
        )
        print("SIZE can be used to specify the size of the output database.")
        exit(-1)

    # Read command line parameters.
    fname = sys.argv[1]
    if len(sys.argv) > 2:
        keys = [sys.argv[2]]
    else:
        keys = ["prop_imgs", "grasp_imgs"]
    idx_map_size = -1
    if len(sys.argv) == 4:
        idx_map_size = int(sys.argv[3])

    with tab.open_file(fname, "r") as f:
        labels = np.asarray(f.root.labels, np.int)
        if labels.ndim > 2:
            print("Error: node /labels has wrong dimensions!")
            sys.exit(-1)
        print(f"Input database contains labels with shape: {labels.shape}")

        # Create the index map over all labels (e.g., for the grasp classifier images).
        gc_idxs, pos = createIndexMap(labels)
        total = np.max(gc_idxs.shape)
        print(f"Balanced database will have {total} elements")

        # Create the index map over the rows (e.g., for the proposal images).
        gen_idxs = []
        if labels.ndim == 2:
            gen_idxs = createIndexMapFromRows(labels, pos)

        # Randomly subsample the indices to shrink the database (if desired).
        if idx_map_size > 0:
            if len(gen_idxs) > idx_map_size:
                subset = np.random.choice(
                    np.arange(len(gen_idxs)), idx_map_size, replace=False
                )
                gen_idxs = gen_idxs[subset]
                print(f"Randomly subsampled the row indices: {gen_idxs.shape}")

            subset = np.random.choice(np.arange(total), idx_map_size, replace=False)
            gc_idxs = gc_idxs[subset]
            print(f"Randomly subsampled the indices: {gc_idxs.shape}")

        if labels.ndim == 1:
            idxs_list = {"images": gc_idxs}
        elif labels.ndim == 2:
            if "prop_imgs" in keys and "grasp_imgs" in keys:
                idxs_list = {"prop_imgs": gen_idxs, "grasp_imgs": gc_idxs}
            elif "images" in keys:
                idxs_list = {"images": gen_idxs}

        # Create the balanced databases and store them as files.
        out_dir = fname[: fname.rfind("/") + 1]
        suffix = fname[fname.rfind("/") + 1 : fname.rfind("_")]
        for k in keys:
            print("-------------------------------------------------------------")
            fname_out = out_dir + k + "_" + suffix + ".h5"
            print(k, fname_out)
            createDatabase(f, idxs_list, k, fname_out)
            print("-------------------------------------------------------------\n")


if __name__ == "__main__":
    main()

"""Create a random subset of a database.

Given a database and a size <n> as input, sample a random subsample of size <k> from the
database and create a new database that contains the subset. The elements of the 
subsample can either be drawn from the rows of </meta/samples> or </labels>.
"""

import os
import sys
from functools import partial

import numpy as np
import tables as tab

from utils.db_utils import calcMaxInMemory

max_in_memory = 10000
skip_groups = ["/meta"]
# skip_nodes = ["categories", "samples", ]

os.environ["BLOSC_NTHREADS"] = "4"

filters = tab.Filters(complevel=5, complib="blosc")


def copySubNode(fout, where, node, indices, blocks):
    func = partial(fout.create_carray, filters=filters)
    # if node.name == 'labels':
    if node.name.find("imgs") == -1 and node.name.find("images") == -1:
        dset = func(where, node.name, obj=np.asarray(node)[indices])
        print(f"Created dataset: {dset}, {dset.dtype}, {node.dtype}")
        return

    name = "images" if len(nodes) == 2 else node.name
    new_shape = (num_samples,) + node.shape[1:]
    print(f"name: {name}")
    print(where)
    dset = fout.create_carray(
        where, name, tab.UInt8Atom(), shape=new_shape, filters=filters
    )
    print(f"Created dataset: {dset}, {dset.dtype}, {node.dtype}")
    start = 0

    for i in range(1, len(blocks)):
        data = [node[j, :] for j in indices[blocks[i - 1] : blocks[i]]]
        stop = start + len(data)
        dset[start:stop] = np.array(data)
        print(
            f"({i}/{len(blocks)-1}) Done with block: "
            + f"[{blocks[i-1]}, {blocks[i]}], start: {start}, "
            + f"stop: {stop}, which contains {len(data)} indices"
        )
        start = stop


# Read command line parameters.
if len(sys.argv) < 5:
    print("Error: not enough input arguments!")
    print("Usage: python subsample_database.py H5_IN H5_OUT SIZE DB_TYPE")
    print("DB_TYPE: 0 if the DB has a node </labels>, 1 if it has </meta/samples>")
    exit(-1)
fname_in = sys.argv[1]
fname_out = sys.argv[2]
num_samples = int(sys.argv[3])
db_type = int(sys.argv[4])

with tab.open_file(fname_in, "r") as fin, tab.open_file(fname_out, "w") as fout:
    if db_type == 0:
        node = fin.get_node("/labels")
    elif db_type == 1:
        node = fin.get_node("/meta/samples")

    # Draw random subsample.
    n = len(node)
    indices = np.arange(n)
    indices = np.random.choice(indices, num_samples, replace=False)
    indices = np.sort(indices)
    print(f"Randomly selected {num_samples} out of {n} examples.")
    print(f"  indices: {indices.shape}")

    groups = list(fin.walk_groups())
    if db_type == 1:
        groups = groups[1:]
        prefix = "/"
    else:
        prefix = ""
    groups = [g for g in groups if g._v_pathname not in skip_groups]

    for group in groups:
        if group._v_name in skip_groups:
            print(f"Skipping group {group._v_name}")
            continue
        print("Processing group:", group._v_name)
        if type(group) == tab.group.Group:
            new_group = fout.create_group("/", group._v_name)
            print(f"Created group {new_group} in {fout}")
        nodes = fin.list_nodes(group)
        nodes = [n for n in nodes if n._v_pathname not in skip_groups]
        names = [n._v_pathname for n in nodes]
        print(f"Nodes available in this group: {names}")

        # Determine a set of blocks such that each block fits into memory.
        blocks = np.arange(0, len(indices), max_in_memory)
        blocks = list(blocks) + [len(indices)]
        print("blocks:", blocks)

        for node in nodes:
            if "meta" in node._v_pathname:
                continue
            print(f"Copying subsample from node {node._v_pathname} ...")
            print(f"  Node: {node.dtype}, {node.shape}")
            copySubNode(fout, prefix + group._v_name, node, indices, blocks)
        print("============================================================\n")

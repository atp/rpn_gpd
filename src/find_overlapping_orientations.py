"""Find the unique orientations for a hands generator.
"""

import sys

import numpy as np
import tables as tab
from scipy.spatial.transform import Rotation as Rot

from hand_params import antipodal_params, hand_params
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator

np.set_printoptions(precision=4, suppress=True)


def findUniqueOrientations(quats, min_diff=0.001) -> list:
    """Find the unique elements of a list of orientations.

    Args:
        quats (list): the orientations.
        min_diff (float, optional): the difference between two values from which they
            are considered to be distinct.

    Returns:
        list: the unique orientations.
    """
    unique_quats = []

    for q in quats:
        is_unique = True
        for w in unique_quats:
            dist_pos = np.abs(q - w)
            dist_neg = np.abs(q + w)
            if np.all(dist_pos < min_diff) or np.all(dist_neg < min_diff):
                is_unique = False
                print("Not unique:", q, w)
                break
        if is_unique:
            unique_quats.append(q)

    return unique_quats


def main():
    if len(sys.argv) < 2:
        print("Error: not enough input arguments.")
        print("Usage: python find_overlapping_orientations.py METHOD")
        sys.exit(-1)

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    if sys.argv[1] == "ma":
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif sys.argv[1] == "qd":
        gen = QdHandsGenerator(hand_geom, antip_prms)
    else:
        print(f"Error: wrong input argument!")
        exit(-1)

    idxs = np.arange(gen.getNumHandsPerSample())
    rots = gen.calcRotations(idxs)

    quats = [Rot.from_matrix(x).as_quat() for x in rots]
    unique_quats = findUniqueOrientations(quats)
    print("unique quats:", len(unique_quats))

    q1 = Rot.from_rotvec(np.pi / 2 * np.array([1, 0, 0])).as_quat()
    q2 = -q1
    unique_quats = findUniqueOrientations([q1, q2])
    print("quats:", [q1, q2])
    print("unique quats:", unique_quats)


if __name__ == "__main__":
    main()

"""Create a database with grasp images given a database with proposal images.

This script avoids calculating labels by using the labels from the given
database. The decrease in runtime compared to <create_data.py> is large.

It can be used for single object scenes and single object scenes with a table
plane, but not for clutter scenes because the objects are randomly selected,
placed and scaled for those.
"""

import csv
import os
from argparse import ArgumentParser
from datetime import datetime
from functools import partial
from time import time

import numpy as np
import open3d as o3d
import tables
from scipy.spatial.transform import Rotation as Rot

from create_data import antipodal_modes
from data_generator import DataGenerator
from grasp_data_generator import subsampleProposals
from grasp_image import GraspImageGenerator
from hand_params import hand_params
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.qd_hands_generator import QdHandsGenerator
from object_set import ThreeDNetObjectSet
from scene import ClutterScene, SingleObjectScene, calcLookAtPose

grasp_img_shape = [60, 60, -1]

os.environ["BLOSC_NTHREADS"] = str(os.cpu_count())
filters = tables.Filters(complevel=5, complib="blosc:lz4hc")
chunk_cache_size = 2 * 2 ** 30  # 2GB

np.set_printoptions(precision=4, suppress=True)

draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)


def createSingleObjectScene(path: str, scale: float):
    scene = SingleObjectScene(path)
    scene.resetToScale(scale)

    return scene


def createClutterScene(self, num_instances, max_objects_range=[20, 40]):
    sample_extents = np.array([[-0.1, -0.1, 0.0], [0.1, 0.1, 0.01]])

    max_objects = np.random.randint(max_objects_range[0], max_objects_range[1])
    categories = np.random.randint(0, len(self.object_set), max_objects)
    instances = np.random.randint(0, self.num_instances, max_objects)
    print("Randomly selected objects:")
    print([(self.object_set.objects[c], i) for c, i in zip(categories, instances)])

    filepaths = [
        self.object_set.get([categories[j], instances[j]]) for j in range(max_objects)
    ]
    scene = ClutterScene(filepaths, self.min_extent, self.max_extent, sample_extents)
    scene.reset()

    return scene


def parseArgs():
    """Parse command line arguments."""
    parser = ArgumentParser(description="Create database for grasp images")
    parser.add_argument(
        "categories",
        metavar="CATEGORIES_TXT",
        help="path to TXT file with object categories",
    )
    parser.add_argument("out_path", metavar="OUT_PATH", help="path to output database")
    parser.add_argument(
        "database",
        metavar="DATABASE",
        help="path to input database with proposal images",
    )
    parser.add_argument(
        "csv_path",
        help="path to CSV file with object infos (produced by <create_data.py>)",
    )
    parser.add_argument("-s", "--seed", default=0, type=int, help="random seed")
    parser.add_argument(
        "-i",
        "--in_dir",
        default="/home/andreas/data/watertight/3dnet/",
        help="path to object set",
    )
    parser.add_argument(
        "-n", "--num_samples", default=100, type=int, help="number of samples"
    )
    parser.add_argument(
        "-gi",
        "--grasp_img_channels",
        type=int,
        default=12,
        help="number of grasp image channels",
    )
    parser.add_argument(
        "-am",
        "--antipodal_mode",
        default="strict",
        help="antipodal mode: <less1>, <less2>, <strict>, <bowl>",
    )
    parser.add_argument(
        "-ap", "--add_plane", action="store_true", help="add support plane to object"
    )
    parser.add_argument(
        "-2v",
        "--use_two_views",
        action="store_true",
        help="use two viewpoints per point cloud",
        default="",
    )
    args = parser.parse_args()

    return args


class Database:
    def __init__(self, path: str, labels_shape: tuple, images_shape: tuple):
        self.file = tables.open_file(path, "w", chunk_cache_size=chunk_cache_size)
        self.labels = self.file.create_carray(
            "/", "labels", tables.Float32Atom(), shape=labels_shape, filters=filters
        )
        self.images = self.file.create_carray(
            "/", "images", tables.UInt8Atom(), shape=images_shape, filters=filters
        )
        self.rot_idxs = self.file.create_carray(
            "/",
            "rot_idxs",
            tables.Int32Atom(),
            shape=(labels_shape[0],),
            filters=filters,
        )


class ViewpointProcessor:
    """A helper to generate grasp images and extract labels per viewpoint."""

    def __init__(
        self,
        db_in_path: str,
        num_samples: int,
        hands_gen: QdHandsGenerator,
        db_out: Database,
        add_plane: bool,
        use_two_views: bool,
    ):
        """Construct a viewpoint processor.

        Args:
            db_in_path (str): path to input database.
            num_samples (int): the number of samples to draw for each viewpoint.
            hands_gen (QdHandsGenerator): the hand pose generator.
            db_out (Database): the output database.
            add_plane (bool): if a plan is added to the scene.
        """
        with tables.open_file(db_in_path, "r") as f:
            self.samples = np.array(f.get_node("/meta/samples"))
            self.labels = np.array(f.get_node("/labels"))
        self.num_samples = num_samples
        self.hands_gen = hands_gen
        self.grasp_img_gen = GraspImageGenerator(tuple(grasp_img_shape), self.hands_gen)
        self.db_out = db_out
        self.add_plane = add_plane
        self.use_two_views = use_two_views
        self.out_count = 0

    def processViewpoint(
        self, start: int, stop: int, scene, viewpoint: np.array, look_at=np.zeros(3)
    ):
        sub_labels = self.labels[start:stop]
        sub_samples = self.samples[start:stop]

        if self.add_plane:
            raise NotImplementedError("Not supported. Use <create_data.py> for now.")
        else:
            idxs = subsampleProposals(sub_labels, self.num_samples)
            rots = self.hands_gen.calcRotations(idxs[1])

            view_points = np.array([viewpoint])
            if self.use_two_views:
                R = Rot.from_rotvec(np.pi / 2.0 * np.array([0.0, 1.0, 0.0])).as_matrix()
                view_points = np.vstack((view_points, np.dot(R, view_points[0])))

            cloud = o3d.geometry.PointCloud()
            cam_poses = []
            for v in view_points:
                print(f"viewpoint: {v}")
                cam_poses.append(calcLookAtPose(v, look_at))
                cloud += scene.createProcessedPointCloudFromView(cam_poses[-1])

            prep_cloud, prep_samples = self.hands_gen.preprocess(
                cloud, sub_samples, cam_poses[0]
            )
            # draw_geoms([cloud], f"Point cloud seen from {len(view_points)} viewpoints")

            time_images = time()
            images = self.grasp_img_gen.calcImagesAtIndicesSP(
                prep_cloud, prep_samples, idxs, rots, []
            )
            time_images = time() - time_images
            print(f"Created {images.shape} grasp images in {time_images:.3f}s.")

            out_stop = self.out_count + len(images)
            self.db_out.images[self.out_count : out_stop] = images
            self.db_out.labels[self.out_count : out_stop] = sub_labels[idxs[0], idxs[1]]
            self.db_out.rot_idxs[self.out_count : out_stop] = idxs[1]
            self.out_count = out_stop
            print(f"Output database contains {self.out_count} elements.")


def main():
    # Read command line arguments.
    args = parseArgs()
    print(f"Command line arguments:\n{args}")
    grasp_img_shape[-1] = args.grasp_img_channels

    # Ensure that the same sequence of random numbers is generated.
    np.random.seed(args.seed)

    # Create the hands generator and the grasp image generator.
    hand_geom = HandGeometry(**hand_params)
    antipodal_params = antipodal_modes[args.antipodal_mode]
    antip_params = AntipodalParams(**antipodal_params)
    hands_gen = QdHandsGenerator(hand_geom, antip_params)

    # Load the object set.
    object_set = ThreeDNetObjectSet(args.in_dir, args.categories, rescale=True)
    print("Available object categories:\n", object_set.objects)

    # Get the scaling factors from the CSV file.
    with open(args.csv_path, "r") as f:
        object_infos = [r for r in csv.reader(f, delimiter=",")]
    object_infos = object_infos[1:]
    object_infos = [x[:2] + [float(y) for y in x[2:]] for x in object_infos]
    scales = {x[1]: np.max(np.array(x[-3:])) for x in object_infos}
    print("scales:", scales)

    # Get the data from the proposals database.
    with tables.open_file(args.database, "r") as f:
        instances = f.get_node("/meta/instances")
        categories = f.get_node("/meta/categories")
        viewpoints = f.get_node("/meta/viewpoints")

        db_infos = [
            (i, x.decode("ascii"), y.decode("ascii"), v)
            for i, (x, y, v) in enumerate(zip(categories, instances, viewpoints))
        ]
        # print(*infos, sep="\n")

    # Calculate the size of the output database.
    info = object_infos[0]
    viewpts_dict = [
        (x[0], x[3]) for x in db_infos if x[1] == info[0] and x[2] == info[1]
    ]
    viewpts = np.array([x[1] for x in viewpts_dict])
    unique_views, unique_idxs = np.unique(viewpts, axis=0, return_index=True)
    unique_idxs = sorted(unique_idxs)
    num_unique_views = len(unique_views)
    n = len(scales) * num_unique_views * args.num_samples

    # Create the output database.
    labels_shape = (n,)
    images_shape = (n,) + tuple(grasp_img_shape)
    db_out = Database(args.out_path, labels_shape, images_shape)

    processor = ViewpointProcessor(
        args.database,
        args.num_samples,
        hands_gen,
        db_out,
        args.add_plane,
        args.use_two_views,
    )

    # Process object by object.
    for i in range(len(scales)):
        info = object_infos[i]
        s = f"[{datetime.now().replace(microsecond=0)}] "
        s += f"Object ({i}/{len(scales)}), category: {info[0]}, instance: {info[1]}"
        print(s)
        mesh_path = object_set.getByStringID(info[0], info[1]) + ".ply"
        scene = createSingleObjectScene(mesh_path, info[2])
        if args.add_plane:
            scene.addSupportPlane()

        # Find viewpoints which match the category and instance of this object.
        viewpts_dict = [
            (x[0], x[3]) for x in db_infos if x[1] == info[0] and x[2] == info[1]
        ]
        viewpts = np.array([x[1] for x in viewpts_dict])
        unique_views, unique_idxs = np.unique(viewpts, axis=0, return_index=True)
        unique_idxs = sorted(unique_idxs)
        num_unique_views = len(unique_views)
        starts = [viewpts_dict[j][0] for j in unique_idxs] + [viewpts_dict[-1][0]]

        for j in range(num_unique_views):
            start, stop = starts[j], starts[j + 1]
            viewpoint = viewpts[unique_idxs[j]]
            print(f"View: {j+1}/{num_unique_views}, viewpoint: {viewpoint}")
            processor.processViewpoint(start, stop, scene, viewpoint)

        print("---------------------------------------------------------------------\n")


if __name__ == "__main__":
    main()

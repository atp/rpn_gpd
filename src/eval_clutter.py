"""Evaluate a grasp detector on cluttered scenes with 3DNet objects.

Usage: python eval_clutter.py OBJECTS_DIR OBJECTS_TXT METHOD OUT_DIR

"""

import sys
from argparse import ArgumentParser

import numpy as np
import open3d as o3d

from eval_grasp_detector import antipodal_modes, createGraspDetector, evaluators, net_cfg
from grasp_detector import GpdGraspDetector, MaGcGraspDetector, MaGraspDetector
from hand_params import antipodal_params, hand_params
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from networks.network import LenetLike
from object_set import ThreeDNetObjectSet
from proposal import downsampleByDistance, sampleFromCube
from scene import ClutterScene, calcLookAtPose
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot

min_obj_extent = 0.005
max_obj_extent = [0.1, 0.2]
# min_obj_extent = 0.01
# max_obj_extent = [0.07, 0.7]
sample_extents = np.array([[-0.1, -0.1, 0.0], [0.1, 0.1, 0.01]])
# sample_extents = np.array([[-0.2, -0.2, -0.1], [0.2, 0.2, 0.1]])
# sample_extents = np.array([[-0.05, -0.05, -0.05], [0.05, 0.05, 0.05]])
num_scene_objects = [20, 40]
num_instances = 10
num_views = 5
num_scenes = 10
camera_distance = 0.5
cube_size = 1.0
num_samples = 100
look_at = [0.0, 0.0, 0.0]

# thresh_step = 0.2
thresh_step = 0.1
# thresh_step = 0.05

# prop_params = {"image_size": 60, "image_type": "occupancy"}
prop_params = {"image_size": 60, "image_type": "depth"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], 3]
# grasp_img_shape = (60, 60, 3)
# grasp_img_shape = (60, 60, 12)
grasp_img_shape = (60, 60, 4)

np.set_printoptions(precision=3, suppress=True)
np.random.seed(1)

# net_cfg["rot-gc"]["weights_paths"] = {
# "rot": "../models/clutter/bbbcm2/qd/rot/lenet_wl/checkpoints/best.pth",
# "gc": "../models/clutter/bbbcm2/qd/gc/lenet/checkpoints/best.pth",
# }


def preprocessPointCloud(cloud, pose, voxel_size=0.003):
    cloud_out = cloud.voxel_down_sample(voxel_size)
    print(f"Downsampled cloud to {len(cloud_out.points)} points.")
    cloud_out = estimateNormals(cloud_out, pose[:3, 3])
    print(f"Estimated {len(cloud_out.normals)} surface normals.")
    return cloud_out


def createArgParser():
    parser = ArgumentParser(description="Evaluate a grasp detector on clutter")
    parser.add_argument("objects_dir", help="path to object set directory")
    parser.add_argument(
        "categories_file", help="path to TXT file that lists the object categories"
    )
    parser.add_argument("results_dir", help="where to store the results")
    parser.add_argument("method", help="grasp pose detector")
    parser.add_argument(
        "-c",
        "--check_collisions",
        action="store_true",
        help="Check for collisions after detections",
    )
    parser.add_argument(
        "-ap", "--add_plane", action="store_true", help="add support plane to object"
    )

    return parser


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Setup the object dataset.
    object_set = ThreeDNetObjectSet(args.objects_dir, args.categories_file, True)

    # Setup the grasp detector and the method evaluator.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_modes["strict"])
    detector = createGraspDetector(
        hand_geom, antip_prms, args.method, prop_params, grasp_img_shape, net_cfg
    )
    evaluator = evaluators[args.method](thresh_step)

    # Sample uniform view points on a sphere.
    viewpoints = np.random.normal(size=(num_views, 3))
    viewpoints /= np.linalg.norm(viewpoints, axis=1)[:, np.newaxis]
    viewpoints *= camera_distance
    if args.add_plane:
        viewpoints[:, 2] = np.abs(viewpoints[:, 2])

    for i in range(num_scenes):
        print(f"Scene: {i+1}/{num_scenes}")
        results = []

        # Create a cluttered scene.
        max_objects = np.random.randint(num_scene_objects[0], num_scene_objects[1])
        categories = np.random.randint(0, len(object_set), max_objects)
        instances = np.random.randint(0, num_instances, max_objects)
        print("Randomly selected objects:")
        print([(object_set.objects[c], i) for c, i in zip(categories, instances)])
        filepaths = [
            object_set.get([categories[j], instances[j]]) for j in range(max_objects)
        ]
        scene = ClutterScene(filepaths, min_obj_extent, max_obj_extent, sample_extents)
        scene.reset()

        plane_z = -1.0
        if args.add_plane:
            scene.addSupportPlane()
            plane_z = scene.scene.get_pose(scene.table_node)[2, 3]
            print("Table plane is at z =", plane_z)

        # Create the complete point cloud.
        # mesh_cloud = scene.createMultiViewPointCloud()
        mesh_cloud = scene.createMultiViewPointCloud(num_pts=100000)
        mesh_tree = o3d.geometry.KDTreeFlann(mesh_cloud)
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
        # o3d.visualization.draw_geometries([mesh_cloud, origin_frame], "", 640, 480)
        # o3d.visualization.draw_geometries(
        # [mesh_cloud, origin_frame], "Complete point cloud", 640, 480
        # )
        # continue

        for view_idx in range(num_views):
            print(f"View {view_idx + 1}/{num_views}")

            # Get point cloud for this viewpoint.
            camera_pose = calcLookAtPose(viewpoints[view_idx], look_at)
            cloud = scene.createPointCloudFromView(camera_pose)
            cloud = preprocessPointCloud(cloud, camera_pose)
            cloud_tree = o3d.geometry.KDTreeFlann(cloud)

            # For paper figures.
            """
            mesh = scene.createMultiViewPointCloud()
            p = HandsPlot(hand_geom)
            p.plotCloud(mesh, [1.0, 0.0, 0.0], opacity=0.3)
            p.plotCloud(cloud, [0.0, 0.0, 1.0])
            # p.setCameraPose([1.0, 0, 0], [0, 0, 1], [0, 0, 0])
            # p.setCameraPose([1.0, 0, 0.5], [0, 0, 1], [0, 0, 0])
            p.setCameraPose([0.0, -1.0, 0.5], [0, 0, 1], [0, 0, 0])
            p.show()
            """

            if plane_z == -1.0:
                sample_cloud = cloud
            else:
                horz_offset = 0.005
                horz_thresh = plane_z + horz_offset
                pts = np.asarray(cloud.points)
                pts = pts[pts[:, 2] > horz_thresh, :]
                sample_cloud = numpyToCloud(pts)
                print(f"sample_cloud: {sample_cloud}")
                distr_cloud = downsampleByDistance(sample_cloud, horz_thresh, 2)
                if len(distr_cloud.points) > num_samples:
                    sample_cloud = distr_cloud

            # Get the ground truth.
            samples = sampleFromCube(sample_cloud, cube_size, num_samples)
            labels = detector.getGroundTruth(
                mesh_cloud, samples, mesh_tree, camera_pose
            )
            num_pos = np.count_nonzero(labels)
            num_neg = np.prod(labels.shape) - num_pos
            pos_perc = num_pos / (labels.shape[0] * labels.shape[1])
            neg_perc = num_neg / (labels.shape[0] * labels.shape[1])
            print(
                f"labels: {labels.shape}, positives: {num_pos} ({pos_perc:.3f}%), "
                + f"negatives: {num_neg} ({neg_perc:.3f}%)"
            )
            # p = HandsPlot(hand_geom)
            # p.plotCloud(cloud, [1.0, 0.0, 0.0], opacity=0.5)
            # p.plotCloud(sample_cloud, [0.0, 0.0, 1.0], opacity=0.9)
            # # p.plotCloud(numpyToCloud(samples), [0.0, 0.0, 1.0], opacity=0.9)
            # p.plotCloud(mesh_cloud, [0.5, 0, 0.5], opacity=0.1)
            # p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            # p.setTitle(f"Samples")
            # p.show()

            # Detect grasp poses.
            grasp_data = detector.detectGrasps(cloud, samples, camera_pose)

            fpath = args.results_dir + "clutter_"
            fpath += "coll_" if args.check_collisions else ""
            fpath += f"{args.method}_scene_{i}_view_{view_idx}"

            if args.check_collisions:
                is_free = detector.hands_gen.generateHands(
                    cloud, samples, cloud_tree, camera_pose
                )[0][:, :, 0]
                is_free = is_free == 1
            else:
                is_free = None

            # Evaluate the detector and store the results.
            results = evaluator.evalMethod(grasp_data, labels, is_free)
            np.save(fpath, results)
            print(f"Stored evaluation results at: {fpath}.npy")


if __name__ == "__main__":
    main()

import glob
import os
import os.path as osp

import numpy as np
import open3d as o3d
import tables
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch_geometric.transforms as T
from torch.nn import BatchNorm1d as BN
from torch.nn import Linear as Lin
from torch.nn import ReLU
from torch.nn import Sequential as Seq
from torch_geometric.data import Data, DataLoader, Dataset, InMemoryDataset
from torch_geometric.datasets import ModelNet
from torch_geometric.nn import PointConv, fps, global_max_pool, radius

from pointnet2_classification import MLP, GlobalSAModule, SAModule
from pointnet2_segmentation import FPModule


class PointCloudsDataset(Dataset):
    def __init__(self, root, train=True, transform=None, pre_transform=None):
        self.train = train
        if train:
            self.mode = "train"
        else:
            self.mode = "test"
        labels_filepath = f"/{self.mode}.h5"
        self.labels_database = tables.open_file(root + labels_filepath, "r")
        self.labels = torch.from_numpy(
            np.array(self.labels_database.root.labels)
        ).float()
        # print(self.labels.shape)
        # print(self.labels.sum())
        # exit()
        super(PointCloudsDataset, self).__init__(root, transform, pre_transform)

    def read_labels(self, counter, num_points):
        # print(self.labels.shape)
        return self.labels[counter : counter + num_points]

    def process(self):
        i = 0
        label_counter = 0
        out_path = os.path.join(self.processed_dir, self.mode)
        if not os.path.isdir(out_path):
            os.mkdir(out_path)

        for raw_path in self.raw_paths:
            # Read data from `raw_path`.
            print(raw_path)
            print(self.processed_dir)
            for pcd_file in glob.glob(raw_path + self.mode + "/*.pcd"):
                cloud = o3d.io.read_point_cloud(pcd_file)
                y = self.read_labels(label_counter, len(cloud.points))
                label_counter += len(y)
                data = Data(pos=torch.from_numpy(np.asarray(cloud.points)), y=y)

                if self.pre_filter is not None and not self.pre_filter(data):
                    continue

                if self.pre_transform is not None:
                    data = self.pre_transform(data)

                torch.save(data, osp.join(out_path, "data_{}.pt".format(i)))
                i += 1
                print(" ", i, pcd_file, label_counter)

    @property
    def raw_file_names(self):
        return [""]

    @property
    def processed_file_names(self):
        return glob.glob(self.processed_dir + "/" + self.mode + "/*.pt")
        # return ['training.pt', 'test.pt']

    def len(self):
        return len(self.processed_file_names)

    def get(self, idx):
        path = osp.join(self.processed_dir, self.mode, "data_{}.pt".format(idx))
        data = torch.load(path)
        if not data.pos.dtype == torch.float32:
            data.pos = data.pos.float()
        # print(data.y.shape)
        # exit()
        # data.y = data.y[0]
        return data


class Net(torch.nn.Module):
    def __init__(self, num_classes):
        super(Net, self).__init__()
        self.sa1_module = SAModule(0.2, 0.2, MLP([3, 64, 64, 128]))
        self.sa2_module = SAModule(0.25, 0.4, MLP([128 + 3, 128, 128, 256]))
        self.sa3_module = GlobalSAModule(MLP([256 + 3, 256, 512, 1024]))

        self.fp3_module = FPModule(1, MLP([1024 + 256, 256, 256]))
        self.fp2_module = FPModule(3, MLP([256 + 128, 256, 128]))
        self.fp1_module = FPModule(3, MLP([128, 128, 128, 128]))

        self.lin1 = torch.nn.Linear(128, 128)
        self.lin2 = torch.nn.Linear(128, 128)
        self.lin3 = torch.nn.Linear(128, num_classes)

    def forward(self, data):
        # print("forward:", data)
        sa0_out = (data.x, data.pos, data.batch)
        # print("sa0_out:", [x.shape for x in sa0_out if x is not None])
        sa1_out = self.sa1_module(*sa0_out)
        sa2_out = self.sa2_module(*sa1_out)
        sa3_out = self.sa3_module(*sa2_out)

        fp3_out = self.fp3_module(*sa3_out, *sa2_out)
        fp2_out = self.fp2_module(*fp3_out, *sa1_out)
        # print("fp2_out:", [x.shape for x in fp2_out if x is not None])
        # print("sa0_out:", [x.shape for x in sa0_out if x is not None])
        # print("-----------------------------------------------------")
        x, _, _ = self.fp1_module(*fp2_out, *sa0_out)
        # print("forward:", x.shape)
        # exit()

        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin2(x)
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.lin3(x)
        return F.log_softmax(x, dim=-1)


def train(model, loader, device, criterion, optimizer):
    losses = torch.zeros(len(loader))
    model.train()

    for i, data in enumerate(loader):
        # for data in loader:
        # print(f"Batch #{i}")
        data = data.to(device)
        optimizer.zero_grad()
        # print(data)
        # pred = model(data)
        # print(f"output: {model(data).shape}, {data.y.shape}")
        loss = criterion(model(data), data.y)
        # print(f"loss: {loss}, {type(loss)}")
        loss.backward()
        optimizer.step()
        losses[i] = loss

    # print(f"losses: {losses}")
    return losses.mean()


def test(model, loader, device, thresh: float = 0.5):
    model.eval()

    correct = 0
    count = 0
    for data in loader:
        data = data.to(device)
        with torch.no_grad():
            pred = model(data)
            # print(pred[0,:5])
            pred = torch.sigmoid(pred)
            pred = (pred > thresh).long()
        y = data.y.type(pred.dtype)
        correct += pred.eq(y).sum().item()
        count += y.shape[0] * y.shape[1]
        print(f"correct: {correct}, count: {count}")
        # print(y[0,:5])
        # print(pred[0,:5])
        # print(f"[test] y: {y.shape}, pred: {pred.shape}")

        # correct += pred.eq(data.y).sum().item()
    # return correct / len(loader.dataset)
    return correct / count


if __name__ == "__main__":
    num_classes = 49 * 4
    num_epochs = 100
    batch_size = 32
    learning_rate = 0.001

    path = osp.join(osp.dirname(osp.realpath(__file__)), "..", "data/ModelNet10")
    pre_transform, transform = T.NormalizeScale(), T.SamplePoints(1024)
    # train_dataset = ModelNet(path, '10', True, transform, pre_transform)
    path = "../data/one_stage_antip/bottles"
    train_dataset = PointCloudsDataset("../data/one_stage_antip/bottles", True)
    test_dataset = PointCloudsDataset("../data/one_stage_antip/bottles", False)
    # train_dataset = PointCloudsDataset(path, True, transform, pre_transform)
    # test_dataset = PointCloudsDataset(path, False, transform, pre_transform)
    train_loader = DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True, num_workers=6
    )
    test_loader = DataLoader(
        test_dataset, batch_size=batch_size, shuffle=False, num_workers=6
    )
    # print("ds[0]:", train_dataset[0])
    # print("ds[0]:", train_dataset[0].pos.dtype)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model = Net(num_classes).to(device)
    print(model)
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
    # loss = nn.BCEWithLogitsLoss(pos_weight=pos_weight.to(device))
    # criterion = nn.BCEWithLogitsLoss()
    criterion = nn.BCEWithLogitsLoss(pos_weight=2.0 * torch.ones(num_classes).to(device))

    # Test performance before training the model.
    test_acc = test(model, test_loader, device)
    print("Epoch: 0, Test: {:.4f}".format(test_acc))

    for epoch in range(1, num_epochs):
        train_loss = train(model, train_loader, device, criterion, optimizer)
        test_acc = test(model, test_loader, device)
        print(
            "Epoch: {:03d}, Train loss: {:.4f}, Test accuracy: {:.4f}".format(
                epoch, train_loss, test_acc
            )
        )

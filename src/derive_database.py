"""Create a new database with labels calculated from a given database.

Take a HDF5 database as input and output a new HDF5 database using pytables and
blosc compression. The new database contains the same datasets as the given
database, but calculates new, binary labels based on ground truth conditions
which evaluate if a grasp is collision-free, antipodal, has a minimum number
of points in between the fingers, and has no more shift relative to the
center of the points than a threshold.

"""

import os
import sys

import numpy as np
import psutil
import tables as tab

from ground_truth import findGroundTruthPositives
from utils.db_utils import calcMaxInMemory

os.environ["BLOSC_NTHREADS"] = "4"

# filters = tab.Filters(complevel=5, complib='blosc')
# filters = tab.Filters(complevel=9, complib='blosc')
filters = tab.Filters(complevel=9, complib="blosc:lz4")

# Small filesize, fast compression, and very fast decompression.
# filters = tab.Filters(complevel=9, complib='blosc:lz4hc')

# Has slow decompression.
# filters = tab.Filters(complevel=9, complib='blosc:zstd')

if len(sys.argv) < 2:
    print("Error: not enough input arguments.")
    print("Usage: python derive_database.py HDF5_IN [KEYS] [HDF5_OUT]")
    print("KEYS can be one of: gen_images, gc_images")
    exit(-1)

keys = []
fname_in = sys.argv[1]

if len(sys.argv) > 2:
    if sys.argv[2] == "gen_images":
        keys = ["gen_images", "labels"]
    elif sys.argv[2] == "gc_images":
        keys = ["gc_images", "labels"]
    elif sys.argv[2] == "images":
        keys = ["images", "labels"]
else:
    keys = ["gen_images", "gc_images", "labels"]
print(f"Nodes to be copied: {keys}")

if len(sys.argv) == 4:
    fname_out = sys.argv[3]
else:
    fname_out = fname_in[: fname_in.rfind(".")] + "_derived.h5"

with tab.open_file(fname_in, "r") as fin, tab.open_file(fname_out, "w") as fout:
    print("Loaded file", fname_in)
    Y = fin.root.labels
    print("Loaded labels with shape", Y.shape)

    mask = findGroundTruthPositives(Y)
    positives = np.nonzero(mask)
    if Y.ndim == 2:
        labels_out = np.zeros(len(Y))
        labels_out[positives[0]] = 1
    elif Y.ndim == 3:
        labels_out = np.zeros(Y.shape[:2])
        labels_out[positives[0], positives[1]] = 1

    print("Storing new database as:", fname_out)
    n = fin.list_nodes(fin.root)[0].shape[0]
    # max_in_memory = calcMaxInMemory(fin.root.cls_images)
    if "gc_images" in keys:
        max_in_memory = 10000
    else:
        max_in_memory = 100000
    blocks = list(np.arange(0, n, max_in_memory)) + [n]
    print("blocks:", blocks)

    root = fin.root
    nodes = [n for n in fin.list_nodes(fin.root) if n.name in keys]
    # nodes = [root.labels, root.gen_images, root.cls_images]
    # nodes = [root.labels, root.images]
    print(nodes)
    chunk_shapes = {}

    for n in nodes:
        if n.name == "labels":
            dset = fout.create_carray(
                "/", n.name, obj=np.asarray(labels_out), filters=filters
            )
            print(f"Created dataset: {dset}, {dset.dtype}, {n.dtype}")
            continue

        chunk_shape = (10, labels_out.shape[1]) + n.shape[2:]
        dset = fout.create_carray(
            "/",
            n.name,
            tab.UInt8Atom(),
            shape=n.shape,
            chunkshape=chunk_shape,
            filters=filters,
        )
        print("chunk_shape:", chunk_shape)
        print(f"Created dataset: {dset}, {dset.dtype}, {n.dtype}")

        for i in range(1, len(blocks)):
            print(
                f"({i}/{len(blocks)-1}) Copying block: "
                + f"[{blocks[i-1]}, {blocks[i]}]"
            )
            dset[blocks[i - 1] : blocks[i]] = n[blocks[i - 1] : blocks[i]]

"""Evaluate a network model on a database.

Usage: python test_model.py DATABASE ARCHITECTURE PARAMS_FILE

The evaluation is done by calculating precision and recall of the model at different 
thresholds. The precision-recall curve is plotted and the plot is stored in a file.

"""

import sys
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils import data as torchdata

from datasets import BasicDataset
from method_evaluator import evalClassifier
from networks.network import LenetLikeWithSigmoid, VggLikeWithSigmoid
from networks.resnet_mod import resnet18, resnet34

loader_params = {
    "batch_size": 512,
    "shuffle": False,
    "pin_memory": True,
    "num_workers": 8,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}

lenet_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    # Params for Lenet
    # """ 1FC
    # "conv": [32, 64],
    # "fc": [512],
    # """
    # """ 2FC
    # "conv": [32, 64],
    # "fc": [512, 512],
    # """
    # double conv
    "conv": [64, 128],
    "fc": [512, 512],
    # "fc": [500],
    # "fc": [32],
    # "fc": [512, 512],
    "conv_filter_size": 5,
}

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}

net_params_dict = {
    "lenet": lenet_params,
    "vgg": vgg_params,
    "resnet18": {},
    "resnet34": {},
}
networks_dict = {
    "lenet": LenetLikeWithSigmoid,
    "vgg": VggLikeWithSigmoid,
    "resnet18": resnet18,
    "resnet34": resnet34,
}


def runModel(loader, model, device):
    outputs_list, labels_list = [], []

    with torch.no_grad():
        for i, data in enumerate(loader):
            inputs, labels = data[0].to(device), data[1].to(device)
            outputs = model(inputs)
            outputs_list.append(outputs.flatten())
            labels_list.append(labels.flatten())
            print(i, labels.shape, outputs.shape)

    return labels_list, outputs_list


def main() -> None:
    # Read command line arguments.
    path = sys.argv[1]
    architecture = sys.argv[2]
    params_file = sys.argv[3]
    task = sys.argv[4]

    num_classes = 2 if task == "cls" else -1

    # Load the data.
    net_params = net_params_dict[architecture]
    dset = BasicDataset(path, num_classes)
    loader = torchdata.DataLoader(dset, **loader_params)
    imgs_shape = dset.images.shape[-3:]
    net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    if dset.labels.ndim == 1:
        net_params["out_shape"] = 2
    elif dset.labels.ndim == 2:
        net_params["out_shape"] = dset.labels.shape[1]
    print(net_params)

    # Setup the network model.
    if "resnet" in architecture:
        in_channels = net_params["in_shape"][0]
        num_classes = net_params["out_shape"]
        model = networks_dict[architecture](
            in_channels=in_channels, num_classes=num_classes
        )
    else:
        model = networks_dict[architecture](**net_params)
    checkpoint = torch.load(params_file)
    if type(checkpoint) == dict:
        model.load_state_dict(checkpoint["model_state_dict"], strict=False)
    else:
        model.load_state_dict(checkpoint)
    model.eval()
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    model = model.to(device)
    print(f"Network:\n{model}")

    # Run the model on the complete dataset.
    labels, outputs = runModel(loader, model, device)
    print(len(labels), len(outputs))
    print(type(labels[0]), type(outputs[0]))

    # Evaluate the classifier at different thresholds.
    labels = torch.cat(labels).cpu().numpy()
    outputs = torch.cat(outputs).cpu().numpy()
    print(labels.shape, outputs.shape)
    # thresh_step = 0.05
    thresh_step = 0.025
    # threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[1:-1]
    threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)
    print(threshs)
    metrics_list = evalClassifier(labels, outputs, threshs, False)
    print(outputs)

    # Plot and save the precision-recall curve.
    print("Calculating precision and recall ...")
    recall = [m.recall() for m in metrics_list]
    precision = [m.precision() for m in metrics_list]
    print("recall:", recall)
    print("precision:", precision)
    plt.figure()
    plt.plot(recall, precision, "-x")
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([-0.05, 1.05])
    plt.xlim([-0.05, 1.0])
    timestamp = datetime.now()
    out_path = f"/tmp/precision-recall-{architecture}-{task}-{timestamp}.png"
    plt.savefig(out_path, format="png")
    print("Stored precision-recall plot at: " + out_path)

    arr = np.vstack((precision, recall))
    out_path = f"/tmp/precision-recall-{architecture}-{task}-{timestamp}.npy"
    np.save(out_path, arr)
    print("Stored precision-recall numpy array at: " + out_path)


if __name__ == "__main__":
    main()

# Calculate ground truth grasps from geometric information 5-tuples.
# Conditions for a ground truth grasp are:
#  - collision-free,
#  - antipodal,
#  - occupancy,
#  - less than <max_shift> shift.

import numpy as np

from hand_params import hand_params

# Conditions for a ground truth positive
min_pts_in_closing_region = 10
max_shift = 0.02

inner_left = -0.5 * hand_params["outer_diameter"] + hand_params["finger_width"]
inner_right = 0.5 * hand_params["outer_diameter"] - hand_params["finger_width"]

criteria = ["collision-free", "antipodal", "occupied", "shift", "positive"]


def calcScore(label):
    occupancy = label[2] - min_pts_in_closing_region
    occupancy_score = np.min([1.0, occupancy / 500])

    dist_left = abs(inner_left - label[3])
    dist_right = abs(inner_right - label[4])
    dist = abs(dist_left - dist_right)
    shift_score = (max_shift - dist) / max_shift

    score = 0.3 * occupancy_score + 0.7 * shift_score

    print(f"  occupancy: {occupancy}, occupancy_score: {occupancy_score:.3f}")
    print(f"  dist: {dist:.3f}, shift_score: {shift_score:.3f} ")
    print(f"total: {score:.3f}")

    return score


def findGroundTruthPositives(labels):
    if labels.ndim == 2:
        total = len(labels)
        free = labels[:, 0] == 1
        antipodal = labels[:, 1] == 1
        occupied = labels[:, 2] >= min_pts_in_closing_region
        dist_left = abs(inner_left - labels[:, 3])
        dist_right = abs(inner_right - labels[:, 4])
    elif labels.ndim == 3:
        total = np.prod(labels.shape[:2])
        free = labels[:, :, 0] == 1
        antipodal = labels[:, :, 1] == 1
        occupied = labels[:, :, 2] >= min_pts_in_closing_region
        dist_left = abs(inner_left - labels[:, :, 3])
        dist_right = abs(inner_right - labels[:, :, 4])

    no_shift = abs(dist_left - dist_right) <= max_shift

    # Ground truth positives are grasps that satisfy the above conditions.
    # mask = free
    # mask = free & antipodal
    mask = free & antipodal & occupied & no_shift

    printInfo([free, antipodal, occupied, no_shift, mask], total)

    return mask


def printInfo(info, total):
    for i in range(len(info)):
        n = len(np.nonzero(info[i])[0])
        percentage = float(n) / total
        print(f"{criteria[i]}: {n:7d}/{total:d} ({percentage:.3f})")

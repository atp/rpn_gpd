"""Remove ows from the labels node in a database with not enough positives.
"""

import os
import sys

import numpy as np
import tables as tab

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=9, complib="blosc:lz4")

np.set_printoptions(precision=3)

if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print("Usage: python remove_rows.py DB_IN DB_OUT MIN_POSITIVES")
    exit(-1)

with tab.open_file(sys.argv[1], "r") as f:
    Y = np.asarray(f.root.labels)
    if Y.ndim != 2:
        print(f"Error: <labels> node has the wrong dimensions: {Y.shape}")
        exit(-1)

    # Find positive and negative rows.
    min_positives = int(sys.argv[3])
    pos_mask = np.sum(Y, 1) >= min_positives
    positive_rows = np.count_nonzero(pos_mask)
    negative_rows = np.count_nonzero(np.sum(Y, 1) < min_positives)
    ratio = positive_rows / float(negative_rows)
    print(
        f"rows: {Y.shape[0]}, positives: {positive_rows}, "
        + f"negatives: {negative_rows}, ratio (pos/neg): {ratio:.3f}"
        + f"\n positives are rows with more than {min_positives} positives"
    )
    idxs = np.nonzero(pos_mask)[0]

    # Keep only the positive rows.
    labels_out = Y[idxs]
    images = np.asarray(f.root.images)
    images_out = images[idxs]

    # Store the positive rows in a new database.
    with tab.open_file(sys.argv[2], "w") as fout:
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(f"Created dataset: {labels_dset}")
        images_dset = fout.create_carray(
            "/", "images", tab.UInt8Atom(), obj=images_out, filters=filters
        )
        print(f"Created dataset: {images_dset}")

import json
import sys

import numpy as np
import open3d as o3d

np.set_printoptions(precision=4, suppress=True)

source_cloud = o3d.io.read_point_cloud(sys.argv[1])
target_cloud = o3d.io.read_point_cloud(sys.argv[2])

source_pose_path = sys.argv[3]
target_pose_path = sys.argv[4]
with open(source_pose_path, "r") as f:
    X = np.array(json.load(f)["camera_depth_optical_frame2base"]).reshape((4,4))
    print(X)
    source_cloud = source_cloud.transform(X)

with open(target_pose_path, "r") as f:
    X = np.array(json.load(f)["camera_depth_optical_frame2base"]).reshape((4,4))
    print(X)
    target_cloud = target_cloud.transform(X)

cloud = source_cloud + target_cloud
o3d.visualization.draw_geometries(
    [source_cloud, target_cloud], "Samples", 640, 480
)

if len(sys.argv) == 6:
    o3d.io.write_point_cloud(sys.argv[5], cloud)

# Collect data about meshes in a dataset.

import csv
import glob
import os
import sys

import numpy as np
import trimesh

folder = sys.argv[1]
dirs = os.listdir(folder)
dirs.sort()
print(dirs)
info = []
categories = []

for f in dirs:
    if os.path.isdir(folder + f):
        print("Processing category:", f)
        ply_files = glob.glob(folder + f + "/*.ply")
        categories.append([f, len(ply_files)])
        for ply_file in ply_files:
            tmesh = trimesh.load(ply_file)
            fname = ply_file[ply_file.rfind("/") + 1 :]
            info.append([fname, f, tmesh.extents])

csv_file = open("categories.csv", "w")
fieldnames = ["Category", "No. of instances"]
writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
writer.writeheader()
for x in categories:
    writer.writerow({"Category": x[0], "No. of instances": x[1]})

csv_file = open("extents.csv", "w")
# fieldnames = ['Category', 'Instance', 'Extents']
fieldnames = ["Category", "Instance", "X-Extent", "Y-Extent", "Z-Extent"]
writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
writer.writeheader()
for x in info:
    #    writer.writerow({'Category': x[1], 'Instance': x[0], 'Extents': x[2]})
    writer.writerow(
        {
            "Category": x[1],
            "Instance": x[0],
            "X-Extent": x[2][0],
            "Y-Extent": x[2][1],
            "Z-Extent": x[2][2],
        }
    )

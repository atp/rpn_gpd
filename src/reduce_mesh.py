import sys

import numpy as np
import trimesh as tri

np.set_printoptions(suppress=True, precision=3)

path = sys.argv[1]
mesh = tri.load(path)

idx = np.argmax(mesh.extents)
normal = np.zeros(3)
normal[idx] = 1.0
print(f"Loaded mesh: {mesh}")
print(f"Loaded mesh with extents: {mesh.extents}")
print(f"Largest dimension: {idx}:, normal: {normal}")

top = mesh.slice_plane(mesh.centroid, -normal)
bottom = mesh.slice_plane(mesh.centroid, normal)
print("bottom:", bottom.extents)

# top.show()
# bottom.show()

off = np.zeros(3)
off[idx] = 0.8 * bottom.extents[idx]
print("off:", off)
s = bottom.slice_plane(off, normal)
print("s:", s.extents)
# s.show()

off = np.zeros(3)
off[idx] = 0.5 * bottom.extents[idx]
print("off:", off)
t = bottom.slice_plane(off, -normal)
print("t:", t.extents)
# t.show()

# input("?")
combined = tri.util.concatenate([top, t])
print("combined:", combined)
print("combined:", combined.extents)
combined.show()

print(combined.centroid)
print(combined.bounds)
back = combined.bounds[0, idx]
print(combined.vertices)
print(back)
print(combined.bounds)
corner_idx = np.argmin(combined.vertices[:, idx])
print(corner_idx)

vertices = combined.vertices.view(np.ndarray)
dists = np.abs(vertices[:, idx] - vertices[corner_idx, idx])
closest = np.argsort(dists)[:3]
closest = [x for x in closest if x != corner_idx]
print("closest:", closest)

faces = combined.faces.view(np.ndarray)
new_face = np.array([corner_idx, closest[0], closest[1]])
print("new_face:", new_face)

faces = np.vstack((faces, new_face))
combined.faces = faces
combined.show()

combined.export("test.obj")

exit()

print(t.centroid[idx], s.centroid[idx])
dist = t.bounds[0, idx] - s.bounds[1, idx]
print("dist:", dist)
# dist = t.centroid[idx] - t.bounds[0, idx]
dist = t.centroid[idx] - s.centroid[idx]
print(t.bounds)
print(s.bounds)
print("dist:", dist)
X = np.eye(4)
X[idx, 3] = s.centroid[idx] + 2 * dist
s.apply_transform(X)
print(t.centroid[idx], s.centroid[idx])
combined = tri.util.concatenate([top, s, t])
print("combined:", combined)
print("combined:", combined.extents)
combined.show()
exit()

combined = tri.util.concatenate([top, s, t])
print("combined:", combined)
print("combined:", combined.extents)
combined.show()

exit()

for i in range(2):
    s = s.slice_plane(s.centroid, normal)
    print("s:", s, s.extents)

# for i in range(5):
# dist = top.centroid[idx] - s.centroid[idx]
dist = top.bounds[0, idx] - s.bounds[0, idx]
print("dist:", dist)
X = np.eye(4)
# X = tri.transformations.random_rotation_matrix()
X[idx, 3] = s.centroid[idx] + dist
s.apply_transform(X)
combined = tri.util.concatenate([s, top])
print("combined:", combined)
print("combined:", combined.extents)
print(s.centroid)
combined.show()


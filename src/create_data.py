"""Create a database of grasp data.

Create a database of grasp data for an object set such as 3DNet.

"""

import os

os.environ["PYOPENGL_PLATFORM"] = "egl"
# os.environ['PYOPENGL_PLATFORM'] = 'osmesa'

# import os
# os.environ["PYOPENGL_PLATFORM"] = "egl" #opengl seems to only work with TPU
# !PYOPENGL_PLATFORM=egl python -c "from OpenGL import EGL"
# print(os.environ['PYOPENGL_PLATFORM'])

import csv
import os
from argparse import ArgumentParser
from datetime import datetime
from time import time

import numpy as np
import tables as tab

from data_generator import DataGenerator
from databases import AnchorsDatabase, CombinedDatabase, ImagesDatabase
from grasp_data_generator import (
    GraspDataGeneratorCombined,
    GraspDataGeneratorGPD,
    GraspDataGeneratorMA,
    GraspDataGeneratorMAGrasp,
    GraspDataGeneratorMAProp,
)
from hand_params import hand_params
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.qd_hands_generator import QdHandsGenerator
from object_set import ThreeDNetObjectSet

os.environ["PYOPENGL_PLATFORM"] = "egl"  # opengl seems to only work with TPU

np.set_printoptions(precision=4)

antipodal_modes = {
    "onestage": {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": False,
    },
    "onestage_strict": {
        "extremal_thresh": 0.003,
        "friction_coeff": 20,
        "min_viable": 6,
        "checks_overlap": False,
    },
    "twostage": {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": True,
    },
    # "less1": {"extremal_thresh": 0.006, "friction_coeff": 40, "min_viable": 6},
    # "less2": {"extremal_thresh": 0.006, "friction_coeff": 60, "min_viable": 6},
    # "less3": {"extremal_thresh": 0.006, "friction_coeff": 30, "min_viable": 6},
    # "bowl": {"extremal_thresh": 0.006, "friction_coeff": 20, "min_viable": 6},
}

params = {}
params["debug"] = dict(
    num_instances=1,
    num_train_views=1,
    num_test_views=1,
    num_samples=50,
    camera_distance=0.5,
    min_object_extent=0.01,
    max_object_extent=[0.07, 0.7],
    cube_size=1.0,
)
params["run"] = dict(
    num_instances=10,
    num_train_views=20,
    num_test_views=5,
    num_samples=100,
    camera_distance=0.5,
    min_object_extent=0.01,
    max_object_extent=[0.07, 0.7],
    cube_size=1.0,
)
params["custom"] = dict(
    num_instances=1,
    num_train_views=2,
    num_test_views=5,
    num_samples=100,
    camera_distance=0.5,
    min_object_extent=0.01,
    max_object_extent=[0.07, 0.7],
    cube_size=1.0,
)

prop_params = {"image_size": 60, "image_type": "occupancy"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], -1]
grasp_img_shape = [60, 60, -1]


def storeCategoryInfo(filepath, info, fieldnames):
    """Store info about objects in a CSV file.

    Args:
        filepath (str): the path to the CSV file.
        info (list): dictionaries with object information.
        fieldnames (list): strings with descriptions of the object information.
    """
    with open(filepath, "w") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for x in info:
            writer.writerow(x)


def createGraspDataGenerator(generator, prop_gen=None):
    """Create a grasp data generator.

    Args:
        hand_generator (str): the desired hand generator.
        prop_gen (HandsGenerator, optional): the hands generator.

    Returns:
        GraspDataGenerator: the grasp data generator.
    """
    generators = {
        "gpd": GraspDataGeneratorGPD,
        "ma": GraspDataGeneratorMA,
        "prop": GraspDataGeneratorMAProp,
        "grasp": GraspDataGeneratorMAGrasp,
    }
    generators["qd"] = generators["ma"]
    generators["gqd"] = generators["ma"]
    args = {
        "gpd": {
            "grasp_img_shape": grasp_img_shape,
            "num_subsamples": params["num_samples"],
        },
        "ma": {
            "prop_params": prop_params,
            "grasp_img_shape": grasp_img_shape,
            "use_open_gl_frame": True,
            "prop_gen": prop_gen,
        },
        "prop": {
            "prop_params": prop_params,
            "use_open_gl_frame": True,
            "prop_gen": prop_gen,
        },
        "grasp": {
            "grasp_img_shape": grasp_img_shape,
            "num_subsamples": params["num_samples"],
            "use_open_gl_frame": True,
            "prop_gen": prop_gen,
        },
    }
    args["qd"] = args["ma"]
    args["gqd"] = args["ma"]

    if generator == "combined":
        gpd_gen = GraspDataGeneratorGPD(grasp_img_shape)
        ma_gen = GraspDataGeneratorMA(
            prop_params, grasp_img_shape, use_open_gl_frame=True, prop_gen=prop_gen
        )
        grasp_data_gen = GraspDataGeneratorCombined(gpd_gen, ma_gen)
    else:
        grasp_data_gen = generators[generator](**args[generator])

    return grasp_data_gen


def createDatabases(generator, grasp_data_gen, path, num_categories, first, last):
    """Create training and test databases.

    Args:
        generator (str): the complete name of the grasp generator (e.g., qd-prop).
        grasp_data_gen (GraspGenerator): the grasp data generator.
    """
    num_criteria = 5
    suffix = f"_{generator}_{first}_{last}.h5"
    trainpath = path + "train" + suffix
    testpath = path + "test" + suffix
    if params["num_samples"] == 0:
        max_samples = 1000
        print(
            f"WARNING: <num_samples> is zero. Using {max_samples} to set initial database size."
        )
        n = num_categories * params["num_instances"] * max_samples
    else:
        n = num_categories * params["num_instances"] * params["num_samples"]
    trainrows = n * params["num_train_views"]
    testrows = n * params["num_test_views"]
    if generator == "gpd":
        train_db = ImagesDatabase(trainpath, (trainrows, num_criteria), grasp_img_shape)
        test_db = ImagesDatabase(testpath, (testrows, num_criteria), grasp_img_shape)
    elif generator in ["ma", "qd", "gqd"]:
        cols = grasp_data_gen.prop_gen.getNumHandsPerSample()
        prop_img_shape[-1] = grasp_data_gen.prop_img_gen.num_channels
        train_db = AnchorsDatabase(
            trainpath,
            (trainrows, cols, num_criteria),
            tuple(prop_img_shape),
            grasp_img_shape,
        )
        test_db = AnchorsDatabase(
            testpath,
            (testrows, cols, num_criteria),
            tuple(prop_img_shape),
            grasp_img_shape,
        )
    elif "prop" in generator:
        cols = grasp_data_gen.prop_gen.getNumHandsPerSample()
        prop_img_shape[-1] = grasp_data_gen.prop_img_gen.num_channels
        train_db = ImagesDatabase(
            trainpath, (trainrows, cols, num_criteria), tuple(prop_img_shape)
        )
        test_db = ImagesDatabase(
            testpath, (testrows, cols, num_criteria), tuple(prop_img_shape)
        )
    elif "grasp" in generator:
        train_db = ImagesDatabase(trainpath, (trainrows, num_criteria), grasp_img_shape)
        test_db = ImagesDatabase(testpath, (testrows, num_criteria), grasp_img_shape)
    elif generator == "combined":
        gpd_cols = grasp_data_gen.gpd_gen.prop_gen.getNumHandsPerSample()
        gpd_cfgs = {
            "labels": {"dtype": tab.Float32Atom(), "shape": (gpd_cols, num_criteria)},
            "proposals": {"dtype": tab.UInt8Atom(), "shape": (gpd_cols,)},
            "images": {
                "dtype": tab.UInt8Atom(),
                "shape": (gpd_cols,) + grasp_img_shape,
            },
        }
        ma_cols = grasp_data_gen.ma_gen.prop_gen.getNumHandsPerSample()
        prop_img_shape[-1] = grasp_data_gen.ma_gen.prop_img_gen.num_channels
        ma_cfgs = {
            "labels": {"dtype": tab.Float32Atom(), "shape": (ma_cols, num_criteria)},
            "prop_imgs": {"dtype": tab.UInt8Atom(), "shape": tuple(prop_img_shape)},
            "grasp_imgs": {
                "dtype": tab.UInt8Atom(),
                "shape": (ma_cols,) + grasp_img_shape,
            },
        }
        train_db = CombinedDatabase(trainpath, trainrows, gpd_cfgs, ma_cfgs)
        test_db = CombinedDatabase(testpath, testrows, gpd_cfgs, ma_cfgs)

    return train_db, test_db


def parseArgs():
    """Parse command line arguments."""
    parser = ArgumentParser(description="Create images and labels for learning")
    parser.add_argument(
        "categories",
        metavar="CATEGORIES_TXT",
        help="path to TXT file with object categories",
    )
    parser.add_argument(
        "out_dir",
        metavar="OUT_DIR",
        help="path to output directory for the created data",
    )
    parser.add_argument(
        "generator", metavar="GENERATOR", help="name of ground truth generator"
    )
    parser.add_argument(
        "-f",
        "--first",
        default=0,
        type=int,
        help="first category to be processed",
    )
    parser.add_argument(
        "-l",
        "--last",
        default=-1,
        type=int,
        help="last category to be processed",
    )
    parser.add_argument(
        "-p", "--prohibited", help="path to CSV file with prohibited object instances"
    )
    parser.add_argument("-s", "--seed", default=0, type=int, help="random seed")
    parser.add_argument(
        "-m",
        "--mode",
        default="run",
        help="set to <debug>, <run> or <none>",
    )
    parser.add_argument(
        "-i",
        "--in_dir",
        default="/home/andreas/data/Cat200_ModelDatabase/",
        help="path to object set",
    )
    parser.add_argument(
        "-n", "--num_samples", default=200, type=int, help="number of samples"
    )
    parser.add_argument(
        "-ntrain",
        "--num_train",
        default=20,
        type=int,
        help="number of training viewpoints",
    )
    parser.add_argument(
        "-ntest", "--num_test", default=5, type=int, help="number of test viewpoints"
    )
    parser.add_argument(
        "-ni",
        "--num_instances",
        default=10,
        type=int,
        help="number of object instances per category",
    )
    parser.add_argument(
        "-pi", "--prop_img_type", default="depth", help="proposal image type"
    )
    parser.add_argument(
        "-gi",
        "--grasp_img_channels",
        type=int,
        default=12,
        help="number of grasp image channels",
    )
    parser.add_argument(
        "-am",
        "--antipodal_mode",
        default="onestage",
        help="antipodal mode: <onestage>, <twostage>",
    )
    parser.add_argument(
        "-ap", "--add_plane", action="store_true", help="add support plane to object"
    )
    parser.add_argument(
        "-rp", "--rotate_in_plane", action="store_true", help="rotate object in plane"
    )
    parser.add_argument(
        "-cc", "--create_clutter", action="store_true", help="create clutter scenes"
    )
    parser.add_argument(
        "--store_clouds", action="store_true", help="store view point clouds"
    )
    parser.add_argument(
        "-db", "--database", help="path to database with labels", default=""
    )
    parser.add_argument(
        "-2v",
        "--use_two_views",
        action="store_true",
        help="use two viewpoints per point cloud",
        default="",
    )
    args = parser.parse_args()

    return args


def main():
    global grasp_img_shape, params
    # Read command line arguments.
    args = parseArgs()
    print(f"Command line arguments:\n{args}")
    prop_params["image_type"] = args.prop_img_type
    params = params[args.mode]
    if args.mode == "custom":
        params["num_samples"] = args.num_samples
        params["num_instances"] = args.num_instances
        params["num_train_views"] = args.num_train
        params["num_test_views"] = args.num_test
    params["add_plane"] = args.add_plane
    params["rotate_in_plane"] = args.rotate_in_plane
    params["create_clutter"] = args.create_clutter
    params["database"] = args.database
    params["use_two_views"] = args.use_two_views
    params["out_dir"] = args.out_dir
    params["store_clouds"] = args.store_clouds

    grasp_img_shape[-1] = args.grasp_img_channels
    grasp_img_shape = tuple(grasp_img_shape)

    # Ensure that the same sequence of random numbers is generated.
    np.random.seed(args.seed)

    # Load the object set.
    object_set = ThreeDNetObjectSet(args.in_dir, args.categories, rescale=True)
    print("Available object categories:\n", object_set.objects)
    if args.last == -1:
        args.last = len(object_set) - 1

    # Read in the object categories to be processed.
    num_categories = args.last - args.first + 1
    print(
        f"Processing categories from {args.first} to {args.last}: {[object_set.objects[x] for x in range(args.first, args.last + 1)]}"
    )

    # Read in CSV that tells which object instances are prohibited for each category.
    prohibited = []
    if args.prohibited != None:
        with open(args.prohibited, "r") as f:
            prohibited = [
                r[:2] for r in csv.reader(f, delimiter=",") if r[0] != "category"
            ]
        print("Prohibited object instances:\n", prohibited)
        object_set.setProhibitedObjectInstances(prohibited)

    # Create the grasp proposal generator.
    idx = args.generator.find("-")
    method = args.generator[:idx] if idx >= 0 else args.generator
    component = args.generator[idx + 1 :] if idx >= 0 else args.generator
    print(method, component)
    if method in ["qd", "gqd"]:
        prop_gens = {"qd": QdHandsGenerator, "gqd": GqdHandsGenerator}
        hand_geom = HandGeometry(**hand_params)
        antipodal_params = antipodal_modes[args.antipodal_mode]
        antip_params = AntipodalParams(**antipodal_params)
        prop_gen = prop_gens[method](hand_geom, antip_params)
        print(f"Using hand proposal generator {prop_gen}")
        print(f"Using antipodal_mode <{args.antipodal_mode}>: {antipodal_params}")
    else:
        prop_gen = None

    # Create the grasp data generator.
    grasp_data_gen = createGraspDataGenerator(component, prop_gen)

    # Create the training and test databases.
    train_db, test_db = createDatabases(
        args.generator,
        grasp_data_gen,
        args.out_dir,
        num_categories,
        args.first,
        args.last,
    )
    print("Training database:\n" + str(train_db))
    print("Test database:\n" + str(test_db))

    # Create the data generator that fills the databases with grasp data.
    data_gen = DataGenerator(object_set, train_db, test_db, grasp_data_gen, params)

    # Create the data for each object category.
    cat_infos = []
    for i in range(args.first, args.last + 1):
        print(
            f"[{datetime.now()}] ({i + 1 - args.first }/{num_categories}), "
            + f"category: {object_set.objects[i]}"
        )
        cat_time = time()
        info = data_gen.createCategoryData(i)
        cat_time = time() - cat_time
        remain_time = cat_time * (args.last - i)
        cat_infos += info
        print(f"Runtime (category): {cat_time:.3f}s")
        print(f"Estimated remaining time: {remain_time:.3f}s ({remain_time/3600:.3f}h)")

    # Shrink the databases such that there are no empty rows.
    train_db.shrink()
    test_db.shrink()

    # Replace the grasp geometry information with binary labels.
    train_db.deriveBinaryLabels()
    test_db.deriveBinaryLabels()
    print(f"Derived binary labels for {train_db.filepath} and {test_db.filepath}")

    # Store info about objects: category, instance, scale, extent..
    filepath = args.out_dir + "category_info_%d_%d.csv" % (args.first, args.last)
    storeCategoryInfo(filepath, cat_infos, cat_infos[0].keys())
    print(f"Stored object information at {filepath}")


if __name__ == "__main__":
    main()

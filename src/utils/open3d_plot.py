"""Plot functions for open3d.
"""

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d

from .cloud_utils import numpyToCloud


def plotPointsSubset(pts, idxs, title):
    cloud = numpyToCloud(pts)
    plotCloudSubset(cloud, idxs, title)


def plotCloudSubsets(cloud, idxs_list, title):
    colors_array = [[1.0, 0.5, 0], [1.0, 0, 0]]
    cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
    colors = np.asarray(cloud.colors)
    for i, idxs in enumerate(idxs_list):
        colors[idxs] = colors_array[i]
    cloud.colors = o3d.utility.Vector3dVector(colors)
    frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.01)
    o3d.visualization.draw_geometries([cloud, frame], title, 640, 480)


def plotCloudSubset(cloud, idxs, title):
    cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
    colors = np.asarray(cloud.colors)
    colors[idxs] = [1.0, 0.0, 0.0]
    cloud.colors = o3d.utility.Vector3dVector(colors)
    frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.01)
    o3d.visualization.draw_geometries([cloud, frame], title, 640, 480)


def plotFrame(cloud, frame, title):
    cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
    coord_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.01)
    coord_frame.transform(frame)
    o3d.visualization.draw_geometries([cloud, coord_frame], title, 640, 480)


def plotPointSets(sets, title):
    colors = np.array([[0, 0, 1], [1, 0, 0], [0, 1, 0]])
    clouds = [numpyToCloud(s) for s in sets]
    for c in clouds:
        c.paint_uniform_color(colors)
    o3d.visualization.draw_geometries(clouds, title, 640, 480)


def plotSamples(cloud, samples, title="samples"):
    samples_cloud = numpyToCloud(samples)
    samples_cloud.paint_uniform_color(np.array([1.0, 0.0, 0.0]))
    cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
    o3d.visualization.draw_geometries([cloud, samples_cloud], title, 640, 480)


def plotSamplesAsBoxes(cloud, samples, title="samples", scale=0.002,
                       color=[1.0, 0.0, 0.0]):
    boxes = []
    for x in samples:
        box = o3d.geometry.TriangleMesh.create_box(scale, scale, scale)
        box.translate(x)
        box.paint_uniform_color(color)
        boxes.append(box)
    cloud.paint_uniform_color([0, 0, 1])
    o3d.visualization.draw_geometries([cloud] + boxes, title, 640, 480)

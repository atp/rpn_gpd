import cv2
import matplotlib.pyplot as plt
import numpy as np


def plotImageChannels(images, title="", cmap="gray"):
    rows = len(images)
    cols = 3
    fig = plt.figure(figsize=(8, 8))

    for i in range(rows):
        for j in range(3):
            img = images[i][:, :, j]
            if img.dtype == np.float:
                img = cv2.normalize(
                    img, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
                )
                img = np.uint8(img * 255)
            ax = fig.add_subplot(rows, cols, i * cols + j + 1)
            ax.axis("off")
            plt.imshow(np.asarray(img), cmap=cmap, vmin=0, vmax=255)

    fig.suptitle(title)
    plt.show()

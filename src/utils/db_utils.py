import matplotlib.pyplot as plt
import numpy as np
import psutil
import torch


# Calculate how many elements to put into memory.
def calcMaxInMemory(mat):
    shape = mat.shape
    available_mem = psutil.virtual_memory().free
    print(f"available memory: {available_mem/2**20}MB")
    item_size = np.prod(shape[1:]) * np.uint8().nbytes
    max_in_memory = np.round(0.8 * available_mem / item_size, -4)
    max_in_memory = int(max_in_memory) - 10000
    print(f"max_in_memory: {max_in_memory}, item_size: {item_size}")

    return max_in_memory


def plotBatch(images, labels, pad1, pad0, title, cols=8):
    """Plot a batch of images annotated with their labels.

    Args:
        images ([TODO:type]): [TODO:description]
        labels ([TODO:type]): [TODO:description]
        pad1 ([TODO:type]): [TODO:description]
        pad0 ([TODO:type]): [TODO:description]
        title ([TODO:type]): [TODO:description]
        cols ([TODO:type], optional): [TODO:description]
    """
    n = len(images)
    rows = int(n / cols)
    fig = plt.figure(figsize=(8, 8))

    for i in range(cols * rows):
        img = images[i].permute(1, 2, 0)
        img_label = torch.zeros((68, 68, 3), dtype=np.float)
        img_label[:, :, 2] = pad0(img[:, :, 2])
        # print(i, labels[i], img.mean())
        if labels[i] == 0:
            img_label[:, :, 0] = pad1(img[:, :, 0])
            img_label[:, :, 1] = pad0(img[:, :, 1])
        elif labels[i] == 1:
            img_label[:, :, 0] = pad0(img[:, :, 0])
            img_label[:, :, 1] = pad1(img[:, :, 1])
        ax = fig.add_subplot(rows, cols, i + 1)
        ax.axis("off")
        plt.imshow(np.asarray(img_label))

    fig.suptitle(title)
    plt.show()


def plotBatches(loader, num_batches, names):
    pad1 = torch.nn.ConstantPad2d((4, 4, 4, 4), 1)
    pad0 = torch.nn.ConstantPad2d((4, 4, 4, 4), 0)
    i = 0

    with torch.no_grad():
        for batch in test_loader:
            for j in range(len(batch) - 1):
                plotBatch(batch[j], batch[-1], pad1, pad0, names[j])
            i += 1
            if i == num_batches:
                break



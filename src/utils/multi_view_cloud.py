from dataclasses import dataclass

import numpy as np


@dataclass
class PointCloud:
    pts: np.ndarray

@dataclass
class NormalCloud(PointCloud):
    normals: np.ndarray

#@dataclass
#class MultiViewCloud:

"""Plot grasp poses, clouds, and frames.

Grasp poses are plotted as simplified robot hands: a set of cubes. This set consists 
of one cube for each finger, one for the hand's base and one for the grasp's approach 
direction.

"""

from copy import deepcopy
from datetime import datetime

import numpy as np
import vtk

from hands.hand_eval import HandGeometry

from .plot import (
    VtkPointCloud,
    applyTransformFilter,
    createArrowActor,
    createArrowFromEndpoints,
    createAxesActor,
    createCube,
    createCubeActor,
    createHandsActor,
    createSphereActor,
)


def cubeToPoly(
    cube: vtk.vtkCubeSource, rotation: np.array, position: np.array
) -> vtk.vtkPolyData:
    """Transform a cube and convert it to a PolyData object.

    Args:
        cube (vtk.vtkCubeSource): the cube to be converted.
        rotation (np.array): 3x3 rotation of the cube.
        position (np.array): 3x1 position of the cube.

    Returns:
        vtk.vtkPolyData: the PolyData object.
    """
    transform = np.eye(4)
    transform[:3, :3] = rotation
    transform[:3, 3] = position
    transform_filter = applyTransformFilter(cube, transform)
    poly = vtk.vtkPolyData()
    poly.ShallowCopy(transform_filter.GetOutput())
    return poly


class HandPolyData:
    """Container for PolyData objects that make up a hand.
    """

    def __init__(
        self,
        approach: vtk.vtkPolyData,
        base: vtk.vtkPolyData,
        left_finger: vtk.vtkPolyData,
        right_finger: vtk.vtkPolyData,
    ):
        """Construct a container for the PolyData objects that make up a hand.

        Args:
            approach (vtk.vtkPolyData): [TODO:description]
            base (vtk.vtkPolyData): [TODO:description]
            left_finger (vtk.vtkPolyData): [TODO:description]
            right_finger (vtk.vtkPolyData): [TODO:description]
        """
        self.approach = approach
        self.base = base
        self.left_finger = left_finger
        self.right_finger = right_finger


class HandsPlot:
    """Plot grasp poses, point clouds, and frames.
    """

    def __init__(self, hand_geom, base_depth=0.02, approach_dims=[0.06, 0.02, 0.02]):
        """Constructor.

        Args:
            hand_geom (HandGeometry): the geometry of the hand.
            base_depth (float, optional): the depth of the base cube.
            approach_dims (list, optional): the dimensions of the approach cube.
        """
        self.hand_geom = hand_geom
        self.base_depth = base_depth
        self.base_dims = [base_depth, hand_geom.outer_diameter, hand_geom.height]
        self.finger_dims = [hand_geom.depth, hand_geom.finger_width, hand_geom.height]
        self.approach_dims = approach_dims
        self.half_inner_diameter = 0.5 * (
            hand_geom.outer_diameter - hand_geom.finger_width
        )
        self.renderer = vtk.vtkRenderer()
        self.camera = None
        self.width = 640
        self.height = 480
        self.title = ""

    def clear(self):
        """Clear the plot.
        """
        actors = self.renderer.GetActors()
        if len(actors) > 0:
            for a in actors:
                self.renderer.RemoveActor(a)

    def setCameraPose(self, position, view_up, focal_point):
        """Set the pose of the interactive camera in the plot.

        Args:
            position ([TODO:type]): [TODO:description]
            view_up ([TODO:type]): [TODO:description]
            focal_point ([TODO:type]): [TODO:description]
        """
        self.camera = vtk.vtkCamera()
        self.camera.SetPosition(*position)
        self.camera.SetViewUp(*view_up)
        self.camera.SetFocalPoint(*focal_point)

    def setTitle(self, title):
        """Set the title of the plot window.

        Args:
            title (str): the title.
        """
        self.title = title

    def show(self):
        """Show the plot.
        """
        self.renderer.SetBackground(1, 1, 1)
        self.renderer.ResetCamera()

        # Setup the camera.
        if self.camera == None:
            self.camera = vtk.vtkCamera()
            self.camera.SetPosition(0.5, -0.5, 0.5)
            self.camera.SetViewUp(0.0, 0, 1.0)
            self.camera.SetFocalPoint(0.0, 0.0, 0.0)
        self.renderer.SetActiveCamera(self.camera)

        # Setup the light.
        # light = vtk.vtkLight()
        # light.SetFocalPoint(0.5, 0.0, 0.0)
        # light.SetPosition(0.5, 0.0, 2.0)
        # self.renderer.AddLight(light)

        # Render Window
        renderWindow = vtk.vtkRenderWindow()
        renderWindow.AddRenderer(self.renderer)
        renderWindow.SetWindowName(self.title)
        renderWindow.SetSize(self.width, self.height)

        # Interactor
        interactor = vtk.vtkRenderWindowInteractor()
        interactor.SetRenderWindow(renderWindow)
        interactor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
        # interactor.SetInteractorStyle(HandsInteractorStyle(interactor))

        # Begin Interaction
        renderWindow.Render()
        interactor.Start()

        # Take a screenshot.
        w2if = vtk.vtkWindowToImageFilter()
        w2if.SetInput(renderWindow)
        w2if.Update()
        writer = vtk.vtkPNGWriter()
        writer.SetFileName(f"/tmp/clutter_{datetime.now()}.png")
        writer.SetInputConnection(w2if.GetOutputPort())
        writer.Write()

    def plotAxes(self, pose, lengths=[0.1, 0.1, 0.1], font_size=1):
        """Plot a pose as the axes of a coordinate frame.

        Args:
            pose (np.array): 4x4 pose to be plotted.
            lengths (list, optional): dimensions of the axes.
            font_size (int, optional): the font size of the axes labels.
        """
        actor = createAxesActor(pose, lengths, font_size)
        self.renderer.AddActor(actor)

    def plotAxesList(self, poses, lengths=[]):
        """Plot multiple poses as coordinate frame axes.

        Args:
            poses (list): the poses to be plotted.
            lengths (list, optional): the dimensions for each sub-axes.
        """
        if len(lengths) == 0:
            lengths = [[0.1, 0.1, 0.1] for p in poses]
        for p, l in zip(poses, lengths):
            self.plotAxes(p, l)

    def plotNormals(self, points, normals, colors=[], length=0.12):
        if len(colors) == 0:
            colors = np.zeros((len(normals), 3))
        for i, (p, n) in enumerate(zip(points, normals)):
            X = np.eye(4)
            X[:3, 3] = p
            # actor = createArrowActor(transform=X)
            actors = createArrowFromEndpoints(p, p + length * n, color=colors[i])
            self.renderer.AddActor(actors[0])

    def plotArrow(
        self,
        start: np.array,
        end: np.array,
        shaft_radius=0.005,
        tip_radius=0.020,
        tip_length=0.030,
        color=[1.0, 1.0, 0.0],
    ):
        actor = createArrowFromEndpoints(
            start, end, shaft_radius, tip_radius, tip_length, color
        )[0]
        self.renderer.AddActor(actor)

    def plotCube(self, pose, xlength, ylength, zlength, color, opacity):
        source = createCube(xlength, ylength, zlength)
        # source.SetCenter(pose[:3, 3])

        poly = cubeToPoly(source, pose[:3, :3], pose[:3, 3])
        append_filter = vtk.vtkAppendPolyData()
        append_filter.AddInputData(poly)
        append_filter.Update()
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(append_filter.GetOutputPort())
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetColor(*color)
        actor.GetProperty().SetOpacity(opacity)

        # cube = createCubeActor(center, xlength, ylength, zlength, color, opacity)

        self.renderer.AddActor(actor)

    def plotSphere(self, center, radius, color, opacity):
        sphere = createSphereActor(center, radius, color, opacity)
        self.renderer.AddActor(sphere)

    def plotCloud(self, cloud, color, opacity=1.0, point_size=3.0):
        """Plot a point cloud.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            color (list): the RGB color for the plotted points.
            point_size (float, optional): the size of the plotted points.
        """
        vtk_cloud = VtkPointCloud()
        for x in cloud.points:
            vtk_cloud.addPoint(x)
        vtk_cloud.setColor(color, len(cloud.points))
        vtk_cloud.vtkActor.GetProperty().SetOpacity(opacity)
        vtk_cloud.vtkActor.GetProperty().SetPointSize(point_size)
        self.renderer.AddActor(vtk_cloud.vtkActor)

    def plotHands(self, poses, color, opacity=0.4, scale=1.0):
        """Plot grasp poses.

        Note that if a scaling factor different than 1.0 is used, the plotted hands 
        may not correctly correspond to reality (e.g., colliding hands might appear as 
        not colliding).

        Args:
            poses (np.array or list): the grasp poses.
            cloud (o3d.geometry.PointCloud): the point cloud.
            opacity (float, optional): the opacity of the plotted hands.
            scale (float, optional): the scaling factor of the plotted hands.
        """
        if len(color) == 3 and type(color) == list:
            append_filter = vtk.vtkAppendPolyData()
            for p in poses:
                hand = self.createHandPolys(scale)
                hand = applyTransformFilter(hand, p)
                hand_poly = vtk.vtkPolyData()
                hand_poly.ShallowCopy(hand.GetOutput())
                append_filter.AddInputData(hand_poly)
            append_filter.Update()
            hands_actor = createHandsActor(append_filter, color, opacity)
            self.renderer.AddActor(hands_actor)
        else:
            hands_actor = []
            for i, p in enumerate(poses):
                hand = self.createHandPolys(scale)
                hand = applyTransformFilter(hand, p)
                hand_poly = vtk.vtkPolyData()
                hand_poly.ShallowCopy(hand.GetOutput())
                append_filter = vtk.vtkAppendPolyData()
                append_filter.AddInputData(hand_poly)
                hands_actor.append(createHandsActor(append_filter, color[i]))
            for h in hands_actor:
                self.renderer.AddActor(h)

    def createHandPolys(self, scale=1.0):
        """Create PolyData objects for a hand.

        Args:
            scale (float, optional): the scaling factor of the plotted hands.
        """
        finger_dims = deepcopy(self.finger_dims)
        base_dims = deepcopy(self.base_dims)
        approach_dims = deepcopy(self.approach_dims)
        for dims in [finger_dims, base_dims, approach_dims]:
            dims[2] *= scale
        base_dims[0] *= scale
        finger_dims[1] *= scale
        approach_dims[1] *= scale

        left_finger_cube = createCube(*finger_dims)
        right_finger_cube = createCube(*finger_dims)
        base_cube = createCube(*base_dims)
        approach_cube = createCube(*approach_dims)

        inner_diameter = base_dims[1] - finger_dims[1]
        left_center = [0.5 * finger_dims[0], -0.5 * inner_diameter, 0]
        right_center = [0.5 * finger_dims[0], 0.5 * inner_diameter, 0]
        base_center = [-0.5 * base_dims[0], 0.0, 0.0]
        approach_center = [-(base_dims[0] + 0.5 * approach_dims[0]), 0.0, 0.0]

        left_finger_poly = cubeToPoly(left_finger_cube, np.eye(3), left_center)
        right_finger_poly = cubeToPoly(right_finger_cube, np.eye(3), right_center)
        base_poly = cubeToPoly(base_cube, np.eye(3), base_center)
        approach_poly = cubeToPoly(approach_cube, np.eye(3), approach_center)

        polys = [left_finger_poly, right_finger_poly, base_poly, approach_poly]
        # polys = [approach_poly]
        append_filter = vtk.vtkAppendPolyData()
        for p in polys:
            append_filter.AddInputData(p)
        append_filter.Update()

        return append_filter

    def plotApproaches(self, poses, color, opacity=0.4, scale=[0.1, 1.0, 1.0]):
        for p in poses:
            X = np.eye(4)
            # X[:3, 3] = p[:3, 3]
            X[:3, :3] = p[:3, :3]
            arrow_actor = createArrowActor(transform=X, rgb=color, scale=scale)
            self.renderer.AddActor(arrow_actor)

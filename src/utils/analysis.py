import numpy as np


def paretoFront(pts, strict_pareto):
    """Calculate the Pareto frontier for a given set of points.

    The Pareto frontier is the subset of points such that each element of the 
    subset is preferred to all other points.
    """
    pareto = np.ones(len(pts), dtype=bool)
    # print('pareto:', pareto.shape, 'pts:', pts.shape)

    for i in range(len(pareto)):
        for j in range(len(pareto)):
            # if i == j:
                # continue
            # if strict_pareto and np.all(pts[j,:] > pts[i,:]):
                # pareto[i] = 0
                # break
            # if not strict_pareto and np.all(pts[j,:] >= pts[i,:]) \
                                 # and np.any(pts[j,:] > pts[i,:]):
            # if np.all(pts[j] >= pts[i]) and np.any(pts[j] > pts[i]):
            if np.all(pts[j] > pts[i]):
                pareto[i] = 0
                break
    front = pts[pareto,:]

    return front


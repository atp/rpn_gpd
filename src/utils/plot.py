import random
from copy import deepcopy

import numpy as np
import vtk
from scipy.spatial.transform import Rotation as Rot


class HandsInteractorStyle(vtk.vtkInteractorStyleTrackballCamera):
    def __init__(self, parent):
        self.parent = parent
        self.AddObserver("KeyPressEvent", self.keyPressEvent)

    def keyPressEvent(self, obj, event):
        key = self.parent.GetKeySym()
        if key == "l":
            print(key)
        return


class VtkPointCloud:
    def __init__(self, maxNumPoints=1e6):
        self.maxNumPoints = maxNumPoints
        self.vtkPolyData = vtk.vtkPolyData()
        self.clearPoints()
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputData(self.vtkPolyData)
        mapper.SetColorModeToDirectScalars()
        self.vtkActor = vtk.vtkActor()
        self.vtkActor.SetMapper(mapper)

    def setOpacity(self, opacity):
        self.vtkActor.GetProperty().SetOpacity(opacity)

    def setColor(self, color, n):
        if type(color[0]) == float:
            color = [c * 255 for c in color]
        self.colors = vtk.vtkUnsignedCharArray()
        self.colors.SetNumberOfComponents(3)
        self.colors.SetName("Colors")
        for i in range(n):
            self.colors.InsertNextTuple3(*color)
        self.vtkPolyData.GetPointData().SetScalars(self.colors)

    def setColors(self, colors):
        self.colors = vtk.vtkUnsignedCharArray()
        self.colors.SetNumberOfComponents(3)
        self.colors.SetName("Colors")
        for i in range(len(colors)):
            self.colors.InsertNextTuple3(*colors[i, :])
        self.vtkPolyData.GetPointData().SetScalars(self.colors)

    def addPoint(self, point):
        if self.vtkPoints.GetNumberOfPoints() < self.maxNumPoints:
            pointId = self.vtkPoints.InsertNextPoint(point[:])
            self.vtkCells.InsertNextCell(1)
            self.vtkCells.InsertCellPoint(pointId)
        else:
            r = np.random.randint(0, self.maxNumPoints)
            self.vtkPoints.SetPoint(r, point[:])
        self.vtkCells.Modified()
        self.vtkPoints.Modified()

    def clearPoints(self):
        self.vtkPoints = vtk.vtkPoints()
        self.vtkCells = vtk.vtkCellArray()
        self.vtkPolyData.SetPoints(self.vtkPoints)
        self.vtkPolyData.SetVerts(self.vtkCells)


def plotClouds(clouds, colors=[], title="", camera=None):
    point_sizes = [3, 6]
    if len(colors) == 0:
        colors = [[255, 0, 0], [0, 0, 255]]
    renderer = vtk.vtkRenderer()

    for i in range(len(clouds)):
        vtk_cloud = VtkPointCloud()
        n = len(clouds[i].points)
        for j in range(n):
            vtk_cloud.addPoint(clouds[i].points[j])

        vtk_cloud.setColor(colors[i], n)
        vtk_cloud.vtkActor.GetProperty().SetPointSize(point_sizes[i])
        renderer.AddActor(vtk_cloud.vtkActor)

    show(renderer, title, camera)


# Plot hands onto a view point cloud and its underlying mesh.
def plotColoredHands(
    clouds,
    hands,
    hand_colors,
    hand_params,
    title="Hands",
    plot_base_frame=False,
    camera=None,
    camera_pose=[],
    scale=1.0,
):
    print("Plotting %d hands ..." % (len(hands)))

    # Plot the clouds.
    colors = [[255, 0, 0], [0, 0, 255]]
    vtk_clouds = []
    for i in range(len(clouds)):
        pointCloud = VtkPointCloud()
        for x in clouds[i].points:
            pointCloud.addPoint(x)
        pointCloud.setColor(colors[i], len(clouds[i].points))
        vtk_clouds.append(pointCloud)

    # Plot the hands.
    append_filters = []
    for i in range(len(hands)):
        top_pos = hands[i, :3, 3]
        R = deepcopy(hands[i, :3, :3])
        polys = createHandPolys(top_pos, R, hand_params, scale=scale)
        appendFilter = vtk.vtkAppendPolyData()
        for p in polys:
            appendFilter.AddInputData(p)
        append_filters.append(appendFilter)

    appendFilter.Update()

    hand_actors = []
    for i in range(len(append_filters)):
        hand_actors.append(createHandsActor(append_filters[i], hand_colors[i]))

    # Plot base frame.
    T = np.eye(4)
    vtk_transform = vtk.vtkTransform()
    vtk_transform.SetMatrix(np2vtkMatrix4x4(T))
    actorAxes = vtk.vtkAxesActor()
    actorAxes.SetUserTransform(vtk_transform)
    actorAxes.SetTotalLength([0.1, 0.1, 0.1])
    xAxisLabel = actorAxes.GetXAxisCaptionActor2D()
    xAxisLabel.GetCaptionTextProperty().SetFontSize(6)

    # Create renderer.
    renderer = vtk.vtkRenderer()
    point_sizes = [3, 6]
    for i, c in enumerate(vtk_clouds):
        c.vtkActor.GetProperty().SetPointSize(point_sizes[i])
        renderer.AddActor(c.vtkActor)
    for h in hand_actors:
        renderer.AddActor(h)

    # Draw the origin frame.
    if plot_base_frame:
        renderer.AddActor(actorAxes)

    # Draw the camera frame.
    if len(camera_pose) > 0:
        renderer.AddActor(createAxesActor(camera_pose))

    show(renderer, title, camera)


def createAxesActor(pose, lengths=[0.1, 0.1, 0.1], font_size=1):
    vtk_transform = vtk.vtkTransform()
    vtk_transform.SetMatrix(np2vtkMatrix4x4(pose))
    actorAxes = vtk.vtkAxesActor()
    actorAxes.SetUserTransform(vtk_transform)
    actorAxes.SetTotalLength(lengths)
    xAxisLabel = actorAxes.GetXAxisCaptionActor2D()
    xAxisLabel.GetCaptionTextProperty().SetFontSize(font_size)
    yAxisLabel = actorAxes.GetYAxisCaptionActor2D()
    yAxisLabel.GetCaptionTextProperty().SetFontSize(font_size)
    zAxisLabel = actorAxes.GetZAxisCaptionActor2D()
    zAxisLabel.GetCaptionTextProperty().SetFontSize(font_size)
    actorAxes.GetXAxisCaptionActor2D().GetTextActor().SetTextScaleMode(0)
    actorAxes.GetYAxisCaptionActor2D().GetTextActor().SetTextScaleMode(0)
    actorAxes.GetZAxisCaptionActor2D().GetTextActor().SetTextScaleMode(0)
    return actorAxes


def createHandPolys(bottom_position, R, hand_params, base_depth=0.02, scale=1.0):
    base_depth *= scale
    approach = R[:3, 0]
    closing = R[:3, 1]

    base_bottom = bottom_position
    base_center = base_bottom - approach * 0.5 * base_depth

    left = -0.5 * (hand_params["outer_diameter"] - hand_params["finger_width"])
    right = 0.5 * (hand_params["outer_diameter"] - hand_params["finger_width"])

    left_bottom = base_bottom + left * closing
    right_bottom = base_bottom + right * closing
    left_center = left_bottom + 0.5 * hand_params["depth"] * approach
    right_center = right_bottom + 0.5 * hand_params["depth"] * approach

    T = np.eye(4)
    T[:3, :3] = R
    T[:3, 3] = base_center
    base_cube = createCube(
        base_depth, hand_params["outer_diameter"], scale * 2.0 * hand_params["height"]
    )
    base_transform_filter = applyTransformFilter(base_cube, T)

    T[:3, 3] = left_center
    left_cube = createCube(
        hand_params["depth"],
        scale * hand_params["finger_width"],
        scale * hand_params["height"],
    )
    left_transform_filter = applyTransformFilter(left_cube, T)

    T[:3, 3] = right_center
    right_cube = createCube(
        hand_params["depth"],
        scale * hand_params["finger_width"],
        scale * hand_params["height"],
    )
    right_transform_filter = applyTransformFilter(right_cube, T)

    T[:3, 3] = base_bottom - (0.03 + base_depth) * approach
    approach_cube = createCube(0.06, scale * 0.02, scale * 0.02)
    approach_transform_filter = applyTransformFilter(approach_cube, T)

    base_poly = vtk.vtkPolyData()
    base_poly.ShallowCopy(base_transform_filter.GetOutput())

    left_poly = vtk.vtkPolyData()
    left_poly.ShallowCopy(left_transform_filter.GetOutput())

    right_poly = vtk.vtkPolyData()
    right_poly.ShallowCopy(right_transform_filter.GetOutput())

    approach_poly = vtk.vtkPolyData()
    approach_poly.ShallowCopy(approach_transform_filter.GetOutput())

    return base_poly, left_poly, right_poly, approach_poly


def applyTransformFilter(geom_obj, T):
    vtk_transform = vtk.vtkTransform()
    vtk_transform.SetMatrix(np2vtkMatrix4x4(T))
    transformFilter = vtk.vtkTransformPolyDataFilter()
    transformFilter.SetTransform(vtk_transform)
    transformFilter.SetInputConnection(geom_obj.GetOutputPort())
    transformFilter.Update()
    return transformFilter


def createCube(xlength, ylength, zlength):
    cube = vtk.vtkCubeSource()
    cube.SetXLength(xlength)
    cube.SetYLength(ylength)
    cube.SetZLength(zlength)

    return cube


def createCubeActor(center, xlength, ylength, zlength, color, opacity=1.0):
    cubeSource = createCube(xlength, ylength, zlength)
    cubeSource.SetCenter(center)

    cubeMapper = vtk.vtkPolyDataMapper()
    cubeMapper.SetInputConnection(cubeSource.GetOutputPort())
    cubeActor = vtk.vtkActor()
    cubeActor.SetMapper(cubeMapper)
    cubeActor.GetProperty().SetColor(*color)
    cubeActor.GetProperty().SetOpacity(opacity)

    return cubeActor


def createSphereActor(center, radius, color, opacity=1.0):
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(center)
    sphereSource.SetRadius(radius)

    # Make the surface smooth.
    sphereSource.SetPhiResolution(100)
    sphereSource.SetThetaResolution(100)

    sphereMapper = vtk.vtkPolyDataMapper()
    sphereMapper.SetInputConnection(sphereSource.GetOutputPort())
    sphereActor = vtk.vtkActor()
    sphereActor.SetMapper(sphereMapper)
    sphereActor.GetProperty().SetColor(*color)
    sphereActor.GetProperty().SetOpacity(opacity)

    return sphereActor


def createArrowFromEndpoints(
    start: np.array,
    end: np.array,
    shaft_radius=0.008,
    tip_radius=0.025,
    tip_length=0.030,
    color=[1.0, 1.0, 0.0],
):
    # Create an arrow.
    arrowSource = vtk.vtkArrowSource()
    arrowSource.SetShaftRadius(shaft_radius)
    arrowSource.SetTipRadius(tip_radius)
    arrowSource.SetTipLength(tip_length)

    # Generate a random start and end point
    random.seed(8775070)
    USER_MATRIX = False

    # Compute a basis
    normalizedX = [0 for i in range(3)]
    normalizedY = [0 for i in range(3)]
    normalizedZ = [0 for i in range(3)]

    # The X axis is a vector from start to end
    math = vtk.vtkMath()
    math.Subtract(end, start, normalizedX)
    length = math.Norm(normalizedX)
    math.Normalize(normalizedX)

    # The Z axis is an arbitrary vector cross X
    arbitrary = [0 for i in range(3)]
    arbitrary[0] = random.uniform(-10, 10)
    arbitrary[1] = random.uniform(-10, 10)
    arbitrary[2] = random.uniform(-10, 10)
    math.Cross(normalizedX, arbitrary, normalizedZ)
    math.Normalize(normalizedZ)

    # The Y axis is Z cross X
    math.Cross(normalizedZ, normalizedX, normalizedY)
    matrix = vtk.vtkMatrix4x4()

    # Create the direction cosine matrix
    matrix.Identity()
    for i in range(3):
        matrix.SetElement(i, 0, normalizedX[i])
        matrix.SetElement(i, 1, normalizedY[i])
        matrix.SetElement(i, 2, normalizedZ[i])

    # Apply the transforms
    transform = vtk.vtkTransform()
    transform.Translate(start)
    transform.Concatenate(matrix)
    transform.Scale(length, length, length)

    # Transform the polydata
    transformPD = vtk.vtkTransformPolyDataFilter()
    transformPD.SetTransform(transform)
    transformPD.SetInputConnection(arrowSource.GetOutputPort())

    # Create a mapper and actor for the arrow
    mapper = vtk.vtkPolyDataMapper()
    actor = vtk.vtkActor()
    actor.GetProperty().SetColor(*color)

    if USER_MATRIX:
        mapper.SetInputConnection(arrowSource.GetOutputPort())
        actor.SetUserMatrix(transform.GetMatrix())
    else:
        mapper.SetInputConnection(transformPD.GetOutputPort())

    actor.SetMapper(mapper)

    # Create spheres for start and end point
    sphereStartSource = vtk.vtkSphereSource()
    sphereStartSource.SetCenter(start)
    sphereStartMapper = vtk.vtkPolyDataMapper()
    sphereStartMapper.SetInputConnection(sphereStartSource.GetOutputPort())
    sphereStart = vtk.vtkActor()
    sphereStart.SetMapper(sphereStartMapper)
    sphereStart.GetProperty().SetColor(1.0, 1.0, 0.3)

    sphereEndSource = vtk.vtkSphereSource()
    sphereEndSource.SetCenter(end)
    sphereEndMapper = vtk.vtkPolyDataMapper()
    sphereEndMapper.SetInputConnection(sphereEndSource.GetOutputPort())
    sphereEnd = vtk.vtkActor()
    sphereEnd.SetMapper(sphereEndMapper)
    sphereEnd.GetProperty().SetColor(1.0, 0.3, 0.3)

    return actor, sphereStart, sphereEnd


def createArrowActor(
    shaft_radius=0.001,
    tip_radius=0.006,
    tip_length=0.10,
    transform=[],
    rgb=[0.5, 0.5, 0],
    opacity=0.6,
    scale=[1.0, 1.0, 1.0],
):
    arrow = vtk.vtkArrowSource()
    arrow.SetShaftRadius(shaft_radius)
    arrow.SetTipRadius(tip_radius)
    arrow.SetTipLength(tip_length)
    arrow.InvertOn()

    mapper = vtk.vtkPolyDataMapper()
    if len(transform) > 0:
        # print(transform[:3, 3])
        # transform = np.eye(4)
        transform_filter = applyTransformFilter(arrow, transform)
        poly = vtk.vtkPolyData()
        poly.ShallowCopy(transform_filter.GetOutput())
        append_filter = vtk.vtkAppendPolyData()
        append_filter.AddInputData(poly)
        mapper.SetInputConnection(append_filter.GetOutputPort())
    else:
        mapper.SetInputConnection(arrow.GetOutputPort())

    # Create the actor and set its color and opacity.
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(*rgb)
    actor.GetProperty().SetOpacity(opacity)
    actor.SetScale(scale)

    # Without this, sometimes colors are rendered as black.
    actor.GetProperty().SetAmbient(0.3)
    actor.GetProperty().SetDiffuse(0.5)
    actor.GetProperty().SetSpecular(1.0)

    return actor


def createPlane(center, normal):
    source = vtk.vtkPlaneSource()
    source.SetCenter(center[0], center[1], center[2])
    source.SetNormal(normal[0], normal[1], normal[2])
    source.Update()
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(source.GetOutput())
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(vtk.vtkNamedColors().GetColor3d("Gray"))
    actor.GetProperty().SetOpacity(0.6)
    return actor


def createHandsActor(append_filter, rgb, opacity=0.4):
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(append_filter.GetOutputPort())
    mapper.ScalarVisibilityOff()
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(rgb[0], rgb[1], rgb[2])
    actor.GetProperty().SetOpacity(opacity)

    # print(actor.GetProperty().GetAmbient())
    # print(actor.GetProperty().GetDiffuse())
    # print(actor.GetProperty().GetSpecular())

    # Without this, sometimes colors are rendered as black.
    actor.GetProperty().SetAmbient(0.3)
    actor.GetProperty().SetDiffuse(0.5)
    actor.GetProperty().SetSpecular(1.0)

    return actor


def np2vtkMatrix4x4(mat):
    if mat.shape == (4, 4):
        obj = vtk.vtkMatrix4x4()
        for i in range(4):
            for j in range(4):
                obj.SetElement(i, j, mat[i, j])
        return obj


def show(renderer, title, camera=None, width=640, height=480):
    renderer.SetBackground(1, 1, 1)
    renderer.ResetCamera()

    # Setup the camera.
    if camera == None:
        camera = vtk.vtkCamera()
        camera.SetPosition(0.5, -0.5, 0.5)
        camera.SetViewUp(0.0, 0, 1.0)
        camera.SetFocalPoint(0.0, 0.0, 0.0)
    renderer.SetActiveCamera(camera)

    # Setup the light.
    light = vtk.vtkLight()
    light.SetFocalPoint(0.5, 0.0, 0.0)
    light.SetPosition(0.5, 0.0, 2.0)
    renderer.AddLight(light)

    # Render Window
    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)
    renderWindow.SetWindowName(title)
    renderWindow.SetSize(width, height)

    # Interactor
    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(renderWindow)
    # renderWindowInteractor.GetInteractorStyle().SetCurrentStyleToTrackballCamera()
    interactor.SetInteractorStyle(HandsInteractorStyle(interactor))

    # Begin Interaction
    renderWindow.Render()
    interactor.Start()
    # print('viewup:', renderer.GetActiveCamera().GetViewUp())


def createVtkCamera(position, view_up, focal_point):
    camera = vtk.vtkCamera()
    camera.SetPosition(*position)
    camera.SetViewUp(*view_up)
    camera.SetFocalPoint(*focal_point)
    return camera

"""Utility functions for Open3D point clouds.
"""
import numpy as np
import open3d as o3d
import trimesh


def twoWindowsPlot(geoms_left: list, geoms_right: list, titles: list = ["", ""]):
    """Plot two geometry lists in two different windows side-by-side.

    Args:
        geoms_left (list): the geometries for the left window.
        geoms_right (list): the geometries for the right window.
        titles (list, optional): the titles for the left and right window.
    """
    vis = o3d.visualization.Visualizer()
    vis.create_window(window_name=titles[0], width=960, height=540, left=0, top=0)
    for g in geoms_left:
        vis.add_geometry(g)

    vis2 = o3d.visualization.Visualizer()
    vis2.create_window(window_name=titles[1], width=960, height=540, left=960, top=0)
    for g in geoms_right:
        vis2.add_geometry(g)

    while True:
        # vis.update_geometry()
        if not vis.poll_events():
            break
        vis.update_renderer()

        # vis2.update_geometry()
        if not vis2.poll_events():
            break
        vis2.update_renderer()

    vis.destroy_window()
    vis2.destroy_window()


# def orientNormalsTowardsMean(cloud, tree=None, radius=0.003):
def orientNormalsTowardsMean(cloud, tree=None, radius=0.005):
    if tree == None:
        tree = o3d.geometry.KDTreeFlann(cloud)
    normals = np.asarray(cloud.normals)
    normals_out = np.empty(normals.shape, normals.dtype)

    for i, (x, n) in enumerate(zip(cloud.points, normals)):
        [k, idxs, _] = tree.search_radius_vector_3d(x, radius)
        mean_normal = np.sum(normals[idxs], 0)
        mean_normal /= np.linalg.norm(mean_normal)
        print("#neighbors:", k, "mean_normal:", mean_normal)

        # if np.dot(n, mean_normal) > 0.5:
        n = mean_normal
        # print("  Replaced with mean normal")
        normals_out[i, :] = n

    cloud.normals = o3d.utility.Vector3dVector(normals_out)


def estimateNormals(cloud, camera_position=None, is_flipped=False):
    """Estimate surface normals for a point cloud.

    Estimate surface normals for a point cloud. If a camera position is given, the
    surface normals are oriented toward that position. Alternatively, if `is_flipped`
    is set to True and the camera position is set zero, all surface normals are flipped.

    Args:
        cloud (o3d.geometry.PointCloud): the point cloud.
        camera_position (np.array, optional): the camera position.
        is_flipped (bool, optional): True if all normals should be flipped, false
            otherwise.
    """
    #    print('Estimating surface normals ...')

    if len(cloud.normals) > 0:
        print("  Cloud already has surface normals. Skipping ...")
        return cloud

    cloud.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamRadius(0.01))

    if camera_position is not None:
        cloud.orient_normals_towards_camera_location(camera_position)

    # if is_flipped and np.sum(camera_position == 0) == 3:
    if is_flipped:
        # print("  Flipping surface normals away from object center ...")
        normals = np.asarray(cloud.normals) * (-1.0)
        cloud.normals = o3d.utility.Vector3dVector(normals)

    return cloud


def pointNormalsFromFaceNormals(
    cloud: o3d.geometry.PointCloud, mesh: trimesh.base.Trimesh
):
    [_, _, idxs] = mesh.nearest.on_surface(np.array(cloud.points))
    cloud.normals = o3d.utility.Vector3dVector(mesh.face_normals[idxs])
    print("[pointNormalsFromFaceNormals]", np.array(cloud.points))
    print("[pointNormalsFromFaceNormals]", idxs)


def faceNormalsIndexed(points: np.array, normals: np.array, mesh: trimesh.base.Trimesh):
    [_, _, idxs] = mesh.nearest.on_surface(points)
    normals = o3d.utility.Vector3dVector(mesh.face_normals[idxs])
    print("[faceNormalsIndexed]", points)
    print("[faceNormalsIndexed]", idxs)
    return normals


def replaceNormals(
    cloud: o3d.geometry.PointCloud,
    mesh: trimesh.base.Trimesh,
    min_val: float,
    axis: int = 2,
):
    points = np.asarray(cloud.points)
    normals = np.asarray(cloud.normals)
    mask = points[:, axis] > min_val
    normals[mask] = faceNormalsIndexed(points[mask], normals[mask], mesh)
    cloud.normals = o3d.utility.Vector3dVector(normals)
    print(f"Replaced {np.count_nonzero(mask)} normals")
    # obj_cloud = numpyToCloud(points[mask], normals[mask])
    obj_cloud = cloud.select_down_sample(np.nonzero(mask)[0])
    o3d.visualization.draw_geometries([obj_cloud], "Object cloud", 640, 480)


def createVoxelizedCloud(pts, voxel_size=0.003):
    """Create a voxelized point cloud from a set of points.

    Args:
        pts (np.array): 3 x n points.
        voxel_size (float, optional): the voxel size.
    """
    cloud = o3d.geometry.PointCloud()
    cloud.points = o3d.utility.Vector3dVector(pts)
    cloud = cloud.voxel_down_sample(voxel_size)
    return cloud


def numpyToCloud(pts, normals=[], colors=[]):
    """Create an open3d point cloud from a numpy array.

    Args:
        pts (np.array): n x 3 the points to be stored in the point cloud.
        normals (np.array, optional): n x 3 the surface normals.
        colors (np.array, optional): n x 3 the colors.

    Returns:
        o3d.geometry.PointCloud: the point cloud.
    """
    cloud = o3d.geometry.PointCloud()
    cloud.points = o3d.utility.Vector3dVector(pts)
    if len(normals) > 0:
        cloud.normals = o3d.utility.Vector3dVector(normals)
    if len(colors) > 0:
        cloud.colors = o3d.utility.Vector3dVector(colors)

    return cloud


def preprocessCloudAll(
    cloud,
    voxelize=False,
    remove_plane=False,
    remove_radius_outliers=False,
    estimate_normals=False,
    camera_position=[0, 0, 0],
    voxel_size=0.003,
):
    """Preprocess a point cloud.

    Args:
        cloud (o3d.geometry.PointCloud): the point cloud.
        voxelize (bool): if the point cloud should be voxelized.
        remove_plane (bool): if the support plane should be removed.
        remove_radius_outliers (bool): if statistical outliers should be removed.
        estimate_normals (bool): if surface normals should be estimated.
        camera_position (np.array, optional): 3 x 1 camera position.
        voxel_size (float, optional): the voxel size.
    """
    # Voxelize cloud.
    if voxelize:
        cloud = cloud.voxel_down_sample(voxel_size)
        print(f"Voxelized cloud down to {len(cloud.points)} points.")

    # Remove the ground plane from the cloud.
    if remove_plane:
        _, inliers = cloud.segment_plane(
            distance_threshold=0.01, ransac_n=3, num_iterations=250
        )
        inlier_cloud = cloud.select_down_sample(inliers)
        inlier_cloud.paint_uniform_color([1.0, 0, 0])
        outlier_cloud = cloud.select_down_sample(inliers, invert=True)
        # o3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])
        cloud = outlier_cloud
        print(f"Cloud downsampled to {len(cloud.points)} non-planar points.")

    # Remove points that have no neighbors within a radius.
    if remove_radius_outliers:
        cloud, ind = cloud.remove_radius_outlier(nb_points=10, radius=0.03)

    # Estimate surface normals.
    if estimate_normals:
        cloud = estimateNormals(cloud, camera_position)
        print(f"Estimated {len(cloud.normals)} surface normals.")

    return cloud


def preprocessPointCloud(cloud, pose, voxel_size=0.003):
    cloud_out = cloud.voxel_down_sample(voxel_size)
    print(f"Downsampled cloud to {len(cloud_out.points)} points.")
    cloud_out = estimateNormals(cloud_out, pose[:3, 3])
    print(f"Estimated {len(cloud_out.normals)} surface normals.")
    return cloud_out

"""Create a database with labels to train the CLS network.
"""

import os
import sys

import numpy as np
import tables as tab


def storeDatabase(
    images: np.array, labels_out: np.array, labels_in: np.array, fpath: str
):
    os.environ["BLOSC_NTHREADS"] = "8"
    filters = tab.Filters(complevel=9, complib="blosc:lz4")

    with tab.open_file(fpath, "w") as fout:
        imgs_dset = fout.create_carray(
            "/",
            "images",
            atom=tab.UInt8Atom(),
            obj=np.array(images),
            filters=filters,
        )
        print(f"Created dataset: {imgs_dset}")
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(f"Created dataset: {labels_dset}")
        labels_table_dset = fout.create_carray(
            "/", "labels_table", obj=labels_in, filters=filters
        )
        print(f"Created dataset: {labels_table_dset}")

    print("Stored CLS database at:", fpath)


def main():
    if len(sys.argv) < 2:
        print("Error: not enough input arguments!")
        print("Usage: python create_cls_database.py FILE_IN")
        exit(-1)

    # Read labels from input database.
    fname = sys.argv[1]
    with tab.open_file(fname, "r") as f:
        labels = np.array(f.root.labels)
        if labels.ndim > 2:
            print("Error: node /labels has wrong dimensions!")
            sys.exit(-1)
        print(f"Input database contains labels with shape: {labels.shape}")

        # Derive labels based on the rows.
        negatives = np.sum(labels, 1) == 0
        labels_out = np.ones(len(labels), np.int)
        labels_out[negatives] = 0

        # Store the dervied labels in a new database.
        fname_out = input("Enter the name for the output database: ")
        fpath_out = fname[: fname.rfind("/") + 1] + fname_out
        storeDatabase(np.array(f.root.images), labels_out, labels, fpath_out)


if __name__ == "__main__":
    main()

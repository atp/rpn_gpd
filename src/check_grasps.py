"""Check grasps against ground truth conditions.

Usage: python check_grasps.py NPY_FILE OBJECTS_DIR THRESHOLD [PLOTS]

"""

import sys
from copy import deepcopy
from dataclasses import dataclass
from functools import partial
from time import time

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from generate_hands_mesh import GraspData, SceneData
from grasp_scene_data import GraspData, SceneData
from hand_params import antipodal_params, hand_params
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from scene import SingleObjectScene
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot

# Offset of grasp position along approach axis
position_offset = 0.059

# Rotations to convert from Graspnet to GPD hand frame.
Ry = Rot.from_rotvec(-np.pi / 2 * np.array([0, 1, 0])).as_matrix()
Rx = Rot.from_rotvec(-np.pi / 2 * np.array([1, 0, 0])).as_matrix()

np.set_printoptions(precision=3, suppress=True)

# Ensure that experiment runs are repeatable.
np.random.seed(0)


def pandaHand(hand_params):
    hand_params["finger_width"] = 0.01
    hand_params["outer_diameter"] = 2 * 0.0527 + 2 * hand_params["finger_width"]
    hand_params["depth"] = 0.1053 - 0.059


def posesToFramesAndBoxes(poses, infos):
    frames, boxes = [], []
    for X, info in zip(poses, infos):
        frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.01)
        frame.transform(X)
        frames.append(frame)
        box = o3d.geometry.TriangleMesh.create_box(0.002, 0.002, 0.002)
        box.translate(X[:3, 3])
        if info[0] == 1.0 and info[1] == 1.0:
            box.paint_uniform_color([0.0, 1.0, 0.0])
        else:
            box.paint_uniform_color([1.0, 0.0, 0.0])
        boxes.append(box)

    return frames, boxes


def main() -> None:
    # Read command line arguments.
    if len(sys.argv) < 4:
        print("Error: wrong input arguments!")
        print("Usage: python check_grasps.py NPY_FILE OBJECTS_DIR THRESHOLD [PLOTS]")
        print("PLOTS: 1 high-level visualization, 2 low-level")
        exit(-1)

    data_filepath = sys.argv[1]
    obj_dir = sys.argv[2]
    threshold = float(sys.argv[3])
    if len(sys.argv) == 5:
        plots_highlevel = True if int(sys.argv[4]) >= 1 else False
        plots_lowlevel = True if int(sys.argv[4]) == 2 else False
    else:
        plots_highlevel = 0
        plots_lowlevel = 0

    # Read grasp data from file.
    scene_data = SceneData()
    scene_data.fromFile(data_filepath)
    grasp_data = GraspData()
    grasp_data.fromFile(data_filepath)

    if grasp_data.method == "graspnet":
        pandaHand(hand_params)

    # Create the view point cloud.
    cloud = numpyToCloud(scene_data.cloud[:, :3])
    camera_pose = scene_data.camera_pose
    cloud = estimateNormals(cloud, camera_position=camera_pose[:3, 3])
    o3d.visualization.draw_geometries([cloud], "Point cloud loaded from file", 640, 480)

    # Create the mesh point cloud.
    mesh_filepath = f"{obj_dir}{scene_data.category}/{scene_data.instance}"
    scene = SingleObjectScene(mesh_filepath, 0.01, 0.1)
    scene.resetToScale(scene_data.scale)
    mesh = scene.createMultiViewPointCloud()
    o3d.visualization.draw_geometries(
        [mesh], "Multiview point cloud generated from mesh", 640, 480
    )

    # Check the grasps.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    hand_eval = HandEval(hand_geom, antip_prms)

    poses = grasp_data.grasp_poses
    if type(poses) == list:
        poses = np.array(poses)
    if grasp_data.method == "graspnet":
        for X in poses:
            p = X[:3, 3] + position_offset * X[:3, 2]
            R = np.linalg.multi_dot((X[:3, :3], Ry, Rx))
            bXh = np.eye(4)
            bXh[:3, :3] = R
            bXh[:3, 3] = p
            X = bXh

    # Evaluate grasp poses whose score is above a threshold.
    idxs = grasp_data.scores > threshold
    poses = poses[idxs]
    infos = [hand_eval.evalPose(mesh, p, plots=plots_lowlevel) for p in poses]
    infos = np.array(infos)

    # Plot the grasp poses.
    if plots_highlevel:
        # Plot grasp poses as frames.
        mesh.paint_uniform_color([1.0, 0.0, 0.0])
        cloud.paint_uniform_color([0.0, 0.0, 1.0])
        trimesh = o3d.geometry.TriangleMesh
        origin_frame = trimesh.create_coordinate_frame(0.2)
        camera_frame = trimesh.create_coordinate_frame(0.1, camera_pose[:3, 3])
        camera_frame.rotate(camera_pose[:3, :3])
        dist = np.linalg.norm(camera_pose[:3, 3])
        arrow = trimesh.create_arrow(0.005, 0.008, dist - 0.03, 0.03)
        arrow.rotate(camera_pose[:3, :3], center=False)
        draw_geoms = o3d.visualization.draw_geometries
        frames, boxes = posesToFramesAndBoxes(poses, infos)
        geoms = frames + boxes
        geoms += [origin_frame, camera_frame, arrow]
        draw_geoms(geoms + [mesh, cloud], "Grasp poses (mesh, cloud)", 640, 480)
        draw_geoms(geoms + [cloud], "Grasp poses (cloud)", 640, 480)

        # Plot all grasp poses as simplified robot hands.
        labels = (infos[:, 0] == 1) & (infos[:, 1] == 1)
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        if np.count_nonzero(labels) > 0:
            p.plotHands(poses[labels], [0.0, 1.0, 0.0])
        if np.count_nonzero(~labels) > 0:
            p.plotHands(poses[~labels], [1.0, 0.0, 0.0])
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle(f"cloud and grasp poses")
        p.show()

        # Plot positive grasp poses as simplified robot hands.
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        p.plotHands(poses[labels], [0.0, 1.0, 0.0])
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle(f"cloud and grasp poses (positives)")
        p.show()

    # Print some evaluation stats.
    print("Grasps info: [collision-free?, antipodal?, #ptsInHC, leftPt, rightPt]")
    print(infos)
    total = len(infos)
    collfree = np.sum(infos[:, 0] == 1)
    antipodal = np.sum(infos[:, 1] == 1)
    print(
        f"#grasps: {total}, collison-free: {collfree} ({collfree/total}), "
        + f"antipodal: {antipodal} ({antipodal/total})"
    )


if __name__ == "__main__":
    main()

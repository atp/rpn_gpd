import itertools
import os
from functools import partial

import cv2
import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
import torch.multiprocessing as mp
from scipy.spatial.transform import Rotation as Rot

from geometry.rect import Rect
from proposal import calcAverageDepth, calcDepthChannel
from utils.cloud_utils import numpyToCloud
from utils.hands_plot import HandsPlot

np.set_printoptions(suppress=True)

# volume_dims = {"width": 0.105, "depth": 0.06, "height": 0.04}
volume_dims = {"width": 0.105, "depth": 0.06, "height": 0.08}  # default
# volume_dims = {"width": 0.105, "depth": 0.06, "height": 0.10}
# volume_dims = {"width": 0.105, "depth": 0.10, "height": 0.10}


class GraspImageGenerator:
    def __init__(
        self, image_shape: tuple, generator, volume_dims: dict = volume_dims,
    ):
        self.image_shape = image_shape
        self.has_translation = generator.has_translation
        self.volume_dims = volume_dims
        self.nn_radius = np.max(np.array([volume_dims[k] for k in volume_dims]))
        self.generator = generator

    def calcImages(
        self,
        cloud: o3d.geometry.PointCloud,
        samples: np.array,
        rots: list,
        frames: list = [],
    ):
        pts = np.asarray(cloud.points)
        normals = np.asarray(cloud.normals)
        tree = o3d.geometry.KDTreeFlann(cloud)
        n = len(samples)
        num_classes = len(rots)
        rot_idxs = np.arange(num_classes)
        images = np.zeros((n, num_classes) + self.image_shape, dtype=np.uint8)
        pool = mp.Pool(os.cpu_count())

        for i in range(n):
            [_, nn_idxs, _] = tree.search_radius_vector_3d(samples[i], self.nn_radius)
            pts_nn = pts[nn_idxs, :] - samples[i]
            normals_nn = normals[nn_idxs, :]
            F = frames[i] if len(frames) > 0 else []
            func = partial(
                self.calcImageForRotation,
                pts=pts_nn,
                normals=normals_nn,
                rots=rots,
                F=F,
            )
            images[i, :] = pool.map(func, rot_idxs)

        pool.close()
        pool.join()

        return images

    def calcImageForRotation(
        self, idx: int, pts: np.array, normals: np.array, rots: list, F: list = []
    ):
        # 1. Transform point neighborhood into hand frame.
        R = rots[idx]
        if len(F) > 0:
            R = np.linalg.multi_dot([F, self.generator.R_binormal, R])
        pts_nn = np.dot(R.T, pts.T)
        normals_nn = np.dot(R.T, normals.T)

        image = self.calcImageForNeighborhood(idx, pts_nn, normals_nn)

        return image

    def calcImagesAtIndices(
        self,
        cloud: o3d.geometry.PointCloud,
        samples: np.array,
        indices: np.array,
        rots: list,
        frames: list,
    ):
        tree = o3d.geometry.KDTreeFlann(cloud)
        pts = np.asarray(cloud.points)
        normals = np.asarray(cloud.normals)
        nn_idxs = [
            tree.search_radius_vector_3d(x, self.nn_radius)[1]
            for x in samples[indices[0, :]]
        ]
        pts_nn = [pts[idx] - x for x, idx in zip(samples, nn_idxs)]
        normals_nn = [normals[idx] for idx in nn_idxs]

        n = indices.shape[1]
        images = np.zeros((n,) + self.image_shape, dtype=np.uint8)
        pool = mp.Pool(os.cpu_count())

        func = partial(
            self.calcImageAtIndexMP,
            pts=pts_nn,
            normals=normals_nn,
            rots=rots,
            frames=frames,
        )
        images = pool.map(func, np.arange(n))
        pool.close()
        pool.join()
        images = np.asarray(images)

        return images

    def calcImagesAtIndicesSP(
        self,
        cloud: o3d.geometry.PointCloud,
        samples: np.array,
        indices: np.array,
        rots: list,
        frames: list,
    ):
        tree = o3d.geometry.KDTreeFlann(cloud)
        n = indices.shape[1]
        images = np.zeros((n,) + self.image_shape, dtype=np.uint8)

        func = partial(
            self.calcImageAtIndex,
            samples=samples,
            cloud=cloud,
            tree=tree,
            frames=frames,
        )
        images = np.array([func(idx=indices[:, i], R=rots[i]) for i in range(n)])

        # for i in range(n):
        # images[i] = self.calcImageAtIndex(
        # indices[:, i], samples, cloud, tree, rots[i], frames
        # )

        return images

    def calcImageAtIndexMP(
        self, idx: int, pts: list, normals: list, rots: np.array, frames: list,
    ):
        R = rots[idx]
        pts = pts[idx]
        normals = normals[idx]

        # If GPD was used to generate this candidate.
        if self.has_translation:
            F = frames[idx]
            R = np.linalg.multi_dot([F, self.generator.R_binormal, R])

        # 1. Transform point neighborhood into hand frame.
        pts_rot = np.dot(R.T, pts.T)
        normals_rot = np.dot(R.T, normals.T)

        # 2. Calculate image for transformed point neighborhood.
        image = self.calcImageForNeighborhood(idx, pts_rot, normals_rot)

        return image

    def calcImageAtIndex(
        self,
        idx: list,
        samples: np.array,
        cloud: o3d.geometry.PointCloud,
        tree: o3d.geometry.KDTreeFlann,
        R: np.array,
        frames: list,
        return_descriptor_mask: bool = False,
    ):
        # If GPD was used to generate this candidate.
        if self.has_translation:
            F = frames[idx[0]]
            R = np.linalg.multi_dot([F, self.generator.R_binormal, R])
            # print(f'Calculated rotation {Rot.from_dcm(R).as_quat()} for sample {idx[0]}')

        pts = np.asarray(cloud.points)
        normals = np.asarray(cloud.normals)

        # 1. Find spherical point neighborhood around sample point.
        sample = samples[idx[0], :]
        [_, nn_idx, _] = tree.search_radius_vector_3d(sample, self.nn_radius)

        # 2. Transform point neighborhood into hand frame.
        pts_nn = np.dot(R.T, (pts[nn_idx, :] - sample).T)
        normals_nn = np.dot(R.T, normals[nn_idx, :].T)

        # image = self.calcImageForNeighborhood(idx[1], pts_nn, normals_nn)
        outs = self.calcImageForNeighborhood(
            idx[1], pts_nn, normals_nn, return_descriptor_mask
        )
        if return_descriptor_mask:
            image, mask, bounds = outs[0], outs[1], outs[2]
        else:
            image = outs

        if return_descriptor_mask:
            return image, np.asarray(nn_idx)[mask], bounds

        return image

    def calcImageForNeighborhood(
        self,
        grid_idx: int,
        pts: np.array,
        normals: np.array,
        return_descriptor_mask: bool = False,
    ):
        hand_geom = self.generator.hand_eval.hand_geom

        # 1. Determine left and right finger placement.
        # If GPD was used to generate this candidate.
        if self.generator.has_translation:
            trans_idx = grid_idx % self.generator.grid_trans
            mid = int(len(self.generator.finger_spacing) / 2)
            left = self.generator.finger_spacing[trans_idx]
            right = self.generator.finger_spacing[mid + trans_idx]
        else:
            left = -0.5 * hand_geom.outer_diameter
            right = 0.5 * hand_geom.outer_diameter - hand_geom.finger_width

        # 1. Crop the points based on the hand height (used to calculate hand bottom).
        height = hand_geom.height
        mask = (pts[2, :] > -0.5 * height) & (pts[2, :] < 0.5 * height)
        pts_crop = pts[:, mask]

        # 2. Calculate the deepest hand placement.
        bottom = hand_geom.init_bite - hand_geom.depth
        rect = Rect(0, left, 0, right)
        if self.generator.hand_eval.isCollisionFree(pts_crop.T, rect):
            bottom = self.generator.hand_eval.calcHandBottom(pts_crop, left, right)

        # 4. Calculate grasp image (descriptor).
        left = -0.5 * volume_dims["width"] + hand_geom.finger_width
        right = 0.5 * volume_dims["width"] - hand_geom.finger_width
        top = bottom + self.volume_dims["depth"]
        bounds = Rect(bottom, left, top, right)
        outs = self.calcImage(pts, normals, bounds, return_descriptor_mask)

        return outs

    def calcImage(
        self,
        pts: np.array,
        normals: np.array,
        bounds: Rect,
        return_descriptor_mask: bool = False,
    ):
        image_size = self.image_shape[0]

        # 1. Find points inside the descriptor volume.
        half_height = 0.5 * self.volume_dims["height"]
        mask = (
            bounds.contains(pts.T)
            & (pts[2, :] > -half_height)
            & (pts[2, :] < half_height)
        )
        pts = pts[:, mask]
        if pts.shape[1] == 0:
            # print("[calcImage] Warning: zero points in hand closing region")
            # input("?")
            return np.zeros(self.image_shape, dtype=np.uint8)

        # 2. Transform the points inside the volume to the unit volume.
        pts = self.transformToUnitVolume(pts, bounds.bottom, bounds.left)
        normals = normals[:, mask]

        # 3. Create the grasp image from the transformed points.
        if self.image_shape[-1] == 1:
            image = self.calcImage1Channel(pts)
        elif self.image_shape[-1] == 3:
            image = self.calcImage3Channels(pts, normals, image_size)
        elif self.image_shape[-1] == 4:
            image = calcImage4Channels(pts, normals, (0, 1, 2), image_size)
        elif self.image_shape[-1] == 12:
            image = self.calcImage12Channels(pts, normals)

        if return_descriptor_mask:
            bounds_out = (
                bounds.bottom,
                bounds.top,
                bounds.left,
                bounds.right,
                -half_height,
                half_height,
            )
            return image, mask, bounds_out

        return image

    def calcImage1Channel(self, pts: np.array):
        vert_cells, hort_cells = calcPixelCoordinates(pts, self.image_shape[0])

        image = np.zeros(self.image_shape, dtype=np.uint8)
        image[vert_cells, hort_cells] = 255

        # Fill in holes.
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        dilation = cv2.dilate(image, kernel, iterations=1)

        image = np.zeros(self.image_shape, dtype=np.uint8)
        image[:, :, 0] = dilation

        return image

    def calcImage3Channels(self, pts: np.array, normals: np.array, image_size: int):
        vert, hort = calcPixelCoordinates(pts, self.image_shape[0])
        indices = (hort + vert * self.image_shape[0]).astype(np.int64)

        return calcAverageNormalsImage(pts, normals, indices, image_size)

    def calcImage12Channels(self, pts: np.array, normals: np.array):
        image_size = self.image_shape[0]
        orders = ((0, 1, 2), (2, 1, 0), (0, 2, 1))
        images = [calcImage4Channels(pts, normals, x, image_size) for x in orders]
        image = np.concatenate(images, axis=-1)

        return image

    def transformToUnitVolume(self, pts: np.array, bottom: float, left: float):
        height = self.volume_dims["height"]
        pts[0, :] = (pts[0, :] - bottom) / self.volume_dims["depth"]
        pts[1, :] = (pts[1, :] - left) / self.volume_dims["width"]
        pts[2, :] = (pts[2, :] + 0.5 * height) / height

        return pts


def calcPixelCoordinates(pts, image_size):
    cell_size = 1.0 / image_size
    vert = np.minimum(np.floor(pts[0, :] / cell_size), image_size - 1)
    hort = np.minimum(np.floor(pts[1, :] / cell_size), image_size - 1)
    vert = image_size - 1 - vert
    hort = hort.astype(np.int32)
    vert = vert.astype(np.int32)

    return hort, vert


def calcAverageNormalsImage(
    pts: np.array, normals: np.array, indices: list, image_size: int
):
    unique_idxs = np.unique(indices)
    S = np.array([np.sum(np.abs(normals[:, indices == i]), 1) for i in unique_idxs])
    C = np.array([np.count_nonzero(normals[:, indices == i]) for i in unique_idxs])
    S = S / C[:, None]
    image = np.zeros((image_size, image_size, 3), dtype=np.float32)
    v, h = np.unravel_index(unique_idxs, image.shape[:2])
    image[v, h] = S

    # Fill in holes.
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    dilation = cv2.dilate(image, kernel, iterations=1)

    # Normalize the image.
    norm_image = cv2.normalize(
        dilation, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
    )
    image = np.uint8(norm_image * 255)

    return image


def calcImage4Channels(pts: np.array, normals: np.array, order: tuple, image_size: int):
    pts_proj = np.vstack((pts[order[0]], pts[order[1]], pts[order[2]]))
    normals_proj = np.vstack((normals[order[0]], normals[order[1]], normals[order[2]]))
    vert, hort = calcPixelCoordinates(pts_proj, image_size)
    indices = (hort + vert * image_size).astype(np.int64)
    normals_image = calcAverageNormalsImage(pts_proj, normals_proj, indices, image_size)
    depth_image = calcDepthChannel(vert, hort, pts_proj[2], image_size)
    image = np.concatenate((normals_image, depth_image[..., np.newaxis]), axis=-1)

    return image

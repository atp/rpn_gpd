from copy import deepcopy
from time import time

import numpy as np
import open3d as o3d

from geometry.rect import Rect
from grasp_detector import GraspDetector, findGraspsAbovePlane
from handles import Handle, findHandles, pruneShortHandles, removeEndPoints
from hands.deep_hand_eval import DeepHandEval
from hands.hand_eval import HandEval
from proposal import downsampleByDistance, sampleFromCube
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot

red = [1.0, 0.0, 0.0]
green = [0.0, 1.0, 0.0]


def centerGrasps(
    poses: np.array,
    cloud: o3d.geometry.PointCloud,
    hand_geom: HandGeometry,
    return_widths: bool = False,
    plot_verbose: bool = False,
) -> np.array:
    nn_radius = 0.2
    forward_offset = 0.005
    poses_out = np.zeros_like(poses)
    widths = np.zeros(len(poses))
    tree = o3d.geometry.KDTreeFlann(cloud)

    for i, pose in enumerate(poses):
        position = pose[:3, 3]
        [_, nn_idx, _] = tree.search_radius_vector_3d(position, nn_radius)
        R = pose[:3, :3]
        pts_raw = np.dot(R.T, (np.asarray(cloud.points)[nn_idx] - position).T)
        mask = (pts_raw[2, :] > -0.5 * hand_geom.height) & (
            pts_raw[2, :] < 0.5 * hand_geom.height
        )
        crop_idxs = np.asarray(nn_idx)[mask]
        pts_crop = pts_raw[:, mask]
        left = -0.5 * hand_geom.outer_diameter
        right = 0.5 * hand_geom.outer_diameter
        bottom = 0.0
        top = bottom + hand_geom.depth
        bounds = Rect(
            bottom, left + hand_geom.finger_width, top, right - hand_geom.finger_width
        )
        mask = bounds.contains(pts_crop.T)
        if np.count_nonzero(mask) == 0:
            poses_out[i] = deepcopy(pose)
            continue
        pts_rect = pts_crop[:, mask]
        idxs = crop_idxs[mask]
        y = pts_rect[1, :]
        length_y = np.max(y) - np.min(y)
        center_y = np.min(y) + 0.5 * length_y
        new_position = position + center_y * pose[:3, 1]

        closest_x = np.min(pts_rect[0, :])
        if closest_x < forward_offset:
            new_position = new_position - forward_offset * pose[:3, 0]
            print(f"{i} Hand base close to object. Applied offset along forward axis.")

        print(f"{position} --> {new_position}")
        pose_out = deepcopy(pose)
        pose_out[:3, 3] = new_position
        poses_out[i] = pose_out
        widths[i] = length_y

        if plot_verbose:
            plotColoredGrasps(
                cloud, [[pose], [pose_out]], [red, green], hand_geom, f"{i}a"
            )
            # plotGrasps(cloud, pose_out[np.newaxis, ...], hand_geom, f"{i}b")
            # mask_cloud = cloud.select_by_index(o3d.utility.IntVector(mask_idx))
            # plotGrasps(mask_cloud, pose[np.newaxis, ...], hand_geom, f"{i}c")
            # plotGrasps(mask_cloud, pose_out[np.newaxis, ...], hand_geom, f"{i}d")

    if return_widths:
        return poses_out, widths

    return poses_out


def forwardPushGrasps(
    poses: np.array, cloud: o3d.geometry.PointCloud, hand_eval: HandEval
) -> np.array:
    min_dist = 0.005
    forward_thresh = 0.03
    nn_radius = 0.2
    half_height = 0.5 * hand_eval.hand_geom.height
    finger_width = hand_eval.hand_geom.finger_width
    tree = o3d.geometry.KDTreeFlann(cloud)
    poses_out = np.zeros_like(poses)
    # rots = poses[:, :3, :3]
    # pos = poses[:, :3, 3]
    # nn_idxs = [
    # np.asarray(tree.search_radius_vector_3d(x, nn_radius)[1])
    # for x in poses[:3, :3, 3]
    # ]
    # points = np.asarray(cloud.points)
    # pts_nn = [np.dot(X[:3, :3].T, (points[nn_idx] - X[:3, 3]).T) for X in poses]
    # pts_nn = [x[:, (x[2, :] > -half_height) & (x < half_height)] for x in pts_nn]
    left = -0.5 * hand_eval.hand_geom.outer_diameter + finger_width
    right = 0.5 * hand_eval.hand_geom.outer_diameter - finger_width
    bottom = 0.0
    top = bottom + hand_eval.hand_geom.depth
    bounds = Rect(bottom, left, top, right)
    is_new = False
    # pts_rect = [x[:, bounds.contains(x.T)] for x in pts_nn]
    # closest = [np.min(x[0, :]) for x in pts_rect]
    # masks = [x > forward_thresh for x in closest]
    # [hand_eval.calcHandBottom(x, left, right, bottom) for i,x in enumerate(pts_nn) if ]

    for i, pose in enumerate(poses):
        position = pose[:3, 3]
        [_, nn_idx, _] = tree.search_radius_vector_3d(position, nn_radius)
        R = pose[:3, :3]
        pts_raw = np.dot(R.T, (np.asarray(cloud.points)[nn_idx] - position).T)
        mask = (pts_raw[2, :] > -half_height) & (pts_raw[2, :] < 0.5 * half_height)
        pts_crop = pts_raw[:, mask]
        left = -0.5 * hand_eval.hand_geom.outer_diameter
        right = 0.5 * hand_eval.hand_geom.outer_diameter
        bottom = 0.0
        top = bottom + hand_eval.hand_geom.depth
        bounds = Rect(bottom, left + finger_width, top, right - finger_width)
        mask = bounds.contains(pts_crop.T)
        if np.count_nonzero(mask) == 0:
            poses_out[i] = deepcopy(pose)
            continue
        pts_rect = pts_crop[:, mask]
        closest = np.min(pts_rect[0, :])
        pose_out = deepcopy(pose)
        if closest < min_dist:
            print(f"{i} Hand close to object surface.")
            pose_out[:3, 3] = position - R[:3, 0] * min_dist
            is_new = True
        elif closest > forward_thresh:
            print(f"{i} Hand far from object surface.")
            bottom = hand_eval.calcHandBottom(
                pts_crop, left, right - finger_width, closest
            )
            print("new bottom:", bottom, "closest:", closest)
            if bottom > 0:
                pose_out[:3, 3] = position + R[:3, 0] * bottom
                is_new = True
        if is_new:
            print(f"Correction to position: {position} --> {pose_out[:3,3]}")
            # plotColoredGrasps(
            # cloud,
            # [[pose], [pose_out]],
            # [red, green],
            # hand_eval.hand_geom,
            # f"{i}a",
            # )
            is_new = False
        poses_out[i] = pose_out

    return poses_out

def removeCollidingGrasps(poses, cloud, coll_check, plot):
    runtime = time()
    is_free = coll_check.evalPoses(cloud, poses, checks_antipodal=False)
    num_poses = len(poses)
    mask = is_free[:, 0] == 1
    runtime = time() - runtime
    print(f"Checked {num_poses} poses for collisions in {runtime:.3f}s")
    print(f"  collision-free: {np.count_nonzero(mask)}")
    if plot:
        plotGrasps(cloud, poses[mask], coll_check.hand_geom, "Collision-free grasps")
        # for p in poses[~mask]:
        # plotGrasps(cloud, [p], coll_check.hand_geom, "Colliding grasps")

    return mask


def fillHoles(
    cloud: o3d.geometry.PointCloud, z: float, n: int
) -> o3d.geometry.PointCloud:
    mins = cloud.get_min_bound()
    maxs = cloud.get_max_bound()
    x = np.linspace(mins[0], maxs[0], n)
    y = np.linspace(mins[1], maxs[1], n)
    X, Y = np.meshgrid(x, y)
    Z = np.ones(n ** 2) * z
    X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()
    print([x.shape for x in (X, Y, Z)])
    cloud_out = numpyToCloud(np.vstack((X, Y, Z)).T)

    return cloud_out


def plotGrasps(
    cloud: o3d.geometry.PointCloud,
    poses: np.array,
    camera_pose: np.array,
    hand_geom: HandGeometry,
    title: str = "",
    colors: list = [],
):
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    if len(colors) == 0:
        p.plotHands(poses, [0.0, 1.0, 0.0])
    else:
        p.plotHands(poses, colors)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose, [0.05, 0.05, 0.05])
    p.setTitle(title)
    p.show()


def plotColoredGrasps(
    cloud: o3d.geometry.PointCloud,
    poses: list,
    colors: list,
    hand_geom: HandGeometry,
    title: str = "",
):
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    for x, c in zip(poses, colors):
        p.plotHands(x, c, opacity=0.3)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose, [0.05, 0.05, 0.05])
    p.setTitle(title)
    p.show()


def scoresToColors(scores):
    if type(scores) == dict:
        scores = scores["gc"]
    min_score = np.min(scores)
    max_score = np.max(scores)
    greens = (scores - min_score) / (max_score - min_score)
    colors = np.zeros((len(greens), 3))
    colors[:, 0] = 1.0 - greens
    colors[:, 1] = greens

    return colors


class RobotPipeline:
    def __init__(
        self,
        detector: GraspDetector,
        num_samples: int,
        top_k: dict,
        voxel_size: float,
        remove_below_table: bool,
        check_collisions: bool,
        min_score: float,
        min_grasps_handles: int,
        center_grasps: bool,
        plot_detections: bool,
        plot_all: bool,
        plot_width: int = 640,
        plot_height: int = 480,
        cube_size: float = 1.0,
    ):
        self.detector = detector
        self.num_samples = num_samples
        self.top_k = top_k
        self.voxel_size = voxel_size
        self.remove_below_table = remove_below_table
        self.check_collisions = check_collisions
        self.min_score = min_score
        self.min_grasps_handles = min_grasps_handles
        self.center_grasps = center_grasps
        self.plot_detections = plot_detections
        self.plot_all = plot_all
        self.plot_width = plot_width
        self.plot_height = plot_height
        self.cube_size = cube_size
        self.deep_hand_eval = DeepHandEval(
            self.detector.hands_gen.hand_geom, self.detector.hands_gen.antipodal_params
        )
        self.coll_check = HandEval(
            self.detector.hands_gen.hand_geom, self.detector.hands_gen.antipodal_params
        )

    def draw(self, geoms: list, title: str = ""):
        o3d.visualization.draw_geometries(
            geoms, title, self.plot_width, self.plot_height
        )

    def preprocessPointClouds(
        self,
        full_cloud: o3d.geometry.PointCloud,
        objects_cloud: o3d.geometry.PointCloud,
        camera_pose: np.array,
    ):
        # 1. Voxelize the point clouds.
        if self.voxel_size > 0:
            full_cloud = full_cloud.voxel_down_sample(self.voxel_size)
            print(f"Downsampled <full_cloud> to {len(full_cloud.points)} points.")
            objects_cloud = objects_cloud.voxel_down_sample(self.voxel_size)
            print(f"Downsampled <objects_cloud> to {len(objects_cloud.points)} points.")

        # 2. Calculate surface normals for the objects point cloud.
        objects_cloud = estimateNormals(objects_cloud, camera_pose[:3, 3])
        print(f"Estimated {len(objects_cloud.normals)} surface normals.")

        if self.plot_all:
            origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
            self.draw([objects_cloud, origin_frame], "Surface normals")

        return full_cloud, objects_cloud

    def removeGraspsBelowTable(
        self, cloud: o3d.geometry.PointCloud, offset: float = 0.003
    ):
        _, inliers = cloud.segment_plane(
            distance_threshold=0.01, ransac_n=3, num_iterations=1000
        )

        minor = o3d.__version__
        minor = minor[minor.find(".") + 1 :]
        minor = minor[: minor.find(".")]
        if int(minor) <= 8:
            inlier_cloud = cloud.select_down_sample(inliers)
        else:
            inlier_cloud = cloud.select_by_index(inliers)
        min_plane_height = np.mean(np.asarray(inlier_cloud.points)[:, 2])
        print("The height of the segmented plane is:", min_plane_height)
        plane_height = np.min(np.asarray(cloud.points)[:, 2])
        plane_height = plane_height - offset
        print("The plane height is guessed to be:", plane_height)
        plane_height = min_plane_height
        f = fillHoles(cloud, min_plane_height, 50)
        cloud += f
        if self.plot_all:
            self.draw([cloud], "Filled holes")

        return cloud, plane_height, min_plane_height

    def detectGrasps(
        self,
        full_cloud: o3d.geometry.PointCloud,
        objects_cloud: o3d.geometry.PointCloud,
        camera_pose: np.array,
        plane_height: float = np.nan,
    ) -> np.array:
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)

        # Prefer samples further away from tabletop.
        offset = 0.01
        min_samples = 30
        pts = np.asarray(objects_cloud.points)
        cutoff = pts.max() - 2 * offset
        print("pts:", pts.min(), pts.max(), pts.mean())
        max_plane_height = np.min(np.asarray(objects_cloud.points)[:, 2]) + offset
        samples_cloud = downsampleByDistance(
            objects_cloud, max_plane_height, 2, 0.1, cutoff
        )
        if len(samples_cloud.points) < min_samples:
            samples_cloud = deepcopy(objects_cloud)
        if self.plot_detections:
            self.draw([samples_cloud], "samples_cloud")

        # Draw samples from bounding cube.
        num_samples = np.min([self.num_samples, len(samples_cloud.points)])
        samples = sampleFromCube(samples_cloud, self.cube_size, num_samples)
        samples_cloud = numpyToCloud(samples)
        n = len(samples_cloud.points)
        full_cloud.paint_uniform_color([0.0, 0.0, 1.0])
        samples_cloud.paint_uniform_color([1.0, 0.0, 0.0])
        print(f"Sampled {n} points out of {len(objects_cloud.points)}.")
        if self.plot_detections:
            self.draw([samples_cloud, full_cloud, objects_cloud], "Samples")

        full_cloud = estimateNormals(full_cloud, camera_pose[:3, 3])

        # Detect grasp poses.
        detection_time = time()
        poses, scores = self.detector.detectGraspsBest(
            [full_cloud, objects_cloud], samples, camera_pose, self.top_k, plane_height
        )
        detection_time = time() - detection_time
        print(f"Generated {len(poses)} grasp poses in {detection_time:.3f}s.")
        print(f"Scores:\n{scores}")

        # Debugging: verify that the grasp at the mid index is correct.
        """
        hands = {"indices": np.array([[0], [int(detector.hands_gen.num_hand_orientations/2)]])}
        print(hands)
        tree = o3d.geometry.KDTreeFlann(objects_cloud)
        poses = detector.hands_gen.indicesToPoses(objects_cloud, tree, samples, hands)
        """

        # Show approach directions for a single position.
        """
        m = detector.hands_gen.getNumHandsPerSample()
        hands = {"indices": np.vstack((np.zeros(m,dtype=int), np.arange(m))), "frames": []}
        print("pose indices:", hands["indices"])
        tree = o3d.geometry.KDTreeFlann(objects_cloud)
        poses = detector.hands_gen.indicesToPoses(full_cloud, tree, samples, hands)
        plotGrasps(full_cloud, poses, detector.hands_gen.hand_geom, "Detected grasps")
        """

        return poses, scores

    def processPointClouds(
        self,
        full_cloud: o3d.geometry.PointCloud,
        objects_cloud: o3d.geometry.PointCloud,
        camera_pose: np.array,
    ):
        hand_geom = self.detector.hands_gen.hand_geom

        self.preprocessPointClouds(full_cloud, objects_cloud, camera_pose)

        plane_height = np.nan
        if self.remove_below_table:
            full_cloud, plane_height, min_plane_height = self.removeGraspsBelowTable(
                full_cloud
            )

        # Detect grasp poses in the point cloud.
        poses, scores = self.detectGrasps(full_cloud, objects_cloud, plane_height)
        if self.plot_all or (self.plot_detections and not self.check_collisions):
            plotGrasps(full_cloud, poses, hand_geom, "Detected grasps")
            colors = scoresToColors(scores)
            plotGrasps(full_cloud, poses, hand_geom, "Scored grasps", colors)

        return poses, scores, min_plane_height

        # poses, scores = self.filterGrasps(poses, scores)

    def filterGrasps(
        self,
        poses: list,
        scores: list,
        full_cloud: o3d.geometry.PointCloud,
        min_plane_height: float,
    ):
        hand_geom = self.detector.hands_gen.hand_geom
        mask = scores["gc"] > self.min_score
        if np.count_nonzero(mask) > self.min_grasps_handles:
            before = len(poses)
            poses, scores = poses[mask], scores["gc"][mask]
            print(f"Removed grasps below min score: {before} --> {len(poses)}")
            if self.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Grasps above min score")

        if type(scores) == dict:
            scores = scores["gc"]

        # Center grasps based on visible point cloud.
        if self.center_grasps:
            print("Centering grasps ...")
            poses_old = deepcopy(poses)
            poses, widths = centerGrasps(
                poses,
                full_cloud,
                hand_geom,
                return_widths=True,
            )
            print("widths:", widths.shape, "scores:", scores.shape)
            for i, w in enumerate(widths):
                print(i, w)
            if self.plot_all:
                # plotGrasps(full_cloud, poses, hand_geom, "Centered grasps")
                plotColoredGrasps(
                    full_cloud,
                    [poses_old, poses],
                    [red, green],
                    hand_geom,
                    "Centered grasps (green)",
                )
            poses = forwardPushGrasps(poses, full_cloud, self.deep_hand_eval)

        # Remove grasp poses where a finger is below the table.
        if self.remove_below_table:
            num_poses = len(poses)
            above = findGraspsAbovePlane(poses, hand_geom, min_plane_height)
            # above = findGraspsAbovePlane(poses, hand_geom, min_plane_height, True, full_cloud)
            poses, widths, scores = poses[above], widths[above], scores[above]
            print(
                f"Removed grasps with finger below table: {num_poses} --> {len(poses)}"
            )
            if self.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Grasps above table")

        return poses, widths, scores

    def clusterGrasps(self, poses, widths, scores, min_handles, inliers):
        handles_time = time()
        handle_inliers = findHandles(poses, inliers)
        handles_time = time() - handles_time
        print(f"Found {len(handle_inliers)} handles in {handles_time:.3f}s")
        if len(handle_inliers) < min_handles:
            if args.check_collisions:
                mask = removeCollidingGrasps(
                    poses, full_cloud, coll_check, args.plot_all
                )
                poses, widths, scores = poses[mask], widths[mask], scores[mask]
            np.savez(args.grasps_path, poses=poses, widths=widths, scores=scores)
            print("Using grasps. #poses stored in file:", len(poses))
            if args.plot or args.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Output: grasps")
            continue
        if args.plot_all:
            p = HandsPlot(hand_geom)
            p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            for h in handle_inliers:
                p.plotHands(poses[h], list(np.random.rand(3)))
            p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            p.setTitle(f"Predicted handles")
            p.show()
            # for i,h in enumerate(handle_inliers):
            #     plotGrasps(full_cloud, poses[h], hand_geom, f"handle {i}/{len(handle_inliers)}")

        # Convert to handle objects.
        handles = [Handle(x, poses, widths) for x in handle_inliers]
        print(f"Found {len(handles)} handle grasps")
        # for h in handles:
        #     print(h.center, np.linalg.det(h.pose[:3, :3]))
        if self.plot_all:
            p = HandsPlot(hand_geom)
            p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            p.plotHands([x.pose for x in handles], np.random.rand(len(handles), 3))
            p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            p.setTitle(f"Handle grasps")
            p.show()
            # for i,h in enumerate(handles):
            #     plotColoredGrasps(full_cloud, [poses[h.inliers], [h.pose]], [red, green],
            #                       hand_geom, f"handle {i+1}/{len(handle_inliers)}")
        # np.save(args.grasps_path, [x.pose for x in handles])

        if args.prune_short_handles:
            num_handles = len(handles)
            # handles = pruneShortHandles(handles, poses)
            # print(f"Pruned short handles: {num_handles} --> {len(handles)}")
            handles = removeEndPoints(handles, poses, cutoff=1)
            handle_inliers = [h.inliers for h in handles]
            print(f"Removed handle endpoints")
            # for i, (h, h2) in enumerate(zip(handles, handles2)):
            #     handle1 = poses[h.inliers]
            #     handle2 = poses[h2.inliers]
            #     print(i, len(handle1), "-->", len(handle2))
            #     plotColoredGrasps(full_cloud, (handle1, handle2), [red, green], hand_geom,
            #                       f"Shortened handle {i+1}/{len(handle_inliers)}")

        poses, widths, scores = createOutputs(poses, widths, scores, handles)
        if args.check_collisions:
            mask = removeCollidingGrasps(poses, full_cloud, coll_check, args.plot_all)
            poses, widths, scores = poses[mask], widths[mask], scores[mask]
        np.savez(args.grasps_path, poses=poses, widths=widths, scores=scores)
        print("poses:", poses.shape, "widths:", widths.shape, "scores:", scores.shape)
        if args.plot or args.plot_all:
            # p = HandsPlot(hand_geom)
            # p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            # p.plotHands(poses, [0.0, 1.0, 0.0])
            # p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            # p.setTitle(f"Output: handle inliers and handle poses")
            # p.show()
            colors = scoresToColors(scores)
            plotGrasps(full_cloud, poses, hand_geom, "Output: scored grasps", colors)

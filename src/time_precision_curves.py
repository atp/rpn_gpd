import sys

import matplotlib

# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

from utils.analysis import paretoFront

np.set_printoptions(precision=4)


def finishPlot(xlabel, ylabel, filepath):
    plt.ylim([0.0, 1.0])
    plt.yticks(np.linspace(0, 1, 11))
    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.savefig(filepath)
    print(f"Plot saved as {filepath}")
    plt.draw()


if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python time_precision_curves.py DATA_DIR_1 ... DATA_DIR_N")
    exit(-1)

# Load the data to be plotted.
fig_dirs = [sys.argv[i] for i in range(1, len(sys.argv))]
ys, xs, labels = [], [], []
labels = []

for d in fig_dirs:
    print(f"Loading data from {d} ...")
    ys.append(np.load(d + "precisions.npy"))
    x = {}
    x["recall"] = np.load(d + "recalls.npy")
    x["time"] = np.load(d + "times.npy")
    x["detections per second"] = np.load(d + "pred_pos_secs.npy")
    xs.append(x)
    label = d[: d.rfind("/")]
    labels.append(label[label.rfind("/") + 1 :])
print("labels:", labels)

keys = xs[0].keys()
# labels = ['MA: GC', 'MA: CLS => GC', 'MA: CLS => ROT => GC']
# labels = ['MA: GC', 'MA: CLS => ROT => GC']

for k in keys:
    # if k == 'pred pos per sec':
    #     print(k)
    #     for i in range(len(xs)):
    #         print(type(xs[i][k]), xs[i][k].shape)
    #         print(type(ys[i]))
    #         idxs = np.nonzero(np.logical_and(xs[i][k] == 0, ys[i] > 0))
    #         zero_idxs = np.nonzero(np.logical_and(xs[i][k] == 0, ys[i] == 0))
    #         print(idxs, zero_idxs)
    # continue

    # Caculate Pareto fronts.
    paretos = []
    for i in range(len(xs)):
        print(f"({i}) Calculating Pareto front for {labels[i]}, {k} ...")
        pareto = paretoFront(np.vstack((xs[i][k], ys[i])).T)
        paretos.append(pareto[np.argsort(pareto[:, 0]), :])

    plt.figure()
    for i in range(len(xs)):
        plt.scatter(xs[i][k], ys[i], label=labels[i])
    finishPlot(k, "precision", "precision_" + k + "_scatter")
    plt.show()

    plt.figure()
    for i in range(len(paretos)):
        plt.plot(paretos[i][:, 0], paretos[i][:, 1], label=labels[i])
    finishPlot(k, "precision", "precision_" + k + "_pareto")
    plt.show()

    if k == "recall":
        plt.figure()
        for i in range(len(paretos)):
            paretos[i] = np.vstack(([0.0, 1.0], paretos[i], [1.0, 0.0]))
            plt.plot(paretos[i][:, 0], paretos[i][:, 1], label=labels[i])
        finishPlot(k, "precision", "precision_" + k + "_pareto_ends")
        plt.show()

# plt.figure()
# for i in range(len(paretos)):
#     print(paretos[i].shape)
#     paretos[i] = np.vstack(([0.,1.], paretos[i], [1.,0.]))
#     plt.plot(paretos[i][:,0], paretos[i][:,1], label=labels[i])
# finishPlot('recall', 'precision', '')
# plt.show()

# for k in x.keys():
#     pareto = paretoFront(np.vstack((x[k], precisions)).T)
#     pareto = pareto[np.argsort(pareto[:,0]),:]
#     print(k)
#     print(pareto)
#     plt.figure()
#     plt.plot(x[k], precisions, 'c.')
#     plt.plot(pareto[:,0], pareto[:,1], 'b')
#     finishPlot(k, 'precision', fig_dir + 'scatter_precision_' + k + '.png')
# plt.show()
#
# for k in x.keys():
#     pareto = paretoFront(np.vstack((x[k], precisions)).T)
#     pareto = pareto[np.argsort(pareto[:,0]),:]
#     print(k)
#     print(pareto)
#     plt.figure()
#     # plt.plot(x[k], precisions, 'c.')
#     plt.plot(pareto[:,0], pareto[:,1], 'b')
#     finishPlot(k, 'precision', fig_dir + 'pareto_precision_' + k + '.png')
# plt.show()

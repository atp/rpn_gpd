"""Detect grasps for a 3DNet object.
"""

import sys
from argparse import ArgumentParser
from time import time

import numpy as np
import open3d as o3d

from eval_grasp_detector import createGraspDetector, net_cfg
from grasp_detector import GpdGraspDetector, MaGcGraspDetector, MaGraspDetector
from hand_params import antipodal_params, hand_params
from handles import Handle, findHandles
from hands.hand_eval import AntipodalParams, HandGeometry
from networks.network import LenetLike
from object_set import ThreeDNetObjectSet
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import estimateNormals
from utils.hands_plot import HandsPlot
from utils.plot import createVtkCamera, plotClouds, plotColoredHands

threshs = {
    "gpd": 0.5,
    "ma": {"cls": 0.8, "rot": 0.8, "gc": 0.8},
    # "rot-gc": {"rot": 0.9, "gc": 0.9},
    "rot-gc": {"rot": 0.8, "gc": 0.8},
    "gc": 0.8,
}

min_object_extent = 0.01
max_object_extent = [0.07, 0.7]
num_views = 5
camera_distance = 0.5
look_at = [0.0, 0.0, 0.0]
cube_size = 1.0
use_open_gl_frame = True
prop_params = {"image_size": 60, "image_type": "depth"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], 3]
grasp_img_shape = (60, 60, 4)

net_cfg["rot-gc"]["weights_paths"] = {
    "rot": "../models/comparison/one_stage_antipodal/3dnet/rot_lenet.pth",
    "gc": "../models/comparison/one_stage_antipodal/3dnet/gc_4channels_lenet.pth",
}

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def preprocessPointCloud(cloud, camera_pose, voxel_size=0.003):
    if voxel_size > 0:
        cloud = cloud.voxel_down_sample(voxel_size)
        print(f"Downsampled cloud to {len(cloud.points)} points.")
    cloud = estimateNormals(cloud, camera_pose[:3, 3])
    print(f"Estimated {len(cloud.normals)} surface normals.")

    return cloud


def storeGrasps(
    category: str,
    instance: str,
    scale: str,
    camera_pose: np.array,
    scores: list,
    poses: list,
    cloud: o3d.geometry.PointCloud,
    method: str,
    filename_out: str,
):
    data = {
        "category": category,
        "instance": instance,
        "scale": scale,
        "camera_pose": camera_pose,
        "scores": scores,
        "grasps": poses,
        "cloud": np.asarray(cloud.points),
        "method": method,
    }
    np.save(filename_out, data)
    print(f"Stored grasp poses at: {filename_out}.npy")


def parseArgs():
    """Parse command line arguments."""
    parser = ArgumentParser(description="Generate grasp poses for a 3DNet object")
    parser.add_argument(
        "obj_dir", metavar="OBJECTS_DIR", help="path to 3DNet directory"
    )
    parser.add_argument(
        "categories_txt",
        metavar="CATEGORIES_TXT",
        help="path to TXT file with object categories",
    )
    parser.add_argument(
        "category", metavar="CATEGORY", help="the index/name of the object category"
    )
    parser.add_argument(
        "instance",
        metavar="INSTANCE",
        type=int,
        help="the index of the object instance",
    )
    parser.add_argument("method", metavar="METHOD", help="grasp detector")
    parser.add_argument(
        "num_samples", metavar="NUM_SAMPLES", type=int, help="number of samples"
    )
    parser.add_argument("--seed", default=0, type=int, help="random seed")
    parser.add_argument(
        "--scale", default=-1.0, type=float, help="object scaling factor"
    )
    parser.add_argument("--fast", action="store_true", help="use fast grasp detection")
    parser.add_argument("--thresh", type=float, help="threshold for grasp classifier")
    parser.add_argument("--best", nargs="+", help="top-k for each classifier")
    parser.add_argument("--plot", action="store_true", help="turn plotting on")
    parser.add_argument(
        "--inliers", type=int, help="minimum number of handle inliers", default=5
    )

    args = parser.parse_args()

    return args


def main():
    # Read command line arguments.
    args = parseArgs()

    # Setup the object dataset.
    obj_dir = args.obj_dir
    obj_list_path = args.categories_txt
    object_set = ThreeDNetObjectSet(obj_dir, obj_list_path, True)
    if args.category.isnumeric():
        category_idx = int(args.category)
    else:
        category_idx = object_set.objects.index(args.category)
    print(category_idx)
    instance_idx = args.instance
    method_name = args.method
    num_samples = args.num_samples

    # Setup the detector thresholds.
    grasp_threshs = threshs[method_name]
    if args.fast:
        if args.thresh is not None:
            grasp_threshs["gc"] = args.thresh
        if args.best is not None:
            grasp_threshs = {"rot": int(args.best[0]), "gc": int(args.best[1])}

    # Setup the grasp detector.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    detector = createGraspDetector(
        hand_geom, antip_prms, method_name, prop_params, grasp_img_shape, net_cfg
    )

    # Sample uniform view points on a sphere.
    viewpoints = np.random.normal(size=(num_views, 3))
    viewpoints /= np.linalg.norm(viewpoints, axis=1)[:, np.newaxis]
    viewpoints *= camera_distance

    # TODO: make this a command line argument
    view_idx = np.random.randint(num_views)

    # Create the mesh point cloud.
    print("==========================================================")
    mesh_filepath = object_set.get([category_idx, instance_idx])
    instance = mesh_filepath[mesh_filepath.rfind("/") + 1 :]
    scene = SingleObjectScene(mesh_filepath, min_object_extent, max_object_extent)
    print(f"Loaded mesh from: {mesh_filepath}")
    if args.scale == -1:
        scene.resetToRandomScale()
        args.scale = scene.scale
    else:
        scene.resetToScale(args.scale)
    mesh_cloud = scene.createMultiViewPointCloud()
    mesh_tree = o3d.geometry.KDTreeFlann(mesh_cloud)

    # Create the view point cloud.
    camera_pose = calcLookAtPose(viewpoints[view_idx], look_at)
    cloud = scene.createPointCloudFromView(camera_pose)
    cloud = preprocessPointCloud(cloud, camera_pose)

    # Get the ground truth.
    samples = sampleFromCube(cloud, cube_size, num_samples)
    print(f"Sampled {len(samples)} points from the point cloud")

    print("Calculating ground truth grasp labels ...")
    labels_time = time()
    labels = detector.getGroundTruth(mesh_cloud, samples, mesh_tree, camera_pose)
    labels_time = time() - labels_time
    labels = detector.getGroundTruth(mesh_cloud, samples, mesh_tree, camera_pose)
    num_pos = np.count_nonzero(labels)
    num_neg = np.prod(labels.shape) - num_pos
    print(f"Calculated {np.prod(labels.shape)} labels in {labels_time:.3f}s")
    print(f"labels: {labels.shape}, positives: {num_pos}, negatives: {num_neg}")

    # Detect grasp poses.
    detection_time = time()
    if args.fast:
        if args.thresh is not None:
            poses, scores = detector.detectGraspsThreshs(
                cloud, samples, camera_pose, grasp_threshs
            )
        elif args.best is not None:
            poses, scores = detector.detectGraspsBest(
                cloud, samples, camera_pose, grasp_threshs
            )
        mask = np.ones(len(poses), dtype=bool)
        detection_time = time() - detection_time
        print(f"Generated {len(poses)} grasp poses in {detection_time:.3f}s.")
    else:
        grasp_data = detector.detectGrasps(cloud, samples, camera_pose)
        detection_time = time() - detection_time
        print(
            f"Generated grasp_data with keys {grasp_data.keys()} in {detection_time}s."
        )
        cloud_tree = o3d.geometry.KDTreeFlann(cloud)
        poses, scores = detector.getGraspPoses(
            grasp_data, grasp_threshs, cloud, cloud_tree, samples
        )
        mask = scores > 0.9
        print([(X[:3, 3], s) for X, s in zip(poses, scores)])

    # Visualize the grasps.
    print(
        f"Object category: {object_set.objects[category_idx]}, instance: {instance_idx}"
    )
    if args.plot:
        # labels = np.ones(len(poses), dtype=bool)
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh_cloud, [1.0, 0.0, 0.0], opacity=0.1)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        # p.plotHands(poses[~mask], [1.0, 0.0, 0.0])
        p.plotHands(poses[mask], [0.0, 1.0, 0.0])
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle(f"Predicted grasp poses")
        p.show()

    # Visualize grasps poses colored by their score.
    if args.plot:
        valid_scores = scores["gc"][mask] if type(scores) == dict else scores[mask]
        min_score = np.min(valid_scores)
        max_score = np.max(valid_scores)
        greens = (valid_scores - min_score) / (max_score - min_score)
        colors = np.zeros((len(greens), 3))
        colors[:, 0] = 1.0 - greens
        colors[:, 1] = greens
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh_cloud, [1.0, 0.0, 0.0], opacity=0.1)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        p.plotHands(poses[mask], colors)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle(f"Predicted grasp poses (colored by score)")
        p.show()

    # Store the grasp poses.
    filename_out = f"grasps_{method_name}_{args.category}_{args.instance}"
    storeGrasps(
        object_set.objects[category_idx],
        instance,
        args.scale,
        camera_pose,
        scores,
        poses,
        cloud,
        method_name,
        filename_out,
    )

    # Predict handles.
    handles_time = time()
    handle_inliers = findHandles(poses, args.inliers)
    handles_time = time() - handles_time
    print(f"Found {len(handle_inliers)} handles in {handles_time:.3f}s")
    # if args.plot:
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh_cloud, [1.0, 0.0, 0.0], opacity=0.1)
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    for h in handle_inliers:
        color = list(np.random.rand(3))
        p.plotHands(poses[h], color)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose)
    p.setTitle(f"Predicted handles")
    p.show()

    # Convert to handle objects.
    handles = [Handle(x, poses) for x in handle_inliers]
    for h in handles:
        print(h.center, np.linalg.det(h.pose[:3, :3]))
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh_cloud, [1.0, 0.0, 0.0], opacity=0.1)
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    p.plotHands([x.pose for x in handles], np.random.rand(len(handles), 3))
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose)
    p.setTitle(f"Handle centers")
    p.show()

    exit()

    # Additional plots used for paper figures.
    # o3d.visualization.draw_geometries([mesh_cloud, cloud])
    best_poses = poses[:5]
    best_scores = scores[:5]
    p = HandsPlot(hand_geom)
    p.plotCloud(mesh_cloud, [1.0, 0.0, 0.0], opacity=0.1)
    p.plotCloud(cloud, [0.0, 0.0, 1.0])
    p.plotHands(best_poses, [1.0, 0.0, 0.0])
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose)
    p.setTitle(f"First 5 detections")
    p.show()

    exit()

    # Store the five grasps.
    storeGrasps(
        object_set.objects[category_idx],
        instance,
        args.scale,
        camera_pose,
        best_scores,
        best_poses,
        cloud,
        method_name,
        filename_out + "_5",
    )


if __name__ == "__main__":
    main()

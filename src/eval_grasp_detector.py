"""Evaluate a grasp detector on 3DNet objects.

Evaluate a grasp detector on a list of object categories from 3DNet. For each category,
iterates over a number of object instances, and for each object instance, iterates
over a number of viewpoints sampled from a sphere. For each viewpoint, grasps are
generated using a grasp detector and a confusion matrix is calculated that evaluates
the performance of the detector for that viewpoint at a set of thresholds. A NPY file
is stored for each object instance, containing the confusion matrices for each off the
viewpoints.

See this program's command line options to (optionally) only detect grasps and store
them in a file.

"""

import csv
import os
import sys
from argparse import ArgumentParser
from time import time

import numpy as np
import open3d as o3d
import tables
import torch as tp

from grasp_detector import GpdGraspDetector, MaGcGraspDetector, MaGraspDetector
from grasp_scene_data import GraspData, SceneData, storeGrasps
from hand_params import hand_params
from hands.hand_eval import AntipodalParams, HandGeometry
from method_evaluator import GcEvaluator, GpdEvaluator, GpdGcEvaluator, RotGcEvaluator
from networks.network import LenetLike, VggLike
from networks.resnet_mod import resnet18, resnet34
from object_set import ThreeDNetObjectSet
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import estimateNormals

os.environ["BLOSC_NTHREADS"] = "8"
filters = tables.Filters(complevel=9, complib="blosc:lz4")

antipodal_modes = {
    "onestage": {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": False,
    },
    "twostage": {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": True,
    },
    # "less1": {"extremal_thresh": 0.006, "friction_coeff": 40, "min_viable": 6},
    # "less2": {"extremal_thresh": 0.006, "friction_coeff": 60, "min_viable": 6},
    # "less3": {"extremal_thresh": 0.006, "friction_coeff": 30, "min_viable": 6},
    # "bowl": {"extremal_thresh": 0.006, "friction_coeff": 20, "min_viable": 6},
}

params = dict(
    # num_instances=1,
    # num_views=1,
    num_instances=10,
    # num_instances=5,
    num_views=5,
    # num_samples=2,
    num_samples=100,
    camera_distance=0.5,
    min_object_extent=0.01,
    # max_object_extent=[0.1, 0.2],
    max_object_extent=[0.07, 0.7],
    # max_object_extent=[0.2, 0.7],
    check_all_extents=False,
    cube_size=1.0,
    look_at=[0.0, 0.0, 0.0],
)

use_open_gl_frame = True

# thresh_step = 0.25
# thresh_step = 0.2
# thresh_step = 0.1
thresh_step = 0.05

# prop_params = {"image_size": 60, "image_type": "occupancy"}
prop_params = {"image_size": 60, "image_type": "depth"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], 3]
# grasp_img_shape = (60, 60, 3)
# grasp_img_shape = (60, 60, 12)
grasp_img_shape = (60, 60, 4)

models_dir = "../models/"
# qd_models_dir = models_dir + "qd774/strict_antipodal/"
# qd_models_dir = models_dir + "qd774/simulator2/"
# qd_models_dir = models_dir + "bowl/"
# qd_models_dir = models_dir + "bowl_face_normals/strict/"
# qd_models_dir = models_dir + "bowl_face_normals/less1/"
# qd_models_dir = models_dir + "one_stage_antipodal_bowl/"
# qd_gc_models_dir = models_dir + "bottle_face_normals/less1/gc/lenet/"
# qd_gc_models_dir = models_dir + "bottle_face_normals/strict/gc/lenet/"
# qd_gc_models_dir = models_dir + "bowl_small/strict/gc/"
# qd_gc_models_dir = models_dir + "bowl_small/less1/gc/"
# qd_gc_models_dir = models_dir + "bowl_small/less1/gc/vgg/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal_bowl/gc/"

# Bottle
# qd_models_dir = models_dir + "one_stage_antipodal/bottle/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/bottle/gc/"

# Bowl
# qd_models_dir = models_dir + "one_stage_antipodal/bowl_large_volume/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/bowl_large_volume/gc/"

# Bowl 10cm height
# qd_models_dir = models_dir + "one_stage_antipodal/bowl_large_volume/10cm_height/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/bowl_large_volume/10cm_height/gc/"

# qd_models_dir = models_dir + "TODO"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/12channels/bottle/gc/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/12channels/bowl/gc/"
# qd_gc_models_dir = "../models/one_stage_antipodal/bowl_large_volume/10cm_depth_10cm_height/"
# qd_gc_models_dir = "../models/one_stage_antipodal/4channels/bowl/gc/"
# qd_gc_models_dir = "../models/one_stage_antipodal/4channels/bottle/gc/"

# 12 channels
qd_models_dir = models_dir + "one_stage_antipodal/3dnet/"
# qd_gc_models_dir = models_dir + "one_stage_antipodal/3dnet/gc_12channels/lenet/"
qd_gc_models_dir = models_dir + "one_stage_antipodal/3dnet/gc_4channels/lenet/"

models_dir_prefix = "../models/one_stage_antipodal/3dnet/"

lenet_layout = {
    "conv": [32, 64],
    "fc": [512],
    "conv_filter_size": 5,
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
}
vgg_layout = {
    "use_dropout": False,
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}
net_cfg = {
    "qd-ant": {  # config for antipodal labels
        "network": VggLike,
        "layout": vgg_layout,
        "weights_paths": {
            "cls": qd_models_dir + "cls/vgg/model_ep_73_pre_0.8133.pwf",
            "rot": qd_models_dir + "rot/vgg/model_ep_299_pre_0.61395.pwf",
            "gc": qd_models_dir + "gc/vgg/checkpoints/best.pth",
        },
        "batch_size": 1024,
    },
    "qd-sim": {  # config for simulator labels
        "network": LenetLike,
        "layout": lenet_layout,
        "weights_paths": {  # config for simulator labels
            "cls": qd_models_dir + "cls/lenet/checkpoints/best.pth",
            "rot": qd_models_dir + "rot/lenet/checkpoints/best.pth",
            "gc": qd_models_dir + "gc/lenet/checkpoints/best.pth",
            # "gc": qd_models_dir + "gc/lenet_balanced/checkpoints/best.pth",
        },
        "batch_size": 1024,
    },
    "gc": {  # config for antipodal labels
        # "network": VggLike,
        # "layout": vgg_layout,
        "network": LenetLike,
        "layout": lenet_layout,
        "weights_paths": {
            # "gc": "../models/comparison/one_stage_antipodal/gc_4channels_lenet.pth",
            # "gc": "../models/comparison/one_stage_antipodal/gc_12channels_lenet.pth",
            # "gc": "../models/qd_grasp/4channels/lenet/checkpoints/best.pth",
            "gc": "../models/comparison/two_stage_antipodal/3dnet/gc_4channels_lenet.pth",
        },
        "batch_size": 1024,
    },
    "rot-gc": {
        "network": LenetLike,
        "layout": lenet_layout,
        "batch_size": 1024,
        "weights_paths": {
            # "rot": "../models/comparison/one_stage_antipodal/3dnet/rot_lenet.pth",
            # "gc": "../models/comparison/one_stage_antipodal/gc_4channels_lenet.pth",
            # "gc": "../models/comparison/one_stage_antipodal/gc_12channels_lenet.pth",
            # "rot": "../models/one_stage_antipodal/bbbcm/rot/checkpoints/best.pth",
            # "gc": "../models/one_stage_antipodal/bbbcm/gc/checkpoints/best.pth",
            # "rot": "../models/comparison/sim/rot_lenet.pth",
            # "gc": "../models/comparison/sim/gc_4channels_lenet.pth",
            # "rot": "../models/comparison/table_collisions/bbbcm/rot_lenet.pth",
            # "gc": "../models/comparison/table_collisions/bbbcm/gc_4channels_lenet.pth",
            # "rot": models_dir_prefix + "qd/rot/resnet18_3x3/checkpoints/best.pth",
            # "gc": models_dir_prefix
            # + "qd/gc_4channels/resnet18_3x3/checkpoints/best.pth",
            # "rot": models_dir_prefix + "qd/rot/resnet34_3x3/checkpoints/best.pth",
            # "gc": models_dir_prefix
            # + "qd/gc_4channels/resnet34_3x3/checkpoints/best.pth",
            # "gc": "../models/qd_grasp/4channels/lenet/checkpoints/best.pth",
            # "gc": "../models/qd_grasp/4channels/resnet18_3x3/checkpoints/best.pth",
            # "gc": "../models/qd_grasp/4channels/resnet34_3x3/checkpoints/best.pth",
            # "rot": "../models/table_collisions/3dnet/qd/rot/lenet/checkpoints/best.pth",
            # "gc": "../models/table_collisions/3dnet/qd/gc/lenet_wl/checkpoints/best.pth",
            # new models after fixing grasp image param
            # "rot": "../models/comparison/one_stage_antipodal/3dnet/rot_lenet.pth",
            # "gc": "../models/qd_grasp/4channels/lenet/checkpoints/best.pth",
            # "rot": "../models/table_collisions/bbbcm3/qd/rot/lenet/checkpoints/best.pth",
            # "gc": "../models/table_collisions/bbbcm3/qd/gc/lenet/checkpoints/best.pth",
            # "rot": "../models/comparison/sim/rot_lenet.pth",
            # "gc": "../models/sim/gc_4channels_new/lenet_wl/checkpoints/best.pth",
            # "rot": "../models/table_collisions/3dnet2/qd/rot/lenet/checkpoints/best.pth",
            # "gc": "../models/table_collisions/3dnet2/qd/gc/lenet/checkpoints/best.pth",
            # "rot": "../models/comparison/sim/rot_lenet.pth",
            # "gc": "../models/sim/gc_4channels_new/balanced/lenet/checkpoints/best.pth",
            "rot": "../models/comparison/two_stage_antipodal/3dnet/rot_lenet.pth",
            "gc": "../models/comparison/two_stage_antipodal/3dnet/gc_4channels_lenet.pth",
        },
    },
    "rot": {
        "network": LenetLike,
        "layout": lenet_layout,
        "batch_size": 1024,
        "weights_paths": {
            # "rot": "../models/comparison/two_stage_antipodal/3dnet/rot_lenet.pth",
            # "rot": "../models/table_coll/bbbcm/qd/rot/lenet/logs/checkpoints/best.pth",
            "rot": "../models/table_coll/bbbcm/qd/rot/resnet18/best.pth",
        },
    },
    "gpd": {
        "network": LenetLike,
        "layout": lenet_layout,
        # "weights_path": "../models/comparison/one_stage_antipodal/gpd_4channels_lenet.pth",
        # "weights_path": "../models/comparison/one_stage_antipodal/gpd_12channels_lenet.pth",
        # "weights_path": "../models/one_stage_antipodal/bbbcm/gpd/checkpoints/best.pth",
        # "weights_path": "../models/gpd/checkpoints/best.pth",
        # "weights_path": "../models/gpd/3dnet/checkpoints/best.pth",
        # "weights_path": "../models/comparison/one_stage_antipodal/3dnet/gpd_4channels_lenet.pth",
        "weights_path": "../models/comparison/two_stage_antipodal/3dnet/gpd_4channels_lenet.pth",
        "batch_size": 1024,
    },
}
net_cfg["gpd-gc"] = net_cfg["gpd"]

evaluators = {
    "gpd": GpdEvaluator,
    "gpd-gc": GpdGcEvaluator,
    "rot-gc": RotGcEvaluator,
    "gc": GcEvaluator,
    "rot": GcEvaluator,
}
models = {
    "lenet": LenetLike,
    "vgg": VggLike,
    "resnet18": resnet18,
    "resnet34": resnet34,
}

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def preprocessPointCloud(cloud, pose, voxel_size=0.003):
    cloud_out = cloud.voxel_down_sample(voxel_size)
    print(f"Downsampled cloud to {len(cloud_out.points)} points.")
    cloud_out = estimateNormals(cloud_out, pose[:3, 3])
    print(f"Estimated {len(cloud_out.normals)} surface normals.")
    return cloud_out


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Evaluate a grasp detector on an object set")
    parser.add_argument("objects_dir", help="path to object set directory")
    parser.add_argument(
        "categories_file", help="path to TXT file that lists the object categories"
    )
    parser.add_argument("results_dir", help="grasp pose detector")
    parser.add_argument("method", help="grasp pose detector")
    parser.add_argument(
        "--prohibited",
        help="path to a CSV file with prohibited object instances",
        default="",
    )
    parser.add_argument(
        "--detect",
        help="store grasps and network scores in a file (used for simulator labeling)",
        action="store_true",
    )
    parser.add_argument(
        "--score",
        help="store grasps, network scores, and ground truth in a file",
        action="store_true",
    )
    parser.add_argument(
        "-am",
        "--antipodal_mode",
        default="twostage",
        help="antipodal mode: <onestage>, <twostage>",
    )
    parser.add_argument(
        "-c",
        "--check_collisions",
        action="store_true",
        help="Check for collisions after detections",
    )
    parser.add_argument(
        "-ap", "--add_plane", action="store_true", help="add support plane to object"
    )
    parser.add_argument(
        "--net", help="network architecture (lenet, vgg, resenet18, resnet34"
    )

    return parser


def createGraspDetector(
    hand_geom, antipodal_params, method, prop_params, grasp_img_shape, net_cfg=None
):
    if method in ["gpd", "gpd-gc"]:
        detector = GpdGraspDetector(
            hand_geom, antipodal_params, grasp_img_shape, net_cfg["gpd"]
        )
    elif method in ["ma", "qd-ant", "qd-sim", "rot-gc", "rot"]:
        detector = MaGraspDetector(
            method,
            hand_geom,
            antipodal_params,
            prop_params,
            grasp_img_shape,
            net_cfg[method],
            use_open_gl_frame,
        )
    elif method in ["gc"]:
        detector = MaGcGraspDetector(
            method[: method.rfind("-")],
            hand_geom,
            antipodal_params,
            grasp_img_shape,
            net_cfg[method],
            use_open_gl_frame,
        )

    return detector


class InstanceProcessor:
    def __init__(
        self,
        object_set,
        detector,
        evaluator,
        method,
        out_dir,
        is_detecting,
        is_scoring,
        add_plane,
        check_collisions,
    ):
        self.object_set = object_set
        self.detector = detector
        self.evaluator = evaluator
        self.method = method
        self.out_dir = out_dir
        self.is_detecting = is_detecting
        self.is_scoring = is_scoring
        self.add_plane = add_plane
        self.check_collisions = check_collisions
        self.methods_no_preprocessing = "graspnet"

    def processInstance(
        self, category_idx: int, instance_idx: int, viewpoints: np.array
    ):
        # Create the mesh point cloud.
        category = self.object_set.objects[category_idx]
        mesh_path = self.object_set.get([category_idx, instance_idx])
        instance = mesh_path[mesh_path.rfind("/") + 1 : mesh_path.rfind(".")]
        print(f"Instance {instance_idx + 1}/{params['num_instances']}: {instance}")
        scene = SingleObjectScene(
            mesh_path, params["min_object_extent"], params["max_object_extent"]
        )
        scene.resetToRandomScale(params["check_all_extents"])
        mesh_cloud, _ = scene.createCompletePointCloud(add_plane=self.add_plane)
        # o3d.visualization.draw_geometries([mesh_cloud], "mesh_cloud", 640, 480)
        plane_z = None
        if self.add_plane:
            plane_z = scene.scene.get_pose(scene.table_node)[2, 3]
            print(f"Added table plane to scene at z = {plane_z:.3f}")

        # Iterate over viewoints for each instance.
        for view_idx, viewpt in enumerate(viewpoints):
            print(f"View {view_idx + 1}/{params['num_views']}")
            data = self.processViewpoint(viewpt, scene, plane_z)
            scene_data = SceneData(
                category,
                instance,
                scene.scale,
                data["camera_pose"],
                np.asarray(data["cloud"].points),
            )
            suffix = ""

            # Check detected grasps for collisions.
            if self.check_collisions:
                cloud_tree = o3d.geometry.KDTreeFlann(data["cloud"])
                is_free = self.detector.hands_gen.generateHands(
                    data["cloud"], data["samples"], cloud_tree, data["camera_pose"]
                )[0][:, :, 0]
                is_free = is_free == 1
                suffix = "coll_"
            else:
                is_free = None

            suffix += f"{self.method}_{category}_{instance}_{view_idx}.npy"

            if self.is_detecting:
                fname = "grasps_" + suffix
                self.storeGrasps(
                    scene_data, data["cloud"], data["samples"], data["grasps"], fname
                )
            elif self.is_scoring:
                fname = "labeled_grasps_" + suffix
                self.storeGraspsScoresLabels(
                    mesh_cloud,
                    data["camera_pose"],
                    data["cloud"],
                    data["samples"],
                    data["grasps"],
                    fname,
                )
            else:
                fname = "eval_" + suffix
                self.storeEval(
                    mesh_cloud,
                    scene_data.camera_pose,
                    data["samples"],
                    data["grasps"],
                    fname,
                    is_free,
                )

    def processViewpoint(self, viewpoint: np.array, scene, plane_z=None):
        # Get point cloud for this viewpoint.
        camera_pose = calcLookAtPose(viewpoint, params["look_at"])
        cloud = scene.createPointCloudFromView(camera_pose)

        # Preprocess the point cloud.
        if self.method not in self.methods_no_preprocessing:
            cloud = preprocessPointCloud(cloud, camera_pose)

        if self.add_plane:
            idxs = np.nonzero(np.asarray(cloud.points)[:, 2] > plane_z + 0.005)[0]
            sample_cloud = cloud.select_by_index(idxs)
            # sample_cloud = cloud.select_down_sample(idxs)
        else:
            sample_cloud = cloud
        samples = sampleFromCube(
            sample_cloud, params["cube_size"], params["num_samples"]
        )
        # frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.05)
        # o3d.visualization.draw_geometries([sample_cloud, frame], "sample_cloud", 640, 480)
        # o3d.visualization.draw_geometries([cloud], "cloud", 640, 480)

        # Detect grasps for this viewpoint.
        grasps = self.detector.detectGrasps(cloud, samples, camera_pose)

        return {
            "camera_pose": camera_pose,
            "cloud": cloud,
            "samples": samples,
            "grasps": grasps,
        }

    def storeGrasps(self, scene_data, cloud, samples, grasps, fname, store_cloud=False):
        poses = self.indicesToPoses(cloud, samples, scene_data.camera_pose)
        scores = grasps["scores"]
        if type(scores) == np.array or type(scores) == tp.Tensor:
            scores = scores.flatten()
        elif type(scores) == dict:
            scores = {k: scores[k].flatten() for k in scores}
        labels = -1 * np.ones(len(poses))
        grasp_data = GraspData(poses, scores, labels, self.method)
        if not store_cloud:
            scene_data.cloud = np.empty((0, 3), dtype=float)
        storeGrasps(scene_data, grasp_data, self.out_dir + fname)

    def storeGraspsScoresLabels(
        self, mesh_cloud, camera_pose, cloud, samples, grasps, fname
    ):
        # Calculate the ground truth.
        mesh_tree = o3d.geometry.KDTreeFlann(mesh_cloud)
        labels = self.detector.getGroundTruth(
            mesh_cloud, samples, mesh_tree, camera_pose
        )

        # Convert indices to grasp poses.
        poses = self.indicesToPoses(cloud, samples, camera_pose)

        # Store the labeled and scored grasps.
        scores = grasps["scores"]
        if type(scores) == np.array or type(scores) == tp.Tensor:
            scores = scores.flatten()
        elif type(scores) == dict:
            scores = {k: scores[k].flatten() for k in scores}
        data = GraspData(poses, scores, labels, self.method)
        np.save(self.out_dir + fname, vars(data))

    def storeEval(self, mesh_cloud, camera_pose, samples, grasps, fname, is_free=None):
        # Calculate the ground truth.
        time_gt = time()
        num_grasps = len(samples) * self.detector.hands_gen.getNumHandsPerSample()
        print(f"Calculating ground truth for {num_grasps} grasps ...")
        mesh_tree = o3d.geometry.KDTreeFlann(mesh_cloud)
        labels = self.detector.getGroundTruth(
            mesh_cloud, samples, mesh_tree, camera_pose
        )
        print(f"  runtime: {time() - time_gt}")
        num_pos = np.count_nonzero(labels)
        num_neg = np.prod(labels.shape) - num_pos
        print(f"labels: {labels.shape}, positives: {num_pos}, negatives: {num_neg}")

        # Evaluate the detector and store the results.
        results = self.evaluator.evalMethod(grasps, labels, is_free)
        np.save(self.out_dir + fname, results)
        print(f"Stored evaluation results at: {self.out_dir + fname}")

    def indicesToPoses(self, cloud, samples, camera_pose, frames=[]):
        tree = o3d.geometry.KDTreeFlann(cloud)
        n = len(samples)
        m = self.detector.hands_gen.getNumHandsPerSample()
        rows = np.repeat(np.arange(n), m)
        cols = np.tile(np.arange(m), n)
        idxs = np.vstack((rows, cols))
        self.detector.hands_gen.camera_pose = camera_pose
        poses = self.detector.hands_gen.indicesToPoses(
            cloud, tree, samples, {"indices": idxs, "frames": frames}
        )
        return poses

    def storeGroundTruth(self, scene_data, samples, labels, view_idx):
        fname = f"{scene['category']}_{scene['instance']}_{view_idx}.h5"
        with tables.open_file(self.out_dir + fname, "w") as f:
            f.create_carray(
                "/", "samples", tables.Float32Atom, obj=samples, filters=filters
            )
            f.create_carray("/", "labels", tables.BoolAtom, obj=labels, filters=filters)
            f.create_carray(
                "/",
                "camera_pose",
                tables.Float32Atom,
                obj=scene_data.camera_pose,
                filters=filters,
            )
            f.create_carray(
                "/",
                "scale",
                tables.Float32Atom,
                obj=np.array([scene_data.scale]),
                filters=filters,
            )
        print(f"Stored labels at: {self.out_dir + fname}")


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()
    print(f"Command line arguments:\n{args}")

    # Setup the object dataset.
    object_set = ThreeDNetObjectSet(args.objects_dir, args.categories_file, True)

    # Read in CSV that tells which object instances are prohibited for each category.
    if len(args.prohibited) > 0:
        with open(args.prohibited, "r") as f:
            prohibited_instances = [
                r[:2] for r in csv.reader(f, delimiter=",") if r[0] != "category"
            ]
        print("Prohibited object instances:\n", prohibited_instances)
        object_set.setProhibitedObjectInstances(prohibited_instances)

    if args.net is not None:
        for k in net_cfg:
            net_cfg[k]["network"] = models[args.net]

    # Setup the grasp detector and the method evaluator.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_modes[args.antipodal_mode])
    detector = createGraspDetector(
        hand_geom, antip_prms, args.method, prop_params, grasp_img_shape, net_cfg
    )
    evaluator = evaluators[args.method](thresh_step)
    processor = InstanceProcessor(
        object_set,
        detector,
        evaluator,
        args.method,
        args.results_dir,
        args.detect,
        args.score,
        args.add_plane,
        args.check_collisions,
    )

    num_categories = len(object_set.objects)

    # Sample uniform view points on a sphere.
    viewpoints = np.random.normal(size=(params["num_views"], 3))
    viewpoints /= np.linalg.norm(viewpoints, axis=1)[:, np.newaxis]
    viewpoints *= params["camera_distance"]
    if args.add_plane:
        viewpoints[:, 2] = np.abs(viewpoints[:, 2])

    # Iterate over object categories.
    for category_idx in range(num_categories):
        category = object_set.objects[category_idx]
        print(f"Category {category_idx + 1}/{num_categories}: {category}")

        # Iterate over object instances for each category.
        for instance_idx in range(params["num_instances"]):
            processor.processInstance(category_idx, instance_idx, viewpoints)


if __name__ == "__main__":
    main()

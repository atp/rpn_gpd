import torch.nn as nn
from torchvision.models import resnet


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


def make_layer(inplanes, planes, blocks, stride=1, norm_layer=None):
    downsample = None
    if norm_layer is None:
        norm_layer = nn.BatchNorm2d
    if inplanes != planes:
        downsample = nn.Sequential(
            conv1x1(inplanes, planes, stride), norm_layer(planes),
        )

    layers = []
    layers.append(resnet.BasicBlock(inplanes, planes, downsample=downsample))
    for _ in range(1, blocks):
        layers.append(resnet.BasicBlock(planes, planes))

    return nn.Sequential(*layers)


resnet_light = nn.Sequential(
    nn.Conv2d(9, 64, 5),
    nn.BatchNorm2d(64),
    nn.ReLU(),
    nn.MaxPool2d(2, 2),
    make_layer(64, 64, 2),
    make_layer(64, 128, 2),
    make_layer(128, 256, 2),
    make_layer(256, 512, 2),
    nn.AdaptiveAvgPool2d((1, 1)),
    nn.Flatten(1, -1),
    nn.Linear(512, 2),
)


class Resnet(nn.Module):
    def __init__(self, input_shape, num_classes):
        super(Resnet, self).__init__()
        self.model = nn.Sequential(
            nn.Conv2d(input_shape[0], 64, 5),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, 2),
            make_layer(64, 64, 2),
            make_layer(64, 128, 2),
            make_layer(128, 256, 2),
            make_layer(256, 512, 2),
            nn.AdaptiveAvgPool2d((1, 1)),
            nn.Flatten(1, -1),
            nn.Linear(512, num_classes),
        )

    def forward(self, x):
        x = self.model(x)
        return x


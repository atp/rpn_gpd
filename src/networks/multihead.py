import math

import torch
import torch.nn as nn
import torch.nn.functional as F


class MultiheadLenet(nn.Module):
    def __init__(
        self,
        in_shape,
        out_shape,
        conv_filter_size,
        conv,
        fc,
        use_batchnorm,
        replace_pool_with_conv=False,
    ):
        num_network_outputs = 2
        super(MultiheadLenet, self).__init__()

        conv = [in_shape[0]] + conv
        modules = []
        for i in range(1, len(conv)):
            modules.append(nn.Conv2d(conv[i - 1], conv[i], conv_filter_size))
            if use_batchnorm:
                modules.append(nn.BatchNorm2d(conv[i]))
            modules.append(nn.ReLU())
            if replace_pool_with_conv:
                modules.append(nn.Conv2d(conv[i], conv[i], 2, stride=2))
            else:
                modules.append(nn.MaxPool2d(2, 2))
        self.features = nn.Sequential(*modules)

        # Calculate depth of feature map.
        with torch.no_grad():
            size_out = self.features(torch.zeros((1,) + in_shape)).shape[-1]
        print(f"[LenetLike] size of output from <features> part: {size_out}")

        fc = [conv[-1] * size_out * size_out] + fc + [num_network_outputs]
        heads = nn.ModuleList()
        for i in range(out_shape):
            modules = []
            for i in range(1, len(fc)):
                modules.append(nn.Linear(fc[i - 1], fc[i]))
                if i < len(fc) - 1:
                    modules.append(nn.ReLU())
                    if use_batchnorm:
                        modules.append(nn.BatchNorm1d(fc[i]))
            heads.append(nn.Sequential(*modules))
        self.heads = heads

    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, x.shape[1] * x.shape[2] * x.shape[3])
        y = [h(x) for h in self.heads]
        return y


class MultiheadVgg(nn.Module):
    def __init__(self, in_shape, out_shape, conv_filter_size, conv, fc, use_dropout):
        num_network_outputs = 2
        super(MultiheadVgg, self).__init__()

        conv = [in_shape[0]] + conv
        modules = []
        for i in range(1, len(conv)):
            p = int(math.floor((conv_filter_size - 1) / 2))
            modules.append(nn.Conv2d(conv[i - 1], conv[i], conv_filter_size, padding=p))
            modules.append(nn.ReLU())
            modules.append(nn.Conv2d(conv[i], conv[i], conv_filter_size, padding=p))
            modules.append(nn.ReLU())
            modules.append(nn.MaxPool2d(2, 2))
            if use_dropout:
                modules.append(nn.Dropout(0.2))
        self.features = nn.Sequential(*modules)

        # Calculate depth of feature map.
        with torch.no_grad():
            size_out = self.features(torch.zeros((1,) + in_shape)).shape[-1]
        print(f"[LenetLike] size of output from <features> part: {size_out}")

        fc = [conv[-1] * size_out * size_out] + fc + [num_network_outputs]
        heads = nn.ModuleList()
        for i in range(out_shape):
            modules = []
            for i in range(1, len(fc)):
                modules.append(nn.Linear(fc[i - 1], fc[i]))
                if i < len(fc) - 1:
                    modules.append(nn.ReLU())
                    # if use_batchnorm:
                    # modules.append(nn.BatchNorm1d(fc[i]))
            heads.append(nn.Sequential(*modules))
        self.heads = heads

    def forward(self, x):
        x = self.features(x)
        x = x.view(-1, x.shape[1] * x.shape[2] * x.shape[3])
        y = [h(x) for h in self.heads]
        return y

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


class MultiBranchNet(nn.Module):
    def __init__(self, input_shape, num_classes, conv_filter_size, conv, fc, \
                 use_batchnorm):
        super(MultiBranchNet, self).__init__()

        # Create a separate stream (subnetwork) for each input channel.
        features = []
        conv = [3] + conv
        for i in range(int(input_shape[0]/3)):
            modules = []
            for j in range(1,len(conv)):
                modules.append(nn.Conv2d(conv[j-1], conv[j], conv_filter_size))
                if use_batchnorm:
                    modules.append(nn.BatchNorm2d(conv[j]))
                modules.append(nn.ReLU())
                modules.append(nn.MaxPool2d(2, 2))
            features.append(nn.Sequential(*modules))
        self.features = nn.ModuleList(features)

        # Calculate depth of feature map.
        with torch.no_grad():
            x = torch.zeros((1,3) + input_shape[1:])
            size_out = self.features[0](x).shape[-1]
#       print(f'size_out: {size_out}')

        modules = []
#        fc = [len(self.features) * conv[-1] * size_out**2] + fc + [num_classes]
        fc = [conv[-1] * size_out**2] + fc + [num_classes]
#        print(f'fc: {fc}')
        for i in range(1,len(fc)):
            modules.append(nn.Linear(fc[i-1], fc[i]))
            if use_batchnorm and i < len(fc) - 1:
                modules.append(nn.BatchNorm1d(fc[i]))
        self.classifier = nn.Sequential(*modules)

    def forward(self, x):
        f = [self.features[i](x[:,i*3:i*3+3]) for i in range(len(self.features))]
        feat_vec_len = np.prod(f[0].shape[1:4])
        f = [x.view(-1, feat_vec_len) for x in f]
#        print(f'f: {f[0].shape}, feat_vec_len: {feat_vec_len}')
#        f = torch.cat((f),1)
#        f,_ = torch.max(torch.Tensor(torch.cat([f[0],f[1],f[2]],1), 1)
        f = torch.stack((f),2)
#        print(f'f: {f.shape}')
        f = F.max_pool1d(f, 3)
#        print(f'f: {f.shape}')
        f = f.view(f.shape[:-1])
#        print(f'f: {f.shape}')
        y = self.classifier(f)

        return y

# Network with additional sigmoid on top to enable thresholding.
class MultiBranchNetWithSigmoid(MultiBranchNet):
    def __init__(self, input_channels, output_channels, layer_sizes):
        super(MultiBranchNet, self).__init__(input_channels, output_channels,\
                                             layer_sizes)

    def forward(self, x):
        x = super().forward(x)
        x = torch.sigmoid(x)
        return x

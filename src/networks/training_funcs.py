import os

import numpy as np
import torch

from learning import Metrics


def eval(model, loader, device, thresh=0.5):
    print('Testing the network on the test data ...')
    model.eval()
    avg_metrics = Metrics()

    with torch.no_grad():
        for data in loader:
            inputs, labels = data[0].to(device), data[1].to(device)

            outputs = model(inputs)
            if outputs.shape[1] == 2:
                _, predicted = torch.max(outputs.data, 1)
            else:
                outputs = torch.sigmoid(outputs)
                predicted = (outputs > thresh).long()
                labels = labels.type(outputs.dtype)

            act = np.asarray(labels.cpu()).flatten()
            pred = np.asarray(predicted.cpu()).flatten()
            metrics = Metrics(act, pred)
            avg_metrics.concat(metrics)

    print(f'Test set results:')
    print(avg_metrics)

    return avg_metrics

# Train a model on a dataset.
def train(model, criterion, optimizer, data, device):
    inputs, labels = data[0].to(device), data[1].to(device)
    inputs = inputs.to(device)
    labels = labels.to(device)

    # Reset the parameter gradients.
    optimizer.zero_grad()

    outputs = model(inputs)
    loss = criterion(outputs, labels.type(outputs.dtype))
    loss.backward()
    optimizer.step()

    return loss

def trainLoop(train_loader, test_loader, model, criterion, optimizer, \
              scheduler, device, epochs, model_dir, print_step):
    # Evaluate the network on the test set.
    metrics = eval(model, test_loader, device)

    prev_lr = 1

    for epoch in range(epochs):
        running_loss = 0.0
        model.train()

        for param_group in optimizer.param_groups:
            if param_group['lr'] < prev_lr:
                prev_lr = param_group['lr']
                print('lr:', prev_lr)

        # Train the network on all batches.
        for i, batch in enumerate(train_loader):
            loss = train(model, criterion, optimizer, batch, device)
            running_loss += loss
            if i % print_step == print_step - 1:
                print('epoch: %d/%d, batch: %5d/%5d, loss: %.5f' \
                      % (epoch + 1, epochs, i + 1, len(train_loader), \
                         running_loss / print_step))
                running_loss = 0.0

        # Evaluate the network on the test set.
        metrics = eval(model, test_loader, device)

        checkpoint = 'model_' + str(np.around(metrics.precision(),5)) + '.pwf'
        model_path = os.path.join(model_dir, checkpoint)
        torch.save(model.state_dict(), model_path)

        scheduler.step()

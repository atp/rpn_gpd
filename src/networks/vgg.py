import torchvision.models as models
from torch import flatten, nn


class VGG(nn.Module):

    def __init__(self, features, num_classes):
        super(VGG, self).__init__()
        self.features = features
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))
        self.classifier = nn.Sequential(
#            nn.Linear(64 * 7 * 7, 1024),
#            nn.Linear(128 * 7 * 7, 1024),
            nn.Linear(128 * 7 * 7, 512),
            nn.ReLU(True),
            nn.Dropout(),
#            nn.Linear(1024, 1024),
            nn.Linear(512, 512),
            nn.ReLU(True),
            nn.Dropout(),
            nn.Linear(512, num_classes),
#            nn.Linear(1024, num_classes),
        )

    def forward(self, x):
        x = self.features(x)
#        print('[forward] x:', x.shape)
        x = self.avgpool(x)
        x = flatten(x, 1)
        x = self.classifier(x)
        return x

cfgs = {
    'F': [32,'M',64,'M',128,'M'],
    'G': [32,32,32,'M',64,64,64,'M',128,128,128,'M'],
    'H': [32,32,'M',64,64,'M'],
}

def vgg13(num_classes):
    return VGG(models.vgg.make_layers(cfgs['F']), num_classes)

#vgg = VGG(models.vgg.make_layers(cfgs['F']), 1331)
#print(vgg)

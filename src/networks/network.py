import math

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models.resnet import BasicBlock, conv1x1


class ResnetLike(nn.Module):
    def __init__(self, in_shape, out_shape):
        # def __init__(self, in_shape, out_shape, conv_filter_size, conv, fc):
        super(ResnetLike, self).__init__()
        self.conv1 = nn.Conv2d(in_shape[0], 64, kernel_size=5, stride=1, bias=False)
        self.bn1 = nn.BatchNorm2d(64)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=2, stride=2)
        # self.layer1 = BasicBlock(32, 32)
        # self.layer2 = BasicBlock(32, 32)
        # self.layer3 = BasicBlock(32, 32)
        self.layer1 = self.makeLayer(64, 128, True)
        self.layer2 = self.makeLayer(128, 256, True)
        self.layer3 = self.makeLayer(256, 512, True)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(32, out_shape)

    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)

        return x

    def makeLayer(self, inplanes, planes, downsample=None, stride=1):
        block_expansion = 1
        norm_layer = nn.BatchNorm2d
        if downsample:
            downsample = nn.Sequential(
                conv1x1(inplanes, planes * block_expansion, stride),
                norm_layer(planes * block_expansion),
            )
        layers = []
        layers.append(
            BasicBlock(inplanes, planes, stride, downsample, norm_layer=norm_layer)
        )

        return nn.Sequential(*layers)


class VggLike(nn.Module):
    def __init__(self, in_shape, out_shape, conv_filter_size, conv, fc, use_dropout):
        super(VggLike, self).__init__()
        conv = [in_shape[0]] + conv
        modules = []
        for i in range(1, len(conv)):
            p = int(math.floor((conv_filter_size - 1) / 2))
            modules.append(nn.Conv2d(conv[i - 1], conv[i], conv_filter_size, padding=p))
            modules.append(nn.ReLU())
            modules.append(nn.Conv2d(conv[i], conv[i], conv_filter_size, padding=p))
            modules.append(nn.ReLU())
            modules.append(nn.MaxPool2d(2, 2))
            if use_dropout:
                modules.append(nn.Dropout(0.2))
        self.features = nn.Sequential(*modules)

        # Calculate depth of feature map.
        with torch.no_grad():
            size_out = self.features(torch.zeros((1,) + in_shape)).shape[-1]
        print(f"[VggLike] size of output from <features> part: {size_out}")

        modules = []
        fc = [conv[-1] * size_out * size_out] + fc + [out_shape]
        for i in range(1, len(fc)):
            modules.append(nn.Linear(fc[i - 1], fc[i]))
            if i < len(fc) - 1:
                modules.append(nn.ReLU())
                if use_dropout:
                    modules.append(nn.Dropout(0.2))
                    # modules.append(nn.Dropout(0.3))
        self.classifier = nn.Sequential(*modules)

    def forward(self, x):
        x = self.features(x)
        #        print(x.shape)
        x = x.view(-1, x.shape[1] * x.shape[2] * x.shape[3])
        x = self.classifier(x)
        return x


class VggLikeWithSigmoid(VggLike):
    """VGG-like network with an additional sigmoid at the end to enable thresholding.
    """

    def forward(self, x):
        """The network's forward pass.

        Same method as in `VggLike`, but with a sigmoid applied on the network outputs.

        Args:
            x (torch.tensor): the input tensor.

        Returns:
            torch.tensor: the output tensor.
        """
        x = super().forward(x)
        x = torch.sigmoid(x)
        return x


class LenetLike(nn.Module):
    def __init__(
        self,
        in_shape,
        out_shape,
        conv_filter_size,
        conv,
        fc,
        use_batchnorm,
        replace_pool_with_conv=False,
    ):
        super(LenetLike, self).__init__()

        conv = [in_shape[0]] + conv
        modules = []
        for i in range(1, len(conv)):
            modules.append(nn.Conv2d(conv[i - 1], conv[i], conv_filter_size))
            if use_batchnorm:
                modules.append(nn.BatchNorm2d(conv[i]))
            modules.append(nn.ReLU())
            if replace_pool_with_conv:
                modules.append(nn.Conv2d(conv[i], conv[i], 2, stride=2))
            else:
                modules.append(nn.MaxPool2d(2, 2))
        self.features = nn.Sequential(*modules)

        # Calculate depth of feature map.
        with torch.no_grad():
            size_out = self.features(torch.zeros((1,) + in_shape)).shape[-1]
        print(f"[LenetLike] size of output from <features> part: {size_out}")

        modules = []
        fc = [conv[-1] * size_out * size_out] + fc + [out_shape]
        # fc = [conv[-1] * size_out * size_out] + fc + [1] # required for focal loss
        for i in range(1, len(fc)):
            modules.append(nn.Linear(fc[i - 1], fc[i]))
            if i < len(fc) - 1:
                modules.append(nn.ReLU())
                if use_batchnorm:
                    modules.append(nn.BatchNorm1d(fc[i]))
        self.classifier = nn.Sequential(*modules)

    def forward(self, x):
        x = self.features(x)
        #        print(x.shape)
        x = x.view(-1, x.shape[1] * x.shape[2] * x.shape[3])
        x = self.classifier(x)
        return x


# Network with additional sigmoid on top to enable thresholding.
class LenetLikeWithSigmoid(LenetLike):
    def forward(self, x):
        x = super().forward(x)
        x = torch.sigmoid(x)
        return x


# params = {
# "out_shape": 2,
# "in_shape": (4, 60, 60),
# "conv": [128, 128, 128],
# "fc": [512],
# "conv_filter_size": 3,
# }
# r = ResnetLike(**params)
# print(r)
# x = torch.randn((1,) + params["in_shape"])
# y = r(x)
# print(f"y: {y.shape}, y: {y}")


from torch import flatten, nn
from torchvision.models.vgg import make_layers


class Lenet(nn.Module):

    def __init__(self, features, num_classes):
        super(Lenet, self).__init__()
        self.features = features
        # print(f'[Lenet] features: {type(features)}')
        # print(f'{features[-4]}')
        channels = features[-4].out_channels
        self.classifier = nn.Sequential(
             nn.Linear(channels * 15 * 15, 256),
#            nn.Linear(64 * 15 * 15, 512),
#            nn.Linear(128 * 7 * 7, 512),
#            nn.Linear(128 * 7 * 7, 256),
            # nn.Linear(32 * 7 * 7, 256),
            nn.ReLU(True),
            nn.Dropout(),
#            nn.Linear(256, 256),
#            nn.ReLU(True),
#            nn.Dropout(),
            nn.Linear(256, num_classes),
        )

    def forward(self, x):
        x = self.features(x)
        # print(f'[forward] x: {x.shape}')
        x = flatten(x, 1)
        x = self.classifier(x)
        return x

cfgs = {
    'A': [32,'M',64,'M'],
    'B': [32,'M',64,'M',128,'M'],
    'C': [12,12,12,'M',24,24,24,'M',32,32,32,'M']
}

def lenet5(num_classes):
    return Lenet(make_layers(cfgs['A']), num_classes)

def lenet5_bn(num_classes):
    return Lenet(make_layers(cfgs['A'], True), num_classes)

def lenet6_bn(num_classes):
    return Lenet(make_layers(cfgs['B'], True), num_classes)

def lenet7_bn(num_classes):
    return Lenet(make_layers(cfgs['C'], True), num_classes)

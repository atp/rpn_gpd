"""Store data about scenes and grasps.
"""

from dataclasses import dataclass, field

import numpy as np


@dataclass
class SceneData:
    category: str = ""
    instance: str = ""
    scale: float = 1.0
    camera_pose: np.array = np.eye(4)
    cloud: np.array = np.empty((0, 3), dtype=float)

    @classmethod
    def fromFile(cls, fpath: str):
        data = np.load(fpath, allow_pickle=True, encoding="latin1").item()
        scene_data = SceneData()
        scene_data.category = data["category"]
        scene_data.instance = data["instance"]
        scene_data.scale = data["scale"]
        scene_data.camera_pose = data["camera_pose"]
        scene_data.cloud = data["cloud"]

        return scene_data


@dataclass
class GraspData:
    grasp_poses: list = field(default_factory=list)
    scores: list = field(default_factory=list)
    labels: list = field(default_factory=list)
    method: str = ""
    db_indices: np.array = np.empty((2, 0), dtype=float)

    @classmethod
    def fromFile(cls, fpath: str, labels_key: str = "labels_majority"):
        data = np.load(fpath, allow_pickle=True, encoding="latin1").item()
        grasp_data = GraspData()
        if "grasp_poses" in data.keys():
            grasp_data.grasp_poses = data["grasp_poses"]
        if "scores" in data.keys():
            grasp_data.scores = data["scores"]
        if "labels" in data.keys():
            grasp_data.labels = data[labels_key]
        if "method" in data.keys():
            grasp_data.method = data["method"]
        if "db_indices" in data.keys():
            grasp_data.db_indices = data["db_indices"]

        return grasp_data


def storeGrasps(scene_data: SceneData, grasp_data: GraspData, fpath: str):
    data = vars(scene_data)
    data.update(vars(grasp_data))
    np.save(fpath, data)
    print(f"Stored grasp poses at: {fpath}")

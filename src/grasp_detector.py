from abc import ABC, abstractmethod
from functools import partial
from time import time

import numpy as np
import open3d as o3d
import torch as tp

from grasp_data_generator import GraspDataGeneratorGPD, GraspDataGeneratorMA
from grasp_image import GraspImageGenerator
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.hand_eval import HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from networks.resnet_mod import resnet18, resnet34
from utils.cloud_utils import numpyToCloud


class GraspDetector(ABC):
    """Abstract base class for grasp detectors."""

    @abstractmethod
    def detectGrasps(self, cloud, samples=[]):
        """Detect grasps in a point cloud.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array, optional): n x 3 points at which detect grasps.

        Returns:
            dict: grasp data.
        """
        raise NotImplementedError("This is an abstract method.")


class LearningGraspDetector(GraspDetector):
    """Grasp detector with at least one deep learning component."""

    def __init__(self, hands_gen, batch_size):
        """Construct a grasp detector with a machine learning component.

        Args:
            hands_gen (hands.HandsGenerator): the grasp proposal generator.
            batch_size (?): the batch size(s) for the deep learning component(s).
        """
        self.hands_gen = hands_gen
        self.device = tp.device("cuda:0" if tp.cuda.is_available() else "cpu")
        self.batch_size = batch_size
        print("device:", self.device)

    def getGroundTruth(self, mesh, samples, tree, camera_pose):
        """Get the ground truth grasps.

        Args:
            mesh (d): the point cloud corresponding to the object mesh.
            samples (np.array, optional): n x 3 points at which to look for grasps.
            tree (o3d.geometry.KDTreeFlann): the KD tree for the mesh point cloud.

        Returns:
            np.array: ground truth labels for each grasp pose.
        """
        hands, _ = self.hands_gen.generateHands(mesh, samples, tree, camera_pose)
        labels = np.zeros(hands.shape[:2])
        labels[(hands[:, :, 0] == 1) & (hands[:, :, 1] == 1)] = 1
        return labels

    def createNet(self, network, layout, weights_path, input_shape, num_classes):
        """Create a neural network and load weights from pretraining.

        Args:
            network (?): the type of network.
            layout (dict): the laypout of the network.
            weights_path (str): path to the weights file from pretraining.
            input_shape (tuple): the input dimensions for the network.
            num_classes (int): the number of classes (network outputs).

        Returns:
            ?: the network.
        """
        if network.__name__ in ["resnet18", "resnet34"]:
            model = network(in_channels=input_shape[0], num_classes=num_classes)
        else:
            model = network(input_shape, num_classes, **layout)

        checkpoint = tp.load(weights_path)
        if type(checkpoint) == dict:
            # model.load_state_dict(checkpoint["model_state_dict"])
            model.load_state_dict(checkpoint["model_state_dict"], strict=False)
        else:
            model.load_state_dict(checkpoint)
        model.to(self.device)
        model.eval()
        print(f"Created network:\n{model}")
        print("Loaded parameters for network from:", weights_path)

        return model

    def runModel(self, model, loader, class_idx=1):
        """Run a neural network model on given inputs.

        Args:
            model (?): the neural network model.
            loader (tp.data.Data): the inputs.
            class_idx (int, optional): the class index.

        Returns:
            tp.tensor: the outputs of the neural network.
        """
        if class_idx >= 0:
            outputs = tp.empty((0,))
        else:
            outputs = tp.empty((0, self.hands_gen.num_hand_orientations))
        # print("Loading data ...")

        with tp.no_grad():
            for i, data in enumerate(loader, 0):
                # if i % print_step == print_step - 1:
                # print(f'Processing batch {i+1}/{max_batches} ...')
                inputs = data[0].to(self.device)
                # print(i, len(loader), "inputs:", inputs.shape, \
                # str((inputs.element_size() * inputs.nelement())/(2**20)) + "MB")
                if class_idx >= 0:
                    out = model(inputs)[:, class_idx]
                else:
                    out = model(inputs)

                outputs = tp.cat((outputs, out.cpu()))

        outputs = tp.sigmoid(outputs)
        print("outputs:", outputs.shape)

        return outputs


class GpdGraspDetector(LearningGraspDetector):
    """GPD Grasp detector."""

    def __init__(self, hand_geom, antipodal_params, grasp_img_shape, net_cfg):
        """Construct a GPD grasp detector.

        Args:
            hand_geom ([TODO:type]): [TODO:description]
            antipodal_params ([TODO:type]): [TODO:description]
            grasp_img_shape ([TODO:type]): [TODO:description]
            net_cfg ([TODO:type]): [TODO:description]
        """
        hands_gen = GpdHandsGenerator(hand_geom, antipodal_params)
        super().__init__(hands_gen, net_cfg["batch_size"])
        self.grasp_data_gen = GraspDataGeneratorGPD(grasp_img_shape)
        input_shape = (grasp_img_shape[-1],) + grasp_img_shape[:-1]
        self.model = self.createNet(
            net_cfg["network"],
            net_cfg["layout"],
            net_cfg["weights_path"],
            input_shape,
            2,
        )

    def detectGrasps(self, cloud, samples, camera_pose):
        """Detect grasps in a point cloud.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array, optional): n x 3 points at which to detect grasps.

        Returns:
            dict: grasp data.
        """
        grasp_data = self.grasp_data_gen.createImageData(cloud, samples, camera_pose)
        time_predict = time()
        data = {
            "proposals": grasp_data["proposals"],
            "scores": self.runDetection(grasp_data),
            "frames": grasp_data["frames"],
        }
        time_predict = time() - time_predict
        n = grasp_data["images"].shape[0] * grasp_data["images"].shape[1]
        print(f"[GPD] Made {n} predictions in {time_predict:.3f}s.")
        return data

    def runDetection(self, grasp_data):
        """Run the detector.

        Args:
            grasp_data (dict): grasp images.

        Returns:
            tp.tensor: the outputs of the classifier.
        """
        images = tp.from_numpy(grasp_data["images"]).float()
        images = images.permute(0, 1, 4, 2, 3) / 255.0
        images = images.view(-1, *(images.shape[2:]))
        print("images:", images.shape)
        dset = tp.utils.data.TensorDataset(images)
        loader = tp.utils.data.DataLoader(
            dset, batch_size=self.batch_size, shuffle=False
        )
        outputs = self.runModel(self.model, loader)
        outputs = outputs.reshape(grasp_data["images"].shape[:2])
        print("outputs:", outputs.shape)
        return outputs

    def getGraspPoses(self, data, threshs, cloud, tree, samples):
        """Get the grasp poses.

        Args:
            data (dict): grasp data.
            threshs (float): a single threshold for the grasp classifier.
            cloud (o3d.geometry.PointCloud): the point cloud.
            tree (o3d.geometry.KDTreeFlann): the KDTree for the point cloud.
            samples (np.array): n x 3 array of samples.

        Returns:
            np.array: k x 4 x 4 poses (as transformation matrices).
        """
        scores = data["scores"].cpu().numpy()
        mask = (data["proposals"] == 1) & (scores > threshs)
        idxs = np.array(np.nonzero(mask))
        poses = self.hands_gen.indicesToPoses(
            cloud, tree, samples, {"indices": idxs, "frames": data["frames"]}
        )
        scores_out = scores[idxs[0], idxs[1]]

        return poses, scores_out

    def getGroundTruth(self, mesh, samples, tree, camera_pose):
        """Get the ground truth grasps.

        Args:
            mesh (d): the point cloud corresponding to the object mesh.
            samples (np.array, optional): n x 3 points at which to look for grasps.
            tree (o3d.geometry.KDTreeFlann): the KD tree for the mesh point cloud.

        Returns:
            np.array: ground truth labels for each grasp pose.
        """
        hands, _ = self.hands_gen.generateHands(mesh, samples, True, tree, camera_pose)
        labels = np.zeros(hands.shape[:2])
        labels[(hands[:, :, 0] == 1) & (hands[:, :, 1] == 1)] = 1
        return labels


class MaGraspDetector(LearningGraspDetector):
    """MA grasp detector."""

    def __init__(
        self,
        hand_generator,
        hand_geom,
        antipodal_params,
        prop_params,
        grasp_img_shape,
        net_cfg,
        use_open_gl_frame,
    ):
        hands_gen = QdHandsGenerator(hand_geom, antipodal_params)
        num_rots = hands_gen.getNumHandsPerSample()
        super().__init__(hands_gen, net_cfg["batch_size"])
        self.grasp_data_gen = GraspDataGeneratorMA(
            prop_params, grasp_img_shape, use_open_gl_frame, hands_gen
        )
        prop_img_shape = (3, prop_params["image_size"], prop_params["image_size"])
        input_shapes = {
            "cls": prop_img_shape,
            "rot": prop_img_shape,
            "gc": (grasp_img_shape[-1],) + grasp_img_shape[:-1],
        }
        createNet = partial(self.createNet, net_cfg["network"], net_cfg["layout"])
        weights_paths = net_cfg["weights_paths"]
        if len(weights_paths) == 3:
            self.models = {
                "cls": createNet(weights_paths["cls"], input_shapes["cls"], 2),
                "rot": createNet(weights_paths["rot"], input_shapes["rot"], num_rots),
                "gc": createNet(weights_paths["gc"], input_shapes["gc"], 2),
            }
        elif len(weights_paths) == 2:
            self.models = {
                "rot": createNet(weights_paths["rot"], input_shapes["rot"], num_rots),
                "gc": createNet(weights_paths["gc"], input_shapes["gc"], 2),
            }
        elif len(weights_paths) == 1 and "rot" in weights_paths:
            self.models = {
                "rot": createNet(weights_paths["rot"], input_shapes["rot"], num_rots),
            }

    def detectGraspsBest(self, clouds, samples, camera_pose, best, plane_height=np.nan):
        """Detect grasps in a point cloud quickly.

        This method uses the top-k outputs from each classifier as the inputs
        to the next classifier.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 points at which to detect grasps.
            camera_pose (np.array): 4 x 4 camera pose.
            best (dict): how many outputs to select per classifier.

        Returns:
            np.array: n x 4 x 4 grasp poses.
        """
        full_cloud, objects_cloud = clouds[0], clouds[1]
        prep_cloud, prep_samples = self.hands_gen.preprocess(
            objects_cloud, samples, camera_pose
        )
        prop_imgs = self.grasp_data_gen.prop_img_gen.createImages(
            prep_cloud, prep_samples
        )
        prop_img_loader = self.createProposalImageLoader(prop_imgs)

        self.hands_gen.camera_pose = camera_pose

        rot_outs = self.runModel(self.models["rot"], prop_img_loader, class_idx=-1)
        print(f"rot_outs: {[tp.min(rot_outs), tp.max(rot_outs), tp.mean(rot_outs)]}")

        n = len(rot_outs)
        m = rot_outs.shape[1]

        if plane_height is not np.nan:
            filter_time = time()
            rot_outs = rot_outs.cpu().numpy()
            p = np.repeat(np.arange(n), m)
            r = np.tile(np.arange(m), n)
            idxs = np.vstack((p, r))
            print("idxs:", idxs.shape, n, m)
            tree = o3d.geometry.KDTreeFlann(full_cloud)
            hands = {"indices": idxs, "frames": []}
            poses = self.hands_gen.indicesToPosesMP(
                full_cloud, tree, samples, hands, False
            )
            filter_time = time() - filter_time
            print(
                f"Converted {n*m} indices to {poses.shape} poses in {filter_time:.3f}s"
            )
            # print(poses)
            above_plane = findGraspsAbovePlane(
                poses, self.hands_gen.hand_geom, plane_height
            )
            above_plane = above_plane.reshape((n, m))
            print("above_plane:", above_plane.shape)
            rot_outs[np.logical_not(above_plane)] = 0.0

            x_axis = np.array([1.0, 0.0, 0.0])
            valid = findGraspsWithDirection(
                poses, self.hands_gen.hand_geom, x_axis, 0.0
            )
            rot_outs[np.logical_not(valid.reshape((n, m)))] = 0.0

            z_axis = np.array([0.0, 0.0, 1.0])
            valid = findGraspsWithDirection(
                poses, self.hands_gen.hand_geom, -z_axis, 0.0
            )
            rot_outs[np.logical_not(valid.reshape((n, m)))] = 0.0
            print("valid:", np.count_nonzero(rot_outs))

            diag = findDiagonalGrasps(poses, self.hands_gen.hand_geom, 0.01)
            rot_outs[diag.reshape((n, m))] = 0.0
            print("nondiag:", np.count_nonzero(rot_outs))

            # idxs = np.nonzero(rot_outs.flatten())[0]
            # unique = findDuplicateGrasps(poses[idxs])[0]
            # rot_outs[idxs[unique.reshape((n,m))] = 0.0
            # print("unique:", np.count_nonzero(unique))
            # exit()

            # hand_eval = HandEval(self.hands_gen.hand_eval.hand_geom, self.hands_gen.hand_eval.antipodal_params)
            # poses = poses[rot_outs.flatten() > 0]
            # free = hand_eval.evalPosesMP(clouds[1], poses, checks_antipodal=False)
            # rot_outs[free[:,0] == 0] = 0.0
            # print("free:", np.count_nonzero(free[:,0] == 0))
            # exit()

            # rot_outs[:, :80] = 0.0
            # rot_outs[:, 120:] = 0.0

        # Choose random proposals.
        # idxs = np.vstack((np.arange(n),
        #                   np.random.randint(m, size=n)))

        # Choose the best predictions from the whole matrix.
        """
        rot_outs = rot_outs.cpu().numpy()
        sorted_idxs = np.argsort(rot_outs.flatten())
        rot_best = sorted_idxs[-best["rot"] :]
        idxs = np.array(np.unravel_index(rot_best, rot_outs.shape))
        # print("samples:", np.unique(idxs[0]))
        # input("?")
        """

        # Randomly select from the best predictions for each row.
        # """
        row_best = 20
        sorted_idxs = np.argsort(rot_outs, 1)
        rot_best = sorted_idxs[:, -row_best:]
        print("sorted_idxs:", sorted_idxs.shape)
        print("rot_best:", rot_best.shape)
        print("rot_best:", rot_best)

        """
        rows = np.repeat(np.arange(rot_best.shape[0]), row_best) 
        cols = rot_best.flatten()
        idxs = np.ravel_multi_index((rows, cols), rot_outs.shape)
        print(idxs)
        he = self.hands_gen.hand_eval
        hand_eval = HandEval(he.hand_geom, he.antipodal_params)
        poses = poses[idxs]
        free = hand_eval.evalPosesMP(clouds[1], poses, checks_antipodal=False)
        """

        rand_idxs = np.random.randint(n * row_best, size=best["rot"])
        print("rand_idxs:", rand_idxs.shape)
        # print("rand_idxs:", rand_idxs)
        rand_idxs = np.unravel_index(rand_idxs, rot_best.shape)
        rot_best = rot_best[rand_idxs[0], rand_idxs[1]]
        print("rot_best:", rot_best.shape)
        idxs = np.vstack((rand_idxs[0], rot_best))
        # print("idxs:", idxs)
        # input("?")
        # """

        # Remove grasp poses for which a finger is below the table.
        # """
        if plane_height is not np.nan:
            if tree is None:
                tree = o3d.geometry.KDTreeFlann(full_cloud)
            hands = {"indices": idxs, "frames": []}
            poses = self.hands_gen.indicesToPoses(full_cloud, tree, samples, hands)
            print("poses:", poses.shape)
            above_plane = findGraspsAbovePlane(
                poses, self.hands_gen.hand_geom, plane_height
            )
            idxs = idxs[:, above_plane]
        # """

        prep_cloud, _ = self.hands_gen.preprocess(full_cloud, samples, camera_pose)
        rots = self.hands_gen.calcRotations(idxs[1])
        grasp_imgs = self.grasp_data_gen.grasp_img_gen.calcImagesAtIndicesSP(
            prep_cloud, prep_samples, idxs, rots, []
        )
        grasp_img_loader = self.createGraspImageLoader(grasp_imgs)
        gc_outs = self.runModel(self.models["gc"], grasp_img_loader).cpu().numpy()
        sorted_idxs = np.argsort(gc_outs)
        gc_best = sorted_idxs[-best["gc"] :]

        idxs = idxs[:, gc_best]
        if tree is None:
            tree = o3d.geometry.KDTreeFlann(full_cloud)
        poses = self.hands_gen.indicesToPoses(
            full_cloud,
            tree,
            samples,
            {"indices": idxs, "frames": []},
        )

        scores = {"gc": gc_outs[gc_best], "rot": rot_outs[idxs[0, :], idxs[1, :]]}

        return poses, scores

    def detectGraspsThreshs(self, cloud, samples, camera_pose, threshs):
        """Detect grasps in a point cloud quickly.

        Compared to `detectGrasps`, this method does not predict scores for the
        complete n x m grasp pose matrix but only on a subset of the best proposals
        determined by CLS and ROT.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 points at which to detect grasps.

        Returns:
            np.array: k x 4 x 4 poses (as transformation matrices).
        """
        # 1. Create and evaluate grasp proposals.
        prep_cloud, prep_samples = self.hands_gen.preprocess(
            cloud, samples, camera_pose
        )
        prop_imgs = self.grasp_data_gen.prop_img_gen.createImages(
            prep_cloud, prep_samples
        )
        prop_img_loader = self.createProposalImageLoader(prop_imgs)
        if "cls" in self.models:
            cls_outs = self.runModel(self.models["cls"], prop_img_loader)
            print(
                f"cls_outs: {[tp.min(cls_outs), tp.max(cls_outs), tp.mean(cls_outs)]}"
            )

        rot_outs = self.runModel(self.models["rot"], prop_img_loader, class_idx=-1)
        print(f"rot_outs: {[tp.min(rot_outs), tp.max(rot_outs), tp.mean(rot_outs)]}")

        # 2. Select grasp proposals.
        if "cls" in self.models:
            cls_idxs = np.nonzero((cls_outs > threshs["cls"]).cpu().numpy())[0]
            cls_mask = np.zeros(rot_outs.shape, dtype=np.bool)
            cls_mask[cls_idxs, :] = 1
        else:
            cls_mask = np.ones(rot_outs.shape, dtype=bool)
        rot_mask = (rot_outs > threshs["rot"]).cpu().numpy()
        idxs = np.array(np.nonzero(cls_mask & rot_mask))
        if idxs.shape[1] == 0:
            print("Warning: no CLS/ROT predictions above threshold!")
            return []
        print(f"#inputs to GC: {idxs.shape[1]}")

        # 3. Create grasp poses.
        rots = self.hands_gen.calcRotations(idxs[1])
        grasp_imgs = self.grasp_data_gen.grasp_img_gen.calcImagesAtIndices(
            prep_cloud, prep_samples, idxs, rots, []
        )
        grasp_img_loader = self.createGraspImageLoader(grasp_imgs)
        gc_outs = self.runModel(self.models["gc"], grasp_img_loader)
        gc_mask = (gc_outs > threshs["gc"]).cpu().numpy()

        print(f"idxs: {idxs[:, gc_mask]}")
        poses = self.hands_gen.indicesToPoses(
            cloud,
            o3d.geometry.KDTreeFlann(cloud),
            samples,
            {"indices": idxs[:, gc_mask], "frames": []},
        )
        # print(f"gc_outs: {[tp.min(gc_outs), tp.max(gc_outs), tp.mean(gc_outs)]}")

        scores = {"gc": gc_outs[gc_mask].cpu().numpy()}
        if "cls" in self.models:
            scores["cls"] = cls_outs

        return poses, scores

    def detectGrasps(self, cloud, samples, camera_pose):
        """Detect grasps in a point cloud.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array, optional): n x 3 points at which to detect grasps.

        Returns:
            dict: the outputs of the cls, rot, and gc classifiers.
        """
        grasp_data = self.grasp_data_gen.createImageData(cloud, samples, camera_pose)
        outputs = self.runDetection(grasp_data)
        data = {"scores": outputs}
        return data

    def runDetection(self, grasp_data):
        """Run the detector.

        Args:
            grasp_data (dict): proposal images and grasp images.

        Returns:
            tp.tensor: the outputs of the cls, rot, and gc classifiers.
        """
        prop_img_loader = self.createProposalImageLoader(grasp_data["prop_imgs"])
        if "cls" in self.models.keys():
            cls_outs = self.runModel(self.models["cls"], prop_img_loader)
        else:
            cls_outs = tp.ones(len(grasp_data["prop_imgs"]))
        rot_outs = self.runModel(self.models["rot"], prop_img_loader, class_idx=-1)
        outs = {"cls": cls_outs, "rot": rot_outs}

        if "gc" in self.models.keys():
            grasp_img_loader = self.createGraspImageLoader(grasp_data["grasp_imgs"])
            gc_outs = self.runModel(self.models["gc"], grasp_img_loader)
            gc_outs = gc_outs.view(rot_outs.shape[0], rot_outs.shape[1])
            outs["gc"] = gc_outs

        # debugging
        # z = zip(["cls", "rot", "gc"], [cls_outs, rot_outs, gc_outs])
        # print([[s, x.shape] for s, x in z])

        return outs

    def createProposalImageLoader(self, images):
        """Create a data loader for proposal images.

        Args:
            images (?): the proposal images.

        Returns:
            tp.data.DataLoader: data loader for proposal images.
        """
        imgs = tp.from_numpy(images).float()
        imgs = imgs.permute(0, 3, 1, 2) / 255.0
        print("proposal images:", imgs.shape)
        dset = tp.utils.data.TensorDataset(imgs)
        loader = tp.utils.data.DataLoader(
            dset, batch_size=self.batch_size, shuffle=False
        )
        return loader

    def createGraspImageLoader(self, images):
        """Create a data loader for grasp images.

        Args:
            images (?): the grasp images.

        Returns:
            tp.data.DataLoader: data loader for grasp images.
        """
        imgs = tp.from_numpy(images).float() / 255.0
        if len(imgs.shape) == 5:
            imgs = imgs.permute(0, 1, 4, 2, 3)
            imgs = imgs.view(-1, *(imgs.shape[2:]))
        else:
            imgs = imgs.permute(0, 3, 1, 2)
        print("grasp images:", imgs.shape)
        dset = tp.utils.data.TensorDataset(imgs)
        loader = tp.utils.data.DataLoader(
            dset, batch_size=self.batch_size, shuffle=False
        )
        return loader

    def getGraspPoses(self, data, threshs, cloud, tree, samples):
        """Get the grasp poses.

        Args:
            data (dict): grasp data.
            threshs (dict): thresholds for CLS, ROT, and GC.
            cloud (o3d.geometry.PointCloud): the point cloud.
            tree (o3d.geometry.KDTreeFlann): the KDTree for the point cloud.
            samples (np.array): n x 3 array of samples.

        Returns:
            np.array: k x 4 x 4 poses (as transformation matrices).
        """
        scores = data["scores"]
        if "cls" in scores and tp.prod(scores["cls"]) != 1:
            cls_idxs = np.nonzero((scores["cls"] > threshs["cls"]).cpu().numpy())[0]
            cls_mask = np.zeros(scores["rot"].shape, dtype=np.bool)
            cls_mask[cls_idxs, :] = 1
        else:
            cls_mask = np.ones_like(scores["gc"], dtype=bool)
        if "rot" in scores:
            rot_mask = (scores["rot"] > threshs["rot"]).cpu().numpy()
        else:
            rot_mask = np.ones_like(scores["gc"], dtype=bool)
        gc_mask = (scores["gc"] > threshs["gc"]).cpu().numpy()
        idxs = np.array(np.nonzero(cls_mask & rot_mask & gc_mask))
        poses = self.hands_gen.indicesToPoses(
            cloud, tree, samples, {"indices": idxs, "frames": []}
        )
        print(
            f"above (cls, rot, gc): {[np.count_nonzero(x) for x in [cls_mask, rot_mask, gc_mask]]}"
        )
        print(
            f"  cls & rot: {np.count_nonzero(cls_mask & rot_mask)}, cls & rot & gc: {idxs.shape}"
        )
        # print(type(cls_idxs), len(cls_idxs))

        # gc_scores = (scores["gc"] > threshs["gc"]).cpu().numpy()
        # scores_out = gc_scores[idxs[0], idxs[1]]
        scores_out = (scores["gc"][idxs[0], idxs[1]]).cpu().numpy()

        return poses, scores_out


class MaGcGraspDetector(LearningGraspDetector):
    """MA grasp detector with only the GC component."""

    def __init__(
        self,
        hand_generator,
        hand_geom,
        antipodal_params,
        grasp_img_shape,
        net_cfg,
        use_open_gl_frame,
    ):
        hands_gen = QdHandsGenerator(hand_geom, antipodal_params)
        super().__init__(hands_gen, net_cfg["batch_size"])
        self.grasp_img_gen = GraspImageGenerator(grasp_img_shape, self.hands_gen)
        self.rots = self.hands_gen.calcRotations(
            np.arange(self.hands_gen.num_hand_orientations)
        )
        input_shapes = {
            "gc": (grasp_img_shape[-1],) + grasp_img_shape[:-1],
        }
        createNet = partial(self.createNet, net_cfg["network"], net_cfg["layout"])
        weights_paths = net_cfg["weights_paths"]
        self.models = {
            "gc": createNet(weights_paths["gc"], input_shapes["gc"], 2),
        }

    def detectGrasps(self, cloud, samples, camera_pose):
        """Detect grasps in a point cloud.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array, optional): n x 3 points at which to detect grasps.

        Returns:
            dict: the outputs of the cls, rot, and gc classifiers.
        """
        time_images = time()
        prep_cloud, prep_samples = self.hands_gen.preprocess(
            cloud, samples, camera_pose
        )
        images = self.grasp_img_gen.calcImages(prep_cloud, prep_samples, self.rots)
        time_images = time() - time_images
        n = images.shape[0] * images.shape[1]
        print(f"[GC] Created {n} grasp images in {time_images:.3f}s.")
        time_predict = time()
        outputs = self.runDetection(images)
        time_predict = time() - time_predict
        print(f"[GC] Made {n} predictions in {time_predict:.3f}s.")
        data = {"scores": outputs}
        return data

    def runDetection(self, images):
        """Run the detector.

        Args:
            grasp_data (dict): proposal images and grasp images.

        Returns:
            tp.tensor: the outputs of the cls, rot, and gc classifiers.
        """
        grasp_img_loader = self.createGraspImageLoader(images)
        gc_outs = self.runModel(self.models["gc"], grasp_img_loader)
        gc_outs = gc_outs.view(images.shape[0], images.shape[1])
        print([[s, x.shape] for s, x in zip(["gc"], [gc_outs])])
        return {"gc": gc_outs}

    def createGraspImageLoader(self, images):
        """Create a data loader for grasp images.

        Args:
            images (?): the grasp images.

        Returns:
            tp.data.DataLoader: data loader for grasp images.
        """
        imgs = tp.from_numpy(images).float()
        imgs = imgs.permute(0, 1, 4, 2, 3) / 255.0
        imgs = imgs.view(-1, *(imgs.shape[2:]))
        print("grasp images:", imgs.shape)
        dset = tp.utils.data.TensorDataset(imgs)
        loader = tp.utils.data.DataLoader(
            dset, batch_size=self.batch_size, shuffle=False
        )
        return loader


def createGraspDetector(
    hand_geom,
    antipodal_params,
    method,
    prop_params,
    grasp_img_shape,
    net_cfg,
    use_open_gl_frame=True,
):
    if method == "gpd":
        detector = GpdGraspDetector(
            hand_geom, antipodal_params, grasp_img_shape, net_cfg
        )
    elif method in ["ma", "qd-ant", "qd-sim", "rot-gc", "rot"]:
        detector = MaGraspDetector(
            method,
            hand_geom,
            antipodal_params,
            prop_params,
            grasp_img_shape,
            net_cfg,
            use_open_gl_frame,
        )
    elif method in ["gc"]:
        detector = MaGcGraspDetector(
            method[: method.rfind("-")],
            hand_geom,
            antipodal_params,
            grasp_img_shape,
            net_cfg,
            use_open_gl_frame,
        )

    return detector


def findGraspsAbovePlane(
    poses: np.array, geom: HandGeometry, height: float, plot=False, cloud=None
):
    offset = 0.02
    positions = poses[:, :3, 3]
    approach_axis = poses[:, :3, 0]
    closing_axis = poses[:, :3, 1]
    left_pos = positions - 0.5 * geom.outer_diameter * closing_axis
    right_pos = positions + 0.5 * geom.outer_diameter * closing_axis
    left_top = left_pos + geom.depth * approach_axis
    right_top = right_pos + geom.depth * approach_axis
    above_plane = (
        (left_pos[:, 2] > height)
        & (right_pos[:, 2] > height)
        & (left_top[:, 2] > height)
        & (right_top[:, 2] > height)
        & (positions[:, 2] > height + offset)
    )

    if plot:
        print(positions[:3])
        print(left_pos[:3])
        print(right_pos[:3])
        print("lb:", left_pos.shape)
        print("plane_height:", height)
        print("above_plane:", above_plane.shape, np.count_nonzero(above_plane))
        print("positions:", positions.shape)
        pos_cloud = numpyToCloud(positions)
        pos_cloud.paint_uniform_color([0.0, 0.0, 1.0])
        lb_cloud = numpyToCloud(left_pos)
        lb_cloud.paint_uniform_color([1.0, 0.0, 0.0])
        plane = o3d.geometry.TriangleMesh.create_box(1.0, 1.0, 0.01)
        xy = np.asarray(cloud.points)[:, :2]
        ctr = np.min(xy, 0) + 0.5 * (np.max(xy, 0) - np.min(xy, 0))
        plane.translate((ctr[0], ctr[1], height), relative=False)
        o3d.visualization.draw_geometries(
            [cloud, plane, lb_cloud, pos_cloud], "", 640, 480
        )

    return above_plane


def findGraspsWithDirection(
    poses: np.array, geom: HandGeometry, direction: np.array, thresh: float = -0.4
):
    approach_axis = poses[:, :3, 0]
    dots = np.dot(approach_axis, direction)
    is_valid = dots > thresh
    print("dots:", dots.shape, "#valid:", np.count_nonzero(is_valid))

    return is_valid


def findDiagonalGrasps(poses: np.array, geom: HandGeometry, thresh: float):
    positions = poses[:, :3, 3]
    closing_axis = poses[:, :3, 1]
    left_pos = positions - 0.5 * geom.outer_diameter * closing_axis
    right_pos = positions + 0.5 * geom.outer_diameter * closing_axis
    mask = np.abs(right_pos[:, 2] - left_pos[:, 2]) > thresh

    return mask


def findDuplicateGrasps(poses, min_pos_dist=0.003, min_rot_dist=3.0):
    print("[findDuplicateGrasps]", poses.shape)
    min_rot_dist = np.deg2rad(min_rot_dist)
    rots = [X[:3, :3].T for X in poses]
    is_duplicate = np.zeros(len(poses), dtype=bool)

    positions = poses[:, :3, 3]
    pos_dists = np.stack([np.linalg.norm(positions - x, axis=1) for x in positions])
    neighbors = np.sum(pos_dists < min_pos_dist, 1)
    print(pos_dists.shape, neighbors.shape)
    # print(len(pos_dists), pos_dists[0].shape)

    # np.array([np.linalg.norm(X[:3,3] - pose[:3,3]) for X in poses])
    # pos_dists = np.array([np.linalg.norm(X[:3,3] - pose[:3,3]) for X in poses])

    # for i, pose in enumerate(poses):
    # print(i)
    # pos_dists = np.array([np.linalg.norm(X[:3,3] - pose[:3,3]) for X in poses])
    # # rot_dists = [np.arccos(0.5*(np.trace(np.matmul(pose[:3,:3], R)) - 1)) for R in rots]
    # # rot_dists = np.array([np.real(x) for x in rot_dists])
    # # mask = (pos_dists <= min_pos_dist) & (rot_dists <= min_rot_dist)
    # mask = (pos_dists <= min_pos_dist)
    # if np.count_nonzero(mask) > 1:
    # is_duplicate[i] = True

    return is_duplicate

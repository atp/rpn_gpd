"""Check if the given mesh file is watertight.

Used by ../scripts/process_meshes.sh.

"""

import sys

import trimesh

mesh = trimesh.load(sys.argv[1])
print(int(mesh.is_watertight))

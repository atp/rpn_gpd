import sys
from time import time

import torch

from networks.network import LenetLikeWithSigmoid
from networks.resnet_light import Resnet
from networks.resnet_mod import resnet18, resnet34

num_reps = 100
batch_size = 1024

# N x C x H x W
in_shape = (batch_size, 3, 60, 60)

net_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": in_shape[1:],
    # Params for Lenet
    "conv": [32, 64],
    "fc": [500],
    "conv_filter_size": 5,
}

if len(sys.argv) < 2:
    exit(-1)

is_verbose = False
if len(sys.argv) == 3 and sys.argv[2] == "-v":
    is_verbose = True

# Use GPU for training if available.
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# Create the network.
network_name = sys.argv[1]
if network_name == "lenet":
    model = LenetLikeWithSigmoid(**net_params)
elif network_name == "resnet":
    model = Resnet(net_params["input_shape"], net_params["num_classes"])
elif network_name == "resnet18":
    model = resnet18(
        in_channels=net_params["in_shape"][0], num_classes=net_params["out_shape"]
    )
elif network_name == "resnet34":
    model = resnet34(
        in_channels=net_params["in_shape"][0], num_classes=net_params["out_shape"]
    )
model = model.to(device)

# Create some batches with random data.
X = torch.randn((num_reps,) + in_shape)
print(f"X: {X.shape}")

# Measure the runtime of the forward pass.
runtimes = torch.zeros(num_reps)

with torch.no_grad():
    for i in range(num_reps):
        t = time()
        Y = model(X[i].to(device))
        runtimes[i] = time() - t
        if is_verbose:
            print(
                f"({i}/{num_reps}) X: {X[i].shape}, Y: {Y.shape}, t: {runtimes[i]:.6f}s"
            )
print(f"batch size: {batch_size}")
print(f"average runtime over {num_reps} trials: {torch.mean(runtimes):.4f}s")
print(f"standard deviation: {torch.std(runtimes):.4f}s")

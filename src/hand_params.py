# Hand dimensions (based on Robotiq 2F-85)
hand_params = {
    "finger_width": 0.01,  # width of a finger
    "outer_diameter": 0.105,  # inner diameter + finger width
    "depth": 0.06,  # length of fingers
    "height": 0.02,  # -/+ along hand axis
    "init_bite": 0.01,  # minimum extend onto object
}
# hand_params["inner_diameter"] = (
# hand_params["outer_diameter"] - 2 * hand_params["finger_width"]
# )

# Antipodal grasp params
antipodal_params = {
    "extremal_thresh": 0.003,
    "friction_coeff": 30,
    "min_viable": 6,
    "checks_overlap": True,
}

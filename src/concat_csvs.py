# Takes two CSV files as inputs and concatenates them into a new CSV.

import sys

import numpy as np

a = np.loadtxt(sys.argv[1], delimiter=",", skiprows=1)
b = np.loadtxt(sys.argv[2], delimiter=",", skiprows=1)
c = np.vstack((a, b))
np.savetxt(
    sys.argv[3], c, fmt="%.3f", delimiter=",", header="Accuracy, Precision, Recall"
)

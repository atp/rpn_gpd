import numpy as np
from summer import sum_it2d

# from a list (with copy)
# print(sum_it([1, 2, 3, 4.5]))
# from a numpy array of the right type - zero-copy
# print(sum_it(np.array([3.0, 4.0, 5.0])))
# from a 2d numpy array
a = np.array([[3.0, 4.0, 5.0]])
print(a.shape, type(a))
print(sum_it2d(a))

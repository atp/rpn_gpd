import ctypes
import os
import sys

import numpy as np

path = os.path.dirname(__file__)
cdll = ctypes.CDLL(os.path.join(path, "libsummer.so"))
_sum_it = cdll.sum_it
_sum_it.restype = ctypes.c_double

_sum_it2d = cdll.sum_it
_sum_it2d.argtypes = (
    np.ctypeslib.ndpointer(dtype=np.float64, ndim=2, flags="C_CONTIGUOUS"),
    ctypes.c_int,
)
_sum_it2d.restype = ctypes.c_double


def sum_it(l):
    if isinstance(l, np.ndarray) and l.dtype == np.float64 and len(l.shape) == 1:
        # it's already a numpy array with the right features - go zero-copy
        a = l.ctypes.data
    else:
        # it's a list or something else - try to create a copy
        arr_t = ctypes.c_double * len(l)
        a = arr_t(*l)
    return _sum_it(a, len(l))


def sum_it2d(l):
    print(isinstance(l, np.ndarray), l.dtype, len(l.shape))
    # if isinstance(l, np.ndarray) and l.dtype == np.float64 and len(l.shape) == 2:
    # # it's already a numpy array with the right features - go zero-copy
    # a = l.ctypes.data
    # exit()
    # else:
    # # it's a list or something else - try to create a copy
    # raise TypeError("Wrong data type!")
    return _sum_it2d(l, l.shape[0] * l.shape[1])

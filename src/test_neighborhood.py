import numpy as np
import open3d as o3d

from geometry.neighborhood import Neighborhood
from geometry.point_cloud import PointCloud
from utils.cloud_utils import estimateNormals

c = o3d.io.read_point_cloud("/home/andreas/data/2019-06-06_14_36_39_combined_cloud.pcd")
c = PointCloud(c)
c.cloud = estimateNormals(c.cloud)
sample = np.asarray(c.points)[0]
n = Neighborhood.fromPointCloud(c, sample, 0.10)
print(n)


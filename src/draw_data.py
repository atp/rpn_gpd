""" Visualize images from a given HDF5 database.

A surface normals image of shape (w,h,9) is drawn as a row of three RGB
images.

A 12-channels image is drawn as a row of six RGB images, out of which 
the first three are the nine surface normals channels, three per image, 
and the last three are the height maps for each projection plane.

"""

import sys

import matplotlib.pyplot as plt
import numpy as np
import tables
import torch

from ground_truth import findGroundTruthPositives


def plotOccupancyImages(images, title=""):
    rows = len(images)
    cols = 3
    fig = plt.figure(figsize=(8, 8))

    for i in range(rows):
        for j in range(3):
            img = (images[i, :, :, j]).astype(np.float) / 255.0
            ax = fig.add_subplot(rows, cols, i * cols + j + 1)
            ax.axis("off")
            plt.imshow(np.asarray(img))

    fig.suptitle(title)
    plt.show()


def plotNormalImages(images, labels, title):
    pad1 = torch.nn.ConstantPad2d((4, 4, 4, 4), 1)
    pad0 = torch.nn.ConstantPad2d((4, 4, 4, 4), 0)

    cols = 3
    rows = len(images)
    fig = plt.figure(figsize=(8, 8))

    for i in range(rows):
        for j in range(cols):
            img = (images[i, :, :, j * 3 : j * 3 + 3]).astype(np.float) / 255.0
            img = torch.from_numpy(img)
            img_label = torch.zeros((68, 68, 3), dtype=np.float)
            img_label[:, :, 2] = pad0(img[:, :, 2])
            if labels.ndim == 2:
                if np.sum(labels[i]) == 0:
                    img_label[:, :, 0] = pad1(img[:, :, 0])
                    img_label[:, :, 1] = pad0(img[:, :, 1])
                else:
                    img_label[:, :, 0] = pad0(img[:, :, 0])
                    img_label[:, :, 1] = pad1(img[:, :, 1])
            ax = fig.add_subplot(rows, cols, i * cols + j + 1)
            ax.axis("off")
            plt.imshow(np.asarray(img_label))

    fig.suptitle(title)
    plt.show()


def plotNormalAndDepthImage(images):
    rows = len(images)
    cols = 6
    fig = plt.figure(figsize=(8, 8))

    for i in range(rows):
        for j in range(3):
            img = (images[i, :, :, j * 3 : j * 3 + 3]).astype(np.float) / 255.0
            ax = fig.add_subplot(rows, cols, i * cols + j + 1)
            ax.axis("off")
            plt.imshow(np.asarray(img))

        for j in range(3):
            img = (images[i, :, :, 9 + j]).astype(np.float) / 255.0
            ax = fig.add_subplot(rows, cols, i * cols + j + 4)
            ax.axis("off")
            plt.imshow(np.asarray(img))

    fig.suptitle(title)
    plt.show()


num = 5

if len(sys.argv) < 3:
    print("Usage: python draw_data.py HDF5 ARRAY")
    exit(-1)
filepath = sys.argv[1]
array_name = sys.argv[2]

with tables.open_file(filepath, "r") as f:
    images = f.get_node("/", array_name)
    # images = [x for x in f.list_nodes(f.root) if x.name == array_name]
    # if len(images) == 0:
    # print(f"Error: array <{array_name}> does not exist in database!")
    # exit(-1)
    # images = images[0]
    print(f"Found array <{array_name}> in database with shape {images.shape}")

    labels = f.root.labels
    if labels.shape[-1] == 5:
        mask = findGroundTruthPositives(labels)
        positives = np.nonzero(mask)
        if labels.ndim == 2:
            labels_out = np.zeros(len(labels))
            labels_out[positives[0]] = 1
        elif labels.ndim == 3:
            labels_out = np.zeros(labels.shape[:2])
            labels_out[positives[0], positives[1]] = 1
        labels = labels_out
        print(f"Derived binary ground truth labels: {labels.shape}")

    for i in range(0, len(images), num):
        print(f"i: {i}")
        title = array_name + f": [{i}, {i+num}]"
        if images.shape[-1] == 4:
            plotOccupancyImages(images[i : i + num, ..., :-1], title)
        elif images.shape[-1] == 9:
            plotNormalImages(images[i : i + num], labels[i : i + num], title)
        elif images.shape[-1] == 12:
            plotNormalAndDepthImage(images[i : i + num])

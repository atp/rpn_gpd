"""Convert files produced by the simulator to per-view files.
"""

import os
import sys
from functools import partial
from glob import glob

import numpy as np
import torch.multiprocessing as mp

from grasp_scene_data import GraspData
from learning import Metrics

thresh_step = 0.05
# thresh_step = 0.25
threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[:-1]


def calcMetricsRotGc(scores: dict, labels: np.array) -> np.array:
    """Calculate metrics for ROT-GC.

    Calculate confusion matrices and count the number of grasp proposals.

    Args:
        scores (dict): the scores for ROT and GC.
        labels (np.array): the ground truth labels

    Returns:
        np.array: the confusion matrices and numbers of grasp proposals.
    """
    masks = {k: [scores[k] > t for t in threshs] for k in ["rot", "gc"]}
    results = [[Metrics(labels, r & g) for g in masks["gc"]] for r in masks["rot"]]
    confmats = [[x.confmat() for x in sub] for sub in results]
    confmats = np.array(confmats).reshape((-1, len(confmats[0][0])))
    n = len(threshs)
    thresh_pairs = np.vstack((np.repeat(threshs, n), np.tile(threshs, n)))
    num_props = np.array([np.count_nonzero(m) for m in masks["rot"]])
    num_props = np.repeat(num_props, n)
    stats = np.hstack((thresh_pairs.T, confmats, num_props[..., np.newaxis]))

    return stats


def calcMetricsForFile(path: str, labels_key: str) -> np.array:
    """Calculate metrics for a file.

    Args:
        path (str): path to the file.

    Returns:
        np.array: metrics for the file.
    """
    data = GraspData.fromFile(path, labels_key)
    labels, scores = data.labels, data.scores
    if type(labels) == list:
        labels = np.array(labels)
    if type(scores) == list:
        scores = np.array(scores)
    num_proposals = len(data.grasp_poses)
    if "graspnet" in path:
        results = [Metrics(labels, (scores > t).astype(int)) for t in threshs]
        stats = [
            [t,] + list(r.confmat()) + [num_proposals,]
            for t, r in zip(threshs, results)
        ]
    elif "qd" in path and "rot-gc" in path:
        stats = calcMetricsRotGc(scores, labels)

    return np.array(stats)


def main() -> None:
    if len(sys.argv) < 3:
        print("Error: not enough input arguments.")
        print("Usage: python sim2npy.py IN_DIR OUT_DIR [PARALLELIZE?]")
        sys.exit(-1)
    in_dir = sys.argv[1]
    out_dir = sys.argv[2]
    if len(sys.argv) == 4:
        parallelize = int(sys.argv[3])
    else:
        parallelize = False
    # labels_key = "labels"
    labels_key = "labels_majority"

    if not os.path.isdir(in_dir) or not os.path.isdir(out_dir):
        print("Error: requires directory as argument!")
        exit(-1)

    files = sorted(glob(os.path.join(in_dir, f"*.npy")))
    if parallelize:
        print(f"Parallel execution with {os.cpu_count()} processes")
        pool = mp.Pool(os.cpu_count())
        fn = partial(calcMetricsForFile, labels_key=labels_key)
        stats = pool.map(fn, files)
        pool.close()
        pool.join()
        for i, (f, x) in enumerate(zip(files, stats)):
            path = out_dir + f[f.rfind("/") + 1 :]
            np.save(path, x)
            print(f"({i}/{len(files)}) Stored stats of shape {x.shape} at: {path}")
    else:
        for i, f in enumerate(files):
            stats = calcMetricsForFile(f, labels_key)
            path = out_dir + f[f.rfind("/") + 1 :]
            np.save(path, stats)
            print(f"({i}/{len(files)}) Stored stats of shape {stats.shape} at: {path}")


if __name__ == "__main__":
    main()

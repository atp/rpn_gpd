import sys
from time import time

import numpy as np
import open3d as o3d
import trimesh as trim

from scene import SingleObjectScene
from utils.cloud_utils import twoWindowsPlot

# Load the mesh with trimesh and open3d.
path = sys.argv[1]
trim_mesh = trim.load(path)
o3d_mesh = o3d.io.read_triangle_mesh(path)

# Sample a point cloud from the mesh.
cloud = o3d_mesh.sample_points_poisson_disk(30000)

# Get the normals for the point cloud from the mesh.
face_normals_time = time()
[_, _, idxs] = trim_mesh.nearest.on_surface(np.array(cloud.points))
normals = trim_mesh.face_normals[idxs]
cloud.normals = o3d.utility.Vector3dVector(normals)
face_normals_time = time() - face_normals_time
print(f"Nearest face normal runtime: {face_normals_time:.3f}s")

# cloud_estim = o3d.geometry.PointCloud()
# cloud_estim.points = cloud.points
# cloud_estim.estimate_normals(
# search_param=o3d.geometry.KDTreeSearchParamRadius(0.01))

scene = SingleObjectScene(path)
scene.resetToScale(0.3)
mv_cloud = scene.createMultiViewPointCloud(num_pts=30000)

# Visualize the surface normals.
twoWindowsPlot([cloud], [mv_cloud], ["face normals", "estimated normals"])
# o3d.visualization.draw_geometries([cloud], height=640, width=480)

"""Create a barplot that shows per-category results for a set of methods.
"""

import os
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

from plot_antipodal_results import labels, nullifyNans, readDirectory
from plot_precision_recall_qd_vs_gpd import paretoFrontier
from plot_simulation_results import setLimitsLabels


def calcPrecisionRecall(path: str, category: str = "") -> None:
    dirs = [os.path.join(path, f) for f in os.listdir(path)]
    dirs = sorted(dirs)
    if any([os.path.isfile(x) for x in dirs]):
        dirs = [path]
        method = path[: path.rfind("/")]
        key = method[method.rfind("/") + 1 :]
        data = {key: readDirectory(path, category) for k in dirs}
    else:
        data = {k[k.rfind("/") + 1 :]: readDirectory(k + "/", category) for k in dirs}
    print("Loaded results for these methods:", list(data.keys()))

    metrics, avgs, pareto_pr = {}, {}, {}
    for k in data:
        metrics[k] = {
            "recall": [nullifyNans(x.recall()) for x in data[k]],
            "precision": [nullifyNans(x.precision()) for x in data[k]],
        }
        avgs[k] = {l: np.mean(v, axis=0) for l, v in metrics[k].items()}
        pareto_pr[k] = paretoFrontier(
            np.vstack((avgs[k]["recall"], avgs[k]["precision"]))
        )
        idxs = np.argsort(pareto_pr[k][0], axis=0)
        pareto_pr[k] = pareto_pr[k][:, idxs]

    return pareto_pr


def main() -> None:
    if len(sys.argv) < 3:
        print("Error: not enough input arguments.")
        print("Usage: python barplot_categories.py PATH OBJECTS_TXT")
        sys.exit(-1)
    path = sys.argv[1]

    # Read object categories from text file.
    with open(sys.argv[2]) as f:
        categories = f.readlines()
        categories = [x[:-1] for x in categories]
        categories = sorted(categories)
        print(categories)

    results = {c: calcPrecisionRecall(path, c) for c in categories}
    maxs = {c: {k: x[k][:, np.argmax(x[k][1])] for k in x} for c, x in results.items()}
    methods = list(results[categories[0]].keys())
    print(results)
    print(methods)

    if len(methods) == 1:
        m = methods[0]
        for c in categories:
            plt.figure()
            plt.plot(results[c][m][0], results[c][m][1], label=c)
        setLimitsLabels(xlabel="recall", ylabel="precision")
        plt.legend()
        # plt.tight_layout()
        plt.show()

        exit()

    methods = ("gpd", "qd_rot_gc")
    # methods = ("graspnet", "qd_rot_gc")
    y_gpd = [maxs[c][methods[0]][1] for c in categories]
    y_qd = [maxs[c][methods[1]][1] for c in categories]

    # Settings for 3DNet or Graspnet.
    if "3dnet" in sys.argv[2]:
        figsize = (8, 8)
        height = 0.7
    elif "graspnet" in sys.argv[2]:
        figsize = (5, 4)
        height = 0.3

    # Plot precision-recall for each category.
    linestyles = {methods[0]: "solid", methods[1]: "dashed"}
    # plt.figure()
    plt.figure(figsize=(10, 5))
    for i, (c, x) in enumerate(results.items()):
        color = None
        for k in linestyles:
            label = f"{c}({labels[k]})"
            if color is None:
                (p,) = plt.plot(x[k][0], x[k][1], linestyle=linestyles[k], label=label)
                color = p.get_color()
            else:
                plt.plot(
                    x[k][0], x[k][1], color=color, linestyle=linestyles[k], label=label
                )
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend(loc="lower left", fontsize=8, ncol=2)
    plt.tight_layout()
    plt.show()

    # cmaps = {
    # "qd_rot_gc": matplotlib.cm.get_cmap("Blues"),
    # "gpd": matplotlib.cm.get_cmap("Oranges"),
    # }
    # plt.figure()
    # step = 0.5 / len(results[list(results.keys())[0]]["qd_rot_gc"])
    # for i, (c, x) in enumerate(results.items()):
    # for k in cmaps:
    # print(i, k, cmaps[k](i))
    # plt.plot(x[k][0], x[k][1], color=cmaps[k](1.0 - i * step))
    # setLimitsLabels(xlabel="recall", ylabel="precision")
    # plt.show()

    # Plot precision-recall for each category.
    # plt.figure()
    # fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
    # for i, k in enumerate(["qd_rot_gc", "gpd"]):
    # for c, x in results.items():
    # axes[i].plot(x[k][0], x[k][1], label=c)
    # axes[i].legend(loc="lower left")
    # axes[i].set_xlim([-0.05, 1.05])
    # axes[i].set_ylim([-0.05, 1.05])
    # plt.xlabel("recall")
    # plt.ylabel("precision")
    # plt.tight_layout()
    # plt.show()

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
    for i, k in enumerate(methods):
        for c, x in results.items():
            axes[i].plot(x[k][0], x[k][1], label=c)
        axes[i].legend(loc="lower left")
        axes[i].set_xlim([-0.05, 1.05])
        axes[i].set_ylim([-0.05, 1.05])
        axes[i].set_title(k)
    plt.xlabel("recall")
    plt.ylabel("precision")
    plt.tight_layout()
    plt.show()

    x = np.arange(len(categories))

    # Create a horizontal bar plot.
    _, ax = plt.subplots(figsize=figsize)
    ax.barh(x, y_qd, label="QD: ROT-GC", tick_label=categories)#, height=height)
    ax.barh(x, y_gpd, label="GPD", tick_label=categories)#, height=height)
    plt.xlabel("max(precision)")
    plt.ylabel("category")
    plt.legend()
    plt.tight_layout()
    plt.show()

    # Create a scatter plot of precision vs recall.
    x_gpd = [maxs[c][methods[0]][0] for c in categories]
    x_qd = [maxs[c][methods[1]][0] for c in categories]
    cmap = matplotlib.cm.get_cmap("Spectral")
    step = 1.0 / len(x_gpd)
    plt.figure()
    for i, c in enumerate(categories):
        # print(i, c)
        plt.plot(x_qd[i], y_qd[i], "x", color=cmap(i * step), label=c)
        plt.plot(x_gpd[i], y_gpd[i], "o", color=cmap(i * step), label=c)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend(fontsize=8, ncol=3, loc="lower right")
    plt.show()


if __name__ == "__main__":
    main()

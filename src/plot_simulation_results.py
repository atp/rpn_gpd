"""Plot simulation results for a given file or directory.
"""

import os
import sys
from datetime import datetime
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from grasp_scene_data import GraspData
from learning import Metrics
from plot_precision_recall_qd_vs_gpd import paretoFrontier

np.set_printoptions(suppress=True, precision=3)

threshs = np.linspace(0.1, 0.9, 9)

num_hand_orientations = {"ma": 99, "qd": 196}


def setLimitsLabels(
    xlim=[-0.05, 1.05], ylim=[-0.05, 1.05], xlabel="", ylabel="", title="", fpath=""
):
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if len(title) > 0:
        plt.title(title)
    if len(fpath) > 0:
        plt.savefig(fpath)


def plotScatter(
    x,
    y,
    marker="x",
    xlabel="",
    ylabel="",
    title="",
    fpath="",
    xlim=[-0.05, 1.05],
    ylim=[-0.05, 1.05],
):
    plt.figure()
    plt.plot(x, y, marker)
    setLimitsLabels(
        xlabel=xlabel, ylabel=ylabel, title=title, fpath=fpath, xlim=xlim, ylim=ylim
    )
    plt.show()


def readCategories(path: str) -> list:
    if "3dnet" in path:
        objects_filepath = "../cfg/3dnet_train.txt"
    elif "bbbcm" in path:
        objects_filepath = "../cfg/graspnet.txt"

    objects_filepath = "../cfg/graspnet.txt"
    with open(objects_filepath) as fin:
        categories = fin.readlines()
        categories = ["{}".format(c.strip()) for c in categories]

    return categories


def plotDirectory(path: str, category: str = "") -> None:
    """Plot the results for a given directory.

    Args:
        path (str): the path to the directory.
        category (str, optional): the object category.
    """
    files = sorted(glob(os.path.join(path) + f"*{category}*.npy"))
    print(f"Loaded {len(files)} files for category {category}")

    method = np.load(files[0], allow_pickle=True).item()["method"]
    # method = "qd"
    method = "graspnet"
    if method == "ma" or method == "qd":
        num_threshs = len(threshs) ** 3
    else:
        num_threshs = len(threshs)
    data = GraspData()
    metrics = {}
    # files = files[:100]
    for f in files:
        data = GraspData.fromFile(f)
        key = f[f.rfind("/") + 1 : f.rfind(".npy")]
        print(f"Calculating metrics for file {key} ...")
        if method in ["ma", "qd"]:
            metrics[key] = calcMetricsMA(
                data.labels, data.scores, threshs, num_hand_orientations[method]
            )
        else:
            scores = np.array(data.scores)
            if type(data.labels) == list:
                data.labels = np.array(data.labels)
            metrics[key] = [
                Metrics(data.labels, (scores > t).astype(int)) for t in threshs
            ]

    # Plot precision against recall over all categories.
    # num_views = 5
    num_views = len(files)
    print(num_threshs)
    precisions = np.zeros(num_threshs)
    recalls = np.zeros(num_threshs)
    for k, v in metrics.items():
        print(k, len(v))
        prec = [x.precision() for x in v]
        print(len(prec))
        precisions += np.array([x.precision() for x in v])
        recalls += np.array([x.recall() for x in v])
    precisions /= num_views
    recalls /= num_views

    plotScatter(recalls, precisions, "x", "recall", "precision")
    # fpath = f"/tmp/precision_recall_avg_{datetime.now()}.png"
    # plt.savefig(fpath)
    # print("Stored average plot at:", fpath)

    pareto = paretoFrontier(np.vstack((recalls, precisions)))
    idxs = np.argsort(pareto[0], axis=0)
    pareto = pareto[:, idxs]
    plt.figure()
    plt.plot(pareto[0], pareto[1], label="QD: ROT-GC")
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()

    # Plot precision against recall for each category.
    categories = readCategories(path)
    pareto_pr = {}
    plt.figure()
    for c in categories:
        precisions = np.zeros(num_threshs)
        recalls = np.zeros(num_threshs)
        sub_metrics = {k: metrics[k] for k in metrics if c in k}
        for k, v in sub_metrics.items():
            precisions += np.array([x.precision() for x in v])
            recalls += np.array([x.recall() for x in v])
        precisions /= len(sub_metrics)
        recalls /= len(sub_metrics)
        pareto_pr[c] = paretoFrontier(np.vstack((recalls, precisions)))
        idxs = np.argsort(pareto_pr[c][0], axis=0)
        pareto_pr[c] = pareto_pr[c][:, idxs]
        plt.plot(recalls, precisions, "x", label=c)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()
    # fpath = f"/tmp/precision_recall_category_{datetime.now()}.png"
    # plt.savefig(fpath)
    # print("Stored category plot at:", fpath)

    plt.figure()
    for c in categories:
        # plt.plot(pareto_pr[c][0], pareto_pr[c][1], "x", label=c)
        plt.plot(pareto_pr[c][0], pareto_pr[c][1], label=c)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()


def plotFile(path: str) -> None:
    """Plot the results for a given file.

    Args:
        path (str): the path to the file.
    """
    data = GraspData.fromFile(path)
    data.method = "qd"
    if data.method in ["ma", "qd"]:
        metrics_list = calcMetricsMA(
            data.labels, data.scores, threshs, num_hand_orientations[data.method]
        )
    else:
        scores = np.array(data.scores)
        metrics_list = [Metrics(data.labels, (scores > t).astype(int)) for t in threshs]

    precisions = [x.precision() for x in metrics_list]
    recalls = [x.recall() for x in metrics_list]
    plotScatter(recalls, precisions, "x", "recalls", "precisions")


def calcMetricsMA(labels: list, scores: dict, threshs: list, num_rots: int) -> list:
    """Calculate the evaluation metrics for MA or QD.

    Args:
        labels (list): the grasp labels.
        scores (dict): the scores produced by CLS, ROT, and GC.
        threshs (list): the thresholds at which to calculate the metrics.
        num_rots (int): the number of hand orientations.

    Returns:
        list: the evaluation metrics for each threshold.
    """
    num_samples = int(np.floor(len(scores["rot"]) / float(num_rots)))
    shape = (num_samples, num_rots)
    labels = labels.reshape(shape)
    metrics_list = []
    scores = {k: scores[k].numpy() for k in scores}

    for a in threshs:
        cls_mask = np.zeros(shape, dtype=bool)
        cls_mask[scores["cls"] > a, :] = True
        for b in threshs:
            rot_mask = (scores["rot"] > b).reshape(shape)
            for c in threshs:
                gc_mask = (scores["gc"] > c).reshape(shape)
                mask = cls_mask & rot_mask & gc_mask
                preds = mask.astype(int)
                gc_labels = labels[cls_mask & rot_mask]
                gc_preds = preds[cls_mask & rot_mask]
                metrics_list.append(Metrics(gc_labels, gc_preds))

    return metrics_list


def plotDirectories(path: str, category: str = "") -> None:
    """Plot the results for a given directory that contains only other directories.

    Args:
        path (str): the path to the directory.
        category (str, optional): the object category.
    """
    dirs = [os.path.join(path, f) for f in os.listdir(path)]
    dirs = sorted(dirs)
    metrics, precision, recall = {}, {}, {}
    print(dirs)

    m = 2

    for d in dirs:
        method = d[d.rfind("/") + 1 :]
        method = "qd" if "qd" in method else method
        metrics[method] = {}
        files = sorted(glob(os.path.join(d, f"*{category}*.npy")))
        print(f"method: {method}, files: {len(files)}")
        for i, f in enumerate(files):
            if i == m:
                break
            data = GraspData.fromFile(f)
            key = f[f.rfind("/") + 1 : f.rfind(".npy")]
            print(f"Calculating metrics for file {key} ...")
            if "qd" in method:
                metrics[method][key] = calcMetricsMA(
                    data.labels, data.scores, threshs, num_hand_orientations[method]
                )
            else:
                scores = np.array(data.scores)
                if type(data.labels) == list:
                    data.labels = np.array(data.labels)
                metrics[method][key] = [
                    Metrics(data.labels, (scores > t).astype(int)) for t in threshs
                ]
    # print(f"metrics:\n{metrics}")

    # Calculate recall and precision.
    n = len(files)
    for m, item in metrics.items():
        num_threshs = len(list(item.items())[0][1])
        precisions = np.zeros(num_threshs)
        recalls = np.zeros(num_threshs)
        for k, v in item.items():
            # print(k)
            precisions += np.array([x.precision() for x in v])
            recalls += np.array([x.recall() for x in v])
        precision[m] = precisions / n
        recall[m] = recalls / n

    # Calculate Pareto frontier.
    pareto = {k: paretoFrontier(np.vstack((recall[k], precision[k]))) for k in recall}
    print(pareto)
    pareto = {k: x[:, np.argsort(x[0], axis=0)] for k, x in pareto.items()}

    plt.figure()
    for k in recall:
        plt.plot(recall[k], precision[k], "x", label=k)
        # plt.plot(pareto[k][0], pareto[k][1], "x", label=k)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()


def main() -> None:
    if len(sys.argv) < 2:
        print("Error: not enough input arguments.")
        print("Usage: python plot_simulation_results.py PATH [CATEGORY]")
        sys.exit(-1)
    path = sys.argv[1]
    if len(sys.argv) > 2:
        category = sys.argv[2]
    else:
        category = ""

    if os.path.isdir(path):
        is_directory = [os.path.isdir(os.path.join(path, f)) for f in os.listdir(path)]
        if np.all(np.array(is_directory)):
            plotDirectories(path, category)
        else:
            plotDirectory(path, category)
    elif os.path.isfile(path):
        plotFile(path)


if __name__ == "__main__":
    main()

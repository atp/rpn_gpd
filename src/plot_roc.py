"""Plot precision-recall, ROC curves (recall-fpr), precision-dps, etc.

Usage: python plot_roc.py DIRECTORY

This program expects as input the path to a directory with a specific structure. The
directory should contain one folder per method. Each of those folders should contain
one *.npy file per object instance. These *.npy files can be produced by
`eval_grasp_detector.py` or `eval_clutter.py`.

DPS stands for detections per second.

"""

import os
import sys
from glob import glob

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import ConvexHull

np.set_printoptions(suppress=True)

colors = {"gpd": "blue", "graspnet": "black",
          "ma": "orange", "ma_depth": "red"}
markers = {"gpd": "+", "graspnet": "x", "ma": "o", "ma_depth": "."}


def calcDetectionsPerSecond(props: np.array, dets: np.array, method: str) -> np.array:
    """Calculate the number of detections per second.

    Detections per second is calculated as t = n1 * t1 + n2 * t2, where n1 is the
    number of samples, t1 is the time per sample, n2 is the number of proposals, and
    t2 is the time per proposal.

    Args:
        props (np.array): n x 1 number of proposals.
        dets (np.array): n x 1 number of detections.
        method (str): the name of the grasp detection method.

    Returns:
        np.array: n x 1 detections per second.
    """
    time_per_prop = {"graspnet": 0.0, "gpd": 0.004,
                     "ma": 1e-05, "ma_depth": 2e-05}
    time_per_grasp = {"graspnet": 0.0115,
                      "gpd": 0.0005, "ma": 0.0005, "ma_depth": 0.0005}
    # TODO: What are those values for GPD, MA?
    num_samples = {"graspnet": 0, "gpd": 600000,
                   "ma": 600000, "ma_depth": 600000}
    time_forward = 0.0026  # per batch (Lenet)
    batch_size = 64

    # Calculate the total time required to compute all proposals.
    time_props = num_samples[method] * time_per_prop[method]
    print(f'[{method}] time_props: {time_props}')

    # Calculate the total time required to compute all grasps.
    time_grasp_images = props * time_per_grasp[method]
    num_batches = props / batch_size
    if method in ["gpd", "ma"]:
        time_forwards = num_batches * time_forward
    else:
        time_forwards = 0.0

    # Calculate the total time and the number of detections per second.
    total_times = time_props + time_grasp_images + time_forwards
    # total_times = time_grasp_images + time_forwards
    dps = dets / total_times
    # dps = total_times
    # dps = dets / (0.1*np.ones(len(dets)))

    return dps


def calcMetrics(X: np.array, method: str) -> dict:
    """Calculate evaluation metrics for a given confusion matrix.

    Args:
        X (np.array): the confusion matrix for which to calculate the metrics.
        method (str): the name of the corresponding grasp detection method.

    Returns:
        dict: the evaluation metrics.
    """
    # print("[calcMetrics] X:", X.shape)
    metrics = {
        'precision': X[:, 0] / (X[:, 0] + X[:, 1]),  # tp / (tp + fp)
        'recall': X[:, 0] / (X[:, 0] + X[:, 3]),  # tp / (tp + fn)
        'fpr': X[:, 1] / (X[:, 1] + X[:, 2]),  # fp / (fp + tn)
        'proposals': X[:, -1],  # number of GC inputs
        'detections': X[:, 0] + X[:, 1],
        'dps': calcDetectionsPerSecond(X[:, -1], X[:, 0] + X[:, 1], method)
    }

    # print("no skill:", (X[:, 0] + X[:, 3])/(X[:, 1] + X[:, 2]))
    # tp = X[:, 0]
    # positives = X[:, 0] + X[:, 3]
    # print("ground truth positives:", positives.min(),
    # positives.max(), positives.mean())
    # print("true positives:", [tp.min(), tp.max(), tp.mean()])

    # Adjust precision and recall for divisions by zero.
    for k in ["precision", "recall", "fpr"]:
        mask = np.logical_or(np.isinf(metrics[k]), np.isnan(metrics[k]))
        metrics[k][mask] = 0.0

    # Add (precision,recall) value at (1,0).
    metrics["precision"] = np.hstack((np.array([1.0]), metrics["precision"]))
      metrics["recall"] = np.hstack((np.array([0.0]), metrics["recall"]))
       for m in ["fpr", "dps", "detections", "proposals"]:
            metrics[m] = np.hstack((np.array([0.0]), metrics[m]))
        # metrics["fpr"] = np.hstack((np.array([0.0]), metrics["fpr"]))
        # metrics["dps"] = np.hstack((np.array([0.0]), metrics["dps"]))
        # metrics["detections"] = np.hstack((np.array([0.0]), metrics["detections"]))
        # metrics["proposals"] = np.hstack((np.array([0.0]), metrics["proposals"]))

        # Add (precision,recall) value at (0,1).
        metrics["precision"] = np.hstack(
            (metrics["precision"], np.array([0.0])))
        metrics["recall"] = np.hstack((metrics["recall"], np.array([1.0])))
        metrics["fpr"] = np.hstack((metrics["fpr"], np.array([1.0])))
        for m in ["dps", "detections", "proposals"]:
            metrics[m] = np.hstack((np.array([0.0]), metrics[m]))

        # print(metrics["precision"])
        # print(metrics["recall"])

        return metrics

    def calcInstanceMetrics(directory: str):
        """Calculate the average evaluation metrics for each object instance.

        Args:
            directory (str): directory whose subdirectories contain NPY files.
        """
        methods = os.listdir(directory)
        avg_metrics = {}

        for m in methods:
            files = sorted(glob(os.path.join(directory, m) + "/*.npy"))
            avg_metrics[m] = {"precision": 0, "recall": 0, "fpr": 0, "dps": 0}
            for f in files:
                # A = np.load(f)[:, :, -5:]
                # print(A.shape, np.sum(A, 0).shape)
                confmat = np.sum(np.load(f)[:, :, -5:], 0)
                metrics = calcMetrics(confmat, m)
                s = f"method: {m}, object: {f}"
                for k in avg_metrics[m]:
                    avg_metrics[m][k] += metrics[k]
                    s += f", {k}: min,max = [{metrics[k].min():.3f}, {metrics[k].max():.3f}] "
                    s += f"mean = {metrics[k].mean():.3f}"
                print(s)
            avg_metrics[m] = {k: avg_metrics[m][k] /
                              len(files) for k in avg_metrics[m]}

        return avg_metrics

    def calcViewpointMetrics(directory: str, category: str):
        """Calculate the average evaluation metrics from a per-viewpoint basis.

        Args:
            directory (str): directory whose subdirectories contain NPY files.
        """
        methods = os.listdir(directory)
        avg_metrics = {}

        for m in methods:
            files = sorted(
                glob(os.path.join(directory, m) + f"/{category}*.npy"))
            avg_metrics[m] = {"precision": 0, "recall": 0, "fpr": 0, "dps": 0}
            total = 0
            for f in files:
                data = np.load(f)[:, :, -5:]
                total += len(data)
                for r in data:
                    metrics = calcMetrics(r, m)
                    s = f"method: {m}, object: {f}"
                    for k in avg_metrics[m]:
                        avg_metrics[m][k] += metrics[k]
                        s += f", {k}: min,max = [{metrics[k].min():.3f}, {metrics[k].max():.3f}] "
                        s += f"mean = {metrics[k].mean():.3f}"
                    print(s)
            print(f"total: {total}, files: {len(files)}")
            avg_metrics[m] = {k: avg_metrics[m]
                              [k] / total for k in avg_metrics[m]}

        return avg_metrics

    def calcMetricsForDirectory(directory: str, category: str) -> dict:
        """Calculate evaluation metrics based on a given directory.

        Args:
            directory (str): the path to the directory.

        Returns:
            dict: the evaluation metrics.
        """
        num_threshs = {"gpd": 20, "graspnet": 20, "ma": 8000, "ma_depth": 8000}

        methods = os.listdir(directory)
        confmats, metrics = {}, {}
        for m in methods:
            confmats[m] = np.zeros((num_threshs[m], 5))
            files = sorted(
                glob(os.path.join(directory, m) + f"/{category}*.npy"))
            for f in files:
                confmats[m] += np.sum(np.load(f)[:, :, -5:], 0)
            metrics[m] = calcMetrics(confmats[m], m)
        return confmats, metrics

    def calcCategoryMetrics(in_dir: str, out_dir: str):
        """Calculate the average evaluation metrics for each object category.

        Args:
            in_dir (str): [TODO:description]
            out_dir (str): [TODO:description]
        """
        xlabels = ["recall", "fpr", "dps"]
        ylabels = ["precision", "recall", "precision"]

        # Determine the object categories.
        methods = os.listdir(in_dir)
        files = sorted(glob(os.path.join(in_dir, methods[0]) + f"/*.npy"))
        files = [''.join([x for x in f if not x.isdigit()]) for f in files]
        files = set([f[f.rindex("/") + 1: f.rindex("_")] for f in files])
        categories = sorted(list(files))
        # print(list(files)[:5])

        confmats, metrics = {}, {}
        for c in categories:
            confmats[c], metrics[c] = calcMetricsForDirectory(in_dir, c)
            for x, y in zip(xlabels, ylabels):
                print("Plotting:", c, x, y)
                # plt.figure()
                # plotMetrics(metrics[c], x, y)
                # plt.savefig(out_dir + c + f"_{y}-{x}")

        avg_metrics = {}
        for m in methods:
            avg_metrics[m] = {"precision": 0, "recall": 0, "fpr": 0, "dps": 0}
            for c in categories:
                for k in avg_metrics[m]:
                    avg_metrics[m][k] += metrics[c][m][k]
                    if k == "fpr":
                        print(avg_metrics[m][k],
                              "\n----------\n", metrics[c][m][k])
            print("=====================")
            print(avg_metrics[m]["fpr"])
            avg_metrics[m] = {k: avg_metrics[m][k] /
                              len(categories) for k in avg_metrics[m]}
            print(avg_metrics[m]["fpr"], "\n")
        # plt.figure()
        # plotMetrics(avg_metrics, "recall", "precision")
        # plt.show()

        return avg_metrics

    def paretoFrontier(pts: np.array) -> np.array:
        """Calculate the Pareto frontier for a set of points.

        Args:
            pts (np.array): 2 x n the set of points.

        Returns:
            np.array: 2 x n the points on the Pareto frontier.
        """
        pareto = np.ones(pts.shape[1], dtype=bool)
        for i in range(len(pareto)):
            b = np.all(pts.T > pts[:, i], 1)
            if np.any(b):
                pareto[i] = 0

        return pts[:, pareto]

    def plotMetrics(metrics: np.array, xlabel: str, ylabel: str, title: str = ""):
        """Plot a curve for the given evaluation metrics.

        Args:
            metrics (np.array): the evaluation metrics.
            xlabel (str): the metric to be used as the x values.
            ylabel (str): the metric to be used as the y values.
            title (str, optional): the title for the plot.
        """
        colors = {"gpd": "blue", "graspnet": "black",
                  "ma": "orange", "ma_depth": "red"}
        alphas = {"gpd": 0.5, "graspnet": 1.0, "ma": 0.25, "ma_depth": 0.25}
        markers = {"gpd": "+", "graspnet": "x", "ma": "o", "ma_depth": "."}
        markersizes = {"gpd": 8, "graspnet": 10, "ma": 10, "ma_depth": 10}

        for m in metrics:
            plt.plot(metrics[m][xlabel], metrics[m][ylabel], markers[m],
                     color=colors[m], label=m, alpha=alphas[m], markersize=markersizes[m])
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.ylim([-0.05, 1.05])
        if xlabel == "detections":
            plt.xlim([-0.05, 50000000])
    plt.legend()


def main():
    if len(sys.argv) < 3:
        print("Error: not enough input arguments.")
        print("Usage: python plot_roc.py DIRECTORY CALC_AVERAGE [OBJECT]")
        print("DIRECTORY: path to directory with *.npy files")
        print("CALC_AVERAGE: 0 unweighted, 1 category, 2 instance")
        sys.exit(-1)
    directory = sys.argv[1]
    calc_avg = int(sys.argv[2])
    if len(sys.argv) == 4:
        category = sys.argv[3]
    else:
        category = ""

    # Calculate confusion matrices and metrics.
    if calc_avg == 1:
        print("Calculating average metrics based on categories ...")
        metrics = calcCategoryMetrics(directory, "../results/plots/")
    elif calc_avg == 2:
        print("Calculating average metrics based on instances ...")
        metrics = calcInstanceMetrics(directory)
    elif calc_avg == 3:
        print("Calculating average metrics based on viewpoints ...")
        metrics = calcViewpointMetrics(directory, category)
    else:
        confmats, metrics = calcMetricsForDirectory(directory, category)
        print(f"Calculated confusion matrices for {confmats.keys()}")
        print(f"Calculated metrics for {metrics.keys()}:")
        for m in metrics:
            print(f"{m}: {[[k, metrics[m][k].shape] for k in metrics[m]]}")

    # Plot the evaluation metrics as scatter.
    # xlabels = ["recall", "fpr"]
    # ylabels = ["precision", "recall"]
    xlabels = ["recall", "fpr", "dps"]
    ylabels = ["precision", "recall", "precision"]
    # xlabels = ["recall", "fpr", "dps", "detections"]
    # ylabels = ["precision", "recall", "precision", "precision"]
    cols = len(ylabels)
    # plt.figure(figsize=(20, 4))
    plt.figure(figsize=(20, 6))
    # plt.figure(figsize=(15, 3))
    for i, (x, y) in enumerate(zip(xlabels, ylabels)):
        plt.subplot(1, cols, i+1)
        plotMetrics(metrics, x, y)
    res = [i for i in range(len(directory)) if directory.startswith("/", i)]
    middle = directory[res[-2] + 1:-1]
    if len(category) > 0:
        middle += "_" + category
    plt.savefig("/tmp/" + middle + "_scatter.png")
    plt.show()

    # Calculate convex hull or pareto frontiers for each metrics pair.
    print("Calculating Pareto frontiers ...")
    fronts = {}
    for x, y in zip(xlabels, ylabels):
        print(f"  ... for {y}-{x}")
        pts = {m: np.vstack((metrics[m][x], metrics[m][y])) for m in metrics}
        key = x + "_" + y
        fronts[key] = {m: pts[m] for m in metrics}
        ma_keys = [m for m in metrics if "ma" in m]
        ma_keys = [m for m in metrics]
        for m in ma_keys:
            if x == "fpr":
                if "ma" in m:
                    hull = ConvexHull(pts[m].T)
                    fronts[key][m] = pts[m][:, hull.vertices[1:]]
                else:
                    fronts[key][m] = pts[m]
            else:
                fronts[key][m] = paretoFrontier(pts[m])

    # Plot frontiers.
    cols = len(ylabels)
    use_markers_list = [True, False]
    for use_markers in use_markers_list:
        plt.figure(figsize=(20, 6))
        for i, k in enumerate(fronts):
            plt.subplot(1, cols, i+1)
            for m in fronts[k]:
                # _, idxs = np.unique(fronts[k][m][0], return_index=True)
                # fronts[k][m] = fronts[k][m][:, idxs]
                idxs = np.argsort(fronts[k][m][0])
                if use_markers:
                    plt.plot(fronts[k][m][0, idxs], fronts[k][m]
                             [1, idxs], markers[m], label=m, color=colors[m])
                    mid = middle + "_markers"
                else:
                    plt.plot(fronts[k][m][0, idxs], fronts[k][m]
                             [1, idxs], label=m, color=colors[m])
                    mid = middle + "_curves"
            plt.legend()
            plt.xlabel(xlabels[i])
            plt.ylabel(ylabels[i])
        plt.savefig("/tmp/" + mid + "_front.png")
        plt.show()


if __name__ == "__main__":
    main()

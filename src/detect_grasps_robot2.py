"""Detect grasps in point clouds obtained by a robot.
"""

import os
import shutil
import sys
from argparse import ArgumentParser
from copy import deepcopy
from time import sleep, time

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from detect_grasps_3dnet import preprocessPointCloud
from eval_grasp_detector import lenet_layout
from geometry.point_cloud import PointCloud
from geometry.rect import Rect
from grasp_detector import GraspDetector, createGraspDetector, findGraspsAbovePlane
from hand_params import antipodal_params, hand_params
from handles import Handle, findHandles, pruneShortHandles, removeEndPoints
from hands.deep_hand_eval import DeepHandEval
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from networks.network import LenetLike
from proposal import downsampleByDistance, sampleFromCube
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot

# topk = {"rot": 5000, "gc": 100}
# topk = {"rot": 300, "gc": 200}
# topk = {"rot": 200, "gc": 100}
# topk = {"rot": 300, "gc": 300}
# topk = {"rot": 300, "gc": 100}
topk = {"rot": 300, "gc": 150}
# topk = {"rot": 300, "gc": 200}
# topk = {"rot": 400, "gc": 100}
cube_size = 1.0

# TODO: Read in from file automatically.
# The rotation below is needed because the camera pose in OpenGL is flipped.
# camera_pose = np.eye(4)
camera_pose = np.array(
    [
        [0.686, 0.158, 0.71, -0.05],
        [-0.095, 0.987, -0.127, 0.11],
        [-0.721, 0.02, 0.692, 0.41],
        [0.0, 0.0, 0.0, 1.0],
    ]
)
Ty, Tz = np.eye(4), np.eye(4)
Ty[:3, :3] = Rot.from_rotvec(-np.pi / 2.0 * np.array([0, 1, 0])).as_matrix()
camera_pose = np.dot(camera_pose, Ty)
Tz[:3, :3] = Rot.from_rotvec(-np.pi / 2.0 * np.array([0, 0, 1])).as_matrix()
camera_pose = np.dot(camera_pose, Tz)

prop_params = {"image_size": 60, "image_type": "depth"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], 3]
grasp_img_shape = (60, 60, 4)

# weights_dir = "../models/comparison/one_stage_antipodal/3dnet/"
weights_dir = "../models/comparison/two_stage_antipodal/3dnet/"
# weights_dir = "../models/comparison/two_stage_antipodal/two_views/3dnet/"
net_cfgs = {}
net_cfgs["rot-gc"] = {
    "network": LenetLike,
    "layout": lenet_layout,
    "batch_size": 1024,
    "weights_paths": {
        "rot": weights_dir + "rot_lenet.pth",
        "gc": weights_dir + "gc_4channels_lenet.pth",
    },
}

voxel_size = 0  # point clouds from robot are already voxelized to 3mm

red = [1.0, 0.0, 0.0]
green = [0.0, 1.0, 0.0]


def plotColoredGrasps(
    cloud: o3d.geometry.PointCloud,
    poses: list,
    colors: list,
    hand_geom: HandGeometry,
    title: str = "",
):
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    for x, c in zip(poses, colors):
        p.plotHands(x, c, opacity=0.3)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose, [0.05, 0.05, 0.05])
    p.setTitle(title)
    p.show()


def plotGrasps(
    cloud: o3d.geometry.PointCloud,
    poses: np.array,
    hand_geom: HandGeometry,
    title: str = "",
    colors: list = [],
):
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    if len(colors) == 0:
        p.plotHands(poses, [0.0, 1.0, 0.0])
    else:
        p.plotHands(poses, colors)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose, [0.05, 0.05, 0.05])
    p.setTitle(title)
    p.show()


def plotCloudsAndGrasps(
    clouds: list,
    poses: np.array,
    hand_geom: HandGeometry,
    title: str = "",
):
    p = HandsPlot(hand_geom)
    for c in clouds:
        p.plotCloud(c, list(np.random.rand(3)))
    # p.plotHands(poses, [0.0, 1.0, 0.0])
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose, [0.05, 0.05, 0.05])
    p.setTitle(title)
    p.show()


def detectGrasps(
    clouds: list,
    detector: GraspDetector,
    num_samples: int,
    is_plotting: bool,
    plane_height: float = np.nan,
) -> np.array:
    origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
    full_cloud, objects_cloud = clouds[0], clouds[1]

    # Prefer samples further away from tabletop.
    offset = 0.01
    min_samples = 30
    pts = np.asarray(objects_cloud.points)
    cutoff = pts.max() - 2 * offset
    print("pts:", pts.min(), pts.max(), pts.mean())
    max_plane_height = np.min(np.asarray(objects_cloud.points)[:, 2]) + offset
    samples_cloud = downsampleByDistance(
        objects_cloud, max_plane_height, 2, 0.1, cutoff
    )
    if len(samples_cloud.points) < min_samples:
        samples_cloud = deepcopy(objects_cloud)
    if is_plotting:
        o3d.visualization.draw_geometries([samples_cloud], "samples_cloud", 640, 480)

    # Draw samples from bounding cube.
    num_samples = np.min([num_samples, len(samples_cloud.points)])
    samples = sampleFromCube(samples_cloud, cube_size, num_samples)
    # num_samples = np.min([num_samples, len(objects_cloud.points)])
    # samples = sampleFromCube(objects_cloud, cube_size, num_samples)
    samples_cloud = numpyToCloud(samples)
    n = len(samples_cloud.points)
    full_cloud.paint_uniform_color([0.0, 0.0, 1.0])
    samples_cloud.paint_uniform_color([1.0, 0.0, 0.0])
    print(f"Sampled {n} points out of {len(objects_cloud.points)}.")
    if is_plotting:
        o3d.visualization.draw_geometries(
            [samples_cloud, full_cloud, origin_frame], "Samples", 640, 480
        )

    full_cloud = estimateNormals(full_cloud, camera_pose[:3, 3])

    # Detect grasp poses.
    detection_time = time()
    poses, scores = detector.detectGraspsBest(
        clouds, samples, camera_pose, topk, plane_height  # full_cloud
    )
    detection_time = time() - detection_time
    print(f"Generated {len(poses)} grasp poses in {detection_time:.3f}s.")
    print(f"Scores:\n{scores}")

    # Debugging: verify that the grasp at the mid index is correct.
    """
    hands = {"indices": np.array([[0], [int(detector.hands_gen.num_hand_orientations/2)]])}
    print(hands)
    tree = o3d.geometry.KDTreeFlann(objects_cloud)
    poses = detector.hands_gen.indicesToPoses(objects_cloud, tree, samples, hands)
    """

    # Show approach directions for a single position.
    """
    m = detector.hands_gen.getNumHandsPerSample()
    hands = {"indices": np.vstack((np.zeros(m,dtype=int), np.arange(m))), "frames": []}
    print("pose indices:", hands["indices"])
    tree = o3d.geometry.KDTreeFlann(objects_cloud)
    poses = detector.hands_gen.indicesToPoses(full_cloud, tree, samples, hands)
    plotGrasps(full_cloud, poses, detector.hands_gen.hand_geom, "Detected grasps")
    """

    return poses, scores


def forwardPushGrasps(
    poses: np.array, cloud: o3d.geometry.PointCloud, hand_eval: HandEval
) -> np.array:
    min_dist = 0.005
    forward_thresh = 0.03
    nn_radius = 0.2
    half_height = 0.5 * hand_eval.hand_geom.height
    finger_width = hand_eval.hand_geom.finger_width
    tree = o3d.geometry.KDTreeFlann(cloud)
    poses_out = np.zeros_like(poses)
    # rots = poses[:, :3, :3]
    # pos = poses[:, :3, 3]
    # nn_idxs = [
    # np.asarray(tree.search_radius_vector_3d(x, nn_radius)[1])
    # for x in poses[:3, :3, 3]
    # ]
    # points = np.asarray(cloud.points)
    # pts_nn = [np.dot(X[:3, :3].T, (points[nn_idx] - X[:3, 3]).T) for X in poses]
    # pts_nn = [x[:, (x[2, :] > -half_height) & (x < half_height)] for x in pts_nn]
    left = -0.5 * hand_eval.hand_geom.outer_diameter + finger_width
    right = 0.5 * hand_eval.hand_geom.outer_diameter - finger_width
    bottom = 0.0
    top = bottom + hand_eval.hand_geom.depth
    bounds = Rect(bottom, left, top, right)
    is_new = False
    # pts_rect = [x[:, bounds.contains(x.T)] for x in pts_nn]
    # closest = [np.min(x[0, :]) for x in pts_rect]
    # masks = [x > forward_thresh for x in closest]
    # [hand_eval.calcHandBottom(x, left, right, bottom) for i,x in enumerate(pts_nn) if ]

    for i, pose in enumerate(poses):
        position = pose[:3, 3]
        [_, nn_idx, _] = tree.search_radius_vector_3d(position, nn_radius)
        R = pose[:3, :3]
        pts_raw = np.dot(R.T, (np.asarray(cloud.points)[nn_idx] - position).T)
        mask = (pts_raw[2, :] > -half_height) & (pts_raw[2, :] < 0.5 * half_height)
        pts_crop = pts_raw[:, mask]
        left = -0.5 * hand_eval.hand_geom.outer_diameter
        right = 0.5 * hand_eval.hand_geom.outer_diameter
        bottom = 0.0
        top = bottom + hand_eval.hand_geom.depth
        bounds = Rect(bottom, left + finger_width, top, right - finger_width)
        mask = bounds.contains(pts_crop.T)
        if np.count_nonzero(mask) == 0:
            poses_out[i] = deepcopy(pose)
            continue
        pts_rect = pts_crop[:, mask]
        closest = np.min(pts_rect[0, :])
        pose_out = deepcopy(pose)
        if closest < min_dist:
            print(f"{i} Hand close to object surface.")
            pose_out[:3, 3] = position - R[:3, 0] * min_dist
            is_new = True
        elif closest > forward_thresh:
            print(f"{i} Hand far from object surface.")
            bottom = hand_eval.calcHandBottom(
                pts_crop, left, right - finger_width, closest
            )
            print("new bottom:", bottom, "closest:", closest)
            if bottom > 0:
                pose_out[:3, 3] = position + R[:3, 0] * bottom
                is_new = True
        if is_new:
            print(f"Correction to position: {position} --> {pose_out[:3,3]}")
            # plotColoredGrasps(
                # cloud,
                # [[pose], [pose_out]],
                # [red, green],
                # hand_eval.hand_geom,
                # f"{i}a",
            # )
            is_new = False
        poses_out[i] = pose_out

    return poses_out


def centerGrasps(
    poses: np.array,
    cloud: o3d.geometry.PointCloud,
    hand_geom: HandGeometry,
    return_widths: bool = False,
    plot_verbose: bool = False,
) -> np.array:
    nn_radius = 0.2
    forward_offset = 0.005
    poses_out = np.zeros_like(poses)
    widths = np.zeros(len(poses))
    tree = o3d.geometry.KDTreeFlann(cloud)

    for i, pose in enumerate(poses):
        position = pose[:3, 3]
        [_, nn_idx, _] = tree.search_radius_vector_3d(position, nn_radius)
        R = pose[:3, :3]
        pts_raw = np.dot(R.T, (np.asarray(cloud.points)[nn_idx] - position).T)
        mask = (pts_raw[2, :] > -0.5 * hand_geom.height) & (
            pts_raw[2, :] < 0.5 * hand_geom.height
        )
        crop_idxs = np.asarray(nn_idx)[mask]
        pts_crop = pts_raw[:, mask]
        left = -0.5 * hand_geom.outer_diameter
        right = 0.5 * hand_geom.outer_diameter
        bottom = 0.0
        top = bottom + hand_geom.depth
        bounds = Rect(
            bottom, left + hand_geom.finger_width, top, right - hand_geom.finger_width
        )
        mask = bounds.contains(pts_crop.T)
        if np.count_nonzero(mask) == 0:
            poses_out[i] = deepcopy(pose)
            continue
        pts_rect = pts_crop[:, mask]
        idxs = crop_idxs[mask]
        y = pts_rect[1, :]
        length_y = np.max(y) - np.min(y)
        center_y = np.min(y) + 0.5 * length_y
        new_position = position + center_y * pose[:3, 1]

        closest_x = np.min(pts_rect[0, :])
        if closest_x < forward_offset:
            new_position = new_position - forward_offset * pose[:3, 0]
            print(f"{i} Hand base close to object. Applied offset along forward axis.")

        print(f"{position} --> {new_position}")
        pose_out = deepcopy(pose)
        pose_out[:3, 3] = new_position
        poses_out[i] = pose_out
        widths[i] = length_y

        if plot_verbose:
            plotColoredGrasps(
                cloud, [[pose], [pose_out]], [red, green], hand_geom, f"{i}a"
            )
            # plotGrasps(cloud, pose_out[np.newaxis, ...], hand_geom, f"{i}b")
            # mask_cloud = cloud.select_by_index(o3d.utility.IntVector(mask_idx))
            # plotGrasps(mask_cloud, pose[np.newaxis, ...], hand_geom, f"{i}c")
            # plotGrasps(mask_cloud, pose_out[np.newaxis, ...], hand_geom, f"{i}d")

    if return_widths:
        return poses_out, widths

    return poses_out


def removeDuplicates(poses, min_pos_dist=0.003, min_rot_dist=3.0):
    min_rot_dist = np.deg2rad(min_rot_dist)
    rots = [X[:3, :3].T for X in poses]
    poses_out = []

    for i, pose in enumerate(poses):
        pos_dists = np.array([np.linalg.norm(X[:3, 3] - pose[:3, 3]) for X in poses])
        rot_dists = [
            np.arccos(0.5 * (np.trace(np.matmul(pose[:3, :3], R)) - 1)) for R in rots
        ]
        rot_dists = np.array([np.real(x) for x in rot_dists])
        mask = (pos_dists <= min_pos_dist) & (rot_dists <= min_rot_dist)
        # print(i, "similar:", np.count_nonzero(mask))
        if np.count_nonzero(mask) == 1:
            poses_out.append(pose)

    return np.array(poses_out)


def removeIsolated(poses, min_pos_dist=0.01, min_neighbors=1):
    poses_out = []

    for i, pose in enumerate(poses):
        pos_dists = np.array([np.linalg.norm(X[:3, 3] - pose[:3, 3]) for X in poses])
        mask = pos_dists <= min_pos_dist
        if np.count_nonzero(mask) > min_neighbors:
            poses_out.append(pose)

    return np.array(poses_out)


def fillHoles(
    cloud: o3d.geometry.PointCloud, z: float, n: int
) -> o3d.geometry.PointCloud:
    mins = cloud.get_min_bound()
    maxs = cloud.get_max_bound()
    x = np.linspace(mins[0], maxs[0], n)
    y = np.linspace(mins[1], maxs[1], n)
    X, Y = np.meshgrid(x, y)
    Z = np.ones(n ** 2) * z
    X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()
    print([x.shape for x in (X, Y, Z)])
    cloud_out = numpyToCloud(np.vstack((X, Y, Z)).T)

    return cloud_out


def scoresToColors(scores):
    if type(scores) == dict:
        scores = scores["gc"]
    min_score = np.min(scores)
    max_score = np.max(scores)
    greens = (scores - min_score) / (max_score - min_score)
    colors = np.zeros((len(greens), 3))
    colors[:, 0] = 1.0 - greens
    colors[:, 1] = greens

    return colors


def parseArgs():
    """Parse command line arguments."""
    parser = ArgumentParser(description="Generate grasp poses for robot")
    parser.add_argument("method", help="grasp detector")
    parser.add_argument("full_pcd_path", help="path to PCD file produced by ROS node")
    parser.add_argument(
        "objects_pcd_path", help="path to PCD file produced by ROS node"
    )
    parser.add_argument("grasps_path", help="path to grasps file")
    parser.add_argument(
        "-rf", "--remove_file", action="store_true", help="remove file?"
    )
    parser.add_argument("--plot", action="store_true", help="visualize output grasps")
    parser.add_argument("--plot_all", action="store_true", help="visualize all steps")
    parser.add_argument(
        "--inliers", type=int, help="minimum number of handle inliers", default=5
    )
    parser.add_argument(
        "-n", "--num_samples", type=int, help="number of samples", default=100
    )
    parser.add_argument(
        "-cc",
        "--check_collisions",
        action="store_true",
        help="check collisions for detected grasps",
    )
    parser.add_argument(
        "-cg",
        "--center_grasps",
        action="store_true",
        help="center grasps based on visible point cloud",
    )
    parser.add_argument(
        "-rbt",
        "--remove_below_table",
        action="store_true",
        help="remove grasps for which a finger is below the tabletop",
    )
    parser.add_argument(
        "-kc",
        "--keep_closest",
        action="store_true",
        help="only keep grasps which are close a hande centroid",
    )
    parser.add_argument(
        "-rd",
        "--remove_duplicates",
        action="store_true",
        help="remove grasps that have other grasps close-by",
    )
    parser.add_argument(
        "-ri",
        "--remove_isolated",
        action="store_true",
        help="remove grasps that have not enough close-by grasps",
    )
    parser.add_argument(
        "-ph",
        "--prune_short_handles",
        action="store_true",
        help="remove handles which are too short",
    )

    args = parser.parse_args()

    return args


def removeCollidingGrasps(poses, cloud, coll_check, plot):
    runtime = time()
    is_free = coll_check.evalPoses(cloud, poses, checks_antipodal=False)
    num_poses = len(poses)
    mask = is_free[:, 0] == 1
    runtime = time() - runtime
    print(f"Checked {num_poses} poses for collisions in {runtime:.3f}s")
    print(f"  collision-free: {np.count_nonzero(mask)}")
    if plot:
        plotGrasps(cloud, poses[mask], coll_check.hand_geom, "Collision-free grasps")
        # for p in poses[~mask]:
        # plotGrasps(cloud, [p], coll_check.hand_geom, "Colliding grasps")

    return mask


def createOutputs(poses, widths, scores, handles):
    # Add handle inliers.
    inliers = [x.inliers for x in handles]
    inliers = [j for i in inliers for j in i]
    poses_out = np.array([poses[i] for i in inliers])
    widths_out = np.array([widths[i] for i in inliers])
    scores_out = np.array([scores[i] for i in inliers])
    print([x.shape for x in (poses_out, widths_out, scores_out)])

    # Add handle poses.
    poses_out = np.vstack((poses_out, [x.pose for x in handles]))
    widths_out = np.hstack((widths_out, [x.width for x in handles]))
    scores_out = np.hstack((scores_out, 2.0 * np.ones(len(handles))))

    # Add remaining grasps.
    outliers = [i for i in range(len(poses)) if i not in inliers]
    if len(outliers) > 1:
        outlier_poses = np.array([poses[i] for i in outliers])
        poses_out = np.vstack((poses_out, outlier_poses))
        widths_out = np.hstack((widths_out, [widths[i] for i in outliers]))
        scores_out = np.hstack((scores_out, [scores[i] for i in outliers]))

    print("scores_out:", scores_out)

    return poses_out, widths_out, scores_out


def main():
    # min_grasps_handles = int(np.floor(topk["gc"]/2))
    min_grasps_handles = 20
    min_handles = 1
    # min_grasps_handles = 30
    # min_grasps_handles = 50
    # min_grasps_handles = 100
    # min_score = 0.4
    min_score = 0.2

    # Read command line arguments.
    args = parseArgs()
    paths = [args.full_pcd_path, args.objects_pcd_path]

    # Create the grasp detector.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    net_cfg = net_cfgs[args.method]
    detector = createGraspDetector(
        hand_geom, antip_prms, args.method, prop_params, grasp_img_shape, net_cfg
    )
    deep_hand_eval = DeepHandEval(hand_geom, antip_prms)

    # Create the collision checker.
    coll_check = HandEval(hand_geom, antip_prms)

    # Ensure that there is no PCD file.
    if args.remove_file:
        for p in paths:
            if os.path.exists(p) and os.path.isfile(p):
                os.remove(p)
                print("Removed file", p)
    is_running = True

    while is_running:
        # Wait for the point cloud file to be updated.
        print("\n-------------------------------------------------------------")
        print("Waiting for PCD files ...")
        while not os.path.exists(args.objects_pcd_path):
            sleep(1)
        if not os.path.isfile(args.objects_pcd_path):
            raise ValueError("%s is not a file!" % args.pcd_path)
        print("Received PCD file")

        # Read the point cloud from the file and optionally remove the file.
        full_cloud = o3d.io.read_point_cloud(args.full_pcd_path)
        objects_cloud = o3d.io.read_point_cloud(args.objects_pcd_path)
        clouds = [full_cloud, objects_cloud]
        print("Loaded point cloud with %d points" % len(full_cloud.points))
        if args.remove_file:
            for p in paths:
                if os.path.exists(p) and os.path.isfile(p):
                    stop = p.rfind("/") + 1
                    backup_path = p[:stop] + p[stop : p.rfind(".")] + "_bak.pcd"
                    shutil.copy2(p, backup_path)
                    print("Backed up file", backup_path)
                    os.remove(p)
                    print("Removed file", p)
            sleep(0.5)
            if os.path.exists(args.grasps_path) and os.path.isfile(args.grasps_path):
                os.remove(args.grasps_path)
                print(f"Removed file: {args.grasps_path}")

        # Preprocess the point clouds.
        if voxel_size > 0:
            full_cloud = full_cloud.voxel_down_sample(0.003)
            print(f"Downsampled <full_cloud> to {len(full_cloud.points)} points.")
        objects_cloud = preprocessPointCloud(objects_cloud, camera_pose, voxel_size)
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
        if args.plot_all:
            o3d.visualization.draw_geometries(
                [objects_cloud, origin_frame], "Surface normals", 640, 480
            )

        plane_height = np.nan
        if args.remove_below_table:
            offset = 0.003
            plane_model, inliers = full_cloud.segment_plane(
                distance_threshold=0.01, ransac_n=3, num_iterations=1000
            )

            minor = o3d.__version__
            minor = minor[minor.find(".") + 1 :]
            minor = minor[: minor.find(".")]
            if int(minor) <= 8:
                inlier_cloud = full_cloud.select_down_sample(inliers)
            else:
                inlier_cloud = full_cloud.select_by_index(inliers)
            min_plane_height = np.mean(np.asarray(inlier_cloud.points)[:, 2])
            print("The height of the segmented plane is:", min_plane_height)
            plane_height = np.min(np.asarray(full_cloud.points)[:, 2])
            plane_height = plane_height - offset
            print("The plane height is guessed to be:", plane_height)
            plane_height = min_plane_height
            f = fillHoles(full_cloud, min_plane_height, 50)
            full_cloud += f
            if args.plot_all:
                o3d.visualization.draw_geometries(
                    [full_cloud], "Filled holes", 640, 480
                )

        # Detect grasp poses in the point cloud.
        clouds = [full_cloud, objects_cloud]
        poses, scores = detectGrasps(
            clouds, detector, args.num_samples, args.plot_all, plane_height
        )
        if args.plot_all or (args.plot and not args.check_collisions):
            plotGrasps(full_cloud, poses, hand_geom, "Detected grasps")
            colors = scoresToColors(scores)
            plotGrasps(full_cloud, poses, hand_geom, "Scored grasps", colors)

        mask = scores["gc"] > min_score
        if np.count_nonzero(mask) > min_grasps_handles:
            before = len(poses)
            poses, scores = poses[mask], scores["gc"][mask]
            print(f"Removed grasps below min score: {before} --> {len(poses)}")
            if args.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Grasps above min score")

        # Remove isolated grasps.
        # if args.remove_isolated:
        # num_poses = len(poses)
        # poses = removeIsolated(poses)
        # print(f"Removed isolated: {num_poses} --> {len(poses)}")
        # input("?")

        # Remove duplicate (close-by) grasps.
        # if args.remove_duplicates:
        # num_poses = len(poses)
        # poses = removeDuplicates(poses)
        # print(f"Removed duplicates: {num_poses} --> {len(poses)}")

        # Remove grasp poses which are in collision.
        # if args.check_collisions:
        # coll_time = time()
        # is_free = coll_check.evalPoses(full_cloud, poses, checks_antipodal=False)
        # num_poses = len(poses)
        # poses = poses[is_free[:, 0] == 1]
        # coll_time = time() - coll_time
        # print(f"Checked {num_poses} poses for collisions in {coll_time:.3f}s")
        # print(f"  collision-free: {len(poses)}")
        # if args.plot_all:
        # plotGrasps(full_cloud, poses, hand_geom, "Collision-free grasps")

        if type(scores) == dict:
            scores = scores["gc"]

        # Center grasps based on visible point cloud.
        if args.center_grasps:
            print("Centering grasps ...")
            poses_old = deepcopy(poses)
            poses, widths = centerGrasps(
                poses,
                full_cloud,
                hand_geom,
                return_widths=True,
            )
            print("widths:", widths.shape, "scores:", scores.shape)
            for i, w in enumerate(widths):
                print(i, w)
            if args.plot_all:
                # plotGrasps(full_cloud, poses, hand_geom, "Centered grasps")
                plotColoredGrasps(
                    full_cloud,
                    [poses_old, poses],
                    [red, green],
                    hand_geom,
                    "Centered grasps (green)",
                )
            poses = forwardPushGrasps(poses, full_cloud, deep_hand_eval)

        # Remove grasp poses where a finger is below the table.
        if args.remove_below_table:
            num_poses = len(poses)
            above = findGraspsAbovePlane(poses, hand_geom, min_plane_height)
            # above = findGraspsAbovePlane(poses, hand_geom, min_plane_height, True, full_cloud)
            poses, widths, scores = poses[above], widths[above], scores[above]
            print(
                f"Removed grasps with finger below table: {num_poses} --> {len(poses)}"
            )
            if args.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Grasps above table")

        # Store the grasp poses in a file.
        if len(poses) < min_grasps_handles:
            print(f"Less than {min_grasps_handles} grasps. Not calculating handles.")
            if args.check_collisions:
                mask = removeCollidingGrasps(
                    poses, full_cloud, coll_check, args.plot_all
                )
                poses, widths, scores = poses[mask], widths[mask], scores[mask]
            np.savez(args.grasps_path, poses=poses, widths=widths, scores=scores)
            print("#poses stored in file:", len(poses))
            if args.plot or args.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Output: grasps")
            continue

        # Convert grasps to handles.
        handles_time = time()
        handle_inliers = findHandles(poses, args.inliers)
        handles_time = time() - handles_time
        print(f"Found {len(handle_inliers)} handles in {handles_time:.3f}s")
        if len(handle_inliers) < min_handles:
            if args.check_collisions:
                mask = removeCollidingGrasps(
                    poses, full_cloud, coll_check, args.plot_all
                )
                poses, widths, scores = poses[mask], widths[mask], scores[mask]
            np.savez(args.grasps_path, poses=poses, widths=widths, scores=scores)
            print("Using grasps. #poses stored in file:", len(poses))
            if args.plot or args.plot_all:
                plotGrasps(full_cloud, poses, hand_geom, "Output: grasps")
            continue
        if args.plot_all:
            p = HandsPlot(hand_geom)
            p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            for h in handle_inliers:
                p.plotHands(poses[h], list(np.random.rand(3)))
            p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            p.setTitle(f"Predicted handles")
            p.show()
            # for i,h in enumerate(handle_inliers):
            #     plotGrasps(full_cloud, poses[h], hand_geom, f"handle {i}/{len(handle_inliers)}")

        # Convert to handle objects.
        handles = [Handle(x, poses, widths) for x in handle_inliers]
        print(f"Found {len(handles)} handle grasps")
        # for h in handles:
        #     print(h.center, np.linalg.det(h.pose[:3, :3]))
        if args.plot_all:
            p = HandsPlot(hand_geom)
            p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            p.plotHands([x.pose for x in handles], np.random.rand(len(handles), 3))
            p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            p.setTitle(f"Handle grasps")
            p.show()
            # for i,h in enumerate(handles):
            #     plotColoredGrasps(full_cloud, [poses[h.inliers], [h.pose]], [red, green],
            #                       hand_geom, f"handle {i+1}/{len(handle_inliers)}")
        # np.save(args.grasps_path, [x.pose for x in handles])

        if args.prune_short_handles:
            num_handles = len(handles)
            # handles = pruneShortHandles(handles, poses)
            # print(f"Pruned short handles: {num_handles} --> {len(handles)}")
            handles = removeEndPoints(handles, poses, cutoff=1)
            handle_inliers = [h.inliers for h in handles]
            print(f"Removed handle endpoints")
            # for i, (h, h2) in enumerate(zip(handles, handles2)):
            #     handle1 = poses[h.inliers]
            #     handle2 = poses[h2.inliers]
            #     print(i, len(handle1), "-->", len(handle2))
            #     plotColoredGrasps(full_cloud, (handle1, handle2), [red, green], hand_geom,
            #                       f"Shortened handle {i+1}/{len(handle_inliers)}")

        poses, widths, scores = createOutputs(poses, widths, scores, handles)
        if args.check_collisions:
            mask = removeCollidingGrasps(poses, full_cloud, coll_check, args.plot_all)
            poses, widths, scores = poses[mask], widths[mask], scores[mask]
        np.savez(args.grasps_path, poses=poses, widths=widths, scores=scores)
        print("poses:", poses.shape, "widths:", widths.shape, "scores:", scores.shape)
        if args.plot or args.plot_all:
            # p = HandsPlot(hand_geom)
            # p.plotCloud(full_cloud, [0.0, 0.0, 1.0])
            # p.plotHands(poses, [0.0, 1.0, 0.0])
            # p.plotAxes(np.eye(4))
            # p.plotAxes(camera_pose)
            # p.setTitle(f"Output: handle inliers and handle poses")
            # p.show()
            colors = scoresToColors(scores)
            plotGrasps(full_cloud, poses, hand_geom, "Output: scored grasps", colors)


if __name__ == "__main__":
    main()

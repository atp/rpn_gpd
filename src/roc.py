"""Plot an ROC curve for files in a given folder.

The given folder is expected to contain files called *recalls.npy and 
*precisions.npy. The ROC curve plots the true positive rate (TPR) against the 
false negative rate (FNR).
"""

import os
import sys
from datetime import datetime
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from utils.analysis import paretoFront


def labelsAndLegend(xlabel, ylabel, outdir, suffix=""):
    plt.legend()
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    title = f"{ylabel}_{xlabel}_{suffix}"
    plt.title(title)
    plt.savefig(outdir + title + ".png")
    plt.show()


# Read command line arguments.
if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python roc.py INPUT_DIR [PATTERN]")
    print("PATTERN is the filename pattern to be matched")
    exit(-1)
input_dir = sys.argv[1]

if len(sys.argv) == 3:
    pattern = sys.argv[2]
else:
    pattern = "confmats"

# Create a new directory in which to store the plots.
outdir = "/home/andreas/Pictures/rpn_gpd/" + str(datetime.now()) + "/"
os.mkdir(outdir)

# Read data files. Order of values: tp, fp, tn, fn, k.
files = glob(input_dir + "*" + pattern + "*.npy")
data = {}
for f in files:
    k = f[f.rfind("/") + 1 : f.rfind("_")]
    data[k] = np.load(f)
    print(k)
    print(data[k])

metrics = {}
for k in data:
    X = data[k]
    metrics[k] = {}
    metrics[k]["recalls"] = X[:, 0] / (X[:, 0] + X[:, 3])  # tp / (tp + fn)
    metrics[k]["fprs"] = X[:, 1] / (X[:, 1] + X[:, 2])  # fp / (fp + tn)

# Sort keys such that shortest is first.
keys = sorted(data.keys(), key=lambda x: len(x))

# Draw all in the same plot.
plt.figure()
for k in keys:
    plt.plot(metrics[k]["fprs"], metrics[k]["recalls"], "o", label=k)
labelsAndLegend("fpr", "tpr", outdir, "scatter")

# Draw all in the same plot.
plt.figure()
for k in keys:
    D = np.vstack((metrics[k]["recalls"], -metrics[k]["fprs"]))
    pareto = paretoFront(D.T, False)
    # print(pareto)
    # idxs = np.argsort(pareto[:,1])
    # pareto = pareto[idxs,:]
    plt.plot(-pareto[:, 1], pareto[:, 0], "x", label=k)
labelsAndLegend("fpr", "tpr", outdir, "paretoscatter")

# Draw all in the same plot.
plt.figure()
for k in keys:
    D = np.vstack((metrics[k]["recalls"], -metrics[k]["fprs"]))
    pareto = paretoFront(D.T, False)
    idxs = np.argsort(pareto[:, 0])
    pareto = pareto[idxs, :]
    plt.plot(-pareto[:, 1], pareto[:, 0], label=k)
labelsAndLegend("fpr", "tpr", outdir, "paretofront")

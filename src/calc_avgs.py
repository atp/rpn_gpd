# Calculates averages from CSV files in directory.

import os
import sys

import numpy as np
from tabulate import tabulate

dir1 = sys.argv[1]
avg = {}

for f in os.listdir(dir1):
    if f.endswith(".csv"):
        x = np.loadtxt(dir1 + f, delimiter=",", skiprows=1)
        avg[f[: f.rfind("_")]] = np.mean(x, 0)

print(avg)
# y = np.array([avg['CLS'], avg['ROT'], avg['GPD'], avg['FINAL']])
# print(y)
# np.savetxt(dir3 + 'avgs.csv', y, fmt='%.3f', delimiter=',', \
#    header='Accuracy, Precision, Recall')
# print(tabulate(y, tablefmt="latex", floatfmt=".2f", \
#        headers=['accuracy', 'precision', 'recall']))

# print(tabulate(y.T, tablefmt="latex", floatfmt=".2f", \
#        headers=['CLS', 'ROT', 'GPD', 'FINAL']))

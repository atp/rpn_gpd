"""Detect grasps in point clouds obtained by a robot.
"""

import os
import sys
from argparse import ArgumentParser
from time import sleep, time

import numpy as np
import open3d as o3d

import hand_params
from grasp_detector import GpdGraspDetector, MaGcGraspDetector, MaGraspDetector
from hands.hand_eval import AntipodalParams, HandGeometry
from networks.network import LenetLike
from object_set import ThreeDNetObjectSet
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot
from utils.plot import createVtkCamera, plotClouds, plotColoredHands

# num_samples = 400
# num_samples = 1000
num_samples = 2000
# threshs = {"ma": {"cls": 0.95, "rot": 0.95, "gc": 0.8}, "gpd": 0.8}
threshs = {"ma": {"cls": 0.8, "rot": 0.8, "gc": 0.8}, "gpd": 0.8}
viewpoint = np.array([[0.051], [0.110], [0.484]])
cube_size = 1.0
use_open_gl_frame = False
prop_params = {"image_size": 60, "image_type": "occupancy"}
prop_img_shape = [prop_params["image_size"], prop_params["image_size"], 3]
grasp_img_shape = (60, 60, 3)

models_dir = "../models/comparison/"
lenet_layout = {
    "conv": [32, 64],
    "fc": [500],
    "conv_filter_size": 5,
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
}
net_cfg = {
    "gpd": {
        "network": LenetLike,
        "layout": lenet_layout,
        "weights_path": models_dir + "gpd/lenet.pwf",
        "batch_size": 1024,
    },
    "ma": {
        "network": LenetLike,
        "layout": lenet_layout,
        "weights_paths": {
            "cls": models_dir + "ma/cls_lenet.pwf",
            "rot": models_dir + "ma/rot_lenet.pwf",
            "gc": models_dir + "ma/gc_lenet.pwf",
        },
        "batch_size": 1024,
    },
}

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def preprocessPointCloud(cloud, viewpoint, voxel_size=0.003):
    cloud_out = cloud.voxel_down_sample(voxel_size)
    print(f"Downsampled cloud to {len(cloud_out.points)} points.")
    cloud_out = estimateNormals(cloud_out, viewpoint)
    print(f"Estimated {len(cloud_out.normals)} surface normals.")
    return cloud_out


def createGraspDetector(method, hand_geom):
    antipodal_params = AntipodalParams(**hand_params.antipodal_params)
    if method == "gpd":
        detector = GpdGraspDetector(
            hand_geom, antipodal_params, grasp_img_shape, net_cfg["gpd"]
        )
    elif method == "ma":
        detector = MaGraspDetector(
            hand_geom,
            antipodal_params,
            prop_params,
            grasp_img_shape,
            net_cfg["ma"],
            use_open_gl_frame,
        )
    elif method == "ma-gc":
        detector = MaGcGraspDetector(
            hand_geom,
            antipodal_params,
            prop_params,
            grasp_img_shape,
            net_cfg["ma"],
            use_open_gl_frame,
        )
    return detector


def main():
    if len(sys.argv) < 4:
        print("Error: not enough input arguments.")
        print(
            "Usage: python detect_grasps_robot.py METHOD PCD_FILE GRAPS_FILE [REMOVE_PCD_FILE]"
        )
        sys.exit(-1)
    method = sys.argv[1]
    pcd_filepath = sys.argv[2]
    grasps_filepath = sys.argv[3]
    if len(sys.argv) == 5:
        remove_file = int(sys.argv[4])
    else:
        remove_file = False

    # Create the grasp pose detector.
    hand_geom = HandGeometry(**hand_params.hand_params)
    detector = createGraspDetector(method, hand_geom)

    # Ensure that there is no PCD file.
    if remove_file:
        if os.path.exists(pcd_filepath) and os.path.isfile(pcd_filepath):
            os.remove(pcd_filepath)
            print("Removed file", grasps_filepath)

    is_running = True
    while is_running:
        # Wait for the point cloud file to be updated.
        print("Waiting for PCD file at %s ..." % pcd_filepath)
        while not os.path.exists(pcd_filepath):
            sleep(1)
        if not os.path.isfile(pcd_filepath):
            raise ValueError("%s is not a file!" % pcd_filepath)
        print("Received PCD file")

        # Read the point cloud from the file and optionally remove the file.
        # TODO: extract viewpoint from file
        original_cloud = o3d.io.read_point_cloud(pcd_filepath)
        print("Loaded point cloud with %d points" % len(original_cloud.points))
        if remove_file:
            os.remove(pcd_filepath)
            print("Removed file", pcd_filepath)

        # Preprocess the point cloud.
        cloud = preprocessPointCloud(original_cloud, viewpoint)
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
        o3d.visualization.draw_geometries(
            [cloud, origin_frame], "Surface normals", 640, 480
        )

        # Draw samples from bounding cube.
        samples = sampleFromCube(cloud, cube_size, num_samples)
        samples_cloud = numpyToCloud(samples)
        cloud.paint_uniform_color([0.0, 0.0, 1.0])
        samples_cloud.paint_uniform_color([1.0, 0.0, 0.0])
        print(f"Sampled {len(samples_cloud.points)} points out of {len(cloud.points)}.")
        o3d.visualization.draw_geometries(
            [cloud, samples_cloud, origin_frame], "Samples", 640, 480
        )

        # Detect grasp poses.
        t = time()
        poses = detector.detectGraspsFast(cloud, samples, threshs[method])
        t = time() - t
        print(f"Generated {len(poses)} grasp poses in {t}s.")

        # Visualize the grasp poses.
        p = HandsPlot(hand_geom)
        p.plotCloud(cloud, [1.0, 0.0, 1.0])
        p.plotHands(poses, [0.0, 1.0, 0.0])
        p.plotAxes(np.eye(4))
        p.setTitle("Detected grasps")
        p.show()

        # Store the grasp poses in a file.
        np.save(grasps_filepath, poses)


if __name__ == "__main__":
    main()

"""Create a binary-valued database from a multi-label database.
"""

import os
import sys

import numpy as np
import tables as tab

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=9, complib="blosc:lz4")

np.set_printoptions(precision=3)

if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print("Usage: python create_binary_database.py DB_IN DB_OUT CLASS")
    exit(-1)

with tab.open_file(sys.argv[1], "r") as f:
    Y = np.asarray(f.root.labels)
    if Y.ndim != 2:
        print(f"Error: <labels> node has the wrong dimensions: {Y.shape}")
        exit(-1)

    # Find positive and negative rows.
    class_idx = int(sys.argv[3])
    pos = np.nonzero(Y[:, class_idx] == 1)[0]
    neg = np.nonzero(Y[:, class_idx] == 0)[0]
    print(f"Found {len(pos)} positives and {len(neg)} negatives for class {class_idx}")

    # Balance the output labels.
    if len(pos) > len(neg):
        pos = np.random.choice(pos, len(neg))
    elif len(neg) > len(pos):
        neg = np.random.choice(neg, len(pos))
    print(f"Balanced DB will have {len(pos)} positives and {len(neg)} negatives")
    idxs = np.hstack((pos, neg))
    labels_out = Y[idxs, class_idx].flatten()
    images = np.asarray(f.root.images)
    images_out = images[idxs]

    # Store the positive rows in a new database.
    with tab.open_file(sys.argv[2], "w") as fout:
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(f"Created dataset: {labels_dset}")
        images_dset = fout.create_carray(
            "/", "images", tab.UInt8Atom(), obj=images_out, filters=filters
        )
        print(f"Created dataset: {images_dset}")

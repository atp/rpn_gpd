"""Relabel a database for ROT using a CLS model.

This program takes a database for ROT and a CLS model as input, and outputs a
new database that contains only those elements of the input which are
classified as positives by the CLS model.

"""

import os
import sys
from argparse import ArgumentParser

import numpy as np
import tables
import torch
import torchvision.transforms as transforms
from torch.utils import data as torchdata

from datasets import BasicDataset
from image_utils import preprocessImages
from networks.network import LenetLikeWithSigmoid, VggLikeWithSigmoid
from networks.resnet_light import Resnet

os.environ["BLOSC_NTHREADS"] = "8"
filters = tables.Filters(complevel=9, complib="blosc:lz4")

thresh = 0.5

lenet_params = {
    "use_batchnorm": False,
    "out_shape": 2,
    "conv": [32, 64],
    # "fc": [500],
    "fc": [32],
    "conv_filter_size": 5,
}
vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}

print_step = 100

# This is the mean and std calculated using calc_dataset_mean_std.py for QD train.
mean = [0.0724, 0.0793, 0.0831]
std = [0.2019, 0.2076, 0.2012]


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Relabel database for ROT using CLS model")
    parser.add_argument("in_file", help="path to input database (*.h5)")
    parser.add_argument("model_file", help="path to CLS model (*.pwf)")
    parser.add_argument("out_file", help="path to output database (*.h5)")
    parser.add_argument("network", help="<lenet> or <vgg>")
    parser.add_argument(
        "--labels_name", help="name of labels node in input database", default="/labels"
    )
    return parser


def findPredictedPositiveRows(path, out_shape, net, device):
    loader_params = {
        "batch_size": 512,
        "shuffle": False,
        "pin_memory": True,
        "num_workers": 4,
        "drop_last": True,  # avoids a problem with batch norm when batch has size one
    }
    transform = transforms.Normalize(
        mean=[0.0724, 0.0793, 0.0831], std=[0.2019, 0.2076, 0.2012]
    )
    transform = None
    dset = BasicDataset(path, out_shape, transform)
    loader = torchdata.DataLoader(dset, **loader_params)

    # Find the indices of the rows predicted to be positive.
    idxs_out = torch.zeros(0, dtype=int).to(device)
    print("Making predictions ...")
    with torch.no_grad():
        for i, data in enumerate(loader):
            inputs = data[0].to(device)
            outputs = net(inputs)
            idxs = torch.nonzero(outputs[:, 1] > thresh).flatten()
            idxs_out = torch.cat((idxs_out, i * loader_params["batch_size"] + idxs))
            if i % print_step == print_step - 1:
                print(
                    f"  iteration: {i + 1}/{len(loader)}, above thresh: {len(idxs_out)}"
                )
    idxs_out = idxs_out.cpu().numpy()

    return idxs_out


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Load the input database.
    fin = tables.open_file(args.in_file, "r")
    names = [x.name for x in fin.list_nodes(fin.root)]
    if "images" in names:
        I = fin.root.images
    elif "prop_imgs":
        I = fin.root.gen_images
    else:
        print("Error: no matching images node found!")
        exit(-1)

    # Setup the CLS model.
    imgs_shape = I.shape[-3:]
    net_params = {"lenet": lenet_params, "vgg": vgg_params}
    net_params = net_params[args.network]
    net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    params_file = args.model_file
    if args.network == "lenet":
        net = LenetLikeWithSigmoid(**net_params)
    elif args.network == "vgg":
        net = VggLikeWithSigmoid(**net_params)
    else:
        print("Error: network type not supported!")
        exit(-1)
    # net = Resnet(input_channels=12)
    net.load_state_dict(torch.load(params_file))
    net.eval()
    print("Net:")
    print(net)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)
    net = net.to(device)

    idxs_out = findPredictedPositiveRows(
        args.in_file, net_params["out_shape"], net, device
    )
    print("Rows predicted to be positive:", idxs_out.shape)
    print("idxs_out:", idxs_out)

    # Extract the rows which are predicted to be positive from the input database.
    images_out = np.array(fin.root.images)[idxs_out]
    labels = np.array(fin.get_node(args.labels_name))
    labels_out = np.array(labels)[idxs_out]

    # Store the extracted elements in a new database.
    print("Writing elements to the output database ...")
    with tables.open_file(args.out_file, "w") as fout:
        images_dset = fout.create_carray(
            "/", "images", tables.UInt8Atom(), obj=images_out, filters=filters
        )
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(images_dset)
        print(labels_dset)
    fin.close()
    print("Saved to HDF5 file:", args.out_file)


if __name__ == "__main__":
    main()

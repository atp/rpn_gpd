"""Reduce the number of channels in a database.
"""

import os
import sys

import numpy as np
import tables as tab


def copyNode(fout, where, node, indices, stop, blocks):
    func = partial(fout.create_carray, filters=filters)

    new_shape = (num_samples,) + node.shape[1:]
    print(f"name: {name}")
    print(where)
    print(f"Created dataset: {dset}, {dset.dtype}, {node.dtype}")
    start = 0

    for i in range(1, len(blocks)):
        data = [
            node[
                j,
            ]
            for j in indices[blocks[i - 1] : blocks[i]]
        ]
        stop = start + len(data)
        dset[start:stop] = np.array(data)
        print(
            f"({i}/{len(blocks)-1}) Done with block: "
            + f"[{blocks[i-1]}, {blocks[i]}], start: {start}, "
            + f"stop: {stop}, which contains {len(data)} indices"
        )
        start = stop


if len(sys.argv) < 4:
    print("Error: wrong arguments!")
    print(
        "Usage: python reduce_image_channels.py IN_HDF5 OUT_HDF5 STOP [MAX_IN_MEMORY]"
    )
    exit(-1)

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=9, complib="blosc:lz4")

h5_in = sys.argv[1]
h5_out = sys.argv[2]
max_channel = int(sys.argv[3])
if len(sys.argv) == 5:
    max_in_memory = int(sys.argv[4])
else:
    max_in_memory = -1

with tab.open_file(h5_in, "r") as fin, tab.open_file(h5_out, "w") as fout:
    # Copy the labels.
    labels = np.array(fin.get_node("/labels"))
    labels_out = fout.create_carray("/", "labels", obj=labels, filters=filters)
    print(f"Created dataset:\n{labels_out}")

    # Copy the images.
    images = fin.get_node("/images")

    if max_in_memory == -1:
        print("Loading complete node <images> into memory ...")
        images = np.array(images[..., :max_channel])
        print("Writing <images> to output database ...")
        images_out = fout.create_carray(
            "/",
            "images",
            tab.UInt8Atom(),
            obj=images,
            filters=filters,
        )
    else:
        images_shape = images.shape[:-1] + (max_channel,)
        images_out = fout.create_carray(
            "/", "images", tab.UInt8Atom(), shape=images_shape, filters=filters
        )
        blocks = np.arange(0, len(images), max_in_memory)
        blocks = list(blocks) + [len(images)]
        for i in range(1, len(blocks)):
            start, stop = blocks[i - 1], blocks[i]
            images_out[start:stop] = np.array(images[start:stop, :, :, :max_channel])
            print(
                f"({i}/{len(blocks)-1}) Done with block: "
                + f"[{blocks[i-1]}, {blocks[i]}], start: {start}, "
                + f"stop: {stop}"
            )
    print(f"Created dataset:\n{images_out}")

print("Wrote new database to:", sys.argv[2])

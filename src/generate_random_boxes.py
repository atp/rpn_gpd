import sys

import numpy as np
import open3d as o3d

min_dim = 0.01
max_dim = 0.15
num_boxes = 50

np.set_printoptions(suppress=True, precision=3)

out_dir = sys.argv[1]
create_cylinders = False
shape = "box"
if len(sys.argv) == 3 and sys.argv[2] == "1":
    create_cylinders = True
    shape = "cylinder"

for i in range(num_boxes):
    print(f"Mesh {i}/{num_boxes-1}")

    # Generate mesh with random dimensions.
    if create_cylinders:
        radius = (max_dim - min_dim) * np.random.rand() + min_dim
        height = (max_dim - min_dim) * np.random.rand() + min_dim
        box = o3d.geometry.TriangleMesh.create_cylinder(
            radius, height, resolution=40, split=8
        )
        dims = np.array([radius, height])
        print(f"radius, height: {dims}")
    else:
        width = (max_dim - min_dim) * np.random.rand() + min_dim
        height = (max_dim - min_dim) * np.random.rand() + min_dim
        depth = (max_dim - min_dim) * np.random.rand() + min_dim
        box = o3d.geometry.TriangleMesh.create_box(width, height, depth)
        dims = np.array([width, height, depth])
        print(f"width, height, depth: {dims}")
    tight = box.is_watertight()
    print(f"Is the mesh watertight? {tight}")

    # Rescale the mesh such that its largest dimension is 1m.
    scale = 1.0 / np.max(dims)
    box = box.scale(scale)
    print(f"scale: {scale}, dims: {dims*scale}")

    # Store the mesh.
    filepath = out_dir + f"{shape}_{i}.ply"
    o3d.io.write_triangle_mesh(filepath, box)
    print(f"Stored mesh at: {filepath}")


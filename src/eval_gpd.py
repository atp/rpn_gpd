"""Evaluate GPD on a set of thresholds.

Calculate recall and precision at different thresholds for GPD.

The dataset, given as a command line parameter, is assumed to contain 
two arrays:
1. images (N x W x H x C),
2. labels (N).
Here, N is the number of samples, and W, H, C are the width, height, and 
number of channels of an image.
"""

import os
import sys
from copy import deepcopy
from datetime import datetime
from time import time

import numpy as np
import torch
import torch.utils.data as torchdata

from datasets import BasicDataset
from learning import Metrics
from networks.network import LenetLike

loader_params = {
    "batch_size": 64,
    "shuffle": False,
    "pin_memory": True,
    # 'num_workers': 8
    "num_workers": 1,
}

models_dir = "../models/gpd30/"

configs = {}

configs["lenet"] = {}
configs["lenet"]["model"] = LenetLike
configs["lenet"]["params_path"] = models_dir + "lenet.pwf"
configs["lenet"]["layout"] = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "num_classes": 2,
    "input_shape": None,
    "conv": [32, 64],
    "fc": [500],
    "conv_filter_size": 5,
}

max_batches = -1
# max_batches = 30
num_threshs = 10
# num_threshs = 4
print_step = 25

np.random.seed(0)
np.set_printoptions(precision=4)


def createPretrainedNet(config):
    """Create a pretrained network."""
    print(config["layout"])
    if len(config["layout"]) > 1:
        net = config["model"](**config["layout"])
        net.load_state_dict(torch.load(config["params_path"]))
    else:
        net = config["model"]()
        net.model.load_state_dict(torch.load(config["params_path"]))
    net.to(device)
    net.eval()
    return net


def evalThresholds(labels, outputs, threshs):
    """Evaluate a classifier on different thresholds."""
    results = [Metrics() for i in range(threshs.shape[0])]

    for i in range(threshs.shape[0]):
        above_thresh = torch.nonzero(outputs >= threshs[i])
        pred = torch.zeros(outputs.shape[0], dtype=torch.long)
        pred[above_thresh] = 1
        results[i] = Metrics(labels, pred)
        print(f"  thresh: {threshs[i]:.2f}; {results[i]}")

        # Stop when full recall has been attained
        if results[i].recall() == 1.0:
            break

    return results


def runModel(model, loader, device, input_idx, class_idx=1):
    """Run a model on all inputs."""
    outputs = torch.empty((0,))
    print("Loading data ...")

    for i, data in enumerate(loader, 0):
        if i % print_step == print_step - 1:
            print(f"Processing batch {i+1}/{max_batches} ...")
        inputs = data[input_idx].to(device)
        out = model(inputs)[:, class_idx]
        outputs = torch.cat((outputs, out.cpu()))
        if i + 1 == max_batches:
            break

    outputs = torch.sigmoid(outputs)

    return outputs


def evalModel(model, threshs, loader, device, class_idx=1):
    """Evaluate the model: gc.

    Returns a list of lists, with each sublist containing
    [tp, fp, tn, fn, k]. Here, ... and k is the number of inputs to GC.
    """
    confmats = []

    outputs = torch.empty((0,))
    labels = torch.empty((0,), dtype=torch.int)

    # Run the model on all examples.
    print("Loading data ...")
    for i, data in enumerate(loader, 0):
        print(f"Processing batch {i+1}/{max_batches} ...")
        labels = torch.cat((labels, data[-1]))
        inputs = data[0].to(device)
        out = model(inputs)[:, class_idx]
        outputs = torch.cat((outputs, out.cpu()))
        if i + 1 == max_batches:
            break
    outputs = torch.sigmoid(outputs)

    # Evaluate the model at each threshold.
    labels = labels.flatten().long()
    outputs = outputs.flatten()
    results = evalThresholds(labels, outputs, threshs)
    confmats = [[r.tp, r.fp, r.tn, r.fn, labels.numel()] for r in results]

    return confmats


"""
================ MAIN ================
"""
if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python eval_gpd.py H5_FILE")
    exit(-1)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# Load the dataset.
print("Loading dataset ...")
data_path = sys.argv[1]
dataset = BasicDataset(data_path)
loader = torchdata.DataLoader(dataset, **loader_params)
print(
    f"  positives: {np.count_nonzero(dataset.labels)}, "
    + f"negatives: {np.count_nonzero(dataset.labels==0)}"
)

# Determine the shape of the image array in the dataset.
imgs_shape = dataset.images.shape[-3:]
imgs_shape = (imgs_shape[-1],) + imgs_shape[:-1]
configs["lenet"]["layout"]["input_shape"] = imgs_shape
model = createPretrainedNet(configs["lenet"]).to(device)

# Create the thresholds at which each model is evaluated.
threshs = np.hstack(
    (np.linspace(0.0, 1.0, num_threshs + 1)[:-1], np.array([0.95, 0.99]))
)
# threshs = np.array([0.3, 0.5, 0.7, 0.9])
# threshs = np.array([0.2, 0.3])
print("thresholds:", threshs)

# Sort thresholds in descending order for speed up.
threshs = threshs[::-1]
print("thresholds in reverse order:", threshs)

now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
fig_dir = "/home/andreas/Pictures/rpn_gpd2/compare_all_" + now + "/"
os.mkdir(fig_dir)

# Evaluate the model on the dataset.
with torch.no_grad():
    print("------------------------------------------------------")
    print(f"Evaluating model ...")
    print(model)
    confmats = evalModel(model, threshs, loader, device)
    np.save(fig_dir + "gpd_confmats.npy", np.array(confmats))
    print("------------------------------------------------------")

print("Results stored at:", fig_dir)

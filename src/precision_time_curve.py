"""Plot a curve of precision against detections per second.

The given folder is expected to contain files called recalls*.npy and 
*precisions.npy. TODO: finish doc

"""

import sys
from datetime import datetime
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from utils.analysis import paretoFront

# average runtime per image on 8-core CPU
time_per_grasp_image = 0.0005

# average runtime per forward pass for a single batch with 64 elements
# on a Nvidia 1080 GPU
time_forward = {}
time_forward["lenet"] = 0.0026
time_forward["resnet"] = 0.0865

batch_size = 64

"""=============== MAIN ===============
"""
# Read command line arguments.
if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python precision_recall_curve.py INPUT_DIR [NETWORK]")
    print("NETWORK is the name of the neural network, default: lenet")
    exit(-1)
input_dir = sys.argv[1]
if len(sys.argv) == 3:
    network = sys.argv[2]
else:
    network = "lenet"

# Read data files.
totals_list = [np.load(f) for f in glob(input_dir + "*totals.npy")]
detections_list = [np.load(f) for f in glob(input_dir + "*detections.npy")]
precisions_list = [np.load(f) for f in glob(input_dir + "*precisions.npy")]

for i in range(len(totals_list)):
    T, D, P = totals_list[i], detections_list[i], precisions_list[i]

    idxs = np.nonzero(T)[0]
    print(f"Removing {len(T) - len(idxs)} zero elements")
    T, D, P = T[idxs], D[idxs], P[idxs]

    print("T:", np.min(T), np.max(T))

    # Estimate the time it would take to calculate all grasp images.
    time_images = T * time_per_grasp_image

    # Estimate the time it would take to forward pass all grasp images.
    num_batches = T / batch_size
    time_forwards = num_batches * time_forward[network]
    print("num_batches (in thousands):\n", num_batches)
    print("  ", np.min(num_batches), np.max(num_batches))

    # Calculate detections per second (DPS).
    total_times = time_images + time_forwards
    dps = D / total_times
    print("detections per second:\n", dps)

    # Calculate Pareto front.
    F = paretoFront(np.vstack((dps, P)).T)

    # Find the cut-off point where x is constant and y is increasing.
    F = F[np.argsort(F[:, 0]), :]
    xrounded = np.around(F[:, 0], -1)
    max_idxs = np.nonzero(xrounded == np.max(xrounded))[0]
    exclude = np.nonzero(F[max_idxs, 1] != np.max(F[max_idxs, 1]))[0]
    F = np.delete(F, max_idxs[exclude], 0)
    print(max_idxs, max_idxs[exclude])

    # print(f'avg precision: {np.mean(F[:,1]):.3f}')

    plt.figure()
    plt.plot(dps, P, "o")
    plt.plot(F[:, 0], F[:, 1], "o")
    plt.xlabel("detections per second"), plt.ylabel("precision"), plt.show()

    # Plot Pareto front as scatter.
    plt.figure()
    plt.plot(F[:, 0], F[:, 1], "x")
    plt.xlabel("detections per second"), plt.ylabel("precision"), plt.show()

    # Plot Pareto front as curve.
    plt.figure()
    plt.plot(F[:, 0], F[:, 1])
    plt.ylim([-0.05, 1.05])
    plt.xlabel("detections per second"), plt.ylabel("precision"), plt.show()

    # Zoom into upper, left part.
    # idx = np.where(F[:,0] >= 500)[0][0]
    idx = np.where(F[:, 1] <= 0.5)[0][0]
    plt.figure()
    plt.plot(F[:idx, 0], F[:idx, 1])
    plt.ylim([-0.05, 1.05])
    plt.xlabel("detections per second"), plt.ylabel("precision"), plt.show()

plt.show()

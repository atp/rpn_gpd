"""Convert a directory that contains NPY files to a HDF5 database.

The directory should contain one NPY file for each viewpoint.

"""

import os
from argparse import ArgumentParser
from glob import glob

import numpy as np
import tables

from grasp_scene_data import GraspData, SceneData
from hdf5_to_npy import readDatabase

os.environ["BLOSC_NTHREADS"] = "8"
filters = tables.Filters(complevel=9, complib="blosc:lz4")


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(
        description="Convert a directory that contains NPY files to a HDF5 database"
    )
    parser.add_argument("in_dir", help="path to input directory with NPY files")
    parser.add_argument(
        "in_hdf5", help="path to the HDF5 database from which the folder was created"
    )
    parser.add_argument("out_hdf5", help="path to output HDF5 database file")
    parser.add_argument(
        "-l", "--labels_key", help="labels key for NPY files", default="labels"
    )
    parser.add_argument(
        "-m", "--max_in_memory", help="max images in memory", type=int, default=-1
    )

    return parser


def main():
    num_hand_orientations = {"qd": 196}

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Determine what method was used to generate the grasps.
    if "qd" in args.in_hdf5:
        method = "qd"
    elif "gpd" in args.in_hdf5:
        method = "gpd"
    else:
        method = ""

    # Read the NPY files from the directory.
    files = sorted(glob(args.in_dir + "*.npy"))
    print(f"Loaded {len(files)} NPY files")

    with tables.open_file(args.in_hdf5, "r") as fin:
        labels_shape = fin.get_node("/labels").shape
        labels_dims = len(labels_shape)

    # Extract the grasp data from the files.
    data = [np.load(f, allow_pickle=True, encoding="latin1").item() for f in files]
    labels = [x[args.labels_key] for x in data]
    labels = np.concatenate(labels)
    db_indices = [x["db_indices"] for x in data]
    db_indices = np.concatenate(db_indices, axis=1)
    rows = db_indices[0, :]

    # Check if the indices are valid.
    unique, counts = np.unique(rows, return_counts=True)
    if "prop" in args.in_dir:
        assert np.all(counts == num_hand_orientations[method])
        print(f"Each row index appears {counts[0]} times")
        rows = rows[:: num_hand_orientations[method]]
    elif "grasp" in args.in_dir:
        print(f"Using images in order: {rows}")
        print(f"labels:", labels.shape)

    # Read the images from the database.
    with tables.open_file(args.in_hdf5, "r") as fin:
        print("Reading images from HDF5 database into memory ...")
        images = np.array(fin.get_node("/images"))

    print(f"Need {len(rows)} images")
    print(f"Using images in order: {rows}")
    print(f"labels:", labels.shape)
    if labels_dims == 2:
        labels = labels.reshape((-1, num_hand_orientations[method]))
        print(f"labels: {labels.shape}, images: {images.shape}")
        # images = images[:: num_hand_orientations[method]]
        # assert labels.shape[0] == labels_shape[0] and labels.shape[1] == labels_shape[1]

    if args.max_in_memory == -1 or len(rows) < args.max_in_memory:
        # Use the images at the row indices stored in the NPY files.
        images = images[rows]

        # Write the images and the labels to a new database.
        with tables.open_file(args.out_hdf5, "w") as fout:
            images_out = fout.create_carray(
                "/", "images", tables.UInt8Atom(), obj=images, filters=filters
            )
            labels_out = fout.create_carray("/", "labels", obj=labels, filters=filters)
            print("Created datasets:")
            print(images_out)
            print(labels_out)
    else:
        with tables.open_file(args.out_hdf5, "w") as fout:
            labels_out = fout.create_carray("/", "labels", obj=labels, filters=filters)
            images_out = fout.create_carray(
                "/",
                "images",
                tables.UInt8Atom(),
                shape=(len(rows),) + images.shape[1:],
                filters=filters,
            )
            print("Created datasets:")
            print(images_out)
            print(labels_out)

            print(f"Copying images in blocks of size {args.max_in_memory} ...")
            blocks = list(np.arange(0, len(rows), args.max_in_memory)) + [len(rows)]
            for i in range(1, len(blocks)):
                stop, start = blocks[i], blocks[i - 1]
                idxs = rows[start:stop]
                images_out[start:stop] = images[idxs]
                print(f"{i}/{len(blocks)} Copied block from {start} to {stop}")

    print("Wrote new database to:", args.out_hdf5)


if __name__ == "__main__":
    main()

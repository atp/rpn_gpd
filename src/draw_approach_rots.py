"""Visualize the rotations about the approach axis.
"""

import sys
from argparse import ArgumentParser
from copy import deepcopy
from functools import partial
from time import time

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from grasp_image import GraspImageGenerator, volume_dims
from grasp_scene_data import GraspData, SceneData, storeGrasps
from ground_truth import findGroundTruthPositives
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from proposal import ProposalImageGenerator, sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import numpyToCloud, orientNormalsTowardsMean
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes
from utils.plot import plotColoredHands

cube_size = 1.0
grasp_img_shape = (60, 60, 4)

grey = [0.4, 0.4, 0.4]
cyan = [0.0, 1.0, 0.0]
orange = [1.0, 0.6, 0.0]
yellow = [1.0, 1.0, 0.0]
green = [0.0, 1.0, 0.0]
red = [1.0, 0.0, 0.0]
blue = [0.0, 0.0, 1.0]
black = [0.0, 0.0, 0.0]
purple = [0.6, 0.15, 0.9]

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Visualize the frames used in our method")
    parser.add_argument("file", help="path to mesh file")
    parser.add_argument("scale", help="scaling factor", type=float)
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument("num_samples", help="number of samples", type=int)

    return parser


def createHandsGenerator(method, hand_geom, antip_prms, hand_eval):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, num_approaches=1)
        # gen = QdHandsGenerator(hand_geom, antip_prms)
        gen = QdHandsGenerator(hand_geom, antip_prms, hand_eval=hand_eval)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def main() -> None:
    draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # data = createPointCloudFromMesh(args.file, args.scale)
    scene = SingleObjectScene(args.file)
    scene.resetToScale(args.scale)
    mesh, views_cloud = scene.createCompletePointCloud()

    camera_pose = scene.scene.get_pose(scene.camera_node)
    # camera_pose[:3, 3] -= 0.02 * camera_pose[:3, 2]
    camera_pose = calcLookAtPose([1.0, 0.0, 0.0], np.zeros(3))
    # cloud = scene.createPointCloudFromView(camera_pose)
    cloud = scene.createProcessedPointCloudFromView(camera_pose)
    tree = o3d.geometry.KDTreeFlann(cloud)

    # draw_geoms([mesh, views_cloud],
    # "Multiview point cloud and view points", 640, 480)
    # draw_geoms([mesh], "Surface normals", 640, 480)

    # Draw sample points from the point cloud.
    samples = sampleFromCube(cloud, cube_size, args.num_samples)
    # samples[0, 2] += 0.02
    pts = np.asarray(cloud.points)
    mins, maxs = np.min(pts, 0), np.max(pts, 0)
    print(mins, maxs)
    # samples[0, 0] = mins[0] + 0.5 * (maxs[0] - mins[0])
    samples[0] = mins + 0.5 * (maxs - mins)
    # print(f"samples: {samples}")
    # plotSamplesAsBoxes(mesh, samples, "Samples")
    plotSamplesAsBoxes(cloud, samples, "Samples")
    print(f"samples[0]: {samples[0]}")

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antipodal_params = {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": True,
    }
    antip_prms = AntipodalParams(**antipodal_params)
    hand_eval= HandEval(hand_geom, antip_prms)
    gen = createHandsGenerator(args.method, hand_geom, antip_prms, hand_eval)

    # Generate the grasp poses.
    t = time()
    camera_pose = scene.scene.get_pose(scene.camera_node)
    hands, frames = gen.generateHands(cloud, samples, camera_pose=camera_pose)
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")
    # idxs = np.array(np.nonzero(findGroundTruthPositives(hands)))
    collision_free = hands[:, :, 0] == 1
    antipodal = hands[:, :, 1] == 1

    # Plot only poses corresponding to ground truth positives.
    idxs = np.array(np.nonzero(collision_free & antipodal))

    # Plot all poses for a single sample.
    idxs = np.vstack(
        (np.zeros(gen.num_hand_orientations), np.arange(gen.num_hand_orientations))
    ).astype(int)

    total = hands.shape[0] * hands.shape[1]
    print(f"collision_free: {np.sum(collision_free)}/{total}")
    print(f"antipodal: {np.sum(antipodal)}/{total}")

    # camera_mesh = o3d.io.read_triangle_mesh(
    # "/home/andreas/data/camera_simple.ply")
    camera_body = o3d.geometry.TriangleMesh.create_box(depth=2.0)
    camera_head = o3d.geometry.TriangleMesh.create_cone(radius=0.5, height=1.0)
    camera_body = camera_body.translate([-0.5, -0.5, 0.4])
    camera_mesh = camera_body + camera_head
    # camera_mesh = camera_mesh.scale(0.02, camera_mesh.get_center())
    camera_mesh = camera_mesh.transform(camera_pose)
    # camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.86 * camera_pose[:3, 2])
    camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.875 * camera_pose[:3, 2])
    # camera_cloud = camera_mesh.sample_points_poisson_disk(1000)
    camera_cloud = camera_mesh.sample_points_uniformly(10000)

    Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0]))
    camera_frame = deepcopy(camera_pose)
    camera_frame[:3, :3] = np.dot(camera_frame[:3, :3], Rx.as_matrix())
    print(camera_frame)

    # Calculate the grasp poses.
    poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )

    p = HandsPlot(hand_geom)
    # p.plotCloud(mesh, [0.5, 0.0, 0.0])
    # p.plotCloud(cloud, [0.0, 0.0, 1.0])
    # p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
    mid = int(np.floor(len(poses) / 2))
    print(f"mid: {mid}, poses: {len(poses)}")
    mid = mid - 2
    l = poses[mid : mid + 4]
    print("l:", len(l), mid, mid + 4)
    # p.plotHands([poses[mid:mid+4]], [1.0, 1.0, 0.0])
    # p.plotHands(l, [0.0, 0.0, 0.0], 1.0)
    # p.plotHands(l, [1.0, 1.0, 0.0], 1.0)
    # colors = [red, green, blue, yellow]
    for i in range(4):
        # p.plotHands([l[i]], [1.0-i*0.2, 1.0-i*0.2, 0.0], 1.0)
        # p.plotHands([l[i]], [0., 1.0-i*0.15, 0.0], 1.0, 0.5)
        p.plotHands([l[i]], [0., 1.0, 0.0], 1.0, 0.5)
        # p.plotHands([l[i]], colors[i], 1.0, 0.5)
    # p.plotAxes(camera_frame)
    # p.plotAxes(hand_frame, [0.15, 0.15, 0.15])
    # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
    p.setTitle("rotations about approach axis")
    p.show()
    exit()


if __name__ == "__main__":
    main()

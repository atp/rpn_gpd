from argparse import ArgumentParser

import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
import torch as tp
import torch.nn as nn
from torch.utils import data as torchdata

from datasets import BasicDataset
from networks.network import LenetLikeWithSigmoid, VggLikeWithSigmoid
from networks.resnet_mod import resnet18, resnet34
from proposal import projCloudOrth
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import estimateNormals
from utils.plot_images import plotImageChannels

loader_params = {
    "batch_size": 512,
    "shuffle": False,
    "pin_memory": True,
    "num_workers": 8,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}

lenet_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    # Params for Lenet
    # """ 1FC
    # "conv": [32, 64],
    # "fc": [512],
    # """
    # """ 2FC
    # "conv": [32, 64],
    # "fc": [512, 512],
    # """
    # double conv
    "conv": [64, 128],
    "fc": [512, 512],
    # "fc": [500],
    # "fc": [32],
    # "fc": [512, 512],
    "conv_filter_size": 5,
}

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}

net_params_dict = {
    "lenet": lenet_params,
    "vgg": vgg_params,
    "resnet18": {},
    "resnet34": {},
}
networks_dict = {
    "lenet": LenetLikeWithSigmoid,
    "vgg": VggLikeWithSigmoid,
    "resnet18": resnet18,
    "resnet34": resnet34,
}


def fc2Conv(module):
    # Convert the nn.Linear to nn.Conv
    fc = module.state_dict()
    in_ch = fc["weight"].size(1)
    out_ch = fc["weight"].size(0)
    conv = nn.Conv2d(in_ch, out_ch, 1, 1)

    conv.load_state_dict(
        {"weight": fc["weight"].view(out_ch, in_ch, 1, 1), "bias": fc["bias"]}
    )
    return conv


def printLayerOutputs(model, shape=None, device=None):
    if shape is None:
        x = tp.rand(1, 3, 60, 60)
    else:
        x = tp.rand(1, shape[0], shape[1], shape[2])

    if device is not None:
        x = x.to(device)

    with tp.no_grad():
        for i, module in enumerate(model):
            x = module(x)
            print(f"{i:3d}, {x.shape}, {module}")


def parseArgs():
    """Parse command line arguments."""
    parser = ArgumentParser(description="Create database for grasp images")
    parser.add_argument("model", metavar="MODEL", help="network model")
    parser.add_argument(
        "weights_path", metavar="WEIGHTS_PATH", help="path to weights file"
    )
    # parser.add_argument(
        # "database_path", metavar="DATABASE_PATH", help="path to weights file"
    # )
    parser.add_argument("task", metavar="TASK", help="classification task (cls or rot)")
    parser.add_argument(
        "cloud_path", metavar="CLOUD_PATH", help="path to point cloud file"
    )
    parser.add_argument("scale", help="object scaling factor", type=float, default=0.2)

    args = parser.parse_args()

    return args


def main() -> None:
    args = parseArgs()
    net_params = net_params_dict[args.model]
    num_classes = 2 if args.task == "cls" else -1
    # dset = BasicDataset(args.database_path, num_classes)
    # loader = torchdata.DataLoader(dset, **loader_params)
    # imgs_shape = dset.images.shape[-3:]
    imgs_shape = (60, 60, 3)
    net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    if args.task == "cls":
        net_params["out_shape"] = 2
    elif args.task == "rot":
        net_params["out_shape"] = 196
    # if dset.labels.ndim == 1:
        # net_params["out_shape"] = 2
    # elif dset.labels.ndim == 2:
        # net_params["out_shape"] = dset.labels.shape[1]
    print(net_params)
    model = networks_dict[args.model](**net_params)
    print(model)

    # y = model.features(tp.rand(1,3,60,60)) # --> [1, 128, 12, 12]
    # print(y.shape)
    printLayerOutputs(model.features)
    # exit()

    # Convert FC layers to conv layers.
    # fc_layers = nn.Sequential(*list(model.classifier.children())[:-1])
    fc_layers = nn.Sequential(*list(model.classifier.children()))
    fc = fc_layers[0].state_dict()
    in_ch = 128
    out_ch = fc["weight"].size(0)
    print(fc["weight"].shape)
    # exit()
    first_conv = nn.Conv2d(in_ch, out_ch, (12, 12))
    first_conv.load_state_dict(
        {
            "weight": fc["weight"].view(out_ch, in_ch, 12, 12),
            "bias": fc["bias"],
        }
    )
    conv_list = [first_conv]
    for module in fc_layers[1:]:
        if isinstance(module, nn.Linear):
            conv_list += [fc2Conv(module)]
        # elif isinstance(module, nn.BatchNorm1d):
        # conv_list += [nn.BatchNorm2d(module.state_dict()["weight"].size(0))]
        elif not isinstance(module, nn.BatchNorm1d):
            conv_list += [module]
        # else:
        # conv_list += [module]
    print(conv_list)
    # l = list(model.classifier.children())
    # conv_list += [list(model.classifier.children())[-1]]

    # Create the new model with only conv layers.
    # fcn_model = nn.Sequential(*(list(model.features) + [nn.Flatten(), nn.Unflatten(2, [512])] + list(conv_list)))
    fcn_model = nn.Sequential(*(list(model.features) + list(conv_list)))
    print(f"NEW MODEL:\n{fcn_model}")

    camera_pose = np.eye(4)
    voxel_size = 0.003
    # cloud = o3d.io.read_point_cloud(args.cloud_path)

    if args.cloud_path[-3:] == "ply" and "watertight" in args.cloud_path:
        scene = SingleObjectScene(args.cloud_path)
        scene.resetToScale(args.scale)

        # Create a point cloud from a random viewpoint on a sphere.
        camera_distance = 0.5
        look_at = np.zeros(3)
        x = np.random.normal(size=3)
        x = x / np.linalg.norm(x) * camera_distance
        camera_pose = calcLookAtPose(x, look_at)
        cloud = scene.createProcessedPointCloudFromView(camera_pose)

    # Preprocess the point cloud.
    cloud = cloud.voxel_down_sample(voxel_size)
    print(f"Downsampled cloud to {len(cloud.points)} points.")
    cloud = estimateNormals(cloud, camera_pose[:3, 3])
    print(f"Estimated {len(cloud.normals)} surface normals.")

    # Create the input image from the point cloud.
    input_image = projCloudOrth(cloud)
    # plotImageChannels([input_image], "point cloud projections onto cube")
    print(f"input_image: {input_image.shape}")
    input_batch = tp.from_numpy(input_image[np.newaxis, ...]).float()
    input_batch = input_batch.permute(0, 3, 1, 2) / 255.0
    print(f"input_batch: {input_batch.shape}")
    # input_batch = tp.utils.data.TensorDataset(input_batch)
    # loader = tp.utils.data.DataLoader(dset, batch_size=1, shuffle=False)
    # loader = tp.utils.data.DataLoader(
    # dset, batch_size=loader_params["batch_size"], shuffle=False
    # )

    # printLayerOutputs(model.features)
    printLayerOutputs(fcn_model, (3, 600, 600))

    device = tp.device("cuda")
    num_classes = 196

    """
    up_layers = []
    up_layers += [nn.Conv2d(128, num_classes, kernel_size=1)]
    # up_layers.append(nn.ConvTranspose2d(num_classes, num_classes, kernel_size=8, stride=4, padding=2))
    up_layers.append(nn.ConvTranspose2d(num_classes, num_classes, kernel_size=10, stride=5, padding=3, output_padding=1))
    # up_layers.append(nn.ConvTranspose2d(num_classes, num_classes, kernel_size=5, stride=2, padding=1))
    upsampling_model = nn.Sequential(*list(model.features) + up_layers)
    upsampling_model.to(device)
    printLayerOutputs(upsampling_model, (3,60,60), device)
    print("-----------------------")
    y = upsampling_model(tp.rand(1,3,60,60).to(device))
    print(y.shape)
    y = upsampling_model(tp.rand(1,3,600,600).to(device))
    print(y.shape)
    exit()
    """

    up_layers = []
    # up_layers.append(nn.Upsample(scale_factor=4, mode="bilinear", align_corners=True))
    up_layers.append(nn.Upsample(size=(600, 600), mode="bilinear", align_corners=True))
    upsampling_model = nn.Sequential(*list(fcn_model) + up_layers)
    printLayerOutputs(upsampling_model, (3, 600, 600))
    y = upsampling_model(tp.rand(1, 3, 600, 600))
    # y = upsampling_model(tp.zeros(1,3,600,600))
    # y = upsampling_model(tp.ones(1,3,600,600))
    # y_imgs = y.permute(0, 2, 3, 1)
    y = upsampling_model(input_batch)
    y[0, 0] = tp.sigmoid(y[0, 0])
    print(y[0, 0].min(), y[0, 0].max())
    nrows = 2
    ncols = 3
    fig, axarr = plt.subplots(nrows, ncols)
    # for i in range(ncols):
    # axarr[i].imshow(y[0,i].detach().numpy())
    for i in range(ncols):
        y[0, i] = (y[0, i] - y[0, i].min()) / (y[0, i].max() - y[0, i].min())
        print(y[0, i].min(), y[0, i].max())
    x = input_image
    axarr[0, 0].imshow(x[:, :, 0])
    axarr[0, 1].imshow(x[:, :, 1])
    axarr[0, 2].imshow(x[:, :, 2])
    for i in range(ncols):
        axarr[1, i].imshow(y[0, i].detach().numpy(), cmap="hot", vmin=0, vmax=1)
    # axarr[1,0].imshow(y[0,0].detach().numpy())
    # axarr[1,1].imshow(y[0,1].detach().numpy())
    # axarr[1,2].imshow(y[0,2].detach().numpy())
    # axarr[0].imshow(y[1,0].detach().numpy())
    # axarr[1].imshow(y[1,1].detach().numpy())
    plt.show()
    exit()

    up_layers = []
    # up_layers.append(nn.Upsample(scale_factor=4, mode="bilinear", align_corners=True))
    up_layers.append(nn.Upsample(size=(600, 600), mode="bilinear", align_corners=True))
    upsampling_model = nn.Sequential(*list(fcn_model) + up_layers)
    printLayerOutputs(upsampling_model, (3, 600, 600))
    exit()

    # up_layers = []
    # # up_layers.append(nn.Upsample(scale_factor=5, mode="bilinear", align_corners=True))
    # upsampling_model = nn.Sequential(*list(model.features) + up_layers)
    # printLayerOutputs(upsampling_model, (3,600,600))
    with tp.no_grad():
        output = upsampling_model(tp.rand(1, 3, 600, 600))
        print(f"output: {output.shape}")

    with tp.no_grad():
        output = fcn_model(tp.rand(1, 3, 600, 600))
        # output = fcn_model(input_batch)
        # for i, data in enumerate(loader):
        # print(f"i: {i}, data[0]: {data[0].shape}, data[1]: {data[1].shape}")
        # output = fcn_model(data[0])
        print(f"output: {output.shape}")


if __name__ == "__main__":
    main()

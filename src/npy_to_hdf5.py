"""Convert a directory that contains NPY files to a HDF5 database.

The directory should contain one NPY file for each viewpoint.

"""

import os
from argparse import ArgumentParser
from glob import glob

import numpy as np
import tables

from grasp_scene_data import GraspData, SceneData
from hdf5_to_npy import readDatabase

os.environ["BLOSC_NTHREADS"] = "8"
filters = tables.Filters(complevel=9, complib="blosc:lz4")


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(
        description="Convert a directory that contains NPY files to a HDF5 database"
    )
    parser.add_argument("in_dir", help="path to input directory with NPY files")
    parser.add_argument(
        "in_hdf5", help="path to the HDF5 database from which the folder was created"
    )
    parser.add_argument("out_hdf5", help="path to output HDF5 database file")
    parser.add_argument(
        "-l", "--labels_key", help="labels key for NPY files", default="labels"
    )
    parser.add_argument(
        "-m", "--max_in_memory", help="max images in memory", type=int, default=-1
    )

    return parser


def labelsForIndices(
    files: list,
    grasps: list,
    scenes: list,
    used_instances: list,
    instance: str,
    start: int,
    stop: int,
    labels_key: str,
) -> np.array:
    # Find the files corresponding to this instance.
    matches = [f for f, s in zip(files, scenes) if s.instance == instance]
    matches = [x[x.rfind("/") + 1 : x.rfind(".")] for x in matches]

    # Check if this instance was accessed before.
    duplicates = [x for x in matches if x in used_instances]
    if len(duplicates) > 0:
        print("duplicates:", duplicates)
        exit(-1)
    used_instances_out = used_instances + matches

    # Extract the labels for the matching files.
    labels = [g[labels_key] for g, s in zip(grasps, scenes) if s.instance == instance]
    if len(labels) > 0:
        labels = np.concatenate(labels)

    return labels, used_instances_out


def extractLabels(
    path: str, grasps: list, scenes: list, files: list, labels_key: str
) -> np.array:
    with tables.open_file(path, "r") as fin:
        db = readDatabase(fin)
        labels_shape = fin.get_node("/labels").shape
        if len(labels_shape) == 1:
            labels_out = np.zeros((0,))
        elif len(labels_shape) == 2:
            labels_out = np.zeros((0, labels_shape[1]))
        else:
            raise ValueError(f"Shape {labels_shape} is not supported!")
        instances = []

        # Iterate over the database indices corresponding to the unique instances.
        for k, v in db.indices.items():
            labels, instances = labelsForIndices(
                files, grasps, scenes, instances, k, v[0], v[1] + 1, labels_key
            )
            if len(labels) > 0:
                if labels.min() == -1.0:
                    c = [s.category for s in scenes if s.instance == k][0]
                    print("labels.min() == -1!")
                    print("labels:", labels.shape)
                    print("category:", c, "instance:", k)
                    # exit(-1)
                if labels_out.ndim == 2:
                    labels = labels.reshape((-1, labels_out.shape[-1]))
                labels_out = np.concatenate((labels_out, labels))
            info = f"instance: {k}, category: {db.categories[v[0]]}"
            print(info + f", start, stop: {v}, labels_out: {labels_out.shape}")

    return labels_out


def copyImagesBlockwise(fout, images, max_in_memory: int):
    images_out = fout.create_carray(
        "/", "images", tables.UInt8Atom(), shape=images.shape, filters=filters,
    )
    print(f"Created dataset:\n{images_out}")

    print(f"Copying images in blocks of size {max_in_memory} ...")
    blocks = list(np.arange(0, len(images), max_in_memory)) + [len(images)]
    for i in range(1, len(blocks)):
        stop, start = blocks[i], blocks[i - 1]
        images_out[start:stop] = images[start:stop]
        print(f"{i}/{len(blocks)} Copied block from {start} to {stop}")

    return images_out


def main():
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Check that only train or only test files are used.
    paths = (args.in_dir, args.in_hdf5, args.out_hdf5)
    train = np.array([1 for x in paths if "train" in x])
    test = np.array([1 for x in paths if "test" in x])
    if np.sum(train) < 3 and np.sum(test) < 3:
        print("Error: you're mixing train and test files!")
        exit(-1)

    # Read the NPY files from the directory.
    files = sorted(glob(args.in_dir + "*.npy"))
    print(f"Found {len(files)} files")

    # Extract the grasp data from the files.
    scenes = [SceneData.fromFile(f) for f in files]
    # grasps = [GraspData.fromFile(f) for f in files]
    grasps = [np.load(f, allow_pickle=True, encoding="latin1").item() for f in files]

    # Ensure that the ordering of the files is the same as in the database.
    with tables.open_file(args.in_hdf5, "r") as fin:
        labels_out = extractLabels(args.in_hdf5, grasps, scenes, files, args.labels_key)
        labels_in = fin.get_node("/labels")
        print(f"labels_out: {labels_out.shape}, labels_in: {labels_in.shape}")

        # Change negative labels so they work with pytorch.
        if labels_out.min() == -1.0:
            print("Min is -1")
            labels_out[labels_out == -1.0] = 0.0

        # Write the images and the new labels to a new database.
        with tables.open_file(args.out_hdf5, "w") as fout:
            labels_array = fout.create_carray(
                "/", "labels", obj=labels_out, filters=filters
            )
            images = fin.get_node("/images")
            if args.max_in_memory == -1:
                images = np.array(images)
                images_array = fout.create_carray(
                    "/", "images", tables.UInt8Atom(), obj=images, filters=filters
                )
            else:
                images_array = copyImagesBlockwise(fout, images, args.max_in_memory)
            print("Created datasets:")
            print(images_array)
            print(labels_array)

    print("Wrote new database to:", args.out_hdf5)


if __name__ == "__main__":
    main()

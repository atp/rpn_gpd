import csv
import os

import numpy as np
import trimesh

from hand_params import hand_params


# Stores information about object category in a CSV file.
def storeCategoryInfo(filepath, category_info, fieldnames):
    with open(filepath, "w") as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for x in category_info:
            writer.writerow(x)


def createCategoryInfo(category, obj_id, scene):
    info = {}
    info["category"] = category
    info["mesh"] = obj_id
    info["scale"] = scene.scale
    info["x_extent"] = np.around(scene.mesh.extents[0], 4)
    info["y_extent"] = np.around(scene.mesh.extents[1], 4)
    info["z_extent"] = np.around(scene.mesh.extents[2], 4)

    return info


class ObjectSet:
    """A set of objects.

    Attributes:
        root_dir (str): directory that contains mesh files of the objects.
        objects (list of str): list of object categories.
        rescale (bool): if objects need to be rescaled.
    """

    def __init__(self, root_dir, obj_list_path, scale):
        self.root_dir = root_dir
        with open(obj_list_path, "r") as f:
            self.objects = f.readlines()
            self.objects = [o.strip() for o in self.objects]
            self.scale = scale

    def __len__(self):
        """Return the number of object categories."""
        return len(self.objects)

    def get(self, idx):
        """Get the path to an object mesh."""
        raise NotImplementedError("Please implement this method")


class BigbirdObjectSet(ObjectSet):
    """A class used to represent a set of objects from the Bigbird dataset."""

    def __init__(self, root_dir, obj_list_path, rescale, suffix):
        super(BigbirdObjectSet, self).__init__(root_dir, obj_list_path, rescale)
        self.suffix = suffix

    def get(self, idx):
        """Get the path to an object mesh.

        Args:
            idx (int): object index.
        """
        idx = idx[0]
        mesh_file = os.path.join(self.root_dir, self.objects[idx], self.suffix)

        return mesh_file

    def getNumInstances(self, idx):
        """Return the number of object instances in a given category."""
        return 1


class ThreeDNetObjectSet(ObjectSet):
    """A class used to represent a set of objects from the 3DNet dataset."""

    def __init__(self, root_dir, obj_list_path, rescale):
        super(ThreeDNetObjectSet, self).__init__(root_dir, obj_list_path, rescale)
        self.prohibited = []

    def get(self, idx):
        """Get the path to an object mesh.

        Args:
            idx (list of int): contains the object category index and the
                object instance index.
        """
        category_idx, instance_idx = idx
        obj_dir = os.path.join(self.root_dir, self.objects[category_idx])

        # Check for objects that are prohibited.
        if len(self.prohibited) == 0:
            mesh_files = [f for f in os.listdir(obj_dir) if f.endswith(".ply")]
        else:
            mesh_files = [
                f
                for f in os.listdir(obj_dir)
                if f.endswith(".ply") and f not in self.prohibited[category_idx]
            ]
            print("prohibited files:\n", self.prohibited[category_idx])
            print("allowed files:\n", mesh_files)
        mesh_files.sort()
        mesh_file = os.path.join(obj_dir, mesh_files[instance_idx])

        return mesh_file

    def getNumInstances(self, idx):
        """Return the number of object instances in a given category.

        Args:
            idx (int): the category index.
        """
        obj_dir = os.path.join(self.root_dir, self.objects[idx])
        mesh_files = [f for f in os.listdir(obj_dir) if f.endswith(".ply")]

        return len(mesh_files)

    def getByStringID(self, category, instance):
        mesh_file = os.path.join(self.root_dir, category, instance)

        return mesh_file

    def setProhibitedObjectInstances(self, prohibited):
        categories = sorted(set([x[0] for x in prohibited]))
        print("categories:", categories)
        self.prohibited = []
        for i in range(len(categories)):
            insts = [x[1] + ".ply" for x in prohibited if x[0] == categories[i]]
            self.prohibited.append(insts)
        print("This object set prohibits the following instances:")
        for i in range(len(categories)):
            print(f"Category {i}: {self.objects[i]}")
            print(self.prohibited[i])

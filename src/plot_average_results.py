import os
import sys
from glob import glob

import matplotlib.pyplot as plt
import numpy as np


def addEndpoints(
    precision: np.array, recall: np.array, pts: list = [[1.0, 0.0], [0.0, 1.0]]
):
    precision_out = np.hstack((pts[0][0], precision))
    recall_out = np.hstack((pts[0][1], recall))

    precision_out = np.hstack((precision_out, pts[1][0]))
    recall_out = np.hstack((recall_out, pts[1][1]))

    return precision_out, recall_out


def removeNans(x: np.array, y: np.array):
    idxs = np.nonzero((np.isnan(x)) | (np.isnan(y)))
    xout = np.delete(x, idxs)
    yout = np.delete(y, idxs)

    return xout, yout


def calcAveragePrecisionRecall(in_dir: str, category: str) -> tuple:
    files = sorted(glob(os.path.join(in_dir) + f"/{category}*.npy"))
    results = [np.load(f) for f in files]

    avg_precision = 0.0
    avg_recall = 0.0
    avg_fpr = 0.0
    count = 0

    # Iterate over object instances.
    for i, r in enumerate(results):
        for j, view in enumerate(r):
            threshs = view[:, 0]
            precision = view[:, 1] / (view[:, 1] + view[:, 2])
            recall = view[:, 1] / (view[:, 1] + view[:, 4])
            fpr = view[:, 2] / (view[:, 2] + view[:, 3])
            avg_precision += precision
            avg_recall += recall
            avg_fpr += fpr
            count += 1
            if plots_intermediate:
                plt.plot(threshs, precision, label="precision")
                plt.plot(threshs, recall, label="recall")
                plt.xlabel("thresholds")
                plt.legend()
                plt.xlim([-0.05, 1.05])
                plt.ylim([-0.05, 1.05])
                instance = files[i][files[i].rfind("/") + 1 : files[i].rfind(".")]
                plt.title(f"Instance: {instance}, View: {j}")
                plt.show()

    out = {
        "precision": avg_precision / count,
        "recall": avg_recall / count,
        "fpr": avg_fpr / count,
        "threshs": threshs,
        "views": count,
    }

    return out


def plotMetrics(x: np.array, y: np.array, label: str, marker: str = "-x"):
    plt.plot(x, y, marker, label=label)
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.xlabel("recall")
    plt.ylabel("precision")
    plt.title(f"Average precision-recall over {metrics['views']} views")
    plt.legend()
    plt.show()


# Read command line arguments.
in_dir = sys.argv[1]
category = sys.argv[2]
if category == "all":
    category = ""
plots_intermediate = int(sys.argv[3])

metrics = calcAveragePrecisionRecall(in_dir, category)
plt.plot(metrics["threshs"], metrics["precision"], label="precision")
plt.plot(metrics["threshs"], metrics["recall"], label="recall")
plt.xlabel("thresholds")
plt.legend()
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.title(f"Average precision/recall-threshold over {metrics['views']} views")
plt.show()

metrics["precision"], metrics["recall"] = removeNans(
    metrics["precision"], metrics["recall"]
)
metrics["precision"], metrics["recall"] = addEndpoints(
    metrics["precision"], metrics["recall"]
)

methods = ["gpd", "ma", "qd", "graspnet"]
method = [x for x in methods if x in in_dir][0]

plotMetrics(metrics["recall"], metrics["precision"], method)

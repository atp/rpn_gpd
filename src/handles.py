"""Helper functions to detect handles (groups of grasps).

A handle is a set of grasps whose elements are geometrically aligned.

"""

from copy import deepcopy

import numpy as np


def findHandles(poses, min_inliers=10):
    """Find geometrically aligned sets of grasps.

    Two grasps are geometrically aligned if their positions, up axes, and forward axes are close.

    Args:
        poses (np.array): n x 4 x 4 grasp poses.
        min_inliers (int, optional): the minimum number of grasps that need to
            be aligned to form a handle.

    Returns:
        list: sets of indices of geometrically aligned grasps.
    """
    max_line_dist = 0.01
    max_up_axis_dist = 0.34
    max_forward_axis_dist = 0.34

    # max_line_dist = 0.02
    # max_up_axis_dist = 0.52
    # max_forward_axis_dist = 0.52

    # Find out which grasps are geometrically aligned.
    up_axes = poses[:, :3, 2]
    forward_axes = poses[:, :3, 0]
    positions = poses[:, :3, 3]
    # print([x.shape for x in [up_axes, forward_axes, positions]])

    forward_dists = [np.arccos(np.dot(forward_axes, x)) for x in forward_axes]
    up_dists = [np.arccos(np.around(np.dot(up_axes, x))) for x in up_axes]
    up_dists = [np.minimum(x, np.pi - x) for x in up_dists]
    line_dists = [
        np.dot(
            (np.eye(3) - np.outer(x, x)),
            (positions - y).T,
        )
        for x, y in zip(up_axes, positions)
    ]
    line_dists = [np.linalg.norm(x, axis=0) for x in line_dists]
    # print("forward_dists:", forward_dists[0].shape, forward_dists[0])
    # print("up_dists:", up_dists[0].shape, up_dists[0])
    # print("line_dists:", line_dists[0].shape, line_dists[0])

    forward_mask = [x < max_forward_axis_dist for x in forward_dists]
    up_mask = [x < max_up_axis_dist for x in up_dists]
    line_mask = [x < max_line_dist for x in line_dists]
    # print("forward_mask:", forward_mask[0].shape, forward_mask[0])
    # print("up_mask:", up_mask[0].shape, up_mask[0])
    # print("line_mask:", line_mask[0].shape, line_mask[0])

    mask = [x & y & z for x, y, z in zip(forward_mask, up_mask, line_mask)]
    inliers = [np.nonzero(x)[0] for x in mask]
    handles = [x for x in inliers if len(x) >= min_inliers]
    # print("mask:", len(mask), mask[0].shape, mask[0])
    # print("inliers:", inliers)
    # print("handles:", handles)

    # Remove shorter handles contained in long handle.
    overlap = [[x for x in handles if x[0] == y[0]] for y in handles]
    handles = [x[np.argmax([len(y) for y in x])] for x in overlap]
    print("handles:")
    for h in handles:
        print(h)

    # Remove duplicate rows.
    handles = list(set([tuple(x) for x in handles]))
    handles = [list(x) for x in handles]
    print("unique handles:")
    for h in handles:
        print(h)

    return handles


def convertToHandles(poses, indices_list):
    """Convert grasp poses to handles.

    [TODO:description]

    Args:
        poses ([TODO:type]): [TODO:description]
        indices_list ([TODO:type]): [TODO:description]
    """
    handles = []

    return handles

def pruneShortHandles(handles, poses, min_length=0.01):
    positions = [np.dot(x.up_axis, poses[x.inliers][:,:3,3].T) for x in handles]
    lengths = [np.max(x) - np.min(x) for x in positions]
    print("[pruneShortHandles] lengths:", np.array(lengths))
    handles_out = [x for i,x in enumerate(handles) if lengths[i] >= min_length]

    return handles_out

def removeEndPoints(handles, poses, cutoff=1):
    positions = [np.dot(x.up_axis, poses[x.inliers][:,:3,3].T) for x in handles]
    idxs = [np.argsort(x) for x in positions]
    handles_out = []
    for handle, idx in zip(handles, idxs):
        handle_out = deepcopy(handle)
        if len(handle_out.inliers) > 2*cutoff:
            handle_out.inliers = np.array(handle.inliers)[idx[cutoff:-cutoff]]
        handles_out.append(handle_out)
        # print("[removeEndPoints]", idx, "-->", idx[1:-1])

    return handles_out

class Handle:
    """Class for storing a geometrically aligned set of grasps."""

    def __init__(self, inliers: list, poses: np.array, widths: np.array):
        self.inliers = inliers
        self.up_axis = self.calcAxis(inliers, poses)
        idx = self.calcCenter(inliers, poses)
        pose = poses[inliers[idx]]
        self.width = widths[inliers[idx]]
        self.center = pose[:3, 3]
        self.approach_axis = pose[:3, 0]
        self.closing_axis = np.cross(self.up_axis, self.approach_axis)
        self.pose = np.eye(4)
        self.pose[:3, :3] = np.column_stack(
            (self.approach_axis, self.closing_axis, self.up_axis)
        )
        self.pose[:3, 3] = self.center

    def calcAxis(self, inliers: list, poses: np.array):
        axes = poses[inliers, :3, 2]
        vals, vecs = np.linalg.eig(np.dot(axes.T, axes))

        return vecs[:, np.argmax(vals)]

    def calcCenter(self, inliers: list, poses: np.array):
        positions = poses[inliers, :3, 3]
        dists = np.array([np.dot(self.up_axis, x) for x in positions])
        print("[calcCenter]: dists", dists)
        center = np.min(dists) + 0.5 * (np.max(dists) - np.min(dists))
        dists = np.abs(dists - center)
        print("[calcCenter]: dists", dists, "center:", center)

        return np.argmin(dists)

    def keepClosestGrasps(self, poses, max_dist=0.02):
        dists = [np.linalg.norm(self.center - p[:3, 3]) for p in poses]
        closest = np.array(dists) <= max_dist

        return poses[closest]

        # pos_dists = [np.linalg.norm(self.center - p[:3, 3]) for p in poses[self.inliers]]
        # rot_dists = [np.arccos(0.5*(np.trace(p[:3,:3]) - 1)) for p in poses[self.inliers]]
        # rot_dists = [np.real(x) for x in rot_dists]
        #
        # dists = [0.5*(np.min([p/max_dist, 1])) + 0.5*(r/np.pi) for (p,r) in zip(pos_dists, rot_dists)]
        # # print("dists:", dists)
        # # input("?")
        #
        # closest = np.argsort(dists)
        #
        # return closest

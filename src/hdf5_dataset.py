"""A dataset that can load data from multiple HDF5 files."""

import os
from dataclasses import dataclass, field
from glob import glob

import numpy as np
import tables
import torch


@dataclass
class HDF5FileInfo:
    path: str
    start: int
    stop: int


class HDF5Dataset:
    """A dataset that can load data from multiple HDF5 files."""

    def __init__(
        self,
        path: str,
        cache_size: int = 2,
        load_all_data: bool = False,
        nodes: list = ["/images", "/labels"],
    ):
        super().__init__()
        self.infos = []
        self.cache = {}
        self.cache_size = cache_size
        self.labels = []
        self.length = 0

        if path[-3:] == ".h5":
            paths = [path]
        else:
            paths = sorted(glob(os.path.join(path, "*.h5")))

        if len(paths) < 1:
            raise RuntimeError(f"No HDF5 files found at {path}")

        start = 0
        for p in paths:
            with tables.open_file(p, "r") as f:
                labels = f.get_node(nodes[-1])
                stop = start + labels.shape[0]
                self.labels.append(labels)
            self.infos.append(HDF5FileInfo(f, start, stop))
            print(p, start, stop)
            stop = start

    # def __getitem__(self, index):
    # x = self.getData("data", index)
    # if self.transform:
    # x = self.transform(x)
    # else:
    # x = torch.from_numpy(x)

    # y = self.getData("label", index)
    # y = torch.from_numpy(y)

    # return (x, y)

    # def __len__(self):
    # return self.length

    # def _addInfo(self, path: str, nodes: list, load_all_data: bool):
    # with tables.open_file(path, "r") as f:
    # ds = [x for x in f.get_node(nodes)]
    # idx = -1
    # if load_all_data:
    # idx = self._addToCache(ds, path)
    # self.infos.append({"path": path, "shape": ds.shape, "cache_idx": idx})
    # self.length += ds[0].shape[0]

    # def _addToCache(self, path: str, data) -> int:
    # if path not in self.cache:
    # self.cache[path] = [data[node_names[1]]]
    # else:
    # self.cache[path].append(data)
    # idx = len(self.cache[path]) - 1

    # return idx

    # def getData(self, idx):
    # pass

    # def getInfos(self, data_type: str):
    # infos = [x for x in self.infos if x["type"] == data_type]

    # return infos


ds = HDF5Dataset("../data/multi/")

"""Calculate per channel mean and standard deviation for a database of images.
"""

import sys
from typing import Tuple

import torch
from torch.utils import data as torchdata

from datasets import BasicDataset

loader_params = {
    "batch_size": 512,
    "shuffle": True,
    "pin_memory": True,
    "num_workers": 4,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}
print_step = 500


def calcMeanStd(loader: torchdata.DataLoader) -> Tuple[float, float]:
    channels_sum, channels_sum_squared = 0.0, 0.0
    num_batches = len(loader)

    for i, (data, _) in enumerate(loader):
        if i % print_step == print_step - 1:
            print(f"batch: {i+1}/{num_batches}")
        channels_sum += torch.mean(data, dim=[0, 2, 3])
        channels_sum_squared += torch.mean(data ** 2, dim=[0, 2, 3])

    mean = channels_sum / num_batches
    std = (channels_sum_squared / num_batches - mean ** 2) ** 0.5

    return mean, std


def main() -> None:
    if len(sys.argv) < 3:
        print("Error: not enough input arguments!")
        print("Usage: python calc_dataset_mean_std.py HDF5 NUM_CLASSES")
        exit(-1)
    filepath = sys.argv[1]
    num_classes = int(sys.argv[2])
    dataset = BasicDataset(filepath, num_classes)
    loader = torchdata.DataLoader(dataset, **loader_params)
    mean, std = calcMeanStd(loader)
    print("mean,std:", mean, std)


if __name__ == "__main__":
    main()

"""Generate grasps for a mesh file.
"""

import sys
from argparse import ArgumentParser
from copy import deepcopy
from time import time

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from grasp_scene_data import GraspData, SceneData, storeGrasps
from ground_truth import findGroundTruthPositives
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose, randomStableRotation3D
from utils.cloud_utils import numpyToCloud, orientNormalsTowardsMean
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes
from utils.plot import plotColoredHands

cube_size = 1.0

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Generate grasp poses for an object mesh file")
    parser.add_argument("file", help="path to mesh file")
    parser.add_argument("scale", help="scaling factor", type=float)
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument("num_samples", help="number of samples", type=int)
    parser.add_argument("--plot", help="turn plotting on", action="store_true")
    parser.add_argument("--store", help="store grasp poses", action="store_true")
    parser.add_argument(
        "--single", help="visualize grasps at a single sample", action="store_true"
    )
    parser.add_argument(
        "-t", "--thresh", help="extremal threshold", type=float, default=0.003
    )
    parser.add_argument(
        "-c", "--coeff", help="friction coefficient", type=float, default=30
    )
    parser.add_argument("-v", "--viable", help="min viable", type=int, default=6)
    parser.add_argument("-2s", "--twostage", help="", action="store_true")
    parser.add_argument(
        "-ap", "--add_plane", action="store_true", help="add support plane to object"
    )
    parser.add_argument(
        "-rp", "--rotate_in_plane", action="store_true", help="rotate object in plane"
    )

    return parser


def createHandsGenerator(method, hand_geom, antip_prms):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        gen = QdHandsGenerator(hand_geom, antip_prms)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def main() -> None:
    draw_geoms = o3d.visualization.draw_geometries

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # data = createPointCloudFromMesh(args.file, args.scale)
    scene = SingleObjectScene(args.file)
    scene.resetToScale(args.scale)
    if args.rotate_in_plane:
        tf = np.eye(4)
        tf[:3, :3] = randomStableRotation3D(flip_prob=1.0)
        scene.setObjectPose(tf)
    mesh, views_cloud = scene.createCompletePointCloud(add_plane=args.add_plane)
    tree = o3d.geometry.KDTreeFlann(mesh)
    if args.plot:
        draw_geoms(
            [mesh, views_cloud], "Multiview point cloud and view points", 640, 480
        )
        draw_geoms([mesh], "Surface normals", 640, 480)

    # orientNormalsTowardsMean(mesh, tree)
    # if args.plot:
    # draw_geoms([mesh], "Surface normals after smoothing", 640, 480)

    # Draw sample points from the point cloud.
    pts = np.asarray(mesh.points)
    samples_cloud = numpyToCloud(pts[pts[:, 2] > 0])
    samples = sampleFromCube(samples_cloud, cube_size, args.num_samples)
    if args.plot:
        plotSamplesAsBoxes(mesh, samples, "Samples")

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antipodal_params = {
        "extremal_thresh": args.thresh,
        "friction_coeff": args.coeff,
        "min_viable": args.viable,
        "checks_overlap": args.twostage,
    }
    print("Antipodal parameters:", antipodal_params)
    # antipodal_params = {"extremal_thresh": 0.003,
    # "friction_coeff": 30,
    # "min_viable": 6}
    # antipodal_params = {"extremal_thresh": 0.006,
    # "friction_coeff": 20,
    # "min_viable": 6}
    # antipodal_params = {"extremal_thresh": 0.006,
    # "friction_coeff": 40,
    # "min_viable": 6}
    antip_prms = AntipodalParams(**antipodal_params)
    gen = createHandsGenerator(args.method, hand_geom, antip_prms)

    # Generate the grasp poses.
    t = time()
    camera_pose = scene.scene.get_pose(scene.camera_node)
    hands, frames = gen.generateHands(mesh, samples, camera_pose=camera_pose)
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")
    # idxs = np.array(np.nonzero(findGroundTruthPositives(hands)))
    collision_free = hands[:, :, 0] == 1
    antipodal = hands[:, :, 1] == 1
    idxs = np.array(np.nonzero(collision_free & antipodal))
    idxs_generator = idxs
    total = hands.shape[0] * hands.shape[1]
    print(f"collision_free: {np.sum(collision_free)}/{total}")
    print(f"antipodal: {np.sum(antipodal)}/{total}")

    # Visualize the grasp poses.
    poses = gen.indicesToPoses(mesh, tree, samples, {"indices": idxs, "frames": frames})
    if args.plot:
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh, [1.0, 0.0, 0.0])
        p.plotHands(poses, [1.0, 1.0, 0.0], scale=0.5)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("valid grasp poses")
        p.show()

    # Store the grasps.
    if args.store:
        category = args.file
        instance = category[category.rfind("/") + 1 :]
        category = category[: category.rfind("/")]
        category = category[category.rfind("/") + 1 :]
        scene_data = SceneData(
            category, instance, args.scale, camera_pose, np.asarray(mesh.points)
        )
        grasp_data = GraspData(poses, np.ones(len(poses)), args.method)
        instance = instance[: instance.rfind(".")]
        fpath = f"./ground_truth_{args.method}_{category}_{instance}"
        storeGrasps(scene_data, grasp_data, fpath)

    if args.single == False:
        exit()

    # Visualize all hand orientations for a single grasp position.
    # Used to check algorithms and make figures.
    print("\nEvaluating a single grasp position ...")
    m = gen.num_hand_orientations
    idxs = np.vstack((np.zeros(m), np.arange(m))).astype(np.int)
    idxs = idxs[:, int(m / 2)][:, np.newaxis]
    samples[0] = [0.0, -0.0125, 0.0]
    print(idxs.shape)
    poses = gen.indicesToPoses(mesh, tree, samples, {"indices": idxs, "frames": frames})

    if args.plot:
        # poses = [poses[0]]
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh, [0.0, 0.0, 1.0])
        # p.plotApproaches(poses, [0.0, 1.0, 1.0], scale=[0.1, 1.0, 1.0])
        p.plotHands(poses, [0.0, 1.0, 0.0], scale=1.0)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("approach vectors (single position)")
        p.show()

    if args.plot:
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh, [0.0, 0.0, 1.0])
        p.plotHands(poses, [0.0, 1.0, 0.0], scale=0.5)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle("grasp poses (single position)")
        p.show()
        if args.method == "qd":
            hand_colors = np.random.rand(int(len(poses) / gen.num_approaches), 3)
            hand_colors = np.tile(hand_colors, (1, gen.num_approaches))
            hand_colors = hand_colors.reshape((len(poses), 3))
        else:
            hand_colors = np.zeros((len(poses), 3), dtype=np.float)
            hand_colors[:, 0] = np.linspace(0.1, 1.0, len(poses))
            hand_colors = np.random.rand(len(poses), 3)
        plotColoredHands(
            [mesh],
            poses,
            hand_colors,
            hand_params,
            "grasp poses (single position)",
            plot_base_frame=True,
        )

    # Label the grasp poses.
    hand_eval = HandEval(hand_geom, antip_prms)
    infos = hand_eval.evalPoses(mesh, poses)
    labels_dict = {
        "collision-free": infos[:, 0] == 1,
        "antipodal": infos[:, 1] == 1,
        "collision_free and antipodal": infos[:, 0] * infos[:, 1] == 1,
    }
    idxs_checker = np.array(np.nonzero(labels_dict["collision_free and antipodal"]))
    print(
        "infos:",
        infos.shape,
        [(k, np.count_nonzero(labels_dict[k])) for k in labels_dict],
    )

    p = HandsPlot(hand_geom)
    p.plotCloud(mesh, [1.0, 0.0, 1.0])
    p.plotHands(
        poses[labels_dict["collision-free"]], [0.0, 0.1, 0.0], opacity=0.1, scale=0.1
    )
    p.plotHands(poses[labels_dict["antipodal"]], [0.0, 1.0, 0.0], scale=0.1)
    p.plotAxes(np.eye(4))
    p.plotAxes(camera_pose)
    p.show()
    # if args.plot:
    # for k in labels_dict:
    # p = HandsPlot(hand_geom)
    # p.plotCloud(mesh, [1.0, 0.0, 1.0])
    # if np.count_nonzero(~labels_dict[k]) > 0:
    # p.plotHands(poses[~labels_dict[k]], [1.0, 0.0, 0.0], scale=0.1)
    # if np.count_nonzero(labels_dict[k]) > 0:
    # p.plotHands(poses[labels_dict[k]], [0.0, 1.0, 0.0], scale=0.1)
    # p.plotAxes(np.eye(4))
    # p.plotAxes(camera_pose)
    # p.setTitle(f"{k} grasp poses (single position)")
    # p.show()

    # Ensure that the antipodal labels are the same, for a single sample.
    if args.num_samples == 1:
        if args.method == "gpd":
            print("Error: this check is not reliable for GPD (reason: proposals).")
            exit(-1)
        mask = idxs_generator[1] == idxs_checker
        all_equal = np.all(mask)
        assert all_equal
        print(f"Are the labels calculated by the hands generator correct?")
        print(f"  Answer: {all_equal}")


if __name__ == "__main__":
    main()

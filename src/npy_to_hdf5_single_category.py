"""Convert a directory that contains NPY files to a HDF5 database.

The directory should contain one NPY file for each viewpoint.

"""

import os
from argparse import ArgumentParser
from glob import glob

import numpy as np
import tables

from grasp_scene_data import GraspData, SceneData
from hdf5_to_npy import readDatabase

os.environ["BLOSC_NTHREADS"] = "8"
filters = tables.Filters(complevel=9, complib="blosc:lz4")


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(
        description="Convert a directory that contains NPY files to a HDF5 database"
    )
    parser.add_argument("in_dir", help="path to input directory with NPY files")
    parser.add_argument(
        "in_hdf5", help="path to the HDF5 database from which the folder was created"
    )
    parser.add_argument("out_hdf5", help="path to output HDF5 database file")
    parser.add_argument(
        "-l", "--labels_key", help="labels key for NPY files", default="labels"
    )

    return parser


def main():
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Read the NPY files from the directory.
    # files = sorted(glob(args.in_dir + "*.npy"))
    files = sorted(
        glob(args.in_dir + "*.npy"),
        key=lambda x: (x, int(x[x.rfind("_") + 1 : x.rfind(".")])),
    )
    nums = np.array([int(x[x.rfind("_") + 1 : x.rfind(".")]) for x in files])
    start = np.min(nums)
    stop = np.max(nums)

    instances = set([f[f.rfind("/") + 1 : f.rfind("_")] for f in files])
    instances = sorted(list(instances))
    print(instances)

    files = []
    for x in instances:
        files += [args.in_dir + x + f"_{i}.npy" for i in range(start, stop + 1)]

    print(f"Found {len(files)} files")
    for i, f in enumerate(files):
        print(f"({i}) {f[f.rfind('/')+1:f.rfind('.npy')]}")

    # Extract the grasp data from the files.
    data = [np.load(f, allow_pickle=True, encoding="latin1").item() for f in files]
    labels = [x[args.labels_key] for x in data]
    labels = np.concatenate(labels)

    idxs = [x["db_indices"] for x in data]
    print([x[0][0] for x in idxs])

    with tables.open_file(args.in_hdf5, "r") as fin:
        images = np.array(fin.get_node("/images"))

    # Write the images and the labels to a new database.
    with tables.open_file(args.out_hdf5, "w") as fout:
        images_out = fout.create_carray(
            "/", "images", tables.UInt8Atom(), obj=images, filters=filters
        )
        labels_out = fout.create_carray("/", "labels", obj=labels, filters=filters)
        print("Created datasets:")
        print(images_out)
        print(labels_out)

    print("Wrote new database to:", args.out_hdf5)


if __name__ == "__main__":
    main()

"""Evaluate a sequence of classifiers at different thresholds.

Depending on the number of input arguments, this script can both evaluate the
classifier sequence cls=>gc and cls=>rot=>gc.
"""

import logging as log
from datetime import datetime
from itertools import product

import matplotlib

matplotlib.use("Agg")
import os
import sys
from time import time

import numpy as np
import torch
import torch.utils.data as torchdata

from datasets import MatrixDataset
from learning import Metrics
from networks.multi_branch_net import MultiBranchNetWithSigmoid
from networks.network import LenetLikeWithSigmoid

# max_batches = 1
# max_batches = 3
max_batches = 10
# max_batches = 30
# max_batches = -1
num_threshs = 10
# num_threshs = 20
# num_threshs = 30
# num_threshs = 3
# num_threshs = 5

# Network parameters.
net_params = {"use_batchnorm": True, "input_channels": 3, "conv": [32, 64], "fc": [500]}

# Dataloader parameters.
loader_params = {
    "batch_size": 64,
    "shuffle": True,
    "pin_memory": True,
    # 'num_workers': 4 # Cannot use multiple workers here b/c of HDF5.
}

configs = {
    "ma-cls": {"image_array_name": "gen_images", "num_classes": 2},
    "ma-rot": {"image_array_name": "gen_images", "num_classes": 56},
    "ma-gc": {"image_array_name": "cls_images", "num_classes": 2},
}

time_per_image = 0.0005  # average runtime per image on 8-core

np.random.seed(0)
np.set_printoptions(precision=4)


def createPretrainedNet(model, net_params, num_classes, model_path):
    # net = LenetWithSigmoid(input_channels, num_classes, units)
    net_params["num_classes"] = num_classes
    net = model(**net_params)
    net.load_state_dict(torch.load(model_path))
    net.to(device)
    net.eval()
    return net


# def suppressNonMax(indices, scores, generator):
#     indices_out = []
#
#     rows = np.unique(indices[:,0])
#
#     rot_mats = generator.calcRotations(indices[:,1])
#     quats = [Rot.from_dcm(rot_mats[i]).as_quat() for i in range(len(rot_mats))]
#
#     for r in rows:
#         sel = indices[indices[:,0] == r, :]
#         max_idx = scores[sel[:,0], sel[:,1]]
#
#
#     # quats = np.zeros((len(rot_mats), 4))
#     # for i in range(len(rot_mats)):
#     #     quats[i] = Rot.from_dcm(rot_mats[i]).as_quat()
#     # print('quats:\n', quats)
#
#     while len(indices_left) > 0:
#         # print('indices_left:', indices_left)
#         last_idx = len(indices_left) - 1
#         indices_out.append(last_idx)
#
#         sample = samples[indices[0,last_idx]]
#         positions = samples[indices[0, indices_left[:last_idx]]]
#         pos_dist = np.sum(np.power(positions - sample, 2), 1)
#         pos_idx = np.where(pos_dist < self.pos_thresh)[0]
#         prune = [last_idx]
#         if len(pos_idx) > 0:
#             rot_idx = indices_left[last_idx]
#             rot_dist = calcRotationDistance(quats[indices_left[pos_idx]], \
#                                             quats[rot_idx])
#             rot_idx = np.where(rot_dist < self.rot_thresh)[0]
#             prune = np.concatenate((prune, pos_idx[rot_idx]))
#             # print('  pos_idx:', pos_idx)
#         # print('prune:', prune)
#         # input('?')
#         indices_left = np.delete(indices_left, prune)
#         # print('indices_left:', indices_left)
#
#     # print('indices_out:', indices_out)
#
#     return indices[:, indices_out]


def evalAtThresholdsClsAndRot(models, threshs, inputs, labels, class_idx=1):
    metrics = Metrics()

    if np.any(threshs == 1):
        return metrics, 0

    print("threshs:", threshs)

    # Run the models for candidate generation.
    outputs = [models[i](inputs[0]) for i in range(2)]

    # Select the candidates for the grasp classifier.
    rows = torch.unique(torch.nonzero(outputs[0][:, class_idx] < threshs[0]))
    mask = outputs[1] > threshs[1]
    mask[rows, :] = 0
    indices = torch.nonzero(mask)
    num_rows_above = len(labels) - len(rows)
    log.info(
        f"rows above thresh: {num_rows_above}/{len(labels)}, "
        + f"candidate indices: {indices.shape[0]}/{num_rows_above*mask.shape[1]}"
    )
    if indices.numel() == 0:
        return metrics, 0

    # Estimate the time it would take to calculate the images for GC.
    time_images = np.prod(indices.shape) * time_per_image
    log.info(f"time_images: {time_images:.3f}s")

    # Run the grasp classifier.
    time_gc = time()
    gc_in = inputs[-1][indices[:, 0], indices[:, 1]]
    gc_out = models[-1](gc_in.contiguous())
    pred_pos = torch.nonzero(gc_out[:, class_idx] > threshs[-1])
    time_gc = time() - time_gc
    log.info(
        f"gc_in: {gc_in.shape}, gc_out: {gc_out.shape}, "
        + f"gc_pred_pos: {pred_pos.shape}"
    )

    # Evaluate the grasp classifier's performance.
    pred = torch.zeros(gc_out.shape[0], dtype=torch.long)
    pred[pred_pos] = 1
    labels = labels[indices[:, 0], indices[:, 1]].cpu().flatten().long()
    #    log.info('labels:', labels.shape, 'pred:', pred.shape)
    metrics = Metrics(labels, pred.cpu())

    outputs.append(gc_out)
    log.info([[torch.min(x), torch.max(x), torch.mean(x)] for x in outputs])
    # log.info('max for each outputs:', [torch.max(x) for x in outputs])

    log.info(
        f"threshs: {threshs}, runtime(gc,imgs): {time_gc:.3f}s, "
        + f"{time_images:.3f}s, indices: {indices.shape}"
    )
    log.info(metrics)

    time_total = time_images + time_gc
    log.info(f"time_total: {time_total:.3f}")
    log.info("-------------------------------------------------------------------")

    return metrics, time_total


def evalAtThresholdsCls(models, threshs, inputs, labels, class_idx=1):
    metrics = Metrics()

    if np.any(threshs == 1):
        return metrics, 0

    gen_model, gc_model = models[0], models[1]
    gen_in, gc_in = inputs[0], inputs[1]
    gen_thresh, gc_thresh = threshs[0], threshs[1]

    # Run the candidate generator.
    gen_out = gen_model(gen_in)
    gen_above_thresh = torch.nonzero(gen_out[:, class_idx] > gen_thresh)
    rows = torch.unique(gen_above_thresh[:, 0])
    if rows.shape[0] == 0:
        return metrics, 0
    log.info(
        f"gen_in: {gen_in.shape}, gen_out: {gen_out.shape}, gen_above_thresh: {gen_above_thresh.shape}"
    )

    # Estimate the time it would take to calculate the images for GC.
    time_per_gen_image = 0.00013
    time_gen = labels.shape[0] * time_per_gen_image
    time_images = len(rows) * labels.shape[1] * time_per_image
    time_images += time_gen
    log.info(f"labels: {labels.shape}, time_images: {time_images:.3f}")

    # Run the grasp classifier.
    time_gc = time()
    gc_in = gc_in[rows, :]
    gc_in_shape = (gc_in.shape[0] * gc_in.shape[1],) + gc_in.shape[2:]
    gc_in = gc_in.reshape(gc_in_shape)
    gc_out = gc_model(gc_in)
    gc_above_thresh = torch.nonzero(gc_out[:, class_idx] > gc_thresh)
    time_gc = time() - time_gc
    log.info(
        f"gc_in: {gc_in.shape}, gc_out: {gc_out.shape}, gc_above_thresh: {gc_above_thresh.shape}"
    )

    pred = torch.zeros(gc_out.shape[0], dtype=torch.long)
    pred[gc_above_thresh] = 1
    labels = labels[rows, :].cpu().flatten().long()
    #    log.info('labels:', labels.shape, 'pred:', pred.shape)
    metrics = Metrics(labels, pred.cpu())

    log.info(
        f"threshs: {threshs}, runtime(gc,imgs): {time_gc:.3f}s, "
        + f"{time_images:.3f}s, #rows: {len(rows)}, "
        + f"TP: {metrics.tp}, FP: {metrics.fp}"
    )

    time_total = time_images + time_gc
    log.info(f"time_total: {time_total:.3f}")
    log.info("-------------------------------------------------------------------")

    return metrics, time_total


def evalAtThresholdsGc(models, threshs, inputs, labels, class_idx=1):
    metrics = Metrics()

    if np.any(threshs == 1):
        return metrics, 0

    gc_model = models[0]
    gc_in = inputs[-1]
    gc_thresh = threshs[0]

    # Estimate the time it would take to calculate the images for GC.
    if len(gc_in.shape) == 5:
        time_images = np.prod(labels.shape) * time_per_image
    else:
        time_per_grid_eval = 0.0000603
        time_images = np.prod(labels.shape) * (time_per_image + time_per_grid_eval)
    log.info(f"labels: {labels.shape}, time_images: {time_images:.3f}")

    # Run the grasp classifier.
    time_gc = time()
    if len(gc_in.shape) == 5:
        gc_in_shape = (gc_in.shape[0] * gc_in.shape[1],) + gc_in.shape[2:]
        gc_in = gc_in.reshape(gc_in_shape)
    log.info(f"gc_in: {gc_in.shape}")
    gc_out = gc_model(gc_in)
    gc_above_thresh = torch.nonzero(gc_out[:, class_idx] > gc_thresh)
    time_gc = time() - time_gc
    log.info(
        f"gc_in: {gc_in.shape}, gc_out: {gc_out.shape}, gc_above_thresh: {gc_above_thresh.shape}"
    )

    pred = torch.zeros(gc_out.shape[0], dtype=torch.long)
    pred[gc_above_thresh] = 1
    labels = labels.cpu().flatten().long()
    log.info("labels:", labels.shape, "pred:", pred.shape)
    metrics = Metrics(labels, pred.cpu())

    log.info(
        f"threshs: {threshs}, runtime(gc,imgs): {time_gc:.3f}s, "
        + f"{time_images:.3f}s, inputs: {gc_in.shape}"
    )
    log.info(metrics)

    time_total = time_images + time_gc
    log.info(f"time_total: {time_total:.3f}")
    log.info("-------------------------------------------------------------------")

    return metrics, time_total


def evalModels(eval_func, models, threshs, loader, device, class_idx=1):
    num_batches = 0
    thresh_prod = np.array(list(product(*([threshs] * len(models)))))
    metrics = [Metrics() for i in range(thresh_prod.shape[0])]
    times = np.zeros(len(metrics))
    print(thresh_prod)
    print(thresh_prod.shape)

    # batch_idxs = np.random.choice(len(loader), max_batches, replace=False)

    print("Loading data ...")
    # for data in loader:
    for i, data in enumerate(loader, 0):
        # if i not in batch_idxs:
        #     print(f'Skipping batch {i+1}')
        #     continue
        print(f"Processing batch {i+1}, {num_batches}/{max_batches} ...")
        inputs = [data[j].to(device) for j in range(len(data) - 1)]
        labels = data[-1].to(device)
        #        print('labels:', labels.shape)

        for i in range(thresh_prod.shape[0]):
            m, t = eval_func(models, thresh_prod[i], inputs, labels, class_idx)
            metrics[i].concat(m)
            times[i] += t
        #            print(f'({i+1}/{all_threshs.shape[1]}) threshs: {all_threshs[:,i]},'\
        #                  + f'{metrics[i]}')

        num_batches += 1
        print(f"Done with batch {num_batches}/{max_batches}")
        if num_batches == max_batches:
            break

    # print('times:\n', times)
    #    print('metrics:\n', [m.toArray() for m in metrics])
    #    print('metrics:\n', [str(m) for m in metrics])

    return metrics, times


if len(sys.argv) < 3:
    print("Error: not enough input arguments!")
    print("Usage: python time_precision_data.py HDF5_FILE MODEL_1 ... MODEL_N")
    exit(-1)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# Load the dataset.
print("Loading dataset ...")
data_path = sys.argv[1]
dataset = MatrixDataset(data_path)
loader = torchdata.DataLoader(dataset, **loader_params)

model = MultiBranchNetWithSigmoid

# Setup the networks.
model_paths = [sys.argv[i] for i in range(2, len(sys.argv))]
eval_func = None
if len(model_paths) == 1:
    gc = createPretrainedNet(model, net_params, 2, model_paths[0])
    models = [gc]
    eval_func = evalAtThresholdsGc
elif len(model_paths) == 2:
    cls = createPretrainedNet(net_params, 2, model_paths[0])
    gc = createPretrainedNet(net_params, 2, model_paths[1])
    models = [cls, gc]
    eval_func = evalAtThresholdsCls
elif len(model_paths) == 3:
    cls = createPretrainedNet(net_params, 2, model_paths[0])
    print(dataset.labels.shape[1])
    rot = createPretrainedNet(net_params, dataset.labels.shape[1], model_paths[1])
    gc = createPretrainedNet(net_params, 2, model_paths[2])
    models = [cls, rot, gc]
    eval_func = evalAtThresholdsClsAndRot
else:
    print("Error: wrong input arguments")
    exit(-1)
for m in models:
    print(m)

# Create the thresholds at which each model is evaluated.
threshs = np.linspace(0.0, 1.0, num_threshs + 1)
threshs_end = np.linspace(0.9, 1.0, num_threshs + 1)
threshs_begin = np.linspace(0.0, 0.1, num_threshs + 1)
threshs = np.hstack((threshs_begin, threshs[2:-2], threshs_end))
print("thresholds:", threshs)

# Evaluate the sequence of models on the dataset.
with torch.no_grad():
    metrics, times = evalModels(eval_func, models, threshs, loader, device)

# Store the results (quality and runtime).
now = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
fig_dir = "/home/andreas/Pictures/rpn_gpd2/ma_complete/" + now + "/"
os.mkdir(fig_dir)
fname = ".png"
recalls = np.array([m.recall() for m in metrics])
precisions = np.array([m.precision() for m in metrics])
times = np.array(times)
pred_pos = np.array([m.tp + m.fp for m in metrics])
pred_pos_sec = pred_pos / times
mask = np.isnan(pred_pos_sec) | np.isinf(pred_pos_sec)
print("#invalid:", mask.sum())
pred_pos_sec[mask] = 0
print(f"Top-5 recalls: {np.sort(recalls)[-5:]}, precisions: {np.sort(precisions)[-5:]}")

print(times[:5])
print(pred_pos[:5])
print(pred_pos_sec[:5])

np.save(fig_dir + "times.npy", times)
np.save(fig_dir + "recalls.npy", recalls)
np.save(fig_dir + "precisions.npy", precisions)
np.save(fig_dir + "pred_pos_secs.npy", pred_pos_sec)
print("Results stored at:", fig_dir)

# Concatenates CSV files from two directories and stores the result in a third
# directory.

import os
import sys

import numpy as np
from tabulate import tabulate

dir1 = sys.argv[1]
dir2 = sys.argv[2]
dir3 = sys.argv[3]
avg = {}

for f in os.listdir(dir1):
    if f.endswith(".csv"):
        f1 = dir1 + f
        f2 = dir2 + f
        x1 = np.loadtxt(f1, delimiter=",", skiprows=1)
        x2 = np.loadtxt(f2, delimiter=",", skiprows=1)
        y = np.vstack((x1, x2))
        print("Concatenated: %s and %s" % (f1, f2))
        print(" Resulting shape:", y.shape)
        np.savetxt(
            dir3 + f, y, fmt="%.3f", delimiter=",", header="Accuracy, Precision, Recall"
        )
        avg[f[: f.rfind("_")]] = np.mean(y, 0)

print(avg)
y = np.array([avg["CLS"], avg["ROT"], avg["GPD"], avg["FINAL"]])
print(y)
np.savetxt(
    dir3 + "avgs.csv",
    y,
    fmt="%.3f",
    delimiter=",",
    header="Accuracy, Precision, Recall",
)
print(tabulate(y, tablefmt="latex", floatfmt=".2f"))

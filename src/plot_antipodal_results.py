"""Plot antipodal results for a given file or directory.

Run python plot_antipodal_results.py --help for detailed usage instructions.

"""

import os
import sys
from collections import OrderedDict
from copy import deepcopy
from dataclasses import dataclass, field
from datetime import datetime
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from plot_precision_recall_qd_vs_gpd import paretoFrontier
from plot_simulation_results import plotScatter, readCategories, setLimitsLabels

np.set_printoptions(suppress=True, precision=3)

labels = {
    "qd_gc": "QD: GC",
    "qd_rot": "QD: ROT",
    "qd_rot_gc": "QD",
    "lenet": "LENET",
    "resnet18": "RESNET18",
    "resnet34": "RESNET34",
    "qd_rot_gc_coll": "QD-COLL",
    "qd_coll": "QD-COLL",
    "qd_geom_coll": "QD-GCOL",
    "qd_learn_coll": "QD-LCOL",
    "qd_clut": "QD-CLUT",
    "gpd": "GPD",
    "gpd_gc": "GPD: GC",
    "graspnet": "Graspnet",
}
labels["rot_gc"] = labels["qd_rot_gc"]
labels["gc"] = labels["qd_gc"]
labels["rot"] = labels["qd_rot"]
# labels["qd_resnet18"] = labels["qd_rot_gc_resnet18"]
# labels["qd_resnet34"] = labels["qd_rot_gc_resnet34"]


@dataclass
class ThresholdData:
    thresholds: np.array
    tp: np.array
    fp: np.array
    tn: np.array
    fn: np.array
    proposals: np.array

    @classmethod
    def fromFile(cls, path):
        arr = np.load(path)
        filename = path[path.rfind("/") + 1 :]
        return cls.fromArray(arr, filename)

    @classmethod
    def fromArray(cls, arr, method):
        if arr.ndim == 3 and arr.shape[0] == 1:
            arr = np.squeeze(arr, axis=0)
        thresholds = arr[:, :-5]
        if thresholds.ndim == 2:
            thresholds = thresholds.flatten()
        tp = arr[:, -5]
        fp = arr[:, -4]
        tn = arr[:, -3]
        fn = arr[:, -2]
        proposals = arr[:, -1]
        data = ThresholdData(thresholds, tp, fp, tn, fn, proposals)
        data.detectionsPerSecond(method)
        return data

    def precision(self):
        return self.tp / (self.tp + self.fp)

    def recall(self):
        return self.tp / (self.tp + self.fn)

    def detectionsPerSecond(self, method):
        # These values are calculated ../logs/qd774/simple_antipodal_prop_data.txt.
        times = {
            "prop_image": 0.00016,
            "grasp_image": 0.00117,  # 4channels
            # "grasp_image": 0.00820,  # 12channels
            "lenet": 0.0001,
            "resnet18": 0.0002,
            "resnet34": 0.0004,
            "graspnet": 0.0115,
        }

        networks = ["lenet", "resnet18", "resnet34"]
        network = [x for x in networks if x in method]
        if len(network) > 0:
            network = network[0]
        else:
            network = "lenet"

        time_prop = 0.0
        time_grasp = 0.0

        # Calculate the time needed to generate proposals.
        if "rot" in method:
            num_prop_images = 100  # per viewpoint
            # time_prop_images = num_prop_images * times["prop_image"]
            time_prop = num_prop_images * (times["prop_image"] + times[network])
        elif "gpd" in method and "gc" not in method:
            time_prop = 0.4  # for 100 samples using proposal strategy

        # Calculate the time needed to generate grasps.
        if "gc" in method or "gpd" in method:
            num_grasp_images = self.proposals
            # time_grasp_images = num_grasp_images * times["grasp_image"]
            time_grasp = num_grasp_images * (times["grasp_image"] + times[network])
            # if "gc" in method and "rot" not in method:
            # print("time_grasp:", time_grasp)
            # print("num_grasp_images:", num_grasp_images)
            # print("method:", method)
            # exit()
        elif "graspnet" in method:
            time_grasp = self.proposals * times["graspnet"]

        total_time = time_prop + time_grasp
        if type(total_time) == float:
            total_time = np.ones(len(self.tp), float) * total_time

        detections = self.tp + self.fp

        # Cases of full recall.
        sums = self.tp + self.fp + self.tn + self.fn
        mask = sums == 0
        detections[mask] = np.max(sums)

        self.detections_per_second = detections / total_time

        # print(f"\nMETHOD: {method}")
        # print(total_time, time_prop, time_grasp)
        # print(type(total_time), total_time.shape)
        # print(self.detections_per_second)
        # print(f"max #grasp_images: {np.max(num_grasp_images)}")
        # print(f"precision: {(self.precision()[0], self.precision()[-1])}")
        # print(f"recall: {(self.recall()[0], self.recall()[-1])}")
        # print(
        # f"detections: {(detections[0], detections[-1])}, min,max: {(np.min(detections), np.max(detections))}")
        # print(f"precision: {self.precision()}")

        # print("max(sums):", np.max(sums))
        # print(
        # f"method: {method}, detections(min,max): {np.min(detections), np.max(detections)}")
        # print("DPS:", self.detections_per_second)
        # print("~DPS:", nondetections)
        # if "gc" in method:
        # input("?")
        # exit()

        # print(time_prop_images, time_grasp_images.shape)
        """
        print("thresholds:", self.thresholds)
        print("tp:", self.tp)
        print("fp:", self.fp)
        print("tn:", self.tn)
        print("fn:", self.fn)
        print("num_grasp_images:", num_grasp_images)
        print("time_grasp_images:", time_grasp_images)
        print("total_time:", total_time)
        print("detections:", detections)
        print("precision:", self.precision())
        print("detections_per_second:", self.detections_per_second)
        input("?")
        """


def loadInstanceResults(path: str) -> list:
    arr = np.load(path)
    filename = path[path.rfind("/") + 1 :]
    l = [ThresholdData.fromArray(row, filename) for row in arr]
    return l


def nullifyNans(arr):
    arr_out = deepcopy(arr)
    arr_out[np.isnan(arr_out)] = 0.0
    return arr_out


def plotFile(path: str) -> None:
    """Plot the results for a given file.

    Args:
        path (str): the path to the file.
    """
    data = ThresholdData.fromFile(path)
    recall = data.recall()
    precision = data.precision()
    mask = (~(np.isnan(recall))) | (~(np.isnan(precision)))
    recall = recall[mask]
    precision = precision[mask]
    plotScatter(data.recall(), data.precision(), "x", "recall", "precision")
    filename = path[path.rfind("/") + 1 : path.rfind(".")]
    fpath = f"/tmp/{filename}_{datetime.now()}.png"
    plt.savefig(fpath)
    print(f"Stored plot at: {fpath}")


def readDirectory(path: str, category: str = "") -> list:
    """Read the results from a given directory.

    Args:
        path (str): the path to the directory.
        category (str, optional): the object category.
    """
    files = sorted(glob(os.path.join(path) + f"*{category}*.npy"))
    print(f"Loaded {len(files)} files for category {category} from {path}")

    # Remove files for which the filename contains the category as a suffix.
    if len(category) > 0 and not "graspnet" in files[0]:
        filenames = [f[f.rfind("/") + 1 :] for f in files]
        filenames = [f[f.find("_") + 1 :] for f in filenames]
        filenames = [f[f.find("_") + 1 :] for f in filenames]
        files = [f for f, n in zip(files, filenames) if n.startswith(category)]
    print(f"Loaded {len(files)} files for category {category}")
    print("first file:", files[0])

    data = [ThresholdData.fromFile(f) for f in files]

    return data


def plotDirectories(path: str, category: str = "") -> None:
    """Plot the results for a given directory that contains only other directories.

    Args:
        path (str): the path to the directory.
        category (str, optional): the object category.
    """
    dirs = [os.path.join(path, f) for f in os.listdir(path)]
    dirs = sorted(dirs)
    print(dirs)

    data = {
        k[k.rfind("/") + 1 :]: readDirectory(k + "/", category=category) for k in dirs
    }
    print("Loaded results for these methods:", data.keys())
    # plotScatter(data["graspnet"][0].recall(), data["graspnet"][0].precision())

    metrics, avgs, pareto_pr, pareto_pdps = {}, {}, {}, {}
    for k in data:
        metrics[k] = {
            "recall": [nullifyNans(x.recall()) for x in data[k]],
            "precision": [nullifyNans(x.precision()) for x in data[k]],
            # "detections": [nullifyNans(x.detections) for x in data[k]],
            "detectionsPerSecond": [x.detections_per_second for x in data[k]],
        }
        avgs[k] = {l: np.mean(v, axis=0) for l, v in metrics[k].items()}
        pareto_pr[k] = paretoFrontier(
            np.vstack((avgs[k]["recall"], avgs[k]["precision"]))
        )
        idxs = np.argsort(pareto_pr[k][0], axis=0)
        pareto_pr[k] = pareto_pr[k][:, idxs]
        pareto_pdps[k] = paretoFrontier(
            np.vstack((avgs[k]["detectionsPerSecond"], avgs[k]["precision"]))
        )
        idxs = np.argsort(pareto_pdps[k][0], axis=0)
        pareto_pdps[k] = pareto_pdps[k][:, idxs]

    markers = ["x", ".", "o", "*", "+", "-"]
    markersizes = [12, 8, 6, 6, 4, 4]
    plt.figure()
    for i, k in enumerate(sorted(avgs, reverse=True)):
        print(k)
        v = avgs[k]
        plt.plot(
            v["detectionsPerSecond"],
            v["precision"],
            markers[i],
            markersize=markersizes[i],
            label=k,
        )
    plt.xlabel("detections per second")
    plt.ylabel("precision")
    plt.ylim([-0.05, 1.05])
    plt.xscale("log")
    plt.legend()
    plt.show()

    plt.figure()
    for k, v in avgs.items():
        print(k, v["precision"].shape, v["recall"].shape)
        print(k, v["precision"], v["recall"])
        plt.plot(v["recall"], v["precision"], "x", label=k)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()

    plt.figure()
    for i, (k, v) in enumerate(avgs.items()):
        plt.plot(
            v["recall"], v["precision"], markers[i], markersize=markersizes[i], label=k
        )
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()

    plt.figure()
    for k, v in pareto_pr.items():
        plt.plot(v[0], v[1], "x", label=k)
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()

    # Order the dictionary by a custom ordination.
    ordering = [
        "QD-CLUT",
        "QD-COLL",
        "QD-LCOL",
        "QD-GCOL",
        "LENET",
        "RESNET34",
        "RESNET18",
        "QD",
        "QD: GC",
        "QD: ROT",
        "GPD",
        "GPD: GC",
        "Graspnet",
    ]
    pareto_pr = dict([(labels[k], pareto_pr[k]) for k in pareto_pr])
    ordering = [x for x in ordering if x in pareto_pr]
    pareto_pr = OrderedDict([(k, pareto_pr[k]) for k in ordering])
    pareto_pdps = dict([(labels[k], pareto_pdps[k]) for k in pareto_pdps])
    pareto_pdps = OrderedDict([(k, pareto_pdps[k]) for k in ordering])

    # Plot the Pareto frontiers.
    plt.figure()
    for i, (k, v) in enumerate(pareto_pr.items()):
        plt.plot(v[0], v[1], label=k)
        # plt.plot(v[0], v[1], styles[i], label=labels[k])
    setLimitsLabels(xlabel="recall", ylabel="precision")
    plt.legend()
    plt.show()

    # Plot precision against detections per second.
    plt.figure()
    for k, v in pareto_pdps.items():
        plt.plot(v[0], v[1], label=k)
    plt.xlabel("detections per second")
    plt.ylabel("precision")
    plt.ylim([-0.05, 1.05])
    plt.xscale("log")
    plt.legend()
    plt.show()


def plotDirectory(path: str) -> None:
    """Plot the results for a given directory.

    Args:
        path (str): the path to the directory.
    """
    data = readDirectory(path)
    metrics = {
        "recall": [nullifyNans(x.recall()) for x in data],
        "precision": [nullifyNans(x.precision()) for x in data],
        # "detections": [nullifyNans(x.detections) for x in data],
        "detectionsPerSecond": [x.detections_per_second for x in data],
    }
    # plotScatter(metrics["recall"][0], metrics["precision"][0], "x")

    avg = {k: np.mean(v, axis=0) for k, v in metrics.items()}
    fpath = f"/tmp/precision_recall_avg_{datetime.now()}.png"
    plotScatter(
        avg["detectionsPerSecond"],
        avg["precision"],
        "x",
        xlabel="detections per second",
        ylabel="precision",
        xlim=[-0.05, 500000],
        fpath=fpath,
    )
    plotScatter(avg["recall"], avg["precision"], "x", fpath=fpath)
    print("Stored average plot at:", fpath)

    # Plot results category-wise.
    categories = readCategories(path)
    plt.figure()
    for c in categories:
        data = readDirectory(path, c)
        metrics = {
            "recall": [nullifyNans(x.recall()) for x in data],
            "precision": [nullifyNans(x.precision()) for x in data],
            # "detections": [nullifyNans(x.detections) for x in data],
        }
        avg = {k: np.mean(v, axis=0) for k, v in metrics.items()}
        plt.plot(avg["recall"], avg["precision"], "x", label=c)
    plt.legend()
    setLimitsLabels(xlabel="recall", ylabel="precision")
    fpath = f"/tmp/precision_recall_category_{datetime.now()}.png"
    plt.savefig(fpath)
    print("Stored category plot at:", fpath)
    plt.show()


def main() -> None:
    if len(sys.argv) < 2:
        print("Error: not enough input arguments.")
        print("Usage: python plot_antipodal_results.py PATH [CATEGORY]")
        sys.exit(-1)
    path = sys.argv[1]

    if len(sys.argv) == 3:
        category = sys.argv[2]
    else:
        category = ""

    if os.path.isdir(path):
        is_directory = [os.path.isdir(os.path.join(path, f)) for f in os.listdir(path)]
        if np.all(np.array(is_directory)):
            plotDirectories(path, category)
        else:
            plotDirectory(path)
    elif os.path.isfile(path):
        plotFile(path)


if __name__ == "__main__":
    main()

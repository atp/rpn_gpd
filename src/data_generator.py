"""Module for data generators.

A data generator creates data for an object set by iteration over a number of object
categories and iteration over a number of object instances for each category. For each
instance, a number of point clouds is generated from uniformly sampled viewpoints on a
sphere. For each point cloud, grasp data (images, labels, etc) is generated using a
GraspDataGenerator.

"""
import os
import sys
from copy import deepcopy
from datetime import datetime
from functools import partial

import numpy as np
import open3d as o3d
import pyrender
from scipy.spatial.transform import Rotation as Rot

from proposal import downsampleByDistance, sampleFromCube
from scene import ClutterScene, SingleObjectScene, calcLookAtPose, randomRotation3D, randomStableRotation3D
from utils.cloud_utils import numpyToCloud, replaceNormals
from utils.hands_plot import HandsPlot

draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)
origin = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)


class DataGenerator:
    """Class to generate data for an object set."""

    def __init__(self, object_set, train_db, test_db, grasp_data_generator, params):
        """Construct a data generator.

        Args:
            object_set (ObjectSet): the object set.
            train_db (Database): the training database.
            test_db (Database): the test database.
            grasp_data_generator (GraspDataGenerator): the grasp data generator.
            params (dict): parameters.
        """
        self.object_set = object_set
        self.train_db = train_db
        self.test_db = test_db
        self.grasp_data_generator = grasp_data_generator
        self.num_instances = params["num_instances"]
        self.num_train_views = params["num_train_views"]
        self.num_test_views = params["num_test_views"]
        self.num_samples = params["num_samples"]
        self.sphere_radius = params["camera_distance"]
        self.min_extent = params["min_object_extent"]
        self.max_extent = params["max_object_extent"]
        self.cube_size = params["cube_size"]
        self.add_plane = params["add_plane"]
        self.rotate_in_plane = params["rotate_in_plane"]
        self.create_clutter = params["create_clutter"]
        if len(params["database"]) > 0:
            self.grasp_data_generator.setLabelsDatabase(params["database"])
        self.min_cloud_size = 50
        self.use_two_views = params["use_two_views"]
        self.is_visualizing = False  # used for debugging purposes
        self.store_clouds = params["store_clouds"]
        self.out_dir = params["out_dir"]

    def createCategoryData(self, category_idx):
        """Create data for an object category.

        Args:
            category_idx (int): the index of the object category.

        Returns:
            tuple of np.arrays: the training and test viewpoints.
        """
        plane_z = -1.0
        info = []
        category = self.object_set.objects[category_idx]

        for i in range(self.num_instances):
            # 1. Create a scene.
            scene = self.createScene(category_idx, i)

            # 2. Get a complete observation of the scene.
            mesh_cloud, views_cloud = scene.createCompletePointCloud(
                add_plane=self.add_plane
            )
            if self.add_plane:
                plane_z = scene.scene.get_pose(scene.table_node)[2, 3]
                print(f"Added table plane to scene at z = {plane_z:.3f}")
            if self.is_visualizing:
                draw_geoms([mesh_cloud, views_cloud, origin], "multi-view cloud new")

            # Using random camera viewpoints.
            print("Creating point clouds for test viewpoints ...")
            test_clouds, test_poses, test_positions = self.createViewClouds(
                scene, self.num_test_views, self.use_two_views
            )
            print("Creating point clouds for train viewpoints ...")
            train_clouds, train_poses, train_positions = self.createViewClouds(
                scene, self.num_train_views, self.use_two_views, test_positions
            )
            # drawViewpoints(mesh, train_pos, test_pos)
            # o3d.visualization.draw_geometries([train_clouds[0]], "", 640, 480)
            filepath = self.object_set.get([category_idx, i])
            instance = filepath[filepath.rfind("/") + 1 : filepath.rfind(".")]
            print(
                f"[{datetime.now()}] Instance {i+1}/{self.num_instances}: {instance}, "
                + f"category: {self.object_set.objects[category_idx]}"
            )

            print("Creating data for training viewpoints ...")
            traindata = self.createInstanceData(
                train_clouds, mesh_cloud, train_poses, train_positions, plane_z
            )
            self.train_db.addInstanceData(traindata, category, instance)
            if self.store_clouds:
                self.storePointClouds(True, train_clouds, category, instance)

            print("Creating data for test viewpoints ...")
            testdata = self.createInstanceData(
                test_clouds, mesh_cloud, test_poses, test_positions, plane_z
            )
            if self.store_clouds:
                self.storePointClouds(False, test_clouds, category, instance)
            self.test_db.addInstanceData(testdata, category, instance)

            info.append(
                {
                    "category": category,
                    "instance": instance,
                    "scale": np.around(scene.scale, 4),
                    "x_extent": np.around(scene.object_extents[0], 4),
                    "y_extent": np.around(scene.object_extents[1], 4),
                    "z_extent": np.around(scene.object_extents[2], 4),
                }
            )

        return info

    def createScene(self, category_idx: int, instance_idx: int):
        if self.create_clutter:
            return self.createClutterScene()

        return self.createSingleObjectScene(category_idx, instance_idx)

    def createSingleObjectScene(self, category_idx: int, instance_idx: int):
        filepath = self.object_set.get([category_idx, instance_idx])
        scene = SingleObjectScene(filepath, self.min_extent, self.max_extent)
        scene.resetToRandomScale()
        if self.rotate_in_plane:
            # if scene.object_extents
            tf = np.eye(4)
            # tf[:3, :3] = randomRotation3D()
            tf[:3, :3] = randomStableRotation3D()
            scene.setObjectPose(tf)
            print(f"Rotated object by:\n{tf}")

        return scene

    def createClutterScene(self, max_objects_range=[20, 40]):
        sample_extents = np.array([[-0.1, -0.1, 0.0], [0.1, 0.1, 0.01]])

        max_objects = np.random.randint(max_objects_range[0], max_objects_range[1])
        categories = np.random.randint(0, len(self.object_set), max_objects)
        instances = np.random.randint(0, self.num_instances, max_objects)
        print(self.num_instances)
        print("Randomly selected objects:")
        print([(self.object_set.objects[c], i) for c, i in zip(categories, instances)])

        filepaths = [
            self.object_set.get([categories[j], instances[j]])
            for j in range(max_objects)
        ]
        scene = ClutterScene(
            filepaths, self.min_extent, self.max_extent, sample_extents
        )
        scene.reset(rotate_in_plane=self.rotate_in_plane)

        return scene

    def createInstanceData(
        self, clouds, mesh, camera_poses, viewpoints, plane_height=-1.0, min_pos=0.1
    ):
        """Create grasp data for an object instance.

        This function expects a list of point clouds which are observations of the
        object instance from different view points.

        Args:
            clouds (list): the point clouds, one for each view point.
            mesh (o3d.geometry.PointCloud): the point cloud of the object mesh.
            camera_poses (list of np.array): 4 x 4 camera poses.
            plane_height (float, optional): the support plane's height (z-position).
            min_pos (float, optional): the minimum percentage of positives required.

        Returns:
            list of dicts: the grasp data, sample points, and view points for this
                object instance.
        """
        data = []

        for i, cloud in enumerate(clouds):
            print("----------------------------------------------------------")
            print(
                f"Cloud {i+1}/{len(clouds)}: {len(cloud.points)} points, "
                + f"viewpoint: {camera_poses[i][:3,3]}"
            )

            if plane_height == -1.0:
                samples_cloud = cloud
            else:
                samples_cloud = self.calcSamplesCloud(cloud, plane_height)

            # rounds = 1
            # r = 0
            images = np.zeros((0, 60, 60, 3))
            num_rots = self.grasp_data_generator.prop_gen.getNumHandsPerSample()
            labels = np.zeros((0, num_rots, 5))

            # for r in range(rounds):
            # while r < rounds:
            if self.num_samples > 0:
                samples = sampleFromCube(
                    samples_cloud, self.cube_size, self.num_samples
                )
            else:
                samples = deepcopy(np.asarray(samples_cloud.points))
            # print(samples.shape)
            # exit()
            grasps = self.grasp_data_generator.createData(
                mesh, cloud, samples, camera_poses[i]
            )
            # grasps = self.calcGrasps(mesh, cloud, samples_cloud, plane_height)
            labels = grasps["labels"]
            total = np.prod(labels.shape[:-1])
            pos = np.count_nonzero(labels[..., 1])
            neg = np.count_nonzero(labels[..., 1] == 0)
            graspdata = {"images": images, "labels": labels}
            print(
                f"total: {total}, +: {pos}({pos/total:.3f}), "
                + f"-: {neg}({neg/total:.3f})"
            )
            data.append(
                {"graspdata": graspdata, "samples": samples, "viewpoint": viewpoints[i]}
            )

        return data

    def calcGrasps(
        self,
        mesh: o3d.geometry.PointCloud,
        cloud: o3d.geometry.PointCloud,
        samples_cloud: o3d.geometry.PointCloud,
        plane_height=-1.0,
    ):
        print("Sampling from cube ...")
        samples = sampleFromCube(samples_cloud, self.cube_size, self.num_samples)
        if self.is_visualizing:
            p = self.createSamplesPlot(cloud, samples, camera_poses[i], title="Samples")

        print("Evaluating grasp poses ...")
        grasps = self.grasp_data_generator.createData(
            mesh, cloud, samples, camera_poses[i]
        )

        if self.is_visualizing:
            p.show()
            # Plot some grasps.
            self.plotHands(cloud, samples, camera_poses[i], labels, title="Grasp poses")

            # idxs = np.nonzero(labels[:, :, 1] >= 1)[0]
            # valid_cloud = sample_cloud.select_down_sample(idxs)
            # p = HandsPlot(self.grasp_data_generator.prop_gen.hand_geom)
            # p.plotCloud(cloud, [1.0, 0.0, 0.0], opacity=0.5)
            # p.plotCloud(valid_cloud, [0.0, 1.0, 0.0], opacity=0.9)
            # p.setTitle(f"Samples with valid grasp poses")
            # p.show()

        return grasps

    def calcSamplesCloud(self, cloud, plane_height):
        horz_offset = 0.005
        horz_thresh = plane_height + horz_offset
        pts = np.asarray(cloud.points)
        pts = pts[pts[:, 2] > horz_thresh, :]
        samples_cloud = numpyToCloud(pts)
        distr_cloud = downsampleByDistance(samples_cloud, horz_thresh, 2)
        print(f"Sample cloud downsampled to {len(distr_cloud.points)}")
        if len(distr_cloud.points) > self.num_samples:
            # distr_cloud.paint_uniform_color([0,1,0])
            # sample_cloud.paint_uniform_color([1,0,0])
            # draw_geoms([distr_cloud], "distr_cloud")
            samples_cloud = distr_cloud

        return samples_cloud

    def createClouds(self, scene, num, camera_pose, used_rots=[]):
        """Create point clouds for a given number of object orientations.

        The object orientations are uniformly sampled. If `used_rots` is given, the
        sampled view points need to be at least 0.01m away from any point in
        `used_rots`.

        Args:
            scene (Scene): the scene for which to create the point clouds.
            num (int): the number of view points from which the scene is observed.
            camera_pose (np.array): 4x4 the camera pose.
            used_rots (np.array, optional): n x 3 used object orientations.
        """
        min_rot_diff = 0.01
        used_quats = [Rot.from_rotvec(x).as_quat() for x in used_rots]
        clouds, object_rots = [], []
        i = 0
        while i < num:
            R = randomRotation3D()
            q = Rot.from_matrix(R).as_quat()

            # Check that the new orientation is not close to any given orientation.
            if len(used_rots) > 0:
                dist_pos = np.sum(np.abs(used_quats - q), 1)
                dist_neg = np.sum(np.abs(used_quats + q), 1)
                mask = (dist_pos > min_rot_diff) & (dist_neg > min_rot_diff)
            if len(used_rots) == 0 or np.all(mask):
                X = np.eye(4)
                X[:3, :3] = R
                scene.setObjectPose(np.eye(4))
                scene.setObjectPose(X)
                cloud = scene.createProcessedPointCloudFromView(camera_pose)
                if len(cloud.points) > self.min_cloud_size:
                    clouds.append(cloud)
                    object_rots.append(Rot.from_quat(q).as_rotvec())
                    i += 1
                    # print(f"Created point cloud {i}/{num}.")
                    # print("========================================")

        return clouds, np.array(object_rots)

    def createViewClouds(
        self,
        scene,
        num_clouds,
        use_two_views=False,
        used_viewpts=[],
        look_at=np.zeros(3),
        plot=False,
    ):
        """Create point clouds for a given number of view points.

        The view points are uniformly sampled from a sphere. If `used_viewpts` is
        given, the sampled view points need to be at least 0.01m away from any point
        in `used_viewpts`.

        Args:
            scene (Scene): the scene for which to create the point clouds.
            num_clouds (int): the number of clouds to be produced.
            use_two_views (bool) : if true, use two viewpoints, o.w. use one.
            used_viewpts (np.array, optional): n x 3 used view points.
            look_at (np.array, optional): 3 x 1 position at which the camera looks.
            plot (bool, optional): if each point cloud is plotted.
        """
        min_camera_height = 0.05
        clouds, view_poses, view_points_out = [], [], []
        i = 0
        while i < num_clouds:
            look_from = np.random.normal(size=3)
            look_from = look_from / np.linalg.norm(look_from)

            # Reject if viewpoint is too close to any given viewpoint.
            view_points = np.array([look_from * self.sphere_radius])
            if len(used_viewpts) > 0 and np.any(
                np.sum(np.square(used_viewpts - view_points[0]), 1) <= 0.01
            ):
                continue

            # Add an "opposite" viewpoint.
            if use_two_views:
                R = Rot.from_rotvec(np.pi / 2.0 * np.array([0.0, 1.0, 0.0])).as_matrix()
                view_points = np.vstack((view_points, np.dot(R, view_points[0])))

            # If a plane is below the objects, find viewpoints on a half-sphere above.
            if self.add_plane:
                view_points[:, 2] = np.abs(view_points[:, 2])
                if np.any(view_points[:, 2] < min_camera_height):
                    continue

            cloud = o3d.geometry.PointCloud()
            cam_poses = []
            for v in view_points:
                cam_poses.append(calcLookAtPose(v, look_at))
                cloud += scene.createProcessedPointCloudFromView(cam_poses[-1])

            if len(cloud.points) > self.min_cloud_size:
                clouds.append(cloud)
                view_poses.append(cam_poses[0])
                view_points_out.append(view_points[0])
                i += 1
                # print(f"Created point cloud {i}/{num}.\n=========================")

            if plot:
                frames = [
                    o3d.geometry.TriangleMesh.create_coordinate_frame(0.2)
                    for i in range(len(view_points))
                ]
                frames = [f.transform(x) for f, x in zip(frames, cam_poses)]
                cloud.paint_uniform_color([0.0, 0.0, 1.0])
                print("view_points:\n" + str(view_points))
                print([np.linalg.norm(x) for x in view_points])
                draw_geoms([cloud, origin] + frames)

        return clouds, np.array(view_poses), np.array(view_points_out)

    def createSamplesPlot(
        self, cloud, samples, camera_pose, mesh_cloud=None, title="Samples"
    ):
        p = HandsPlot(self.grasp_data_generator.prop_gen.hand_geom)
        p.plotCloud(cloud, [1.0, 0.0, 0.0], opacity=0.5)
        p.plotCloud(numpyToCloud(samples), [0.0, 0.0, 1.0], opacity=0.9)
        if mesh_cloud is not None:
            p.plotCloud(mesh_cloud, [0.5, 0, 0.5], opacity=0.1)
        p.plotAxes(np.eye(4))
        p.plotAxes(camera_pose)
        p.setTitle(title)
        return p

    def plotHands(self, cloud, samples, camera_pose, labels, mesh_cloud=None, title=""):
        tree = o3d.geometry.KDTreeFlann(cloud)
        valid = np.array(np.nonzero(labels[:, :, 1] == 1))
        idxs = {"indices": valid, "frames": []}
        idxsToPoses = self.grasp_data_generator.prop_gen.indicesToPoses
        poses = idxsToPoses(cloud, tree, samples, idxs)
        p = self.createSamplesPlot(cloud, samples, camera_pose, mesh_cloud, title)
        p.plotHands(poses, [0.0, 1.0, 0.0])
        p.show()

    def storePointClouds(self, train: bool, clouds: list, category: str, instance: str):
        # raw_path = f"{self.out_dir}/raw"
        raw_path = os.path.join(self.out_dir, "raw")
        if train:
            sub_path = f"{raw_path}/train"
        else:
            sub_path = f"{raw_path}/test"
        if not os.path.isdir(raw_path):
            os.mkdir(raw_path)
        if not os.path.isdir(sub_path):
            os.mkdir(sub_path)

        for i, cloud in enumerate(clouds):
            path = f"{sub_path}/{category}_{instance}_{i}.pcd"
            o3d.io.write_point_cloud(path, cloud)
            print(f"({i}) Stored cloud at {path}")


def drawViewpoints(cloud, train_pos, test_pos):
    """Visualize the view points for a point cloud.

    Args:
        cloud (o3d.geometry.PointCloud): the point cloud.
        train_pos (np.array): n x 3 train view points.
        test_pos (np.array): m x 3 test view points.
    """
    train_views = numpyToCloud(train_pos)
    test_views = numpyToCloud(test_pos)
    train_views.paint_uniform_color([0.0, 1.0, 0.0])
    test_views.paint_uniform_color([1.0, 0.0, 0.0])
    o3d.visualization.draw_geometries(
        [cloud, train_views, test_views], width=640, height=480
    )

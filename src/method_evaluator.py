"""Module to evaluate grasp detectors.

A method evaluator takes the outputs of a grasp detector and evaluates the performance
of the detector by calculating a confusion matrix for a set of thresholds.
"""

from abc import ABC, abstractmethod

import numpy as np
import torch as tp

from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from learning import Metrics


class MethodEvaluator(ABC):
    """Abstract base class for method evaluators."""

    def __init__(self, threshs):
        self.threshs = threshs
        print("Thresholds:", threshs)

    @abstractmethod
    def evalMethod(self, grasp_data, labels, is_free=None):
        """Evaluate a grasp detector.

        Args:
            grasp_data (dict): grasp data.
            labels (np.array): ground truth labels.
            is_free (np.array, optional): partial-view collision checks.

        Returns:
            np.array: confusion matrices for each threshold.
        """
        raise NotImplementedError("This is an abstract method.")


class GpdEvaluator(MethodEvaluator):
    """Method evaluator for GPD."""

    def __init__(self, thresh_step):
        """Construct a method evaluator for GPD.

        Args:
            thresh_step (float): the threshold step size.
        """
        threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[:-1]
        super().__init__(threshs[::-1])

    def evalMethod(self, grasp_data, labels, is_free=None):
        """Evaluate the grasps produced by GPD at different thresholds.

        Args:
            grasp_data (dict): dictionary with proposals and classifier scores.
            labels (np.array): the ground truth labels.

        Returns:
            np.array: confusion matrices for each threshold.
        """
        scores = grasp_data["scores"].cpu().numpy()
        idxs = np.nonzero(grasp_data["proposals"] == 0)
        scores[idxs[0], idxs[1]] = 0
        labels = labels.flatten()
        scores = scores.flatten()
        results = evalClassifier(labels, scores, self.threshs)
        num_proposals = np.count_nonzero(grasp_data["proposals"])
        stats = [
            [t, r.tp, r.fp, r.tn, r.fn, num_proposals]
            for t, r in zip(self.threshs, results)
        ]
        return np.array(stats)


class GpdGcEvaluator(MethodEvaluator):
    """Method evaluator for GPD-GC."""

    def __init__(self, thresh_step):
        """Construct a method evaluator for GPD-GC.

        Args:
            thresh_step (float): the threshold step size.
        """
        threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[:-1]
        super().__init__(threshs[::-1])

    def evalMethod(self, grasp_data, labels, is_free=None):
        """Evaluate the grasps produced by GPD-GC at different thresholds.

        Args:
            grasp_data (dict): dictionary with proposals and classifier scores.
            labels (np.array): the ground truth labels.

        Returns:
            np.array: confusion matrices for each threshold.
        """
        scores = grasp_data["scores"].cpu().numpy()
        labels = labels.flatten()
        scores = scores.flatten()
        results = evalClassifier(labels, scores, self.threshs)
        num_proposals = len(scores)
        stats = [
            [t, r.tp, r.fp, r.tn, r.fn, num_proposals]
            for t, r in zip(self.threshs, results)
        ]
        return np.array(stats)


class RotGcEvaluator(MethodEvaluator):
    """Method evaluator for ROT-GC."""

    def __init__(self, thresh_step):
        """Construct a method evaluator for MA.

        Args:
            thresh_step (float): the threshold step size.
        """
        threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[:-1]
        super().__init__(threshs[::-1])

    def evalMethod(self, grasp_data, labels, is_free=None):
        """Evaluate the grasps produced by MA at different thresholds.

        Args:
            grasp_data (dict): dictionary with rot, and gc classifier scores.
            labels (np.array): the ground truth labels.

        Returns:
            np.array: confusion matrix for each threshold.
        """
        stats = []
        labels = labels.flatten()
        rot_outs = grasp_data["scores"]["rot"].cpu().numpy().flatten()
        gc_outs = grasp_data["scores"]["gc"].cpu().numpy().flatten()
        rot_masks = [rot_outs >= t for t in self.threshs]
        gc_masks = [gc_outs >= t for t in self.threshs]

        if is_free is None:
            is_free = np.ones(labels.shape, dtype=bool)
        else:
            is_free = is_free.flatten()
            print("Taking collisions into account ...")
            # print("is_free:", is_free.shape, is_free.dtype)

        results = [
            [Metrics(labels, r & g & is_free) for g in gc_masks] for r in rot_masks
        ]
        confmats = [[x.confmat() for x in sub] for sub in results]
        confmats = np.array(confmats).reshape((-1, len(confmats[0][0])))
        n = len(self.threshs)
        thresh_pairs = np.vstack((np.repeat(self.threshs, n), np.tile(self.threshs, n)))
        num_props = np.array([np.count_nonzero(m) for m in rot_masks])
        num_props = np.repeat(num_props, n)
        # print("num_props:\n" + str(num_props))
        stats = np.hstack((thresh_pairs.T, confmats, num_props[..., np.newaxis]))

        # for debugging
        # pre_rec = [[[x.precision(), x.recall()] for x in sub] for sub in results]
        # pre_rec = np.array(pre_rec).reshape((-1, len(pre_rec[0][0])))
        # ext_stats = np.hstack((stats, pre_rec))
        # print(f"stats: {stats.shape}")
        # print(f"stats:\n{stats}")
        # print(f"ext_stats:\n{ext_stats}")
        # exit()

        return stats


class GcEvaluator(MethodEvaluator):
    """Method evaluator for GC."""

    def __init__(self, thresh_step):
        threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)[:-1]
        super().__init__(threshs[::-1])

    def evalMethod(self, grasp_data, labels, is_free=None):
        labels = labels.flatten()
        if "gc" in grasp_data["scores"]:
            scores = grasp_data["scores"]["gc"]
        elif "rot" in grasp_data["scores"]:
            scores = grasp_data["scores"]["rot"]
        masks = [scores.flatten() >= t for t in self.threshs]
        results = [Metrics(labels, m) for m in masks]
        confmats = [x.confmat() for x in results]
        confmats = np.array(confmats)
        num_props = np.ones(len(self.threshs)) * np.prod(labels.shape)
        stats = np.hstack(
            (self.threshs[..., np.newaxis], confmats, num_props[..., np.newaxis])
        )

        # for debugging
        # pre_rec = [[x.precision(), x.recall()] for x in results]
        # pre_rec = np.array(pre_rec).reshape((-1, len(pre_rec[0])))
        # ext_stats = np.hstack((stats, pre_rec))
        # print(f"stats: {stats.shape}")
        # print(f"stats:\n{stats}")
        # print(f"ext_stats:\n{ext_stats}")
        # exit()

        return stats


def evalClsAndRotAndGc(labels, scores, threshs, stops_at_full_recall=True):
    """Evaluate a chain of classifiers, CLS and ROT, at different thresholds.

    Args:
        labels (np.array): n x m the ground truth labels.
        outputs (dict): the scores produced by CLS and ROT.
        threshs (np.array): 1 x k the thresholds at which to evaluate the classifiers.

    Returns:
        list: metrics object for each threshold.
    """
    num_classes = labels.shape[1]
    cls_masks = [(scores["cls"] >= t) for t in threshs]
    rot_masks = [(scores["rot"] >= t) for t in threshs]
    counts = [np.count_nonzero(m) for m in masks]
    shapes = [(c * num_classes,) for c in counts]
    metrics = {}

    for i, c in enumerate(cls_masks):
        for j, r in enumerate(rot_masks):
            key = f"{threshs[i],threshs[j]}"
            # TODO
            # metrics[key]
            metrics.update(
                {
                    f"{t:.3f}": evalClassifier(
                        labels[m].reshape(shapes[i]),
                        scores["rot"][m].reshape(shapes[i]),
                        threshs,
                        stops_at_full_recall,
                    )
                    for i, (t, m) in enumerate(zip(threshs, masks))
                }
            )

    return metrics


def evalClsAndRot(labels, scores, threshs, stops_at_full_recall=True):
    """Evaluate a chain of classifiers, CLS and ROT, at different thresholds.

    Args:
        labels (np.array): n x m the ground truth labels.
        outputs (dict): the scores produced by CLS and ROT.
        threshs (np.array): 1 x k the thresholds at which to evaluate the classifiers.

    Returns:
        list: metrics object for each threshold.
    """
    num_classes = labels.shape[1]
    masks = [(scores["cls"] >= t) for t in threshs]
    counts = [np.count_nonzero(m) for m in masks]
    shapes = [(c * num_classes,) for c in counts]

    metrics = {
        f"{t:.3f}": evalClassifier(
            labels[m].reshape(shapes[i]),
            scores["rot"][m].reshape(shapes[i]),
            threshs,
            stops_at_full_recall,
        )
        for i, (t, m) in enumerate(zip(threshs, masks))
    }

    return metrics


def evalClassifier(labels, outputs, threshs, stops_at_full_recall=True):
    """Evaluate a classifier at different thresholds.


    Args:
        labels (np.array): 1 x n the ground truth labels.
        outputs (np.array): 1 x n the classifier scores.
        threshs (np.array): 1 x k the thresholds at which to evaluate.

    Returns:
        list: metrics object for each threshold.
    """
    results = [Metrics() for i in range(threshs.shape[0])]

    for i in range(threshs.shape[0]):
        mask = outputs >= threshs[i]
        pred = np.zeros(len(outputs), dtype=np.int)
        pred[mask] = 1
        results[i] = Metrics(labels, pred)
        # print(f"  thresh: {threshs[i]:.3f}; {results[i]}")

        # Stop when full recall has been attained.
        if stops_at_full_recall and results[i].recall() == 1.0:
            break

    return results


def runModel(model, loader, device, num_classes):
    """Run a neural network model on given inputs.

    Args:
        model (?): the neural network model.
        loader (tp.data.DataLoader): the inputs.
        num_classes (int, optional): the number of classes.

    Returns:
        tp.tensor: the outputs of the neural network.
    """
    if num_classes == 2:
        outputs = tp.empty((0,))
    else:
        outputs = tp.empty((0, num_classes))

    with tp.no_grad():
        for data in loader:
            inputs = data[0].to(device)
            if num_classes == 2:
                out = model(inputs)[:, 1]
            else:
                out = model(inputs)
            outputs = tp.cat((outputs, out.cpu()))

    outputs = tp.sigmoid(outputs)

    return outputs

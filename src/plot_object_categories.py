"""Visualize the 3DNet object categories used during training.

Usage: python plot_object_categories.py OBJECTS_DIR CATEGORY_INFO_CSV SCALE STEP

For each row in CATEGORY_INFO_CSV, visualize the corresponding object by rendering its 
mesh. If SCALE is set to 1, each mesh is scaled by the factor stored in the CSV file. 
The argument STEP determines the step size by which to go through the rows of the CSV 
file, e.g., STEP=1 means that each object contained in the file is visualized.

"""

import csv
import os
import sys
from argparse import ArgumentParser
from time import time

import numpy as np
import open3d as o3d
import trimesh
from scipy.spatial.transform import Rotation as Rot

from hand_params import hand_params
from hands.hand_eval import HandGeometry
from utils.hands_plot import HandsPlot

np.set_printoptions(precision=4)


def custom_draw_geometry_with_custom_fov(title, pcd, fov_step):
    vis = o3d.visualization.Visualizer()
    vis.create_window(title, width=640, height=480)
    vis.add_geometry(pcd)
    ctr = vis.get_view_control()
    print("Field of view (before changing) %.2f" % ctr.get_field_of_view())
    ctr.change_field_of_view(step=fov_step)
    print("Field of view (after changing) %.2f" % ctr.get_field_of_view())
    vis.run()
    vis.destroy_window()


def parseArgs():
    parser = ArgumentParser(description="Create images and labels for learning")
    parser.add_argument(
        "objects_dir", metavar="OBJECTS_DIR", help="path to object set",
    )
    parser.add_argument(
        "--scale", type=float, default=1.0, help="object scaling factor",
    )
    parser.add_argument(
        "--step", type=int, default=1, help="plot every <step>-th object",
    )
    parser.add_argument(
        "--csv", default="", help="path to CSV file with object infos",
    )

    args = parser.parse_args()

    return args


args = parseArgs()
has_csv = len(args.csv) > 0

if has_csv:
    with open(args.csv, "r") as f:
        objects = [r for r in csv.reader(f, delimiter=",") if r[0] != "category"]
else:
    objects = [f for f in os.listdir(args.objects_dir)]

images = []
clouds = []
step = args.step
extents_list = []

for i in range(0, len(objects), step):
    if type(objects[i]) == list:
        o = objects[i]
        filepath = args.objects_dir + o[0] + "/" + o[1] + ".ply"
    else:
        o = objects[i]
        filepath = args.objects_dir + o
    print(f"({int(i/step)}/{int(len(objects)/step)}) {o}")

    # 1. Load mesh from file.
    print("Loaded mesh from:", filepath)
    mesh = o3d.io.read_triangle_mesh(filepath)
    extents_list.append(mesh.get_max_bound() - mesh.get_min_bound())
    pts = np.asarray(mesh.sample_points_uniformly(1000).points)
    # pts = np.asarray(mesh.sample_points_poisson_disk(1000).points)
    extents = np.max(pts, 0) - np.min(pts, 0)
    print(f"Original extents: {extents}")
    max_extent = np.max(extents)
    if max_extent > 1.0:
        print(f"Mesh has invalid dimensions. Scaling with factor {1.0/max_extent} ...")
        for j in range(len(mesh.vertices)):
            mesh.vertices[j] *= 1.0 / max_extent

    # pcd = mesh.sample_points_poisson_disk(10000)
    #
    # pts = np.asarray(pcd.points)
    # extents = np.max(pts, 0) - np.min(pts, 0)
    # print(f'initial extents: {extents}')

    # 2. Scale mesh by given factor.
    time_scaling = time()
    if args.scale == -1.0:
        # scale = float(objects[i][2])
        # scale = 0.2
        dim_x = 0.1
        extents = mesh.get_max_bound() - mesh.get_min_bound()
        # if extents[2] > extents[0]:
        # mesh.rotate(Rot.from_rotvec(np.pi / 2 * np.array([1.0, 0, 0])).as_matrix())
        # print(f"Rotated mesh from category {o[0]}")
        scale = dim_x / extents[0]
        if o[0] in ["padlock", "mug"]:
            scale = 0.1
        elif extents[2] > extents[0] or extents[1] > extents[0]:
            scale *= 0.4
        for j in range(len(mesh.vertices)):
            mesh.vertices[j] *= scale
    elif args.scale > 0 and args.scale < 1:
        scale = args.scale
        for j in range(len(mesh.vertices)):
            mesh.vertices[j] *= scale
        dim_x = 1.1 * args.scale
    extents = mesh.get_max_bound() - mesh.get_min_bound()
    print(f"Mesh extents after scaling: {extents}")
    time_scaling = time() - time_scaling
    print(f"scaling runtime:  {time_scaling:.3f}s")

    time_sampling = time()
    pcd = mesh.sample_points_uniformly(10000)
    # pcd = mesh.sample_points_poisson_disk(10000)
    time_sampling = time() - time_sampling
    print(f"sampling runtime: {time_sampling:.3f}s")

    pts = np.asarray(pcd.points)
    extents = np.max(pts, 0) - np.min(pts, 0)
    print(f"extents for scale {scale}: {extents}")

    # colors = np.tile(np.array([0.0, 0.0, 0.6]), (pts.shape[0],1))
    colors = np.tile(np.array([0.0, 0.0, 0.8]), (pts.shape[0], 1))
    pcd.colors = o3d.utility.Vector3dVector(colors)

    clouds.append(pcd)

print(f"extents_list:")
for x in extents_list:
    print(x)

# Plot the object point clouds in a grid.
step_y = 0.1
cols = 6
p = HandsPlot(HandGeometry(**hand_params))
for i, cloud in enumerate(clouds):
    pts = np.asarray(cloud.points)
    # step_x = np.max(pts[:, 0]) - np.min(pts[:, 0]) + 0.01
    step_x = dim_x + 0.01
    center = np.array([(i % cols) * step_x, np.floor(i / cols) * step_y, 0])
    cloud.points = o3d.utility.Vector3dVector(pts + center)
    cloud = cloud.voxel_down_sample(voxel_size=0.005)
    p.plotCloud(cloud, [1.0, 0.0, 0.0], opacity=0.3)
    print(f"{i}, center: {center}")
p.plotAxes(np.eye(4), [1.0, 1.0, 0.5])
p.show()

"""
# Plot the object point clouds in a grid.
cols = 3
rows = int(len(clouds) / cols)
step_x = 0.35
step_y = 0.35
vis = o3d.visualization.Visualizer()
vis.create_window(o[0] + ": " + o[1], 640, 480, 50, 50, True)
for i in range(len(clouds)):
    pts = np.asarray(clouds[i].points)
    # step_x = np.max(pts[:,0]) - np.min(pts[:,0]) + 0.2
    step_x = np.max(pts[:, 0]) - np.min(pts[:, 0]) + 0.01
    center = np.array([(i % cols) * step_x, np.floor(i / cols) * step_y, 0])
    print(f"{i}, center: {center}")
    clouds[i].points = o3d.utility.Vector3dVector(pts + center)
    clouds[i] = clouds[i].voxel_down_sample(voxel_size=0.01)
    clouds[i].paint_uniform_color([1.0, 0.0, 0.0])
    vis.add_geometry(clouds[i])
    pts = np.asarray(clouds[i].points)
# origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
# vis.add_geometry(origin_frame)
vis.run()
"""

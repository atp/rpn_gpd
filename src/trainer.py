import os

import numpy as np
import torch as tp
from torch.utils import data as torchdata

from learning import Metrics


def createSubsets(dataset):
    labels = dataset.labels.numpy()
    indices = []
    for i in range(labels.shape[1]):
        pos = np.nonzero(labels[:, i] == 1)[0]
        neg = np.nonzero(labels[:, i] == 0)[0]
        neg = np.random.choice(neg, len(pos))
        # idxs = np.hstack((pos, neg))
        idxs = pos
        # print("[createSubsets]", i, len(idxs), len(pos), len(neg), idxs)
        indices.append(idxs)
    return indices


def createSubsetLoader(database, indices, batch_size):
    sampler = torchdata.SubsetRandomSampler(indices)
    batch_sampler = torchdata.BatchSampler(sampler, batch_size, True)
    loader = torchdata.DataLoader(database, batch_sampler=batch_sampler)
    return loader


class Trainer:
    def __init__(
        self,
        model,
        criterion,
        optimizer,
        scheduler,
        device,
        model_dir,
        labels_dtype,
        thresh=0.5,
    ):
        self.model = model
        self.criterion = criterion
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.device = device
        self.model_dir = model_dir
        self.thresh = thresh
        self.labels_dtype = labels_dtype

    def trainLoop(self, train_loader, test_loader, num_epochs):
        # Print at about every 20%-th step.
        print_step = 0.2 * len(train_loader)
        print_step = int(np.around(print_step, -str(print_step).find(".") + 1))
        metrics_out = []

        # Evaluate the network on the test set.
        metrics = self.eval(test_loader)
        metrics_out.append(metrics)

        prev_lr = 1

        for epoch in range(num_epochs):
            info = f"epoch: {epoch + 1}/{num_epochs}, "
            running_loss = 0.0
            self.model.train()

            for param_group in self.optimizer.param_groups:
                if param_group["lr"] < prev_lr:
                    prev_lr = param_group["lr"]
                    print(f"lr: {prev_lr:.9f}")

            # Train the network on all batches.
            for i, batch in enumerate(train_loader):
                loss = self.train(batch)
                running_loss += loss
                if i % print_step == print_step - 1:
                    s = info + f"batch: {i + 1:5d}/{len(train_loader):5d}, "
                    s += f"loss: {running_loss / print_step:.6f}"
                    print(s)
                    running_loss = 0.0

            # Evaluate the network on the test set.
            metrics = self.eval(test_loader)
            metrics_out.append(metrics)

            filename = f"model_ep_{epoch}_pre_{np.around(metrics.precision(), 5)}"
            model_path = os.path.join(self.model_dir, filename + ".pwf")
            tp.save(self.model.state_dict(), model_path)
            print("Stored parameters at: " + model_path)

            self.scheduler.step()

        return metrics_out

    def train(self, data):
        inputs, labels = data[0].to(self.device), data[1].to(self.device)

        # Reset the parameter gradients.
        self.optimizer.zero_grad()

        outputs = self.model(inputs)
        # loss = self.criterion(outputs, labels.type(outputs.dtype))
        # print(outputs.dtype, labels.dtype)
        # print(outputs.shape, labels.shape)
        loss = self.criterion(outputs, labels.type(self.labels_dtype))
        loss.backward()
        self.optimizer.step()

        return loss

    def eval(self, loader):
        print("Testing the network on the test data ...")
        self.model.eval()
        avg_metrics = Metrics()

        with tp.no_grad():
            for data in loader:
                inputs = data[0].to(self.device)
                labels = data[1].to(self.device)

                outputs = self.model(inputs)
                if outputs.shape[1] == 2:
                    _, predicted = tp.max(outputs.data, 1)
                else:
                    outputs = tp.sigmoid(outputs)
                    predicted = (outputs > self.thresh).long()
                    labels = labels.type(outputs.dtype)

                act = np.asarray(labels.cpu()).flatten()
                pred = np.asarray(predicted.cpu()).flatten()
                metrics = Metrics(act, pred)
                avg_metrics.concat(metrics)

        print(f"Test set results:")
        print(avg_metrics)

        return avg_metrics

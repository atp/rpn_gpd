"""Evaluate the classifier chain of CLS and ROT on a database.

Usage: python test_model.py DATABASE ARCHITECTURE CLS_PARAMS_FILE ROT_PARAMS_FILE

The evaluation is done by calculating precision and recall of the model at different 
thresholds. The precision-recall curve is plotted and the plot is stored in a file.

"""

import sys
from argparse import ArgumentParser
from datetime import datetime

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils import data as torchdata

from datasets import BasicDataset
from method_evaluator import evalClassifier, evalClsAndRot, runModel
from networks.network import LenetLikeWithSigmoid, VggLikeWithSigmoid
from test_model import runModel as rmodel

loader_params = {
    "batch_size": 512,
    "shuffle": False,
    "pin_memory": True,
    "num_workers": 8,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}

lenet_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    # Params for Lenet
    "conv": [32, 64],
    "fc": [512],
    # "fc": [500],
    # "fc": [32],
    # "fc": [512, 512],
    "conv_filter_size": 5,
}

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}

net_params_dict = {"lenet": lenet_params, "vgg": vgg_params}
architectures = {"lenet": LenetLikeWithSigmoid, "vgg": VggLikeWithSigmoid}

np.set_printoptions(precision=3, suppress=True)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(
        description="Evaluate the classifier chain of CLS and ROT on a database"
    )
    parser.add_argument("db_file", help="path to datbase file")
    parser.add_argument("architecture", help="network architecture")
    parser.add_argument("cls_weights_file", help="path to CLS weights file")
    parser.add_argument("rot_weights_file", help="path to ROT weights file")

    return parser


def createModels(
    architecture,
    params,
    out_shapes,
    cls_weights_file,
    rot_weights_file,
    device,
    names=["cls", "rot"],
):
    weights_files = [cls_weights_file, rot_weights_file]
    models = []

    for i in range(len(weights_files)):
        params["out_shape"] = out_shapes[i]
        model = architectures[architecture](**params)
        checkpoint = torch.load(weights_files[i])
        if type(checkpoint) == dict:
            model.load_state_dict(checkpoint["model_state_dict"])
        else:
            model.load_state_dict(checkpoint)
        model.eval()
        model = model.to(device)
        models.append(model)
        print(f"Loaded weights from: {weights_files[i]}")
        print(f"{names[i]} network:\n{model}")

    for i in range(len(weights_files)):
        print(models[i])

    return models


def runModels(models, loader, device, model_names=["cls", "rot"]):
    outputs = {k: [] for k in model_names}

    with torch.no_grad():
        for i, data in enumerate(loader):
            print(f"batch: {i+1}/{len(loader)}")
            inputs = data[0].to(device)
            for j, model in enumerate(models):
                out = model(inputs)
                if out.shape[1] == 2:
                    out = out[:, 1]
                outputs[model_names[j]].append(out)

    for k in outputs:
        outputs[k] = torch.cat(outputs[k]).cpu().numpy()

    return outputs


def plotPrecisionRecall(metrics_dict):
    recall, precision = [], []
    for k, v in metrics_dict.items():
        recall += [x.recall() for x in v]
        precision += [x.precision() for x in v]

    plt.figure()
    plt.plot(recall, precision, "o")
    plt.xlabel("Recall")
    plt.ylabel("Precision")
    plt.ylim([-0.05, 1.05])
    plt.xlim([-0.05, 1.05])

    out_path = f"/tmp/precision-recall-{datetime.now()}.png"
    plt.savefig(out_path, format="png")
    print("Stored precision-recall plot at: " + out_path)


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Load the data.
    net_params = net_params_dict[args.architecture]
    dset = BasicDataset(args.db_file, net_params["out_shape"])
    loader = torchdata.DataLoader(dset, **loader_params)
    imgs_shape = dset.images.shape[-3:]
    net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    print("net_params:", net_params)

    # Use the GPU if possible.
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # Setup the network models.
    out_shapes = [2, dset.labels.shape[1]]
    models = createModels(
        args.architecture,
        net_params,
        out_shapes,
        args.cls_weights_file,
        args.rot_weights_file,
        device,
    )

    # Run the network models on the data.
    outputs = runModels(models, loader, device)
    print("outputs:")
    print(outputs)

    # Evaluate the classifier at different thresholds.
    thresh_step = 0.025
    # thresh_step = 0.05
    # thresh_step = 0.25
    threshs = np.linspace(0.0, 1.0, int(np.floor(1.0 / thresh_step)) + 1)
    print(threshs)
    labels = dset.labels.numpy()
    labels = labels[: len(outputs["cls"])]
    print("labels:", labels.shape)
    metrics_dict = evalClsAndRot(labels, outputs, threshs, False)
    print(metrics_dict.keys())

    # Store the results.
    out_path = f"/tmp/metrics_cls_rot_{args.architecture}.npy"
    np.save(out_path, metrics_dict)
    print("Stored metrics dictionary at: " + out_path)

    # Plot and store the precision-recall curve.
    plotPrecisionRecall(metrics_dict)


if __name__ == "__main__":
    main()

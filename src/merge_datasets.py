# Merges all 'train'/'test' HDF5 files in a given folder into a single file.

import os
import sys

import numpy as np
import tables as tab

from utils.db_utils import calcMaxInMemory

os.environ["BLOSC_NTHREADS"] = "8"

filters = tab.Filters(complevel=9, complib="blosc:lz4")


def count(input_dir, start):
    n = 0
    files = []

    for f in os.listdir(input_dir):
        if f.startswith(start) and f.endswith(".h5"):
            print(f"Counting rows in {f} ... ", end=" ")
            files.append(input_dir + f)
            with tab.open_file(files[-1], "r") as fin:
                n += len(fin.root.labels)
                print(f" found {len(fin.root.labels)} rows.")

    return n, files


def copyDatabase(filepath, fout, max_in_memory, num_copied):
    with tab.open_file(filepath) as fin:
        m = len(fin.root.labels)
        idxs = list(np.arange(0, m, max_in_memory)) + [m]
        fin_nodes = fin.list_nodes(fin.root)
        fout_nodes = fout.list_nodes(fout.root)
        print("file:", filepath, "blocks to be copied:", idxs)

        for i in range(1, len(idxs)):
            start, stop = idxs[i - 1], idxs[i]
            start_out = num_copied + start
            stop_out = num_copied + stop
            print(
                f"({i}/{len(idxs)-1}) Copying block from [{start}, {stop}]"
                + f" to [{start_out}, {stop_out}]"
            )

            for j in range(len(fin_nodes)):
                if fout_nodes[j].name != fin_nodes[j].name:
                    print("Error: not copying matching CArrays")
                    exit(-1)
                print(
                    f"  ({j+1}) Copying {fin_nodes[j].name}: "
                    + f"{(stop - start,) + fin_nodes[j].shape[1:]}"
                )
                fout_nodes[j][start_out:stop_out] = fin_nodes[j][start:stop]

        num_copied += m

    return num_copied


if len(sys.argv) < 2:
    print("Error: Not enough input arguments!")
    print("Usage: python pytables_merge_datasets.py DIR")
    exit(-1)

input_dir = sys.argv[1]
starts = ["train", "test"]

for i in range(len(starts)):
    # Count the number of files that match the pattern.
    total, files = count(input_dir, starts[i])
    if total == 0:
        print("No files found that start with:", starts[i])
        continue
    print(f"Merged database will have {total} rows in each array.")

    # Create a new database that contains all the data.
    with tab.open_file(files[0]) as fin:
        max_in_memory = 1000
        #        max_in_memory = 60000
        nodes = fin.list_nodes(fin.root)
        fout = tab.open_file(input_dir + starts[i] + "_merged.h5", "w")
        for node in nodes:
            if str(node.dtype) == "uint8":
                dtype = tab.UInt8Atom()
            elif str(node.dtype) == "float32":
                dtype = tab.Float32Atom()
            elif "S" in str(node.dtype):
                dtype = tab.StringAtom(50)
            print(f"Existing node has datatype: {node.dtype}")
            dset = fout.create_carray(
                "/", node.name, dtype, shape=(total,) + node.shape[1:], filters=filters
            )
            print(f"Created dataset: {dset}, {dset.dtype} {node.dtype}")

    # Fill the new database file by file, block by block.
    num_copied = 0
    for fname in files:
        num_copied = copyDatabase(fname, fout, max_in_memory, num_copied)

    print("-------------------------------------------------------------------")
    print("Created database:\n", fout)
    print("-------------------------------------------------------------------")
    fout.close()

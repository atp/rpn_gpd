import os
import sys

import numpy as np
import trimesh as tri

np.set_printoptions(suppress=True, precision=3)


def shrinkMesh(mesh: tri.base.Trimesh, scale: np.array):
    """in-place"""
    mesh.apply_scale(scale)

    s = 1.0 / np.max(mesh.extents)
    mesh.apply_scale(s)


xscale = 0.001
yscale = 0.002
zscale = 0.004

in_path = sys.argv[1]
out_path = sys.argv[2]

scale = np.array([xscale, yscale, zscale])

if os.path.isfile(in_path) and not os.path.isdir(out_path):
    mesh = tri.load(in_path)
    print(f"Loaded mesh with extents: {mesh.extents}")
    mesh.apply_scale(scale)
    print(f"After nonuniform scaling. New extents: {mesh.extents}")
    s = 1.0 / np.max(mesh.extents)
    mesh.apply_scale(s)
    print(f"After normalizing. New extents: {mesh.extents}")
    mesh.export(out_path)
    print(f"Stored scaled mesh at: {out_path}")
elif os.path.isdir(in_path) and os.path.isdir(out_path):
    filenames = os.listdir(in_path)
    for i, f in enumerate(filenames):
        mesh = tri.load(os.path.join(in_path, f))
        if np.max(mesh.extents) / np.min(mesh.extents) > 10:
            shrinkMesh(mesh, scale[np.argsort(scale)[::-1]])
        out_filepath = os.path.join(out_path, f)
        mesh.export(out_filepath)
        print(f"({i+1}/{len(filenames)}) Stored scaled mesh at: {out_filepath}")


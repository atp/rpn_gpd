import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

import utils.hands_plot as hands_plt
from hand_params import hand_params
from hands.hand_eval import HandGeometry

# Plot hand at origin.
hand_geom = HandGeometry(**hand_params)
poses = [np.eye(4)]
p = hands_plt.HandsPlot(hand_geom)
p.plotHands(poses, [1.0, 0.0, 0.0])
p.show()

# Plot hand at origin, scaled by a factor.
p = hands_plt.HandsPlot(hand_geom)
p.plotHands(poses, [1.0, 0.0, 0.0], scale=0.1)
p.show()

# Plot two hands at (0, 0, 90deg) and (90deg, 0, 0).
poses = np.array([np.eye(4)] * 2)
poses[0, :3, :3] = Rot.from_rotvec(np.pi / 2 * np.array([0, 0, 1])).as_matrix()
poses[0, :3, 3] = [-0.1, 0.0, 0.0]
poses[1, :3, :3] = Rot.from_rotvec(np.pi / 2 * np.array([1, 0, 0])).as_matrix()
poses[1, :3, 3] = [0.0, 0.0, 0.1]
p = hands_plt.HandsPlot(hand_geom)
p.plotHands([np.eye(4)], [0.0, 0.0, 1.0])
p.plotHands([poses[0]], [1.0, 0.0, 0.0])
p.plotHands([poses[1]], [0.0, 1.0, 0.0])
p.plotAxes(np.eye(4))
p.setCameraPose([-1, 0, 1], [0, 0, 1], [0, 0, 0])
p.show()

# Draw the point cloud of a sphere.
s = o3d.geometry.TriangleMesh.create_sphere(radius=0.05)
s.translate([0.15, 0.05, 0.12])
c = s.sample_points_poisson_disk(100)
p = hands_plt.HandsPlot(hand_geom)
p.plotCloud(c, [1.0, 0.0, 0.0])
p.plotAxes(np.eye(4))
p.show()

"""Print the distribution of positives and negatives in a database.
"""

import sys

import numpy as np
import tables as tab

np.set_printoptions(precision=3)

with tab.open_file(sys.argv[1], "r") as f:
    # Calculate the overall percentages of negatives and positives.
    Y = np.asarray(f.root.labels)
    ones = np.count_nonzero(Y == 1)
    zeros = np.count_nonzero(Y == 0)
    ones_percent = (ones / (ones + zeros)) * 100.0
    zeros_percent = (zeros / (ones + zeros)) * 100.0
    ratio = ones / float(zeros)
    print(
        f"labels: {zeros + ones}, positives: {ones} ({ones_percent:.2f}%), "
        + f"negatives: {zeros} ({zeros_percent:.2f}%), ratio (pos/neg): {ratio:.3f}"
    )

    if Y.ndim == 2:
        pos_rows = np.count_nonzero(np.sum(Y, 1) > 0)
        neg_rows = np.count_nonzero(np.sum(Y, 1) == 0)
        pos_rows_percent = (pos_rows / (pos_rows + neg_rows)) * 100.0
        neg_rows_percent = (neg_rows / (pos_rows + neg_rows)) * 100.0
        ratio = pos_rows / float(neg_rows) if neg_rows > 0 else np.nan
        print(
            f"rows: {Y.shape[0]}, positives: {pos_rows} ({pos_rows_percent:.2f}%), "
            + f"negatives: {neg_rows} ({neg_rows_percent:.2f}%), "
            + f"ratio (pos/neg): {ratio:.3f}"
        )

        positive_cols = np.count_nonzero(np.sum(Y, 0) > 0)
        negative_cols = np.count_nonzero(np.sum(Y, 0) == 0)
        ratio = positive_cols / float(negative_cols) if negative_cols > 0 else np.nan
        print(
            f"cols: {Y.shape[1]}, positives: {positive_cols}, "
            + f"negatives: {negative_cols}, ratio (pos/neg): {ratio:.3f}"
        )

        if len(sys.argv) == 3 and sys.argv[2] == "-v":
            col_sums = np.sum(Y, 0)
            row_sums = np.sum(Y, 1)
            row_sums = row_sums[row_sums > 0]
            print(f"col_sums:\n{col_sums}")
            print(
                f"row_sums (min, max, mean): {row_sums.min(), row_sums.max(), row_sums.mean()}"
            )
            print(f"#rows with < 10 positives: {len(row_sums[row_sums < 10])}")

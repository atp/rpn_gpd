"""Train a model for a binary classification problem.

"""

import os
import sys

import kornia.augmentation as K
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
from multihead_trainer import MultiheadTrainer
from torch.optim.lr_scheduler import ExponentialLR
from torch.utils import data as torchdata

from datasets import BasicDataset
from learning import Metrics
from networks.multihead import MultiheadLenet
from networks.network import LenetLike, VggLike
from trainer import Trainer
from utils.db_utils import plotBatch

loader_params = {
    "batch_size": 64,
    #    'batch_size': 256, # does not fit onto GPU
    # 'batch_size': 128,
    "shuffle": True,
    "pin_memory": True,
    "num_workers": 8,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}

sgd_params = {
    #    'lr': 0.1, # Resnet
    # 'lr': 0.08,
    # "lr": 0.05,  # Lenet
    "lr": 0.01,  # Lenet
    # "lr": 0.001,
    # "lr": 0.0001,
    "momentum": 0.9,
    # "weight_decay": 0.001
    "weight_decay": 0.0001
    # "weight_decay": 0.00005,
    # "weight_decay": 0.00001,
}
exp_lr_decay = 0.96

net_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    # Params for Lenet
    "conv": [32, 64],
    "fc": [512],
    # "fc": [32],
    # "fc": [512],
    # "conv": [32, 64, 128],
    # "fc": [512, 512],
    "conv_filter_size": 5,
}

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    # "conv": [64, 128, 256],
    # "fc": [512, 512],
    # "fc": [512, 512],
    "conv_filter_size": 3,
}

configs = {
    "cls": 2,
    "rot": -1,
    "gc": 2,
}

# num_epochs = 2 # GC
# num_epochs = 50 # CLS
# num_epochs = 100 # CLS
# num_epochs = 200  # ROT
num_epochs = 300  # ROT halfdome

np.set_printoptions(suppress=True, precision=3)


class AddGaussianNoise:
    def __init__(self, mean=0.0, std=1.0):
        self.std = std
        self.mean = mean

    def __call__(self, x: torch.tensor):
        return x + self.mean + torch.randn(x.size()) * self.std

    def __repr__(self):
        return self.__class__.__name__ + "(mean={0}, std={1})".format(
            self.mean, self.std
        )


def plotBatches(loader, num_batches):
    pad1 = torch.nn.ConstantPad2d((4, 4, 4, 4), 1)
    pad0 = torch.nn.ConstantPad2d((4, 4, 4, 4), 0)
    i = 0
    # names = ['gen_images', 'gc_images']
    names = ["images"]

    with torch.no_grad():
        for batch in loader:
            plotBatch(batch[0], batch[1], pad1, pad0, names[0])
            # Iterate over both GEN and GC images.
            # for j in range(len(batch) - 1):
            # plotBatch(batch[j], batch[-1], pad1, pad0, names[j])
            i += 1
            if i == num_batches:
                break


def initWeights(m):
    if isinstance(m, torch.nn.Linear) or isinstance(m, torch.nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight, nonlinearity="relu")


if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print(
        "Usage: python train_model.py TRAIN_HDF5 TEST_HDF5 "
        + "MODEL_OUT_DIR TASK ARCHITECTURE [NUM_BATCHES_PLOT]"
    )
    print("TASK can be one of: cls, rot.")
    print("ARCHITECTURE can be one of: lenet, vgg.")
    exit(-1)

train_filepath = sys.argv[1]
test_filepath = sys.argv[2]
model_dir = sys.argv[3]

# Determine how many classes to predict.
network_models = {"lenet": LenetLike, "vgg": VggLike, "mh-lenet": MultiheadLenet}
network_model = sys.argv[5]
if network_model == "vgg":
    net_params = vgg_params
net_params["out_shape"] = configs[sys.argv[4]]
if "mh-" in network_model:
    net_params["out_shape"] = 5

# Determine how many batches to plot before training.
num_batches_plot = 0
if len(sys.argv) == 7:
    num_batches_plot = int(sys.argv[6])

# This is the mean calculated using calc_dataset_mean_std.py.
transform = transforms.Normalize(
    mean=[0.0724, 0.0793, 0.0831], std=[0.2019, 0.2076, 0.2012]
)
transform = None

# Add Gaussian noise.
# transform = AddGaussianNoise(0.0, 0.5)
# transform = AddGaussianNoise(0.0, 0.2)
# transform = AddGaussianNoise(0.0, 0.1)

# transform = transforms.RandomApply(
# [
# # AddGaussianNoise(0.0, 0.1),
# # AddGaussianNoise(0.0, 0.2),
# AddGaussianNoise(0.0, 0.3),
# K.RandomRotation(degrees=5.0),
# # transforms.RandomAffine(degrees=5, translate=(0.1, 0.1)),
# # transforms.Lambda(lambda x: x + torch.randn_like(x)),
# ],
# p=0.5,
# )
noise = transforms.RandomApply([AddGaussianNoise(0.0, 0.1)], p=0.5)
affine = transforms.RandomApply(
    [K.RandomAffine(degrees=5.0, translate=(0.05, 0.05))], p=0.5
)
erase = K.RandomErasing(p=0.5, scale=(0.01, 0.05))
# erase = transforms.RandomApply([K.RandomErasing((0.01, 0.05))], p=0.5)
# jitter = transforms.RandomApply([K.ColorJitter(0.1, 0.1, 0.1, 0.1)], p=0.5)
# transform = transforms.Compose([noise, K.RandomRotation(degrees=5.0)])
# transform = transforms.Compose([noise, affine])
# transform = transforms.Compose([noise, affine, erase])
transform = None

# Load the train and test sets.
# train_db = BasicDataset(train_filepath, net_params["out_shape"])
# test_db = BasicDataset(test_filepath, net_params["out_shape"])

# Augment the training set, but not the test set.
train_db = BasicDataset(train_filepath, net_params["out_shape"], transform=transform)
test_db = BasicDataset(test_filepath, net_params["out_shape"])

# train_db = BasicDataset(train_filepath, net_params["out_shape"], transform=transform)
# test_db = BasicDataset(test_filepath, net_params["out_shape"], transform=transform)

train_loader = torchdata.DataLoader(train_db, **loader_params)
test_loader = torchdata.DataLoader(test_db, **loader_params)

if sys.argv[4] == "rot":
    net_params["out_shape"] = test_db.labels.shape[1]

# Determine the shape of the input images.
imgs_shape = test_db.images.shape[-3:]

# Plot some batches.
if num_batches_plot > 0:
    plotBatches(test_loader, num_batches_plot)

# Use GPU for training if available.
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print(device)

# Setup the network.
net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
print(f"net_params['in_shape']: {net_params['in_shape']}")
model = network_models[network_model](**net_params)

# Use Kaiming initialization for all layers.
model.apply(initWeights)

# Calculate class ratio.
pos_per_col = torch.sum(train_db.labels == 1, 0).float()
neg_per_col = torch.sum(train_db.labels == 0, 0).float()
print(f"neg_per_col:\n{neg_per_col.numpy()}")
print(f"pos_per_col:\n{pos_per_col.numpy()}")
class_ratio = torch.mean(pos_per_col / neg_per_col)
print("avg class ratio (pos/neg):", class_ratio)

# Set up the loss function, optimizer, and lr-scheduler.
if net_params["out_shape"] == 2:
    if class_ratio < 1.0:
        class_weights = torch.tensor([1.0, 1.0 / class_ratio])
        criterion = nn.CrossEntropyLoss(class_weights.to(device))
        print("Using class_weights:", class_weights)
    else:
        criterion = nn.CrossEntropyLoss()
    labels_dtype = torch.long
else:
    if class_ratio < 1.0:
        # Per-column weights for positive class.
        pos_weight = neg_per_col / np.maximum(
            pos_per_col, 1
        )  # np.ones(len(pos_per_col)))
        criterion = nn.BCEWithLogitsLoss(pos_weight=pos_weight.to(device))
        # criterion = nn.BCEWithLogitsLoss(
        #    pos_weight=pos_weight.to(device), reduction="none"
        # )
        print("Using pos_weight:", pos_weight.numpy())
    else:
        criterion = nn.BCEWithLogitsLoss()
    labels_dtype = torch.float
print(f"Using loss function: {criterion}")

optimizer = optim.SGD(model.parameters(), **sgd_params)
# optimizer = optim.Adam(
#    model.parameters(), sgd_params["lr"], weight_decay=sgd_params["weight_decay"]
# )
scheduler = ExponentialLR(optimizer, exp_lr_decay)

print("Copying network to GPU ...")
model.to(device)
print(model)

# Train the network.
if "mh-" in network_model:
    trainer = MultiheadTrainer(
        model, criterion, optimizer, scheduler, device, model_dir, labels_dtype
    )
else:
    trainer = Trainer(
        model, criterion, optimizer, scheduler, device, model_dir, labels_dtype
    )
metrics = trainer.trainLoop(train_loader, test_loader, num_epochs)

output = [[x.accuracy(), x.precision(), x.recall()] for x in metrics]
np.save(model_dir + "metrics.npy", np.asarray(output))

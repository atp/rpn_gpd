"""Shrink a database to a given size.

Given a database and a size as input, a new database is created with the same contents 
as the given database, but where each array is shrinked to the given size. This means 
that only the rows up to the given size are copied to the new database.

"""

import os
import sys
from functools import partial

import numpy as np
import tables as tab

from databases import copyBlockwise, filters, shrinkDatabase

# Read command line parameters.
if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print("Usage: python shrink_database.py H5_IN H5_OUT SIZE")
    exit(-1)
fname_in = sys.argv[1]
fname_out = sys.argv[2]
size = int(sys.argv[3])

with tab.open_file(fname_in, "r") as fin, tab.open_file(fname_out, "w") as fout:
    for g in fin.walk_groups():
        if type(g) == tab.group.Group:
            fout.create_group("/", g._v_name)
        nodes = [x for x in fin.list_nodes(g) if type(x) == tab.carray.CArray]
        print(f"Nodes in this group: {[x.name for x in nodes]}")
        for n in nodes:
            path = n._v_pathname[: n._v_pathname.rfind("/") + 1]
            print(f"name: {n.name}, pathname: {n._v_pathname}")

            # Check if the node fits into memory.
            if len(n.shape) < 4:
                fout.create_carray(path, n.name, obj=n[:size], filters=filters)
            else:
                copyBlockwise(n, n.name, fout, size)
            node = fout.get_node(n._v_pathname)
            print(f"Shrinked node {n.name}: {n.shape[0]} --> {node.shape[0]}")
print("================================================================")

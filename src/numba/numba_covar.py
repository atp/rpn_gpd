import math

import numpy as np

from numba import cuda, guvectorize, njit, vectorize


# @vectorize(["float32(float32)"], target="cuda")
# @guvectorize(["(float32[:])"], "(i)->()", target="cuda")
# @cuda.jit(device=True)
# @vectorize(["float32(float32[:])"], target="cuda")
# @cuda.jit
@guvectorize(["(float32[:])"], "(i)->()", target="cuda")
def gpu_std(xs):
    # compute the mean
    mean = 0

    for x in xs:
        mean += x
        mean /= len(xs)

    # compute the variance
    ms = 0
    for x in xs:
        ms += (x - mean) ** 2
    variance = ms / len(xs)
    std = math.sqrt(variance)

    return std


# @vectorize(['float32(float32)'], target='cuda')
# def gpu_sqrt(x):
# return math.sqrt(x)


# a = np.random.normal(0, 1, 100000).astype(np.float32)

# Create the data array - usually initialized some other way
data = np.ones(256)

# Set the number of threads in a block
threadsperblock = 32

# Calculate the number of thread blocks in the grid
blockspergrid = (data.size + (threadsperblock - 1)) // threadsperblock

# Now start the kernel
s = gpu_std[blockspergrid, threadsperblock](data)

# Print the result
print(s)

# s = gpu_sqrt(s)
# s = gpu_std(a)

# s = std(a)
# print(s)

# c_std = njit(std)
# s = c_std(a)
# print(s)


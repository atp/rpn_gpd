import math

import numpy as np

from numba import cuda, guvectorize


@cuda.jit(device=True)
def add(array):
    acc = 0
    for val in array:
        acc += val
    return acc


@cuda.jit(device=True)
def std(xs):
    # compute the mean
    mean = 0
    for x in xs:
        mean += x
        mean /= len(xs)

    # compute the variance
    ms = 0
    for x in xs:
        ms += (x - mean) ** 2
    variance = ms / len(xs)
    std = math.sqrt(variance)

    return std


@guvectorize(["(float32[:], float32[:])"], "(n)->()", target="cuda")
def gpu_std(array, out):
    out[0] = std(array)


@guvectorize(["(float32[:], float32[:])"], "(n)->()", target="cuda")
def gpu_avg(array, out):
    out[0] = add(array) / len(array)


@guvectorize(["(float32[:], float32[:])"], "(n)->()", target="cuda")
def gpu_sum(array, out):
    out[0] = add(array)


a = np.random.normal(0, 1, 100000).astype(np.float32)
avg = gpu_avg(a)
total = gpu_sum(a)
std = gpu_std(a)

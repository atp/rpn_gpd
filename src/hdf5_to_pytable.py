import os
import sys

import numpy as np
import psutil
import tables as tab


# Calculate how many elements to put into memory.
def calcMaxInMemory(f):
    shape = fin.root.cls_images.shape
    available_mem = psutil.virtual_memory().available
    print(f"available memory: {available_mem/2**20}MB")
    item_size = np.prod(shape[1:]) * np.uint8().nbytes
    max_in_memory = np.round(0.8 * available_mem / item_size, -4)
    max_in_memory = int(max_in_memory) - 10000
    print(f"max_in_memory: {max_in_memory}, item_size: {item_size}")

    return max_in_memory


os.environ["BLOSC_NTHREADS"] = "4"

filters = tab.Filters(complevel=9, complib="blosc")

keys = ["labels", "gen_images", "cls_images"]

# Read command line parameters.
fname_in = sys.argv[1]
fname_out = sys.argv[2]

with tab.open_file(fname_in, "r") as fin, tab.open_file(fname_out, "w") as fout:
    n = fin.list_nodes(fin.root)[0].shape[0]
    max_in_memory = calcMaxInMemory(fin)
    blocks = list(np.arange(0, n, max_in_memory)) + [n]
    print("blocks:", blocks)

    root = fin.root
    nodes = [root.labels, root.gen_images, root.cls_images]
    print(nodes)

    for n in nodes:
        if n.name == "labels":
            dset = fout.create_carray("/", n.name, obj=np.asarray(n), filters=filters)
            continue

        print(n.name, n.shape)
        dset = fout.create_carray(
            "/", n.name, tab.UInt8Atom(), shape=n.shape, filters=filters
        )
        print(f"Created dataset: {dset}, {dset.dtype}, {n.dtype}")
        for i in range(1, len(blocks)):
            print(
                f"({i}/{len(blocks)-1}) Copying block: "
                + f"[{blocks[i-1]}, {blocks[i]}]"
            )
            dset[blocks[i - 1] : blocks[i]] = n[blocks[i - 1] : blocks[i]]

"""Visualize the object instances in a given 3DNet category.
"""

import csv
import glob
import os
import sys
from time import time

import numpy as np
import open3d as o3d
import trimesh
from scipy.spatial.transform import Rotation as Rot

scale = 1.0
x_extent = 0.5

np.set_printoptions(precision=4)

if len(sys.argv) < 2:
    print("Error: not enough input arguments!")
    print("Usage: python plot_object_category.py CATEGORY [MAX_INSTANCES]")
    exit(-1)

user = os.environ.get("USER")
root_dir = "/home/" + user + "/data/Cat200_ModelDatabase/"

category = sys.argv[1]
objects = glob.glob(os.path.join(root_dir, category) + "/*.ply")
print(objects)

if len(sys.argv) == 3:
    max_clouds = int(sys.argv[2])
else:
    max_clouds = len(objects)

images = []
clouds = []

for i in range(max_clouds):
    print(f"{i+1}/{max_clouds}")

    # 1. Load mesh from file.
    filepath = objects[i]
    print("Loaded mesh from:", filepath)
    tmesh = trimesh.load(filepath)

    mesh = o3d.io.read_triangle_mesh(filepath)

    # pcd = mesh.sample_points_poisson_disk(10000)
    #
    # pts = np.asarray(pcd.points)
    # extents = np.max(pts, 0) - np.min(pts, 0)
    # print(f'initial extents: {extents}')

    # 2. Scale mesh by given factor.
    time_scaling = time()
    if scale != 1.0:
        scale = float(objects[i][2])
        for i in range(len(mesh.vertices)):
            mesh.vertices[i] *= scale
        time_scaling = time() - time_scaling
        print(f"scaling runtime:  {time_scaling:.3f}s")
    elif x_extent > 0:
        extents_before = mesh.get_max_bound() - mesh.get_min_bound()
        scale = x_extent / extents_before[0]
        for i in range(len(mesh.vertices)):
            mesh.vertices[i] *= scale
        time_scaling = time() - time_scaling
        extents = mesh.get_max_bound() - mesh.get_min_bound()
        scale = 1.0
        print(f"scaling runtime:  {time_scaling:.3f}s")
        print(f"mesh extents: {extents_before} -> {extents}")

    print(f"Poisson-sampling 10k points from mesh ...")
    time_sampling = time()
    pcd = mesh.sample_points_poisson_disk(10000)
    time_sampling = time() - time_sampling
    print(f"sampling runtime: {time_sampling:.3f}s")

    pts = np.asarray(pcd.points)
    extents = np.max(pts, 0) - np.min(pts, 0)
    print(f"extents for scale {scale}: {extents}")

    # colors = np.tile(np.array([0.0, 0.0, 0.6]), (pts.shape[0],1))
    colors = np.tile(np.array([0.0, 0.0, 0.8]), (pts.shape[0], 1))
    pcd.colors = o3d.utility.Vector3dVector(colors)

    clouds.append(pcd)

    if len(clouds) == max_clouds:
        break

cols = 5
rows = int(np.floor(len(clouds) / 5))
step_x = 0.8
step_y = 0.8
vis = o3d.visualization.Visualizer()
vis.create_window(category, 480, 640, 50, 50, True)
for i in range(len(clouds)):
    pts = np.asarray(clouds[i].points)
    # step_x = np.max(pts[:,0]) - np.min(pts[:,0]) + 0.2
    center = np.array([(i % cols) * step_x, (i / cols) * step_y, 0])
    print(i, i % cols, i / cols, "center:", center)
    clouds[i].points = o3d.utility.Vector3dVector(pts + center)
    vis.add_geometry(clouds[i])
vis.run()

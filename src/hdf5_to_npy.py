"""Convert a HDF5 database of grasps to NPY files.

One NPY file is created for each viewpoint.

"""

import csv
import sys
from argparse import ArgumentParser
from collections import namedtuple
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Tuple

import numpy as np
import open3d as o3d
import tables

from eval_grasp_detector import createGraspDetector, preprocessPointCloud
from grasp_scene_data import GraspData, SceneData, storeGrasps
from hand_params import antipodal_params, hand_params
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from scene import SingleObjectScene, calcLookAtPose

look_at = [0.0, 0.0, 0.0]
use_open_gl_frame = True


@dataclass
class Database:
    categories: list = field(default_factory=list)
    instances: list = field(default_factory=list)
    viewpoints: list = field(default_factory=list)
    samples: np.array = np.empty((0, 3), dtype=float)
    labels: np.array = np.empty((0, 0), dtype=float)
    rot_idxs: np.array = np.empty((0,), dtype=float)
    indices: dict = field(default_factory=dict)


def rindex(l: list, value: any) -> int:
    """Return the highest index of value in the given list.

    Args:
        l (list): the list.
        value (any): the value for which to return the highest index.

    Returns:
        int: the highest index of value.
    """
    return len(l) - 1 - l[::-1].index(value)


def allIndices(rows, num_cols):
    """Generate all colum indices for the given row indices.

    Args:
        rows (int): the row indices.
        num_cols (int): the number of colums.

    Returns:
        np.array: n x 2 the index of each element.
    """
    rows_out = np.repeat(rows, num_cols)
    cols = np.tile(np.arange(num_cols), len(rows))
    idxs = np.vstack((rows_out, cols))
    return idxs


def createHandsGenerator(generator: str):
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    hands_generators = {"ma": MaHandsGenerator, "qd": QdHandsGenerator}
    generator_params = {
        "ma": [hand_geom, antip_prms, use_open_gl_frame],
        "qd": [hand_geom, antip_prms],
    }
    hands_generator = hands_generators[generator](*generator_params[generator])

    return hands_generator


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Convert a HDF5 grasps database to NPY files")
    parser.add_argument("objects_dir", help="path to object set directory")
    parser.add_argument(
        "csv_path", help="path to CSV file produced by <create_data.py>"
    )
    parser.add_argument("hdf5_path", help="path to input database")
    parser.add_argument("out_dir", help="path to output directory for NPY files")
    parser.add_argument(
        "hands_generator", help="hands generator used to create the input database"
    )
    parser.add_argument(
        "-f",
        "--first",
        default=-1,
        type=int,
        help="first object category to process",
    )
    parser.add_argument(
        "-l",
        "--last",
        default=-1,
        type=int,
        help="last object category to process",
    )

    return parser


def readDatabase(f: tables.File) -> namedtuple:
    """Read a database from a file into a named tuple.

    Args:
        f (tables.File): the database file

    Returns:
        namedtuple: the named tuple with fields corresponding to the nodes of the
            given database
    """
    categories = f.get_node("/meta/categories")
    categories = [x.decode("ascii") for x in categories]
    instances = f.get_node("/meta/instances")
    instances = [x.decode("ascii") for x in instances]
    unique_instances = list(set(instances))
    viewpoints = np.array(f.get_node("/meta/viewpoints"))
    samples = np.array(f.get_node("/meta/samples"))
    labels = np.array(f.get_node("/labels"))
    print("instances:", len(instances))
    print("unique_instances:", len(unique_instances))

    # Find the start and stop index for each object instance.
    indices = {x: [instances.index(x), rindex(instances, x)] for x in unique_instances}
    indices = {k: v for k, v in sorted(indices.items(), key=lambda x: x[1])}

    if "rot_idxs" in [x._v_name for x in f.list_nodes("/")]:
        rot_idxs = np.array(f.get_node("/rot_idxs"))
    else:
        rot_idxs = np.empty((0,))

    db = Database(categories, instances, viewpoints, samples, labels, rot_idxs, indices)

    return db


class Converter:
    def __init__(self, generator, database, scales, objects_dir, out_dir):
        self.generator = generator
        self.database = database
        self.scales = scales
        self.objects_dir = objects_dir
        self.out_dir = out_dir

    def processInstance(
        self, instance: str, start: int, stop: int, store_point_cloud: bool = False
    ):
        """Process an object instance.

        Convert all grasps that belong to the given object instance.

        Args:
            instance (str): the name of the object instance.
            start (int): the first index in the database for this instance.
            stop (int): the last index in the database for this instance.
            store_point_cloud (bool, optional): if the point cloud is stored.
        """
        category = self.database.categories[start]
        mesh_path = self.objects_dir + category + "/" + instance + ".ply"
        scene = SingleObjectScene(mesh_path, 0.01, 0.1)
        scene.resetToScale(self.scales[instance])
        scene_data = SceneData(category, instance, scene.scale)
        print(
            f"category: {category}, instance: {instance}, scale: {self.scales[instance]}"
        )
        print(f"start: {start}, stop: {stop}")

        # Find the start and stop index for each viewpoint.
        rows = np.arange(start, stop)
        viewpoints = self.database.viewpoints[start:stop]
        idxs = np.sort(np.unique(viewpoints, axis=0, return_index=True)[1])
        idxs = list(idxs) + [len(viewpoints)]
        print(f"rows: ({rows[0]}, ..., {rows[-1]})")
        print("viewpoints:", viewpoints.shape)

        for i in range(len(idxs) - 1):
            print(f"Viewpoint #{i}: {idxs[i], idxs[i + 1]}")
            camera_pose = calcLookAtPose(viewpoints[idxs[i]], look_at)
            grasp_data, cloud = self.processViewpoint(
                scene, camera_pose, rows[idxs[i] : idxs[i + 1]]
            )
            scene_data.camera_pose = camera_pose
            if store_point_cloud:
                scene_data.cloud = np.asarray(cloud.points)
            fpath = self.out_dir + f"{category}_{instance}_{i}.npy"
            storeGrasps(scene_data, grasp_data, fpath)
            print("-------------------------------------------------------------")

    def processViewpoint(
        self, scene: SingleObjectScene, camera_pose: np.array, rows: np.array
    ) -> Tuple[GraspData, o3d.geometry.PointCloud]:
        """Process a viewpoint.

        Args:
            scene (SingleObjectScene): the scene.
            camera_pose (np.array): the camera pose.
            rows (np.array): indices of the desired rows in the database.

        Returns:
            Tuple[GraspData, o3d.geometry.PointCloud]: the grasps calculated and the
                point cloud observed for this viewpoint.
        """
        if isinstance(self.generator, QdHandsGenerator):
            print("Copying camera pose into QdHandsGenerator")
            self.generator.camera_pose = deepcopy(camera_pose)
        cloud = scene.createPointCloudFromView(camera_pose)
        cloud = preprocessPointCloud(cloud, camera_pose)
        poses, indices = self.createGraspPoses(cloud, rows)
        grasp_data = GraspData(poses, method=self.generator.name, db_indices=indices)

        return grasp_data, cloud

    def createGraspPoses(
        self, cloud: o3d.geometry.PointCloud, rows: np.array
    ) -> np.array:
        if self.database.labels.ndim == 1:
            print("Using node <rot_idxs> from database")
            idxs = np.vstack((rows, self.database.rot_idxs[rows].flatten()))
        elif self.database.labels.ndim == 2:
            idxs = allIndices(rows, self.generator.num_hand_orientations)
        else:
            print(f"Error: labels shape {self.database.labels.shape} not supported!")
            exit(-1)
        grasps_dict = {"indices": idxs, "frames": []}
        tree = o3d.geometry.KDTreeFlann(cloud)
        print(f"Converting indices of shape {idxs.shape} to grasp poses ...")
        poses = self.generator.indicesToPoses(
            cloud, tree, self.database.samples, grasps_dict
        )

        return poses, idxs


def main():
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Get the scaling factors from the CSV file.
    with open(args.csv_path, "r") as f:
        object_infos = [r for r in csv.reader(f, delimiter=",")]
    object_infos = object_infos[1:]
    object_infos = [x[:2] + [float(y) for y in x[2:]] for x in object_infos]
    scales = {x[1]: np.max(np.array(x[-3:])) for x in object_infos}
    print("scales:", scales)

    # Setup the grasps generator.
    generator = createHandsGenerator(args.hands_generator)

    with tables.open_file(args.hdf5_path, "r") as f:
        db = readDatabase(f)

    conv = Converter(generator, db, scales, args.objects_dir, args.out_dir)

    # Only consider a subset of the object categories.
    if args.first > -1:
        last = args.first if args.last == -1 else args.last
        unique_categories = sorted(list(set([x[0] for x in object_infos])))
        categories_sub = unique_categories[args.first : last + 1]
        db.indices = {
            k: v for k, v in db.indices.items() if db.categories[v[0]] in categories_sub
        }
        print("Categories to be processed:", unique_categories)

    for k, v in db.indices.items():
        conv.processInstance(k, v[0], v[1] + 1)
        print("=====================================================\n")


if __name__ == "__main__":
    main()

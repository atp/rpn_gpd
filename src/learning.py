import logging

import numpy as np
import torch


def trainMultiClass(model, criterion, optimizer, data, device):
    inputs, labels = data[0].to(device), data[1].to(device)

    # Reset the parameter gradients.
    optimizer.zero_grad()

    outputs = model(inputs)
    loss = criterion(outputs, labels.long())
    loss.backward()
    optimizer.step()

    return loss


def train(model, criterion, optimizer, data, device):
    inputs, labels = data[0].to(device), data[1].to(device)

    # Reset the parameter gradients.
    optimizer.zero_grad()

    outputs = model(inputs)
    loss = criterion(outputs, labels.type(outputs.dtype))
    loss.backward()
    optimizer.step()

    return loss


class Metrics:
    """Class for storing a confusion matrix and calculating associated metrics."""

    def __init__(self, labels=[], pred=[], class_idx=1):
        if len(labels) == 0 or len(pred) == 0:
            self.setToZero()
            return
        if labels.ndim == 0:
            print("[Metrics] Warning: <labels> is not a vector")
            labels = np.array([labels])
        if pred.ndim == 0:
            print("[Metrics] Warning: <pred> is not a vector")
            pred = np.array([pred])
        if labels.ndim > 1:
            raise ValueError("<labels> has wrong number of dimensions (1)")
        if pred.ndim > 1:
            raise ValueError("<pred> has wrong number of dimensions (1)")

        pos = np.where(labels == class_idx)[0]
        neg = np.where(labels != class_idx)[0]
        pred_pos = np.where(pred == class_idx)[0]
        pred_neg = np.where(pred != class_idx)[0]
        self.tp = len(np.intersect1d(pred_pos, pos))
        self.tn = len(np.intersect1d(pred_neg, neg))
        self.fp = len(np.intersect1d(pred_pos, neg))
        self.fn = len(np.intersect1d(pred_neg, pos))
        self.p = len(pos)
        self.n = len(neg)
        self.pred_p = len(pred_pos)
        self.pred_n = len(pred_neg)

    def setToZero(self):
        self.tp = 0
        self.tn = 0
        self.fp = 0
        self.fn = 0
        self.p = 0
        self.n = 0
        self.pred_p = 0
        self.pred_n = 0

    def concat(self, metrics):
        self.tp += metrics.tp
        self.tn += metrics.tn
        self.fp += metrics.fp
        self.fn += metrics.fn
        self.p += metrics.p
        self.n += metrics.n
        self.pred_p += metrics.pred_p
        self.pred_n += metrics.pred_n

    def accuracy(self):
        if self.p + self.n == 0:
            #            print('Warning: divide by zero when calculating accuracy')
            return 0
        return (self.tp + self.tn) / (self.p + self.n)

    def balancedAccuracy(self):
        if self.n > 0:
            tnr = self.tn / self.n
        else:
            tnr = 0
        return (self.recall() + tnr) / 2

    def precision(self):
        if self.tp + self.fp == 0:
            #            print('Warning: divide by zero when calculating precision')
            return 0
        return self.tp / (self.tp + self.fp)

    def fpr(self):
        if self.fp + self.tn == 0:
            #            print('Warning: divide by zero when calculating false positive rate')
            return 0
        return self.fp / (self.fp + self.tn)

    def recall(self):
        if self.tp + self.fn == 0:
            #            print('Warning: divide by zero when calculating recall')
            return 0
        return self.tp / (self.tp + self.fn)

    def toArray(self):
        return np.array([self.accuracy(), self.precision(), self.recall()])

    def confmat(self):
        """Get the confusion matrix [#TP, #FP, #TN, #FN].

        Returns:
            np.array: the confusion matrix as a vector.
        """
        return np.array([self.tp, self.fp, self.tn, self.fn])

    def printConfusionMatrix(self):
        print("===========================================================")
        print(" (actual)     P: %6d,  N: %6d" % (self.p, self.n))
        print(" (pred)       P: %6d,  N: %6d" % (self.pred_p, self.pred_n))
        print("-----------------------------------------------")
        print("                    actual")
        print("  p               P          N")
        print("  r     P    TP: %6d, FP: %6d" % (self.tp, self.fp))
        print("  e     N    FN: %6d, TN: %6d" % (self.fn, self.tn))
        print("  d")
        print("===========================================================")

    def __str__(self):
        s = f"acc: {self.accuracy():.3f}, "
        s += f"pre: {self.precision():.3f}, "
        s += f"rec: {self.recall():.3f}; "
        s += f"TP: {self.tp}, FP: {self.fp}, TN: {self.tn}, FN: {self.fn}; "
        s += f"P: {self.p}, N: {self.n}."
        return s


def calcEvalMetrics(pred_pos, pred_neg, pos, neg, is_printed=True):
    tp = len(np.intersect1d(pred_pos, pos))
    tn = len(np.intersect1d(pred_neg, neg))
    fp = len(np.intersect1d(pred_pos, neg))
    fn = len(np.intersect1d(pred_neg, pos))
    acc = (tp + tn) / (len(pos) + len(neg))
    prec = 0
    rec = 0
    if tp > 0 or fp > 0:
        prec = tp / (tp + fp)
    if tp > 0 or fn > 0:
        rec = tp / (tp + fn)
    if is_printed:
        printMetrics(pos, neg, pred_pos, pred_neg, tp, tn, fp, fn, acc, prec, rec)

    return EvalMetrics(acc, prec, rec)


def calcEvalMetricsBinary(labels, pred):
    pos = np.where(labels == 1)[0]
    neg = np.where(labels == 0)[0]
    pred_pos = np.where(pred == 1)[0]
    pred_neg = np.where(pred == 0)[0]

    return calcEvalMetrics(pred_pos, pred_neg, pos, neg, is_printed=False)


def printMetrics(pos, neg, pred_pos, pred_neg, tp, tn, fp, fn, acc, prec, rec):
    print("(Actual)    P: %d, N: %d" % (len(pos), len(neg)))
    print("(Predicted) P: %d, N: %d" % (len(pred_pos), len(pred_neg)))
    print("TP: %d, TN: %d" % (tp, tn))
    print("FP: %d, FN: %d" % (fp, fn))
    print("Accuracy: %.3f, precision: %.3f, recall: %.3f" % (acc, prec, rec))
    print("==========================================================")


def evalMultiClass(model, loader, device):
    print("Testing the network on the test data ...")
    model.eval()
    correct = 0
    total = 0

    with torch.no_grad():
        for data in loader:
            inputs, labels = data[0].to(device), data[1].to(device)
            outputs = model(inputs)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels.long()).sum().item()

    accuracy = 100.0 * float(correct) / float(total)
    print("Accuracy of the network on the test set: %.3f%%" % (accuracy))

    return accuracy


def evalMultiLabel(
    model, loader, device, thresh=0.5, apply_sigmoid=True, print_confusion_matrix=False
):
    print("Testing the network on the test data ...")
    model.eval()
    correct = 0
    total = 0
    balanced_accuracy = 0
    batches = 0
    metrics = np.zeros(3)
    avg_metrics = Metrics()

    with torch.no_grad():
        for data in loader:
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)

            outputs = model(inputs)
            if apply_sigmoid:
                outputs = torch.sigmoid(outputs)
            y = torch.where(
                outputs > thresh,
                torch.ones(outputs.shape).to(device),
                torch.zeros(outputs.shape).to(device),
            )
            correct += (y.long() == labels.long()).sum().item()
            total += labels.size(0) * labels.size(1)

            act = np.asarray(labels.cpu()).flatten()
            pred = np.asarray(y.cpu()).flatten()
            batch_metrics = Metrics(act, pred)
            balanced_accuracy += batch_metrics.balancedAccuracy()
            metrics += batch_metrics.toArray()
            avg_metrics.concat(batch_metrics)
            batches += 1

    accuracy = 100 * correct / float(total)
    balanced_accuracy = 100.0 * balanced_accuracy / float(batches)
    metrics = metrics / float(batches)

    print(
        f"Test set accuracy: {accuracy:.3f}% ({correct}/{total}), "
        + f"balanced accuracy: {balanced_accuracy:.3f}%"
    )
    print("-------------------------------------------------")
    print("Average Evaluation Metrics")
    print("accuracy, precision, recall:", avg_metrics.toArray())
    print("-------------------------------------------------")
    if print_confusion_matrix:
        avg_metrics.printConfusionMatrix()

    return accuracy, avg_metrics


def evalMultiLabelByClass(num_classes, model, loader, device):
    model.eval()
    class_correct = torch.zeros(num_classes, dtype=torch.long).to(device)
    metrics = 0
    metrics_count = 0
    class_total = 0
    thresh = 0.5

    with torch.no_grad():
        for data in loader:
            inputs, labels = data[0].to(device), data[1].to(device)
            outputs = model(inputs)
            outputs = torch.sigmoid(outputs)
            y = torch.where(
                outputs > thresh,
                torch.ones(outputs.shape).to(device),
                torch.zeros(outputs.shape).to(device),
            )
            class_correct += torch.sum(y.long() == labels.long(), 0)
            class_total += labels.size(0)

            act = np.asarray(labels.cpu()).flatten()
            pred = np.asarray(y.cpu()).flatten()
            batch_metrics = Metrics(act, pred)
            metrics += np.array(
                [
                    batch_metrics.accuracy(),
                    batch_metrics.precision(),
                    batch_metrics.recall(),
                ]
            )
            metrics_count += 1

    for i in range(num_classes):
        accuracy = 100 * class_correct[i] / class_total
        print(
            "Accuracy of class <%d> : %.3f%% (%d/%d)"
            % (i, accuracy, class_correct[i], class_total)
        )

    print("-------------------------------------------------")
    print("Average Evaluation Metrics")
    print("accuracy, precision, recall:", metrics / float(metrics_count))
    print("-------------------------------------------------")

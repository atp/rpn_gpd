import numpy
import torch


# Preprocesses uint8 numpy images by converting them to torch float images.
def preprocessImages(images):
    images = torch.from_numpy(images).float() / 255.0
    images = images.permute(0, 3, 1, 2)  # Pytorch uses NCHW format
    return images

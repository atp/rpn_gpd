"""Module for creation and manipulation of scenes.
"""

from abc import ABC, abstractmethod
from copy import deepcopy
from functools import partial

import numpy as np
import open3d as o3d
import pyrender as pr
import trimesh
from pyrender.constants import RenderFlags
from scipy.spatial.transform import Rotation as Rot

from utils.cloud_utils import (
    createVoxelizedCloud,
    estimateNormals,
    numpyToCloud,
    pointNormalsFromFaceNormals,
    twoWindowsPlot,
)

draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)


def randomRotation3D():
    M = np.random.randn(3, 3)
    Q, _ = np.linalg.qr(M)
    return Q


def randomStableRotation3D(flip_prob=0.5, return_flip=False):
    # 90deg rotation about y axis: changes object from upright to flat (or flat
    # to upright).
    is_flipped = False
    if np.random.rand() < flip_prob:
        axis = np.array([0.0, 1.0, 0.0])
        angle = np.pi / 2
        roty = Rot.from_rotvec(angle * axis)
        is_flipped = True
        print("[randomStableRotation3D] I got flipped!")
    else:
        roty = Rot.identity()

    # Random rotation about z axis.
    axis = np.array([0.0, 0.0, 1.0])
    angle = np.deg2rad(np.random.randint(0, 360))
    rotz = Rot.from_rotvec(angle * axis)

    rot = (rotz * roty).as_matrix()

    if return_flip:
        return rot, is_flipped

    return rot


def randomRotationOnSphere(look_at=np.zeros(3), radius=0.5):
    look_from = np.random.normal()
    look_from /= np.linalg.norm(look_from)
    look_from *= radius
    pose = calcLookAtPose(look_from, look_at)
    return pose, look_from


def depthImageToPointCloud(depth, yfov, camera_pose, plot_steps=False):
    """Create a point cloud from a depth image.

    Args:
        depth (np.array): H x W depth image.
        yfov (float): vertical field of view of the camera.
        camera_pose (np.array): camera pose in world frame.
        plot_steps (bool, optional): if each step is visualized.
    """
    width, height = depth.shape[1], depth.shape[0]
    fy = (0.5 * height) / np.tan(0.5 * yfov)
    aspect_ratio = float(width) / height
    xfov = 2 * np.arctan(np.tan(0.5 * yfov) * (1.0 / aspect_ratio))
    fx = (0.5 * width) / np.tan(0.5 * xfov)

    cx = width / 2.0
    cy = height / 2.0

    K = np.diag([fx, -fy, 1.0])
    K[:2, 2] = [cx, cy]

    y, x = np.where(depth > 0)
    cam_x = x * depth[y, x]
    cam_y = (height - 1 - y) * depth[y, x]
    cam_z = depth[y, x]
    cloud = np.vstack((cam_x, cam_y, cam_z))
    if plot_steps:
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
        draw_geoms([numpyToCloud(cloud.T), origin_frame], "Image space (and depth)")

    # Go to camera space.
    cloud = np.dot(np.linalg.inv(K), cloud)
    if plot_steps:
        draw_geoms(
            [numpyToCloud(cloud.T), origin_frame],
            "[depthImageToPointCloud] Camera space",
        )

    # Go to world space.
    cloud = np.vstack((cloud, np.ones(cam_z.shape[0])))
    bXc = deepcopy(camera_pose)
    bXc[:3, 1] *= -1.0
    bXc[:3, 2] *= -1.0
    cloud = np.dot(bXc, cloud)
    if plot_steps:
        camera_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
            0.1, camera_pose[:3, 3]
        )
        camera_frame.rotate(camera_pose[:3, :3])
        draw_geoms(
            [numpyToCloud(cloud[:3, :].T), origin_frame, camera_frame],
            "[depthImageToPointCloud] World space",
        )

    return cloud[:3, :].T


def depthImageToPointCloudDeprecated(depth, P, V, znear, zfar):
    """Create a point cloud from a depth image.

    See OpenGL and pyrender documentations for details.

    Args:
        depth (np.array): WxH depth image.
        P (np.array): 4x4 projection matrix.
        V (np.array): 4x4 view matrix.
        znear (float): position of near plane.
        zfar (float): position of far plane.
    """
    h = depth.shape[0]
    w = depth.shape[1]

    # fx, fy: focal length (distance between pinhole and image plane)
    # cx, cy: distance between image plane origin and image origin
    fx = P[0, 0] * w / 2.0
    fy = P[1, 1] * h / -2.0
    cx = (P[2, 0] - 1.0) * w / -2.0
    cy = (P[2, 1] + 1.0) * h / 2.0

    d = np.reshape(depth, (depth.shape[0] * depth.shape[1]))

    P = np.eye(3)
    P[0, 0] = fx
    P[1, 1] = fy
    P[0, 2] = cx
    P[1, 2] = cy

    xi = np.ones((w * h, 3))
    xi[:, 0] = d * np.tile(np.arange(w), h)
    xi[:, 1] = d * np.flipud(np.repeat(np.arange(h), w))
    xi[:, 2] = d
    pc = np.dot(np.linalg.inv(P), xi.T).T
    origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
    # draw_geoms(
    # [numpyToCloud(xi), origin_frame], "Image space (?)")
    draw_geoms([numpyToCloud(pc), origin_frame], "Camera space")
    hpc = np.ones((len(pc), 4))
    hpc[:, :3] = pc[:, :3]
    V2 = deepcopy(V)
    V2[:3, 1] *= -1.0
    V2[:3, 2] *= -1.0
    pc = np.dot(V2, hpc.T).T
    print(f"V2:\n{V2}")

    mask = d > znear
    if type(zfar) == float:
        mask = mask & (d < zfar)
    valid = np.where(mask)[0]

    #    print('Created point cloud with', len(pc), 'points.')
    #    print('  Valid points:', len(valid))

    return pc[valid, :3]


def scaleMeshRandomNew(
    mesh: trimesh.base.Trimesh, min_extent: float, max_extent: float
) -> (trimesh.base.Trimesh, float):
    """Scale a mesh by a randomly chosen factor within the given limits.

    Args:
        mesh (trimesh.base.Trimesh): the mesh to be scaled.
        min_extent (float): min extent of mesh after scaling.
        max_extents (list): max extent(s) of mesh after scaling.

    Returns:
        (trimesh.base.Trimesh, float): the scaled mesh, the scaling factor.
    """
    if type(max_extent) == list:
        max_extent = max_extent[0]

    # If necessary, rescale mesh such that the largest side becomes 1m long.
    mesh_out = deepcopy(mesh)
    if np.any(mesh_out.extents > 1.0):
        print("(Initial rescaling) Mesh is too large (>1m)! Rescaling ...")
        scale = 1.0 / np.max(mesh_out.extents)
        mesh_out.vertices *= scale
        print(f"  Rescaled from {mesh.extents} to {mesh_out.extents}")

    # Choose a scaling factor randomly and rescale the mesh.
    min_scale = min_extent / np.min(mesh_out.extents)
    max_scale = max_extent / np.max(mesh_out.extents)
    if max_scale <= min_scale:
        # max_scale = np.around(min_scale, 1) # sim -> boxes
        max_scale = np.around(min_scale + 0.1, 1)
    scale = (max_scale - min_scale) * np.random.rand() + min_scale
    mesh_out.vertices *= scale
    print(f"Scaled mesh from {np.array(mesh.extents)} to {np.array(mesh_out.extents)}")
    print(f"Selected scale: {scale:.3f}; scale range: {min_scale:.3f}, {max_scale:.3f}")

    return mesh_out, scale


def scaleMeshRandom(
    mesh, min_extent, max_extents, attempts=200, check_all_extents=True
):
    """Scale a mesh by a randomly chosen factor.

    If `max_extents` is a single number, then the mesh will be scaled to extents which
    lie within (`min_extent`, `max_extents`). Otherwise, if `max_extents` is a list of
    length 2, then it will first try to find a scaling factor such that the mesh's
    extents lie within (`min_extent`, `max_extents[0]`) and if that fails, try to find
    a factor within (`min_extent`, `max_extents[1]`).

    Args:
        mesh (trimesh): mesh.
        min_extent (float): min extent of mesh after scaling.
        max_extents (float or list): max extent(s) of mesh after scaling.
        attempts (int, optional): the number of scaling attempts.
        check_all_extents (bool, optional): if this is True, all extents need to be
            within [`min_extent`, `max_extents`]; if this is False, only one extent
            needs to be within [`min_extent`, `max_extent`].
    """
    min_scale = 0.05
    max_scale = 1.0
    extents = mesh.extents
    print(f"Raw mesh has extents: {extents}")

    # If necessary, rescale mesh such that the largest side becomes 1m long.
    if np.any(extents > 1.0):
        print("(Initial rescaling) Mesh is too large (>1m)! Rescaling ...")
        scale = 1.0 / np.max(extents)
        mesh.vertices *= scale
        print(f"  Rescaled from {extents} to {mesh.extents}")

    extents = mesh.extents
    print("(Actual rescaling) Sampling the object scale ...")

    if type(max_extents) == float:
        max_extents = [max_extents]
    else:
        max_extents = np.arange(max_extents[0], max_extents[1] + 0.1, 0.1)

    for max_extent in max_extents:
        scales = (max_scale - min_scale) * np.random.rand(attempts) + min_scale
        new_extents = scales * extents[:, np.newaxis]
        valid_min = new_extents > min_extent
        valid_max = new_extents < max_extent
        if check_all_extents:
            valid = np.prod(valid_min & valid_max, 0)
        else:
            valid = np.prod(valid_min, 0) & np.any(valid_max, 0)
        idxs = np.nonzero(valid)[0]
        if len(idxs) > 0:
            idx = idxs[0]
            scale = scales[idx]
            break
    print(f"scale: {scale:.3f}; extents range: {min_extent, max_extent}")
    print(f"new_extents: {new_extents[:,idx]}")

    # Scale the mesh.
    mesh_out = deepcopy(mesh)
    mesh_out.vertices *= scale

    return mesh_out, scale


def calcLookAtPose(look_from, look_at):
    """Calculate the pose for looking from a camera position at a position.

    Args:
        look_from (np.array): camera position in world frame.
        look_at (np.array): where to look at in world frame.
    """
    forward = look_from - look_at
    forward /= np.linalg.norm(forward)

    tmp = np.array([0.0, 1.0, 0.0])
    right = np.cross(tmp, forward)
    right /= np.linalg.norm(right)

    up = np.cross(forward, right)

    pose = np.eye(4)
    pose[:3, :3] = np.array([right, up, forward]).T
    pose[:3, 3] = look_from

    return pose


class Scene(ABC):
    """Abstract base class for scenes.

    A scene consists of a camera, a light, and a renderer for depth images.

    """

    renderer = pr.OffscreenRenderer(640, 480, point_size=1.0)

    def __init__(self, image_width=640, image_height=480):
        """Construct a scene.

        Args:
            image_width (float, optional): depth image width.
            image_height (float, optional): depth image height.
        """
        self.flags = RenderFlags.DEPTH_ONLY | RenderFlags.OFFSCREEN
        self.scene = pr.Scene()
        self.camera = pr.PerspectiveCamera(
            yfov=np.pi / 3.0, aspectRatio=float(image_height) / image_width
        )
        s = np.sqrt(2) / 2
        self.camera_pose = np.array(
            [
                [0.0, -s, s, 0.5],
                [1.0, 0.0, 0.0, 0.0],
                [0.0, s, s, 0.5],
                [0.0, 0.0, 0.0, 1.0],
            ]
        )
        self.camera_node = pr.Node(camera=self.camera)
        self.scene.add_node(self.camera_node)
        light = pr.SpotLight(
            color=np.ones(3), intensity=3.0, innerConeAngle=np.pi / 16.0
        )
        self.scene.add(light, pose=self.camera_pose)
        # self.renderer = pr.OffscreenRenderer(image_width, image_height, point_size=1.0)
        self.table_node = None

    def createProcessedPointCloudFromView(self, pose, voxel_size=0.003):
        """Create a processed point cloud seen from a given camera pose.

        The output point cloud is voxelized and has surface normals estimated.

        Args:
            pose (np.array): camera pose in world frame.
            voxel_size (float, optional): voxel size.

        Returns:
            open3d.geometry.PointCloud: point cloud.
        """
        cloud = self.createPointCloudFromView(pose)
        # print(f"Created cloud with {len(cloud.points)} points.")
        cloud = cloud.voxel_down_sample(voxel_size)
        # print(f"Downsampled cloud to {len(cloud.points)} points.")
        cloud = estimateNormals(cloud, pose[:3, 3])
        # print(f"Estimated {len(cloud.normals)} surface normals.")

        return cloud

    def createPointCloudFromView(self, pose, return_depth_image=False):
        """Create a point cloud seen from a given camera pose.

        Args:
            pose (np.array): camera pose in world frame.

        Returns:
            open3d.geometry.PointCloud: point cloud.
        """
        self.scene.set_pose(self.camera_node, pose)
        # pr.Viewer(self.scene, use_raymond_lighting=True)
        depth = self.renderer.render(self.scene, flags=self.flags)
        V = self.scene.get_pose(self.camera_node)
        P = self.camera.get_projection_matrix()
        pts = depthImageToPointCloud(depth, self.camera.yfov, V)
        # pts = depthImageToPointCloudDeprecated(
        # depth, P, V, self.camera.znear, self.camera.zfar)
        cloud = numpyToCloud(pts)

        if return_depth_image:
            return cloud, depth

        return cloud

    def createCompletePointCloud(self, num_pts=20000, add_plane=False):
        raise NotImplementedError()

    def createMultiViewPointCloudOld(
        self,
        look_at=np.zeros(3),
        num_views=30,
        sphere_radius=0.5,
        num_pts=20000,
        voxel_size=0.003,
        return_views_cloud=False,
        mesh=None,
        use_halfsphere=False,
    ):
        """Create a point cloud seen from multiple view points on a sphere.
        The view points are chosen uniformly on a sphere with a given radius.
        Args:
            look_at ([TODO:type], optional): [TODO:description]
            num_views ([TODO:type], optional): [TODO:description]
            sphere_radius ([TODO:type], optional): [TODO:description]
            num_pts ([TODO:type], optional): [TODO:description]
            voxel_size ([TODO:type], optional): [TODO:description]
            return_views_cloud ([TODO:type], optional): [TODO:description]
        """
        # Render the scene from multiple view points on a sphere.
        look_from = np.random.normal(size=(num_views, 3))
        look_from /= np.linalg.norm(look_from, axis=1)[:, np.newaxis]
        look_from *= sphere_radius

        use_halfsphere = self.table_node != None
        if use_halfsphere:
            look_from[:, 2] = np.abs(look_from[:, 2])

        pts_out = np.empty((0, 3))
        normals_out = np.empty((0, 3))
        print(f"Rendering mesh from {num_views} view points on a sphere ...")

        # Create point cloud where each point corresponds to a camera position.
        if return_views_cloud:
            views_cloud = o3d.geometry.PointCloud()
            views_cloud.points = o3d.utility.Vector3dVector(look_from)
            views_cloud.normals = o3d.utility.Vector3dVector(look_at - look_from)

        # mesh = None

        for i in range(num_views):
            pose = calcLookAtPose(look_from[i], look_at)
            cloud = self.createPointCloudFromView(pose)
            cloud = cloud.voxel_down_sample(voxel_size)
            if mesh is None:
                cloud = estimateNormals(cloud, look_from[i])
            pts_out = np.vstack((pts_out, np.asarray(cloud.points)))
            normals_out = np.vstack((normals_out, np.asarray(cloud.normals)))
            print(
                f"({i}) look_from: {look_from[i]}, pts_out: {pts_out.shape}, "
                + f"normals_out: {normals_out.shape}"
            )

        cloud_out = o3d.geometry.PointCloud()
        cloud_out.points = o3d.utility.Vector3dVector(pts_out)
        cloud_out.normals = o3d.utility.Vector3dVector(normals_out)
        print(f"Combined point cloud: {cloud_out}")
        # o3d.visualization.draw_geometries([cloud_out, views_cloud])

        if len(cloud_out.points) > num_pts:
            sample_step = int(np.ceil(len(cloud_out.points) / num_pts))
            cloud_out = cloud_out.uniform_down_sample(sample_step)
            print(f"Combined point cloud after uniform down sampling: {cloud_out}")
            # o3d.visualization.draw_geometries([cloud_out, views_cloud])

        if mesh:
            # print("Using face normals from mesh")
            pointNormalsFromFaceNormals(cloud_out, mesh)
            # cloud_out.orient_normals_towards_camera_location(look_from[i])

        if return_views_cloud:
            return cloud_out, views_cloud

        return cloud_out

    def createMultiViewPointCloudNew(
        self,
        look_at=np.zeros(3),
        num_views=30,
        sphere_radius=0.5,
        num_pts=20000,
        voxel_size=0.003,
        return_views_cloud=False,
        mesh=None,
        use_halfsphere=False,
    ):
        """Create a point cloud seen from multiple view points on a sphere.

        The view points are chosen uniformly on a sphere with a given radius.

        Args:
            look_at ([TODO:type], optional): [TODO:description]
            num_views ([TODO:type], optional): [TODO:description]
            sphere_radius ([TODO:type], optional): [TODO:description]
            num_pts ([TODO:type], optional): [TODO:description]
            voxel_size ([TODO:type], optional): [TODO:description]
            return_views_cloud ([TODO:type], optional): [TODO:description]
        """
        # Render the scene from multiple view points on a sphere.
        look_from = np.random.normal(size=(num_views, 3))
        look_from /= np.linalg.norm(look_from, axis=1)[:, np.newaxis]
        look_from *= sphere_radius

        use_halfsphere = self.table_node != None
        if use_halfsphere:
            # min_vert_offset = 0.01
            look_from[:, 2] = np.abs(look_from[:, 2])
            # look_from[look_from[:, 2] < min_vert_offset, 2] += 0.02

        print(f"Rendering scene from {num_views} view points on a sphere ...")

        # Create point cloud that contains the camera positions.
        if return_views_cloud:
            views_cloud = o3d.geometry.PointCloud()
            views_cloud.points = o3d.utility.Vector3dVector(look_from)
            views_cloud.normals = o3d.utility.Vector3dVector(look_at - look_from)

        poses = [calcLookAtPose(x, look_at) for x in look_from]
        clouds = [self.createPointCloudFromView(x) for x in poses]
        clouds = [x.voxel_down_sample(voxel_size) for x in clouds]

        # total = np.sum(np.array([len(x.points) for x in clouds]))
        # voxel_size = 0.002 if total > 300000 else 0.001
        voxel_size = 0.003
        if self.table_node or mesh is None:
            clouds = [estimateNormals(x, y[:3, 3]) for x, y in zip(clouds, poses)]
            # clouds = [
            # estimateNormals(x.voxel_down_sample(voxel_size), y[:3, 3])
            # for x, y in zip(clouds, poses)
            # ]
        cloud_out = o3d.geometry.PointCloud()
        for i, c in enumerate(clouds):
            # old = deepcopy(c)
            normals = np.asarray(c.normals)
            mask = normals[:, 2] < 0
            normals[mask] *= -1.0
            # c.normals = o3d.utility.Vector3dVector(normals)
            cloud_out += c
            # print(f"Added cloud {i+1}/{len(clouds)}: {len(cloud_out.points)}")
            """
            if num_views == 30:
                # twoWindowsPlot([old], [c])
                v_cloud = o3d.geometry.PointCloud()
                v_cloud.points = o3d.utility.Vector3dVector(np.array([look_from[i]]))
                v_cloud.normals = o3d.utility.Vector3dVector(
                    np.array([look_at - look_from[i]])
                )
                v_cloud.paint_uniform_color([1.0, 0.0, 0.0])
                pts = np.asarray(v_cloud.points)
                pts = np.vstack((pts, np.zeros(3)))
                ls = o3d.geometry.LineSet()
                ls.points = o3d.utility.Vector3dVector(pts)
                ls.lines = o3d.utility.Vector2iVector([[0, 1]])
                # draw_geoms(
                # [cloud_out, v_cloud, ls], f"cloud {i}"
                # )
            """
        print(f"Concatenated cloud: {cloud_out}")
        # dims = c.get_max_bound() - c.get_min_bound()
        # voxel_size = 0.002 if len(cloud_out.points) > 300000 else 0.001
        # voxel_size = 0.003 if len(cloud_out.points) > 1000000 else voxel_size
        # cloud_out = cloud_out.voxel_down_sample(voxel_size)
        # print(f"Selected voxel size: {voxel_size}, cloud: {len(cloud_out.points)}")
        # draw_geoms([cloud_out], "Voxelized, concatenated cloud")
        # exit()

        if len(cloud_out.points) > num_pts:
            step = int(np.floor(len(cloud_out.points) / num_pts))
            cloud_out = cloud_out.uniform_down_sample(step)
            print(f"After uniform downsampling: {len(cloud_out.points)}")

        if self.table_node is None and mesh:
            pointNormalsFromFaceNormals(cloud_out, mesh)

        if return_views_cloud:
            return cloud_out, views_cloud

        return cloud_out

    def plot(self, cloud=None, mesh_cloud=None):
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
        camera_pose = self.scene.get_pose(self.camera_node)
        camera_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
            0.1, camera_pose[:3, 3]
        )
        camera_frame.rotate(camera_pose[:3, :3])
        geoms = [origin_frame, camera_frame]
        if cloud is not None:
            cloud.paint_uniform_color([0, 0, 1])
            geoms = [cloud] + geoms
        if mesh_cloud is not None:
            cloud.paint_uniform_color([1, 0, 0])
            geoms = [mesh_cloud] + geoms
        draw_geoms(geoms)

    def addSupportPlane(self):
        raise NotImplementedError(
            "addSupportPlane needs to be implemented by sub-classes"
        )


class SingleObjectScene(Scene):
    """Scene with a single object."""

    def __init__(
        self,
        filepath,
        min_extent=0.01,
        max_extent=0.1,
        image_width=640,
        image_height=480,
    ):
        """Construct a scene that contains a single object.

        The object is placed at the origin and randomly scaled.

        Args:
            filepath (str): path to the mesh file that contains the object.
            min_extent (float): min extent of mesh after scaling.
            max_extent (float): max extent of mesh after scaling.
            image_width (float, optional): depth image width.
            image_height (float, optional): depth image height.
        """
        super().__init__(image_width, image_height)
        self.filepath = filepath
        self.base_mesh = trimesh.load(self.filepath)
        self.min_extent = min_extent
        self.max_extent = max_extent
        self.object_extents = []
        self.mesh_node = None
        self.mesh = None

    def resetToRandomScale(self, check_all_extents=True):
        """Reset the scene by randomly rescaling the object mesh."""
        if type(self.max_extent) == list:
            mesh, scale = scaleMeshRandom(
                self.base_mesh, self.min_extent, self.max_extent
            )
        else:
            mesh, scale = scaleMeshRandom(
                self.base_mesh, self.min_extent, self.max_extent
            )
        self.scale = scale
        self.setMesh(mesh)
        self.object_extents = mesh.extents
        print(f"Recaled mesh from extents {self.base_mesh.extents} to {mesh.extents}")

    def resetToScale(self, scale):
        """Reset the scene by scaling the object mesh to the given scale.

        Args:
            scale (float): the scaling factor by which the object is scaled.
        """
        self.scale = scale
        mesh = deepcopy(self.base_mesh)
        extents = mesh.extents

        # If necessary, rescale mesh such that the largest side becomes 1m long.
        if np.any(extents > 1.0):
            print("(Initial rescaling) Mesh is too large (>1m)! Rescaling ...")
            mesh.vertices *= 1.0 / np.max(extents)
            print(f"  Rescaled from {extents} to {mesh.extents}")

        # Actual rescaling.
        mesh.vertices *= scale
        self.setMesh(mesh)
        self.object_extents = mesh.extents
        print(f"(Actual rescaling) Recaled mesh from {extents} to {mesh.extents}")

    def setMesh(self, mesh):
        """Set the object mesh in the scene to the given mesh.

        Args:
            mesh (trimesh.mesh): the mesh.
        """
        if self.mesh_node != None and self.scene.has_node(self.mesh_node):
            self.scene.remove_node(self.mesh_node)
        self.object_extents = mesh.extents
        self.mesh = mesh
        mesh = pr.Mesh.from_trimesh(mesh)
        self.mesh_node = pr.Node(mesh=mesh, matrix=np.eye(4))
        self.scene.add_node(self.mesh_node)

    def setObjectPose(self, pose):
        """Set the object pose to the given pose.

        Args:
            pose (np.array): 4x4 object pose.
        """
        self.scene.set_pose(self.mesh_node, pose)
        self.object_extents = deepcopy(self.scene.extents)

    def createMultiViewPointCloud(
        self,
        look_at=np.zeros(3),
        num_views=30,
        sphere_radius=0.5,
        num_pts=20000,
        voxel_size=0.003,
        return_views_cloud=False,
    ):
        return super().createMultiViewPointCloud(
            look_at,
            num_views,
            sphere_radius,
            num_pts,
            voxel_size,
            return_views_cloud,
            self.mesh,
        )

    def addSupportPlane(self):
        max_ext = np.max([0.1, 2 * np.max(self.object_extents[:2])])
        box_dims = [max_ext, max_ext, 0.002]
        # box_dims = [0.1, 0.1, 0.005]
        # box_dims[:2] = [2 * np.max(self.object_extents[:2])] * 2
        box = o3d.geometry.TriangleMesh.create_box(*box_dims)
        mesh = trimesh.Trimesh(np.asarray(box.vertices), np.asarray(box.triangles))
        mesh = pr.Mesh.from_trimesh(mesh)
        pose = np.eye(4)
        pose[:3, 3] -= 0.5 * np.array(box_dims)
        pose[2, 3] -= 0.5 * self.object_extents[2]
        self.table_node = pr.Node(mesh=mesh, matrix=pose)
        self.scene.add_node(self.table_node)

    def createCompletePointCloud(self, num_pts=20000, add_plane=False):
        mesh_cloud, views_cloud = self.createMultiViewPointCloudNew(
            num_pts=num_pts, return_views_cloud=True, mesh=self.mesh
        )
        # draw_geoms([mesh_cloud], "object cloud")

        if add_plane:
            print("Object cloud:", mesh_cloud)
            self.addSupportPlane()
            plane_z = self.scene.get_pose(self.table_node)[2, 3]
            print("Table plane is at z =", plane_z)
            full_cloud, views_cloud = self.createMultiViewPointCloudNew(
                num_pts=num_pts, return_views_cloud=True
            )
            points = np.asarray(full_cloud.points)
            mask = np.nonzero(points[:, 2] < plane_z + 0.003)[0]
            # full_cloud = full_cloud.select_down_sample(mask)
            full_cloud = full_cloud.select_by_index(mask)
            # draw_geoms([full_cloud], "table plane")
            mesh_cloud += full_cloud
            # draw_geoms([mesh_cloud], "table plane")

        print("Combined cloud:", mesh_cloud)
        if len(mesh_cloud.points) > num_pts:
            step = int(np.ceil(len(mesh_cloud.points) / num_pts))
            mesh_cloud = mesh_cloud.uniform_down_sample(step)
            print("Combined cloud after uniform downsample:", mesh_cloud)

        return mesh_cloud, views_cloud


class ClutterScene(Scene):
    """Scene with multiple overlapping objects."""

    def __init__(self, filepaths, min_extent, max_extent, sample_extents):
        super().__init__()
        self.filepaths = filepaths
        self.base_meshs = [trimesh.load(x) for x in self.filepaths]
        self.min_extent = min_extent
        self.max_extent = max_extent
        self.mesh_nodes = []
        self.sample_extents = sample_extents
        self.max_height = 0.0
        self.scale = 1.0
        self.object_extents = [-1.0, -1.0, -1.0]

    def reset(self, rotate_in_plane):
        """Reset the scene by randomly placing and rescaling the objects."""
        if len(self.mesh_nodes) > 0:
            for i in range(self.mesh_nodes):
                self.scene.remove_node(self.mesh_nodes[i])
            self.mesh_nodes = []
        max_scale = 0.0
        scales = []
        xy = []

        for i, base_mesh in enumerate(self.base_meshs):
            print(f"Scaling and displacing object {i+1}/{len(self.base_meshs)}")
            print(f"  mesh filepath: {self.filepaths[i]}")
            if np.min(base_mesh.extents) < self.min_extent:
                print("  skipping this mesh because min object extent is too small!")
                continue
            tmesh, scale = scaleMeshRandom(base_mesh, self.min_extent, self.max_extent)
            scene_extents = self.sample_extents[1, :] - self.sample_extents[0, :]
            if np.max(tmesh.extents) > np.max(scene_extents):
                print("  skipping this mesh because max object extent is too large!")
                continue
            scales.append(scale)
            if scale > max_scale:
                max_scale = scale
                # print(f"scale: {max_scale:.3f}, instance: {i}")
            mesh = pr.Mesh.from_trimesh(tmesh)
            X = np.eye(4)
            X[:3, 3] = self.sample_extents[0, :] + np.random.rand(3) * (
                self.sample_extents[1, :] - self.sample_extents[0, :]
            )
            X[2, 3] = 0.5 * mesh.extents[2]
            if rotate_in_plane:
                X[:3, :3], is_flipped = randomStableRotation3D(0.5, True)
                if is_flipped:
                    X[2, 3] = abs(mesh.bounds[0, 0])
            xy.append(X[:2, 3])
            self.mesh_nodes.append(pr.Node(mesh=mesh, matrix=X))
            self.scene.add_node(self.mesh_nodes[-1])
            if mesh.extents[2] > self.max_height:
                self.max_height = mesh.extents[2]
        self.scale = max_scale
        # pr.Viewer(self.scene, use_raymond_lighting=True)
        # print(f"scales:\n{np.array(scales)}")
        # print(f"positions on plane:\n{np.array(xy)}")

    def addSupportPlane(self):
        max_ext = np.max(
            [0.3, 2 * (np.max(self.sample_extents) - np.min(self.sample_extents))]
        )
        box_dims = [max_ext, max_ext, 0.002]
        box = o3d.geometry.TriangleMesh.create_box(*box_dims)
        mesh = trimesh.Trimesh(np.asarray(box.vertices), np.asarray(box.triangles))
        mesh = pr.Mesh.from_trimesh(mesh)
        pose = np.eye(4)
        pose[:2, 3] -= 0.5 * np.array(box_dims[:2])
        extents = [self.scene.get_pose(m)[2, 3] for m in self.mesh_nodes]
        # pose[2, 3] -= 0.5 * self.max_height
        print(f"extents: {extents}, pose[2,3]: {pose[2,3]:.3f}")
        self.table_node = pr.Node(mesh=mesh, matrix=pose)
        self.scene.add_node(self.table_node)

    def createCompletePointCloud(self, num_pts=20000, add_plane=False):
        mesh_cloud, views_cloud = self.createMultiViewPointCloudNew(
            num_pts=num_pts, return_views_cloud=True)
        # draw_geoms([mesh_cloud], "object cloud")

        if add_plane:
            print("Object cloud:", mesh_cloud)
            self.addSupportPlane()
            plane_z = self.scene.get_pose(self.table_node)[2, 3]
            print("Table plane is at z =", plane_z)
            full_cloud, views_cloud = self.createMultiViewPointCloudNew(
                num_pts=num_pts, return_views_cloud=True
            )
            points = np.asarray(full_cloud.points)
            mask = np.nonzero(points[:, 2] < plane_z + 0.003)[0]
            # full_cloud = full_cloud.select_down_sample(mask)
            full_cloud = full_cloud.select_by_index(mask)
            # draw_geoms([full_cloud], "table plane")
            mesh_cloud += full_cloud
            # draw_geoms([mesh_cloud], "complete cloud")

        print("Combined cloud:", mesh_cloud)
        if len(mesh_cloud.points) > num_pts:
            step = int(np.ceil(len(mesh_cloud.points) / num_pts))
            mesh_cloud = mesh_cloud.uniform_down_sample(step)
            print("Combined cloud after uniform downsample:", mesh_cloud)

        return mesh_cloud, views_cloud

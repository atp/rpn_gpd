"""Evaluate a grasp proposal generator.

Given an object set and a list of object categories, calculate the average metrics 
(precision, recall, etc) for a grasp proposal generator. This program can only evaluate 
geometry-based proposal generators, e.g., the first step in GPD.

"""

import sys
from argparse import ArgumentParser
from copy import deepcopy
from time import time

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from grasp_scene_data import GraspData, SceneData, storeGrasps
from ground_truth import findGroundTruthPositives
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandEval, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from learning import Metrics
from object_set import ThreeDNetObjectSet
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import preprocessPointCloud
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes
from utils.plot import plotColoredHands

cube_size = 1.0
min_object_extent = (0.01,)
max_object_extent = [0.2, 0.7]

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Evaluate a proposal generator")
    parser.add_argument("objects_dir", help="path to objects directory")
    parser.add_argument(
        "categories_file",
        help="path to TXT file with object categories",
    )
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument(
        "-ni",
        "--num_instances",
        default=10,
        type=int,
        help="number of object instances per category",
    )
    parser.add_argument(
        "-nv",
        "--num_views",
        default=5,
        type=int,
        help="number of viewpoints per object instance",
    )
    parser.add_argument(
        "-n", "--num_samples", default=100, type=int, help="number of samples"
    )

    return parser


def createHandsGenerator(method, hand_geom, antip_prms):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms)
        gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def createRandomCameraPose(scene, camera_distance=0.5, look_at=np.zeros(3)):
    x = np.random.normal(size=3)
    x = x / np.linalg.norm(x) * camera_distance
    camera_pose = calcLookAtPose(x, look_at)
    return camera_pose


def evalViewpoint(mesh, scene, camera_pose, generator, num_samples):
    cloud = scene.createProcessedPointCloudFromView(camera_pose)

    # Draw sample points from the point cloud.
    samples = sampleFromCube(cloud, cube_size, num_samples)

    # Generate the grasp poses.
    t = time()
    hands = generator.generateHands(cloud, samples, camera_pose=camera_pose)[0]
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")

    # Generate the ground truth.
    proposals = hands[:, :, 0] == 1
    grasps = generator.generateHands(mesh, samples, camera_pose=camera_pose)[0]
    positive_grasps = (grasps[:, :, 0] == 1) & (grasps[:, :, 1] == 1)
    labels = positive_grasps.astype(int).flatten()
    predictions = proposals.astype(int).flatten()
    metrics = Metrics(labels, predictions)

    return metrics


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # Load the object set.
    object_set = ThreeDNetObjectSet(
        args.objects_dir, args.categories_file, rescale=True
    )

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antip_prms = AntipodalParams(**antipodal_params)
    generator = createHandsGenerator(args.method, hand_geom, antip_prms)

    metrics_list = []

    for i in range(len(object_set.objects)):
        for j in range(args.num_instances):
            filepath = object_set.get([i, j])
            scene = SingleObjectScene(filepath, min_object_extent, max_object_extent)
            scene.resetToRandomScale()
            mesh = scene.createMultiViewPointCloud()
            for k in range(args.num_views):
                camera_pose = createRandomCameraPose(scene)
                metrics = evalViewpoint(
                    mesh, scene, camera_pose, generator, args.num_samples
                )
                metrics_list.append(metrics)
                print(i, j, k, metrics)

    recalls = np.array([x.recall() for x in metrics_list])
    precisions = np.array([x.precision() for x in metrics_list])
    recall = np.mean(recalls)
    precision = np.mean(precisions)
    print("\n==================================================================")
    print(f"avg precision: {precision:.3f}, recall: {recall:.3f}")


if __name__ == "__main__":
    main()

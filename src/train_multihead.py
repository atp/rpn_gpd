"""Train a model for a binary classification problem.

"""

import os
import sys
from argparse import ArgumentParser

import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
from multihead_trainer import MultiheadTrainer, MultiheadTrainerSplit
from torch.optim.lr_scheduler import ExponentialLR
from torch.utils import data as torchdata

from datasets import BasicDataset
from learning import Metrics
from networks.multihead import MultiheadLenet, MultiheadVgg
from networks.network import LenetLike, VggLike
from trainer import Trainer
from utils.db_utils import plotBatch

loader_params = {
    "batch_size": 64,
    #    'batch_size': 256, # does not fit onto GPU
    # 'batch_size': 128,
    "shuffle": True,
    "pin_memory": True,
    "num_workers": 4,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}

sgd_params = {
    #    'lr': 0.1, # Resnet
    # 'lr': 0.08,
    # "lr": 0.05,  # Lenet
    "lr": 0.01,  # Lenet
    # "lr": 0.001,
    # "lr": 0.0001,
    "momentum": 0.9,
    "weight_decay": 0.001
    # "weight_decay": 0.0001
    # "weight_decay": 0.00001,
}
exp_lr_decay = 0.96

lenet_params = {
    "replace_pool_with_conv": False,
    "use_batchnorm": False,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    # Params for Lenet
    "conv": [32, 64],
    # "fc": [500],
    # "fc": [32],
    # "fc": [64],
    # "fc": [256],
    # "fc": [512],
    "fc": [512, 512],
    # "fc": [500],
    "conv_filter_size": 5,
}

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set autmatically
    "conv": [32, 64, 128],
    "fc": [500],
    "conv_filter_size": 3,
}

net_params_dict = {"lenet": lenet_params, "vgg": vgg_params}

configs = {
    "cls": 2,
    "rot": -1,
    "gc": 2,
}

network_models = {
    "lenet": MultiheadLenet,
    "vgg": MultiheadVgg,
}


np.set_printoptions(suppress=True, precision=3)


class AddGaussianNoise:
    def __init__(self, mean=0.0, std=1.0):
        self.std = std
        self.mean = mean

    def __call__(self, x: torch.tensor):
        return x + self.mean + torch.randn(x.size()) * self.std

    def __repr__(self):
        return self.__class__.__name__ + "(mean={0}, std={1})".format(
            self.mean, self.std
        )


def plotBatches(loader, num_batches):
    pad1 = torch.nn.ConstantPad2d((4, 4, 4, 4), 1)
    pad0 = torch.nn.ConstantPad2d((4, 4, 4, 4), 0)
    i = 0
    # names = ['gen_images', 'gc_images']
    names = ["images"]

    with torch.no_grad():
        for batch in loader:
            plotBatch(batch[0], batch[1], pad1, pad0, names[0])
            # Iterate over both GEN and GC images.
            # for j in range(len(batch) - 1):
            # plotBatch(batch[j], batch[-1], pad1, pad0, names[j])
            i += 1
            if i == num_batches:
                break


def initWeights(m):
    if isinstance(m, torch.nn.Linear) or isinstance(m, torch.nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight, nonlinearity="relu")


def createDatasets(train_path, test_path, num_classes):
    # This is the mean calculated using calc_dataset_mean_std.py.
    transform = transforms.Normalize(
        mean=[0.0724, 0.0793, 0.0831], std=[0.2019, 0.2076, 0.2012]
    )

    # Add Gaussian noise.
    # transform = AddGaussianNoise(0.0, 0.5)
    # transform = AddGaussianNoise(0.0, 0.2)

    # Load the train and test sets.
    # train_db = BasicDataset(train_filepath, net_params["out_shape"])
    # test_db = BasicDataset(test_filepath, net_params["out_shape"])

    train_db = BasicDataset(train_path, num_classes, transform=transform)
    test_db = BasicDataset(test_path, num_classes, transform=transform)

    return train_db, test_db


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Train multihead network")
    parser.add_argument("train_path", help="path to train database (*.h5)")
    parser.add_argument("test_path", help="path to test database (*.h5)")
    parser.add_argument("out_dir", help="path to models output directory")
    parser.add_argument("network", help="<lenet> or <vgg>")
    parser.add_argument(
        "-c", "--num_classes", help="number of classes", type=int, default="2"
    )
    parser.add_argument(
        "-e", "--num_epochs", help="number of epochs", type=int, default="10"
    )
    parser.add_argument(
        "-r", "--num_rounds", help="number of rounds", type=int, default="10"
    )
    parser.add_argument(
        "-b",
        "--num_batches_plot",
        type=int,
        help="number of batches to be plotted before training",
        default="0",
    )

    return parser


def createNetwork(model_name, parameters, device):
    model = network_models[model_name](**parameters)
    model.apply(initWeights)
    print("Copying network to GPU ...")
    model.to(device)

    return model


def classRatio(database):
    pos_per_col = torch.sum(database.labels == 1, 0).float()
    neg_per_col = torch.sum(database.labels == 0, 0).float()
    print(f"neg_per_col:\n{neg_per_col.numpy()}")
    print(f"pos_per_col:\n{pos_per_col.numpy()}")
    ratio = torch.mean(pos_per_col / neg_per_col)
    print("avg class ratio (pos/neg):", class_ratio)

    return ratio


def main() -> None:
    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()
    print(f"Command line arguments:\n{args}")

    # Create the train and test datasets.
    train_db, test_db = createDatasets(
        args.train_path, args.test_path, args.num_classes
    )

    # Plot some batches.
    if args.num_batches_plot > 0:
        test_loader = torchdata.DataLoader(test_db, **loader_params)
        plotBatches(test_loader, args.num_batches_plot)

    # Use GPU if available.
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print(device)

    # Create the network.
    net_params = net_params_dict[args.network]
    imgs_shape = test_db.images.shape[-3:]
    net_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    net_params["out_shape"] = args.num_classes
    network = createNetwork(args.network, net_params, device)
    print(network)

    # Setup the loss function, the optimizer, and the learning rate scheduler.
    criterion = nn.CrossEntropyLoss()
    labels_dtype = torch.long
    optimizer = optim.SGD(network.parameters(), **sgd_params)
    # optimizer = optim.Adam(
    #    network.parameters(), sgd_params["lr"], weight_decay=sgd_params["weight_decay"]
    # )
    scheduler = ExponentialLR(optimizer, exp_lr_decay)

    # Train the network.
    trainer = MultiheadTrainerSplit(
        network, criterion, optimizer, scheduler, device, args.out_dir, labels_dtype
    )
    # trainer = MultiheadTrainer(
    # network, criterion, optimizer, scheduler, device, model_dir, labels_dtype)
    metrics = trainer.trainLoop(train_db, test_db, args.num_rounds, args.num_epochs)
    output = [[x.accuracy(), x.precision(), x.recall()] for x in metrics]
    np.save(args.out_dir + "metrics.npy", np.asarray(output))


if __name__ == "__main__":
    main()

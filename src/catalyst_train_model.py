import json
from argparse import ArgumentParser

import numpy as np
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
from catalyst import dl, utils
from catalyst.contrib.nn import FocalLossBinary, OneCycleLRWithWarmup
from torch.utils.data import DataLoader, TensorDataset

from datasets import BasicDataset
from networks.network import LenetLike, VggLike
from networks.resnet_mod import resnet18, resnet34

lenet_params = {
    "replace_pool_with_conv": False,
    # "use_batchnorm": False,
    "use_batchnorm": True,
    "out_shape": 2,
    "in_shape": None,  # set automatically
    # "conv": [32, 64],
    # "fc": [512],
    "conv": [64, 128],
    "fc": [512, 512],
    # "conv": [32, 64],
    # "fc": [512, 512],
    # "conv": [6, 16],
    # "fc": [64],
    # "conv": [32, 64, 64],
    # "fc": [128, 128],
    "conv_filter_size": 5,
}

# lenet_params = {
# "replace_pool_with_conv": False,
# "use_batchnorm": True,
# "out_shape": 2,
# "in_shape": None,  # set automatically
# # "conv": [32, 64],
# "conv": [64, 64],
# "fc": [512],
# # "conv": [6, 16],
# # "fc": [64],
# # "conv": [32, 64, 64],
# # "fc": [128, 128],
# "conv_filter_size": 5,
# }

# vgg_params = {
# "use_dropout": False,
# # "use_dropout": True,
# "out_shape": 2,
# "in_shape": None,  # set autmatically
# "conv": [32, 32, 32],
# "fc": [32, 32, 32],
# # "conv": [64, 128, 256],
# # "fc": [512, 512],
# # "fc": [512, 512],
# "conv_filter_size": 3,
# }

"""
vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set automatically
    "conv": [128, 128, 128],
    "fc": [512],
    "conv_filter_size": 3,
}
"""

vgg_params = {
    "use_dropout": False,
    # "use_dropout": True,
    "out_shape": 2,
    "in_shape": None,  # set automatically
    "conv": [32, 64, 128],
    "fc": [512],
    "conv_filter_size": 3,
}

sgd_params = {
    # "lr": 0.1,  # Resnet
    # "lr": 0.01,  # Lenet
    "lr": 0.001,  # set by command line argument
    # "lr": 0.0001,
    "momentum": 0.9,
    # "weight_decay": 0.01
    # "weight_decay": 0.001,
    # "weight_decay": 0.0001,
    # "weight_decay": 0.00005,
    # kaiming
    # "weight_decay": 0.00001,  # try 1
    # "weight_decay": 0.001,  # try 2
    # "weight_decay": 0.0001,  # try 3
    # "weight_decay": 0.01,  # try 3
    # "weight_decay": 0.1,
    # Jan2021
    # "weight_decay": 0.0005,
    # "weight_decay": 0.00005,
    # ==================================================================
    # "weight_decay": 0.00001, # Monday #1
    # "weight_decay": 0.0001, # Monday #2 wd1
    "weight_decay": 0.001, # Monday #3 wd2
    "nesterov": True,
}

# Parameters from train_net3.py in github.com/atenpas/gpd
# sgd_params = {
# "lr": 0.001,
# "weight_decay": 0.0005,
# "momentum": 0.9,
# }

model_params_dict = {
    "lenet": lenet_params,
    "vgg": vgg_params,
    "resnet18": {},
    "resnet34": {},
}
models = {
    "lenet": LenetLike,
    "vgg": VggLike,
    "resnet18": resnet18,
    "resnet34": resnet34,
}

loader_params = {
    "batch_size": 64,  # Lenet
    # "batch_size": 256,  # Resnet
    # 'batch_size': 128,
    "shuffle": True,
    "pin_memory": True,
    "num_workers": 8,
    "drop_last": True,  # avoids a problem with batch norm when batch has size one
}


def initWeights(m):
    if isinstance(m, torch.nn.Linear) or isinstance(m, torch.nn.Conv2d):
        torch.nn.init.kaiming_normal_(m.weight, nonlinearity="relu")


class WeightedFocalLoss(torch.nn.Module):
    "Non weighted version of Focal Loss"

    def __init__(self, alpha=0.25, gamma=2):
        super(WeightedFocalLoss, self).__init__()
        self.alpha = torch.tensor([alpha, 1 - alpha]).cuda()
        self.gamma = gamma

    def forward(self, inputs, targets):
        if targets.ndim == 1:
            targets = torch.unsqueeze(targets, 1)
        BCE_loss = F.binary_cross_entropy_with_logits(inputs, targets, reduction="none")
        targets = targets.type(torch.long)
        at = self.alpha.gather(0, targets.data.view(-1))
        pt = torch.exp(-BCE_loss)
        F_loss = at * (1 - pt) ** self.gamma * BCE_loss
        return F_loss.mean()


class BCEWithLogitsLoss_OHEM(torch.nn.BCEWithLogitsLoss):
    def __init__(self, ratio):
        super(BCEWithLogitsLoss_OHEM, self).__init__(None, True)
        self.ratio = ratio

    def forward(self, x, y, ratio=None):
        if ratio is not None:
            self.ratio = ratio
        num_inst = x.size(0)
        num_hns = int(self.ratio * num_inst)
        x_ = x.clone()
        inst_losses = torch.autograd.Variable(torch.zeros(num_inst)).cuda()
        for idx, label in enumerate(y.data):
            # print(idx, label, x.data.shape, y.shape)
            # print(x.data[idx])
            # inst_losses[idx] = -x_.data[idx, label]
            inst_losses[idx] = torch.sum(x_.data[idx])
        # print("inst_losses:", inst_losses.shape, num_hns)
        _, idxs = inst_losses.topk(num_hns)
        x_hn = x.index_select(0, idxs)
        y_hn = y.index_select(0, idxs)

        return torch.nn.functional.binary_cross_entropy_with_logits(x_hn, y_hn)


def parseArgs() -> ArgumentParser:
    """Create a command line argument parser.

    Returns:
        ArgumentParser: the parser.
    """
    parser = ArgumentParser(description="Train a model with Catalyst")
    parser.add_argument("train", help="path to training database")
    parser.add_argument("test", help="path to test database")
    parser.add_argument("out", help="path to output directory for weight files")
    parser.add_argument("task", help="task")
    parser.add_argument("architecture", help="model architecture")
    parser.add_argument(
        "-e", "--epochs", type=int, default=100, help="number of epochs"
    )
    parser.add_argument(
        "-wl",
        "--weighted_loss",
        action="store_true",
        help="if the loss should be weighted based on the training distribution",
    )
    parser.add_argument("-fl", "--focal_loss", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-lr", "--learning_rate", type=float, default=0.01)
    parser.add_argument("-cp", "--checkpoint", help="load model from checkpoint")
    parser.add_argument("-s", "--scheduler", default="exp", help="none, exp")
    parser.add_argument("-ki", "--use_kaiming_init", action="store_true")
    parser.add_argument("--mean", type=float, nargs="+", help="dataset mean")
    parser.add_argument("--std", type=float, nargs="+", help="dataset std")
    parser.add_argument("-ohem", "--online_hard_example_mining", action="store_true")

    args = parser.parse_args()

    return args


def main():
    # Read command line arguments.
    args = parseArgs()

    # Load the data.
    num_classes = 2 if args.task == "cls" else -1

    transform = None
    if args.mean is not None and args.std is not None:
        mean = [float(x) for x in args.mean]
        std = [float(x) for x in args.std]
        transform = transforms.Normalize(mean=mean, std=std)
        print("transform:", transform)

    train_db = BasicDataset(args.train, num_classes, transform=transform)
    test_db = BasicDataset(args.test, num_classes, transform=transform)
    if num_classes == -1:
        num_classes = train_db.labels.shape[1]

    # Create the model.
    model_params = model_params_dict[args.architecture]
    imgs_shape = test_db.images.shape[-3:]
    model_params["in_shape"] = (imgs_shape[-1],) + imgs_shape[:-1]
    if args.task == "cls":
        model_params["out_shape"] = 2
    else:
        model_params["out_shape"] = test_db.labels.shape[1]
    if "resnet" in args.architecture:
        in_channels = model_params["in_shape"][0]
        num_classes = model_params["out_shape"]
        model = models[args.architecture](
            in_channels=in_channels, num_classes=num_classes
        )
    else:
        model = models[args.architecture](**model_params)

    # Use Kaiming initialization for all layers.
    if args.use_kaiming_init:
        print("Using Kaiming initialization for weights")
        model.apply(initWeights)

    if args.checkpoint:
        checkpoint = torch.load(args.checkpoint)
        if type(checkpoint) == dict:
            model.load_state_dict(checkpoint["model_state_dict"])
        else:
            model.load_state_dict(checkpoint)
        print(f"Loaded weights from: {args.checkpoint}")

    # Choose device to use (TPU > GPU > CPU).
    device = utils.get_device()
    model.to(device)
    print("Using device:", device)
    print(f"Created model:\n + {model}")

    # train_db.images = train_db.images.permute(0, 3, 1, 2) / 255.0
    # test_db.images = test_db.images.permute(0, 3, 1, 2) / 255.0

    if args.weighted_loss:
        pos_per_col = torch.sum(train_db.labels == 1, 0).float()
        neg_per_col = torch.sum(train_db.labels == 0, 0).float()
        class_ratio = torch.mean(pos_per_col / neg_per_col)
        print(f"neg_per_col:\n{neg_per_col.numpy()}")
        print(f"pos_per_col:\n{pos_per_col.numpy()}")
        print("avg class ratio (pos/neg):", class_ratio)

    # if args.task == "cls":
    # train_db = TensorDataset(train_db.images, train_db.labels.to(torch.int64))
    # test_db = TensorDataset(test_db.images, test_db.labels.to(torch.int64))
    # else:
    # train_db = TensorDataset(train_db.images, train_db.labels.to(torch.float32))
    # test_db = TensorDataset(test_db.images, test_db.labels.to(torch.float32))

    train_loader = DataLoader(train_db, **loader_params)
    test_loader = DataLoader(test_db, **loader_params)
    loaders = {"train": train_loader, "valid": test_loader}

    # Setup the loss.
    if args.task == "cls":
        if args.weighted_loss:
            weights = torch.tensor([1.0 / class_ratio, class_ratio])
            criterion = torch.nn.CrossEntropyLoss(weight=weights.to(device))
            print(f"Loss function is using weights: {weights}")
        elif args.focal_loss:
            criterion = FocalLossBinary()
            # criterion = WeightedFocalLoss()
        else:
            criterion = torch.nn.CrossEntropyLoss()
    else:
        if args.weighted_loss:
            pos_weight = neg_per_col / np.maximum(pos_per_col, 1)
            # pos_weight = torch.ones(len(neg_per_col)) * 6.0
            criterion = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight.to(device))
            print(f"Loss function is using weights: {pos_weight}")
        else:
            criterion = torch.nn.BCEWithLogitsLoss()
        if args.online_hard_example_mining:
            criterion = BCEWithLogitsLoss_OHEM(0.5)

    # Setup the optimizer and learning rate scheduler depending on the network architecture.
    sgd_params["lr"] = args.learning_rate
    if "resnet" in args.architecture:
        scheduler = None
        optimizer = torch.optim.Adam(model.parameters())
    else:
        optimizer = torch.optim.SGD(model.parameters(), **sgd_params)
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.96)
    # optimizer = torch.optim.Adam(model.parameters())
    # scheduler = None

    # Setup the scheduler.
    if args.scheduler == "exp":
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=0.96)
    elif args.scheduler == "none":
        scheduler = None

    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)
    # scheduler = torch.optim.lr_scheduler.MultiStepLR(optimizer, [10, 20])
    # scheduler = OneCycleLRWithWarmup(
    # optimizer, num_steps=args.epochs, lr_range=(0.001, 0.0001), warmup_steps=1
    # )

    if args.task == "cls":
        callbacks = [
            dl.AccuracyCallback(input_key="logits", target_key="targets", num_classes=2)
        ]
        # callbacks += [dl.PrecisionRecallF1ScoreCallback(num_classes=2)]
        # callbacks += [dl.AveragePrecisionCallback()]
    else:
        callbacks = [
            dl.MultilabelAccuracyCallback(
                input_key="logits", target_key="targets", threshold=0.5
            )
        ]
        # callbacks += [
            # dl.MultilabelPrecisionRecallF1SupportCallback(
                # input_key="logits",
                # target_key="targets",
                # num_classes=model_params["out_shape"],
            # )
        # ]
        # callbacks += [dl.PrecisionRecallF1ScoreCallback(num_classes=num_classes)]
    callbacks += [dl.CheckpointCallback(logdir=args.out, save_n_best=3)]

    # Store the SGD and network parameters.
    with open(args.out + "settings.json", "w") as f:
        model_params["architecture"] = args.architecture
        sgd_params["name"] = str(optimizer)
        json.dump(
            {
                "cmd_args": str(args),
                "train": args.train,
                "test": args.test,
                "optimizer": sgd_params,
                "model": model_params,
                "loader": loader_params,
            },
            f,
        )

    # Train the model.
    runner = dl.SupervisedRunner()
    runner.train(
        model=model,
        criterion=criterion,
        optimizer=optimizer,
        scheduler=scheduler,
        loaders=loaders,
        logdir=args.out,
        num_epochs=args.epochs,
        callbacks=callbacks,
        verbose=args.verbose,
    )


if __name__ == "__main__":
    main()

"""Module for grasp data generators.

A grasp data generator creates data that can be used to train a grasp detector.

Variables used in method docstrings denote:
    n - number of samples,
    m - number of grasp orientations,
    k - length of grasp geometry descriptor (usually 5),
    h - image height,
    w - image width,
    c - number of image channels.
"""

from abc import ABC, abstractmethod
from time import time

import matplotlib.pyplot as plt
import numpy as np
import tables

from grasp_image import GraspImageGenerator
from hand_params import antipodal_params, hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from proposal import ProposalImageGenerator


class GraspDataGenerator(ABC):
    """Abstract base class for grasp data generators."""

    @abstractmethod
    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for a given cloud at a given set of sample points.

        The grasp data depends on the grasp candidates generator. Typically, it
        consists of images and labels. The labels are calculated by evaluating the
        grasps against a given object mesh.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.
        """
        raise NotImplementedError("This is an abstract method.")

    def setLabelsDatabase(self, path, node="/labels"):
        with tables.open_file(path, "r") as f:
            self.known_labels = np.array(f.get_node(node))
        print(
            f"Loaded pre-computed labels of shape {self.known_labels.shape} from "
            + f"database {path}"
        )


class GraspDataGeneratorGPD(GraspDataGenerator):
    """Create grasp images, grasp proposals, and grasp labels for GPD.

    Attributes:
        prop_gen (GpdHandsGenerator): GPD grasp proposal generator.
        grasp_img_gen (GraspImageGenerator): grasp image generator.
        num_subsamples (int): the number of subsamples to be drawn.
    """

    def __init__(self, grasp_img_shape, num_subsamples=-1):
        """Construct a grasp data generator for GPD.

        Args:
            grasp_img_shape (tuple): the shape of the grasp image, (h, w, c).
            num_subsamples (int, optional): the number of grasp proposals to be
                subsampled.
        """
        self.prop_gen = GpdHandsGenerator(
            HandGeometry(**hand_params), AntipodalParams(**antipodal_params),
        )
        self.grasp_img_gen = GraspImageGenerator(grasp_img_shape, self.prop_gen)
        self.num_subsamples = num_subsamples

    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for GPD.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.

        Returns:
            dict: the grasp data as a dictionary with keys "labels", "props",
                that store np.arrays of shapes n x m x k and n x m. The shape of
                "images" is n x m x h x w x c if ``self.num_subsamples`` is less than
                one and n x h x w x c otherwise.
        """
        # 1. Generate ground truth.
        time_labels = time()
        labels, _ = self.prop_gen.generateHands(mesh, samples, check_antipodal=True)
        time_labels = time() - time_labels
        print(f"[GPD] Created {labels.shape} labels in {time_labels:.3f}s.")

        return self.createImageData(cloud, samples, labels)

    def createImageData(self, cloud, samples, labels=[]):
        # 1. Generate grasp candidates.
        time_props = time()
        grasps, frames = self.prop_gen.generateHands(
            cloud, samples, check_antipodal=False
        )
        props = self.prop_gen.findProposals(grasps)
        time_props = time() - time_props
        print(f"[GPD] Created {props.shape} proposals in {time_props:.3f}s.")
        print(f"[GPD] Proposals array contains {np.count_nonzero(props)} ones.")

        # 2. Subsample the grasp candidates if desired and create the grasp images.
        time_imgs = time()
        if self.num_subsamples > 0:
            num_subsamples = np.min([self.num_subsamples, len(samples)])
            idxs = subsampleProposals(labels, num_subsamples)
            rots = self.prop_gen.calcRotations(idxs[1])
            imgs = self.grasp_img_gen.calcImagesAtIndicesSP(
                cloud, samples, idxs, rots, frames
            )
            # imgs = self.grasp_img_gen.calcImagesAtIndices(
            # cloud, samples, idxs, rots, frames
            # )
            labels = labels[idxs[0], idxs[1]]
            props = props[idxs[0], idxs[1]]
        else:
            idxs = np.arange(self.prop_gen.getNumHandsPerSample())
            rots = self.prop_gen.calcRotations(idxs)
            imgs = self.grasp_img_gen.calcImages(cloud, samples, rots, frames)
        time_imgs = time() - time_imgs
        print(f"[GPD] Created {imgs.shape} grasp images in {time_imgs:.3f}s.")

        return {"images": imgs, "proposals": props, "labels": labels, "frames": frames}


class GraspDataGeneratorMA(GraspDataGenerator):
    """Create proposal images, grasp images and their labels for MA.

    Attributes:
        prop_gen (AnchorHandsGenerator): grasp proposal generator.
        prop_img_gen (NormalsAndDepthImageGenerator): MA proposal image generator.
        grasp_img_gen (GraspImageGenerator): grasp image generator.
    """

    def __init__(self, prop_params, grasp_img_shape, use_open_gl_frame, prop_gen=None):
        """Construct a grasp data generator for MA.

        Args:
            prop_params (dict): the parameters for the grasp proposal images.
            grasp_img_shape (tuple): the shape of the grasp image,
                (height, width, channels).
            use_open_gl_frame (bool): if the OpenGL frame convention is used.
            prop_gen (HandsGenerator, optional): the grasp proposal generator. Default
                is MA proposals.
        """
        if prop_gen == None:
            self.prop_gen = MaHandsGenerator(
                HandGeometry(**hand_params),
                AntipodalParams(**antipodal_params),
                use_open_gl_frame=True,
            )
        else:
            self.prop_gen = prop_gen
        self.prop_img_gen = ProposalImageGenerator(**prop_params)
        self.grasp_img_gen = GraspImageGenerator(grasp_img_shape, self.prop_gen)
        self.rots = self.prop_gen.calcRotations(
            np.arange(self.prop_gen.getNumHandsPerSample())
        )

    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for MA.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.

        Returns:
            dict: the grasp data as a dictionary with keys "prop_imgs", "grasp_imgs",
                and "labels" that store np.arrays of shapes n x h x w x c1,
                n x m x h x w x c2, and n x m x k.
        """
        # 1. Generate ground truth.
        time_labels = time()
        labels, _ = self.prop_gen.generateHands(mesh, samples, camera_pose=camera_pose)
        time_labels = time() - time_labels
        print(f"[MA] Created {labels.shape} labels in {time_labels:.3f}s.")

        return {"labels": labels, **self.createImageData(cloud, samples, camera_pose)}

    def createImageData(self, cloud, samples, camera_pose):
        # 1. Create proposal images.
        time_prop_imgs = time()
        prep_cloud, prep_samples = self.prop_gen.preprocess(cloud, samples, camera_pose)
        prop_imgs = self.prop_img_gen.createImages(prep_cloud, prep_samples)
        time_prop_imgs = time() - time_prop_imgs
        print(
            f"[MA] Created {prop_imgs.shape} proposal images "
            + f"in {time_prop_imgs:.3f}s."
        )

        # 2. Create grasp images.
        time_grasp_imgs = time()
        grasp_imgs = self.grasp_img_gen.calcImages(prep_cloud, prep_samples, self.rots)
        time_grasp_imgs = time() - time_grasp_imgs
        print(
            f"[MA] Created {grasp_imgs.shape} grasp images "
            + f"in {time_grasp_imgs:.3f}s."
        )
        print("======================================================================")

        data = {"prop_imgs": prop_imgs, "grasp_imgs": grasp_imgs}

        return data


class GraspDataGeneratorMAProp(GraspDataGenerator):
    """Create proposal images and their labels for MA.

    Attributes:
        prop_gen (MaHandsGenerator): MA grasp proposal generator.
        prop_img_gen (NormalsAndDepthImageGenerator): MA proposal image generator.
    """

    def __init__(self, prop_params, use_open_gl_frame, prop_gen=None):
        """Construct a grasp data generator for proposal images for MA.

        Args:
            prop_params (dict): the parameters for the grasp proposal generator.
            use_open_gl_frame (bool): if the OpenGL frame convention is used.
            prop_gen (HandsGenerator, optional): the grasp proposal generator. Default
                is MA proposals.
        """
        if prop_gen == None:
            self.prop_gen = MaHandsGenerator(
                HandGeometry(**hand_params),
                AntipodalParams(**antipodal_params),
                use_open_gl_frame=True,
            )
        else:
            self.prop_gen = prop_gen
        self.prop_img_gen = ProposalImageGenerator(**prop_params)
        self.rots = self.prop_gen.calcRotations(
            np.arange(self.prop_gen.getNumHandsPerSample())
        )

    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for MA.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.

        Returns:
            dict: the grasp data as a dictionary with keys "prop_imgs" and "labels"
                that store np.arrays of shape n x h x w x c and n x m x k.
        """
        # 1. Generate ground truth.
        time_labels = time()
        labels, _ = self.prop_gen.generateHands(mesh, samples, camera_pose=camera_pose)
        time_labels = time() - time_labels
        print(f"[MA] Created {labels.shape} labels in {time_labels:.3f}s.")

        # 2. Create proposal images.
        time_prop_imgs = time()
        prep_cloud, prep_samples = self.prop_gen.preprocess(cloud, samples, camera_pose)
        imgs = self.prop_img_gen.createImages(prep_cloud, prep_samples)
        time_imgs = time() - time_prop_imgs
        print(f"[MA] Created {imgs.shape} proposal images in {time_imgs:.3f}s.")

        data = {"images": imgs, "labels": labels}

        return data


class GraspDataGeneratorMAGrasp(GraspDataGenerator):
    """Create grasp images and their labels for MA.

    Attributes:
        prop_gen (MaHandsGenerator): MA grasp proposal generator.
        grasp_img_gen (GraspImageGenerator): grasp image generator.
    """

    def __init__(
        self, grasp_img_shape, num_subsamples, use_open_gl_frame, prop_gen=None
    ):
        """Construct a grasp data generator for grasp images for MA.

        Args:
            grasp_img_shape (tuple): the shape of the grasp image,
                (height, width, channels).
            num_subsamples (int): the number of subsamples to be drawn.
            use_open_gl_frame (bool): if the OpenGL frame convention is used.
        """
        if prop_gen == None:
            self.prop_gen = MaHandsGenerator(
                HandGeometry(**hand_params),
                AntipodalParams(**antipodal_params),
                use_open_gl_frame,
            )
        else:
            self.prop_gen = prop_gen
        self.grasp_img_gen = GraspImageGenerator(grasp_img_shape, self.prop_gen)
        self.num_subsamples = num_subsamples
        self.rots = self.prop_gen.calcRotations(
            np.arange(self.prop_gen.getNumHandsPerSample())
        )

    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for MA.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.

        Returns:
            dict: the grasp data as a dictionary with keys "images" and "labels" that
                store np.arrays of shapes n x h x w x c and n x k.
        """
        # 1. Generate ground truth.
        time_labels = time()
        labels, _ = self.prop_gen.generateHands(mesh, samples, camera_pose=camera_pose)
        time_labels = time() - time_labels
        print(f"[MA] Created {labels.shape} labels in {time_labels:.3f}s.")

        # 2. Subsample the proposals.
        num_subsamples = np.min([self.num_subsamples, len(samples)])
        idxs = subsampleProposals(labels, num_subsamples)
        rots = self.prop_gen.calcRotations(idxs[1])

        # 3. Create grasp images.
        time_imgs = time()
        prep_cloud, prep_samples = self.prop_gen.preprocess(cloud, samples, camera_pose)
        imgs = self.grasp_img_gen.calcImagesAtIndices(
            prep_cloud, prep_samples, idxs, rots, []
        )
        time_imgs = time() - time_imgs
        print(f"[MA] Created {imgs.shape} grasp images in {time_imgs:.3f}s.")
        print("======================================================================")

        data = {"images": imgs, "labels": labels[idxs[0], idxs[1]], "rot_idxs": idxs[1]}

        return data


class GraspDataGeneratorCombined(GraspDataGenerator):
    """Create images and labels for both GPD and MA.

    Attributes:
        gpd_gen (GraspDataGeneratorGPD): grasp data generator for GPD.
        ma_gen (GraspDataGeneratorMA): grasp data generator for MA.
    """

    def __init__(self, gpd_gen, ma_gen):
        """Construct a grasp data generator for both GPD and MA.

        Args:
            gpd_gen (GraspDataGeneratorGPD): the grasp data generator for GPD.
            ma_gen (GraspDataGeneratorMA): the grasp data generator for MA.
        """
        self.gpd_gen = gpd_gen
        self.ma_gen = ma_gen

    def createData(self, mesh, cloud, samples, camera_pose=[]):
        """Create grasp data for both GPD and MA.

        Args:
            mesh (o3d.geometry.PointCloud): the object mesh.
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (np.array): n x 3 array of sample points.

        Returns:
            dict: the grasp data for both GPD and MA.
        """
        data = {
            "gpd": self.gpd_gen.createData(
                mesh, cloud, samples, camera_pose=camera_pose
            ),
            "ma": self.ma_gen.createData(mesh, cloud, samples, camera_pose=camera_pose),
        }
        return data


def subsampleProposals(labels, num_samples):
    """Subsample the grasp proposals.

    Args:
        props (np.array): n x m numpy array.
        labels (np.array): n x m x k numpy array.
        num_samples (int): the number of proposals to be subsampled.

    Returns:
        np.array: 2 x n subsample indices.
    """
    if labels.ndim == 3:
        coll_free_gt = labels[:, :, 0] == 1
        antipodal_gt = labels[:, :, 1] == 1
        pos = np.nonzero(coll_free_gt & antipodal_gt)
        neg = np.nonzero(~coll_free_gt | ~antipodal_gt)
    elif labels.ndim == 2:
        pos = np.nonzero(labels == 1)
        neg = np.nonzero(labels == 0)

    print(f"Found {len(neg[0])} negatives and {len(pos[0])} positives")

    half = int(np.floor(num_samples / 2))
    idxs = np.empty((2, 0), np.int)
    if len(pos[0]) > 0:
        if len(pos[0]) > half:
            pos = np.array(pos)[:, np.random.choice(len(pos[0]), half)]
        idxs = np.hstack((idxs, pos))
    if len(neg[0]) > 0:
        if len(neg[0]) > half:
            neg = np.array(neg)[:, np.random.choice(len(neg[0]), half)]
        idxs = np.hstack((idxs, neg))

    # Repeat indices if necessary to fill up array.
    if idxs.shape[1] < num_samples:
        r = np.random.choice(idxs.shape[1], num_samples - idxs.shape[1])
        idxs = np.hstack((idxs, idxs[:, r]))

    sub = labels[idxs[0], idxs[1]]
    p = np.count_nonzero(sub)
    n = np.count_nonzero(sub == 0)
    print(f"Subsample has {p} positives and {n} negatives")
    # if p == 0:
    # input("?")

    return idxs

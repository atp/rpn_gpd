"""Estimate the transformation between a camera (eye) mounted on a robot gripper (hand).
"""

from copy import deepcopy

import cv2
import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from scene import calcLookAtPose

np.set_printoptions(precision=3, suppress=True)


def visualizeFrames(poses: np.array, sizes: np.array, title_prefix=""):
    frames = [o3d.geometry.TriangleMesh.create_coordinate_frame(s) for s in sizes]
    frames = [f.transform(X) for f, X in zip(frames, poses)]
    o3d.visualization.draw_geometries(frames, title_prefix + "frames", 640, 480)


# pose of gripper relative to robot base (from robot kinematics)
R_gripper2base = []
t_gripper2base = []

# pose of target (calibration object) relative to camera (from pose estimation of target)
R_target2cam = []
t_target2cam = []

"""
# Try with zeros.
R_gripper2base = np.zeros((3, 3, 3))
t_gripper2base = np.zeros((3, 3, 1))
R_target2cam = np.zeros((3, 3, 3))
t_target2cam = np.zeros((3, 3, 1))
R_cam2gripper, t_cam2gripper = cv2.calibrateHandEye(
    R_gripper2base, t_gripper2base, R_target2cam, t_target2cam
)
print(f"Calibrated hand-eye:\n{R_cam2gripper}\n{t_cam2gripper}")
"""

"""
# Try with random values.
n = 3
R_gripper2base = np.random.rand(n, 3, 3)
t_gripper2base = np.random.rand(n, 3, 1)
R_target2cam = np.random.rand(n, 3, 3)
t_target2cam = np.random.rand(n, 3, 1)
R_cam2gripper, t_cam2gripper = cv2.calibrateHandEye(
    R_gripper2base, t_gripper2base, R_target2cam, t_target2cam
)
print(f"Calibrated hand-eye:\n{R_cam2gripper}\n{t_cam2gripper}")
"""

# Example with known object pose and known hand-eye.
n = 3  # min required poses
n = 30
X_object = np.eye(4)
Y_gripper2cam = np.eye(4)

# Gripper has same orientation as camera,
gripper_offset = -0.1
Y_gripper2cam[1, 3] = gripper_offset

Y_gripper2cam[:3, 3] = np.random.rand(3) * np.array([0.1, 0.1, 0.1])

degs = 5.0
r = Rot.from_rotvec(np.deg2rad(degs) * np.array([1.0, 0.0, 0.0]))
Y_gripper2cam[:3, :3] = r.as_matrix()

Y_cam2gripper = np.linalg.inv(Y_gripper2cam)

for i in range(n):
    look_from = np.random.normal(size=3)
    look_from = look_from / np.linalg.norm(look_from)
    X_cam = calcLookAtPose(look_from, np.zeros(3))
    X_gripper2base = np.matmul(X_cam, Y_gripper2cam)
    # X_gripper2base = deepcopy(X_cam)
    # X_gripper2base[:3, 3] += gripper_offset * X_cam[:3, 1]
    # X_object2cam = np.matmul(X_cam, X_object)
    X_object2cam = np.matmul(np.linalg.inv(X_cam), X_object)
    print(f"X_object:\n{X_object}")
    print(f"X_cam:\n{X_cam}")
    print(f"X_object2cam:\n{X_object2cam}")
    print(f"X_gripper2base:\n{X_gripper2base}")
    R_gripper2base.append(X_gripper2base[:3, :3])
    t_gripper2base.append(X_gripper2base[:3, 3])
    R_target2cam.append(X_object2cam[:3, :3])
    t_target2cam.append(X_object2cam[:3, 3])
    # visualizeFrames([X_object, X_cam, X_gripper2base], np.ones(4) * 0.1, f"{i} ")
R_gripper2base = np.stack(R_gripper2base)
t_gripper2base = np.stack(t_gripper2base)
R_target2cam = np.stack(R_target2cam)
t_target2cam = np.stack(t_target2cam)
print([x.shape for x in (R_gripper2base, R_target2cam, t_gripper2base, t_target2cam)])
R_cam2gripper, t_cam2gripper = cv2.calibrateHandEye(
    R_gripper2base, t_gripper2base, R_target2cam, t_target2cam, method=0
)
X_hat_cam2gripper = np.vstack(
    (np.hstack((R_cam2gripper, t_cam2gripper)), np.array([0.0, 0.0, 0.0, 1.0]))
)
t_cam2gripper = t_cam2gripper.flatten()
# print(f"(1) Calibrated hand-eye:\n{R_cam2gripper}\n{t_cam2gripper.T}")
t_dist = np.linalg.norm(Y_cam2gripper[:3, 3] - t_cam2gripper)
R_dist = np.arccos(
    0.5 * (np.trace(np.matmul(R_cam2gripper, Y_cam2gripper[:3, :3])) - 1)
)
R_dist = np.real(R_dist)
print(f"expected solution:\n{Y_cam2gripper}")
print(f"estimated solution:\n{X_hat_cam2gripper}")
print(f"translation distance: {t_dist:.3f}m, rotation distance: {R_dist:.3f}rad")
# print(Y_cam2gripper[:3, 3] - t_cam2gripper.flatten())
exit()

# path = "/home/andreas/sofware/handeye_calib_camodocal/example/TransformPairsOutput.yml"
path = "/home/andreas/sofware/handeye_calib_camodocal/example/TransformPairsInput.yml"
with open(path, "r") as f:
    data = yaml.full_load(f)
T_gripper2base = [np.array(v["data"]) for k, v in data.items() if "T1" in k]
T_target2cam = [np.array(v["data"]) for k, v in data.items() if "T2" in k]
T_gripper2base = np.stack([x.reshape((4, 4)) for x in T_gripper2base])
T_target2cam = np.stack([x.reshape((4, 4)) for x in T_target2cam])

# T_gripper2base = np.stack([np.linalg.inv(x) for x in T_gripper2base])
# T_target2cam = np.stack([np.linalg.inv(x) for x in T_target2cam])

print(T_gripper2base.shape, T_target2cam.shape)
R_gripper2base = T_gripper2base[:, :3, :3]
R_target2cam = T_target2cam[:, :3, :3]
t_gripper2base = T_gripper2base[:, :3, 3]
t_target2cam = T_target2cam[:, :3, 3]
# t_gripper2base = np.stack([x.flatten() for x in t_gripper2base])
# t_target2cam = np.stack([x.flatten() for x in t_target2cam])
print([x.shape for x in (R_gripper2base, R_target2cam, t_gripper2base, t_target2cam)])
print(T_gripper2base[-1])
print(T_target2cam[-1])

# Load the solution.
path = "/home/andreas/sofware/handeye_calib_camodocal/example/CalibratedTransform.yml"
with open(path, "r") as f:
    target = yaml.full_load(f)
Y = np.array(target["ArmTipToMarkerTagTransform"]["data"]).reshape((4, 4))
# Y[:3, 3] = np.zeros(3)
print(f"target:\n{Y}")
t_min_dist = 100000.0
frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.2)
frame.transform(Y)
frames = [frame]

Z = np.array(
    [
        [-0.962926, -0.156063, 0.22004, -0.00802514],
        [-0.176531, 0.981315, -0.0765322, 0.0242905],
        [-0.203985, -0.112539, -0.972484, 0.0550756],
        [0.0, 0.0, 0.0, 1.0],
    ]
)
frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
frame.transform(Z)
frames.append(frame)
print(f"Z:\n{Z}")
print(f"Y:\n{Y}")
o3d.visualization.draw_geometries(frames, "Z and Y", 640, 480)

target = Z
# target = np.linalg.inv(target)
frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
frame.transform(target)
frames = [frame]

for i in range(4):
    R_cam2gripper, t_cam2gripper = cv2.calibrateHandEye(
        R_gripper2base, t_gripper2base, R_target2cam, t_target2cam, method=i
    )
    # print(f"(1) Calibrated hand-eye: {t_cam2gripper.T}")
    print(f"(1) Calibrated hand-eye:\n{R_cam2gripper}\n{t_cam2gripper.T}")
    frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.1)
    X = np.eye(4)
    X[:3, :3] = R_cam2gripper
    X[:3, 3] = t_cam2gripper.flatten()
    frame.transform(X)
    frames.append(frame)

    t_dist = np.linalg.norm(target[:3, 3] - t_cam2gripper)
    R_dist = np.arccos(0.5 * (np.trace(np.matmul(R_cam2gripper, target[:3, :3])) - 1))
    R_dist = np.real(R_dist)
    print(f"  t_dist: {t_dist}, R_dist: {R_dist}")
    # if t_dist <


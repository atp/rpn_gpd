"""Visualize the grasps in a database.
TODO: adapt to new hands generators
"""

import csv
import sys

import numpy as np
import open3d as o3d
import pyrender
import tables
from candidates.gpd_candidates_generator import GpdCandidatesGenerator, estimateLocalFrames
from candidates.multi_anchor_candidates_generator import MultiAnchorCandidatesGenerator
from scipy.spatial.transform import Rotation as Rot

from hand_params import hand_params
from scene import SingleObjectScene
from utils.hands_plot import HandsPlot

objects_dir = "/home/andreas/data/Cat200_ModelDatabase/"

if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print("Usage: python visualize_grasps_database.py GENERATOR CAT_INFO HDF5")
    exit(-1)

gen_name = sys.argv[1]
if gen_name == "gpd":
    generator = GpdCandidatesGenerator(hand_params)
elif gen_name == "ma":
    generator = MultiAnchorCandidatesGenerator(hand_params, use_open_gl_frame=True)

with open(sys.argv[2], "r") as f:
    object_infos = [r for r in csv.reader(f, delimiter=",")]
object_infos = object_infos[1:]
print(object_infos)
print("==============================================================")

with tables.open_file(sys.argv[3], "r") as f:
    meta = {x.name: x for x in f.list_nodes("/meta")}

    i = 0
    for i in range(5):
        category = meta["categories"][i].decode("ascii")
        instance = meta["instances"][i].decode("ascii")
        viewpoint = meta["viewpoints"][i]
        sample = meta["samples"][i]
        print(f"Category: {category}, instance: {instance}, viewpoint: {viewpoint}")
        print(f"sample: {sample}")
    exit()

    # Setup the scene.
    mesh_path = objects_dir + category + "/" + instance + ".ply"
    scene = SingleObjectScene(mesh_path, 0.02, 0.10)

    # Scale the object.
    scale = float(
        [x[2] for x in object_infos if x[0] == category and x[1] == instance][0]
    )
    print(f"scale: {scale}")
    scene.resetToScale(scale)
    pyrender.Viewer(scene.scene)
    print(f"initial camera_pose:\n{scene.camera_pose}")

    # stop = len([x for x in obj_ids if x == obj_ids[i]])
    stop = len([x for x in obj_rots if np.any(x == obj_rots[i])])
    print(f"stop: {stop}")

    # Move the camera.
    quat = obj_rots[i]
    camera_pose = np.eye(4)
    camera_pose[:3, 3] = scene.camera_pose[:3, 3]
    camera_pose[:3, :3] = Rot.from_quat(quat).as_matrix().T
    # camera_pose[:3,3] = np.dot(camera_pose[:3,:3], camera_pose[:3,3])
    # print(f'camera_pose:\n{camera_pose}')

    # Rotate the object.
    obj_pose = np.eye(4)
    obj_pose[:3, :3] = Rot.from_quat(quat).as_matrix()
    obj_pose[:3, 3] = np.zeros(3)
    scene.setObjectPose(obj_pose)
    camera_pose = scene.camera_pose

    # Acquire a point cloud.
    cloud = scene.createProcessedPointCloudFromView(camera_pose)
    tree = o3d.geometry.KDTreeFlann(cloud)
    camera_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
        size=0.1, origin=np.zeros(3)
    )
    camera_frame.transform(camera_pose)
    o3d.visualization.draw_geometries([cloud, camera_frame])

    boxes = []
    for x in samples[:stop]:
        box = o3d.geometry.TriangleMesh.create_box(0.01, 0.01, 0.01)
        box.translate(x, False)
        box.paint_uniform_color([1.0, 0.0, 0.0])
        boxes.append(box)
    cloud.paint_uniform_color([0.0, 0.0, 1.0])
    o3d.visualization.draw_geometries([cloud] + boxes)
    exit()

    # Reconstruct the grasp poses from the database.
    samples = np.array([samples[i]])
    frames = estimateLocalFrames(tree, np.asarray(cloud.normals).T, samples)
    print(frames)
    proposals = np.asarray(f.root.gpd_proposals[i])
    print(proposals.shape, np.count_nonzero(proposals))
    valid = np.nonzero(proposals)[0]
    indices = np.vstack((np.zeros(len(valid), dtype=int), valid))
    poses = generator.indicesToPoses(cloud, tree, samples, frames, indices)
    print(f"grasp poses: {poses.shape}")
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    p.plotHands(poses, [0.0, 1.0, 0.0])
    p.setTitle(f"grasp poses")
    p.show()

    indices = np.vstack((np.zeros(10, dtype=int), np.arange(10, dtype=int)))
    poses = generator.indicesToPoses(cloud, tree, samples, frames, indices)
    print(f"grasp poses: {poses.shape}")
    free = proposals[:10]
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    p.plotHands(poses[free], [1.0, 0.0, 0.0])
    p.plotHands(poses[~free], [0.0, 1.0, 0.0])
    p.setTitle(f"collisions: first 10 grasp poses")
    p.show()

    indices = np.vstack(
        (np.zeros(len(proposals), dtype=int), np.arange(len(proposals), dtype=int))
    )
    poses = generator.indicesToPoses(cloud, tree, samples, frames, indices)
    print(f"grasp poses: {poses.shape}")
    p = HandsPlot(hand_geom)
    p.plotCloud(cloud, [1.0, 0.0, 1.0])
    p.plotHands(poses[proposals], [1.0, 0.0, 0.0])
    p.plotHands(poses[~proposals], [0.0, 1.0, 0.0])
    p.setTitle(f"proposals: first 10 grasp poses")
    p.show()

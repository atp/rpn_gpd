"""Play around with pyrender frames.

Visualizes the depth image and point cloud observed from a number of viewpoints. The
viewpoints are sampled uniformly on a sphere.

"""

import sys
from copy import deepcopy
from time import sleep

import numpy as np
import open3d as o3d
import pyrender
from scipy.spatial.transform import Rotation as Rot

from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import twoWindowsPlot

num_views = 20
camera_distance = 0.5
look_at = [0.0, 0.0, 0.0]

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)

if len(sys.argv) < 4:
    print("Error: wrong input arguments!")
    print("Usage: python pyrender_frames.py MESH_PATH SCALE USE_FIXED_VIEW")
    exit(-1)


# Sample uniform view points on a sphere.
viewpoints = np.random.normal(size=(num_views, 3))
viewpoints /= np.linalg.norm(viewpoints, axis=1)[:, np.newaxis]
viewpoints *= camera_distance

# Create a scene with the object at the origin and scale it.
scale = float(sys.argv[2])
scene = SingleObjectScene(sys.argv[1], 0.01, 0.1, 400, 400)
scene.resetToScale(scale)

# Load the mesh and scale it.
mesh = o3d.io.read_triangle_mesh(sys.argv[1])
for v in mesh.vertices:
    v *= scale
mesh.paint_uniform_color([1, 0, 0])

# Use only a single, fixed viewpoint.
if int(sys.argv[3]) == 1:
    viewpoints = np.array([[0.5, 0, 0.5]])

viewpoints_cloud = o3d.geometry.PointCloud()
viewpoints_cloud.points = o3d.utility.Vector3dVector(viewpoints)
viewpoints_cloud.paint_uniform_color([0, 1, 0])
o3d.visualization.draw_geometries(
    [mesh, viewpoints_cloud], "Mesh and viewpoints", 640, 480
)

for i in range(len(viewpoints)):
    view_idx = i

    # mesh_cloud = scene.createMultiViewPointCloud()
    cam_pose = calcLookAtPose(viewpoints[view_idx], look_at)
    cloud, depth = scene.createPointCloudFromView(cam_pose, return_depth_image=True)

    origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)

    # Frame for the camera pose.
    camera_pose = scene.scene.get_pose(scene.camera_node)
    camera_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
        0.1, camera_pose[:3, 3]
    )
    camera_frame.rotate(camera_pose[:3, :3])

    # Arrow pointing from base orgin to camera origin.
    dist = np.linalg.norm(camera_pose[:3, 3])
    arrow = o3d.geometry.TriangleMesh.create_arrow(0.005, 0.008, dist - 0.03, 0.03)
    arrow.rotate(camera_pose[:3, :3], center=False)

    # mesh_cloud.paint_uniform_color([1, 0, 0])
    mesh.paint_uniform_color([1, 0, 0])
    cloud.paint_uniform_color([0, 0, 1])
    # o3d.visualization.draw_geometries(
    # [mesh_cloud, cloud, origin_frame, camera_frame, arrow], height=480, width=640)

    print("viewpoint:", viewpoints[view_idx])

    # Camera frame: the camera's pose rotated by 180deg around its own x-axis.
    frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.05, camera_pose[:3, 3])
    Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0])).as_matrix()
    R = np.dot(camera_pose[:3, :3], Rx)
    frame.rotate(R)

    # Plot the point cloud in the base frame and plot the depth image.
    depth_img = o3d.geometry.Image(depth)
    twoWindowsPlot(
        [mesh, cloud, origin_frame, camera_frame, frame, arrow],
        [depth_img],
        ["point cloud in base frame", "depth image"],
    )

    # Plot the point cloud in the camera frame and plot the depth image.
    X = camera_pose
    X[:3, :3] = R
    X = np.linalg.inv(X)
    cloud.transform(X)
    twoWindowsPlot(
        [cloud, origin_frame],
        [depth_img],
        ["point cloud in camera frame", "depth image"],
    )

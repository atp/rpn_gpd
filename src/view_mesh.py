"""A simple mesh viewer.
"""

import sys

import numpy as np
import open3d as o3d
import pyrender

from scene import SingleObjectScene

if len(sys.argv) < 2:
    print("Error: not enough input arguments")
    print("Usage: python view_mesh.py PLY_FILE")
    exit(-1)

# Create a scene with the mesh at the origin.
scene = SingleObjectScene(sys.argv[1], 0.02, 0.10)
pyrender.Viewer(scene.scene)

# Create a complete point cloud of the mesh.
full_cloud, views_cloud = scene.createMultiViewPointCloud(return_views_cloud=True)
views_cloud.paint_uniform_color(np.array([1.0, 0.0, 0.0]))
o3d.visualization.draw_geometries([full_cloud, views_cloud])

# Create a partial point cloud seen from a camera pose.
partial_cloud = scene.createProcessedPointCloudFromView(scene.camera_pose)
o3d.visualization.draw_geometries([partial_cloud])

# Draw the partial against the complete point cloud.
full_cloud.paint_uniform_color(np.array([1.0, 0.0, 0.0]))
partial_cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
o3d.visualization.draw_geometries([partial_cloud, full_cloud])

camera_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
    size=0.1, origin=scene.camera_pose[:3, 3]
)
camera_frame.transform(scene.camera_pose)

world_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(
    size=0.1, origin=np.zeros(3)
)
o3d.visualization.draw_geometries(
    [partial_cloud, full_cloud, world_frame, camera_frame]
)

"""Add binary labels to a database.

Usage: python add_binary_labels.py HDF5 NODE [IGNORE_SHIFT]

"""

import sys

import numpy as np
import tables

from ground_truth import findGroundTruthPositives


def addBinaryLabels(filepath, node_location, label_type):
    filters = tables.Filters(complevel=5, complib="blosc:lz4")

    with tables.open_file(filepath, "a") as f:
        labels = f.get_node(node_location)
        # Look for collision-free grasps.
        if label_type == 0:
            print("Using collision-free ...")
            if labels.ndim == 2:
                mask = labels[:, 0] == 1
            elif labels.ndim == 3:
                mask = labels[:, :, 0] == 1
        # Look for collision-free and antipodal grasps.
        elif label_type == 1:
            print("Using collision-free and antipodal ...")
            if labels.ndim == 2:
                mask = (labels[:, 0] == 1) & (labels[:, 1] == 1)
            elif labels.ndim == 3:
                mask = (labels[:, :, 0] == 1) & (labels[:, :, 1] == 1)
        # Look for grasps that satisfy all ground truth conditions.
        else:
            print("Using collision-free and antipodal and no-shift ...")
            mask = findGroundTruthPositives(labels)
        positives = np.nonzero(mask)
        print(f"Labels: {labels.shape}, positives: {len(positives[0])}")
        if labels.ndim == 2:
            labels_out = np.zeros(len(labels))
            labels_out[positives[0]] = 1
        elif labels.ndim == 3:
            labels_out = np.zeros(labels.shape[:2])
            labels_out[positives[0], positives[1]] = 1

        node_path = node_location[: node_location.rfind("/") + 1]
        f.rename_node(
            "/labels",
            "original_" + node_location[node_location.rfind("/") + 1 :],
            overwrite=True,
        )
        dset = f.create_carray(
            node_path, "labels", obj=np.asarray(labels_out), filters=filters
        )
        return dset


def main():
    if len(sys.argv) < 3:
        print("Error: not enough input arguments.")
        print("Usage: python add_binary_labels.py HDF5 NODE [LABEL_TYPE]")
        print("LABEL_TYPE: 0 for collisions, 1 for antipodal, 2 for shift")
        print("  i-th label type includes (i-1)-th label type")
        sys.exit(-1)
    filepath = sys.argv[1]
    node_location = sys.argv[2]
    label_type = 2
    if len(sys.argv) > 3:
        label_type = int(sys.argv[3])

    dset = addBinaryLabels(filepath, node_location, label_type)
    print(f"Added dataset {dset} to database {sys.argv[1]}")


if __name__ == "__main__":
    main()

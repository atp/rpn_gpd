"""Visualize the rotations considered by our method.
"""

from argparse import ArgumentParser
from copy import deepcopy
from functools import partial
from time import time

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from hand_params import hand_params
from hands.gpd_hands_generator import GpdHandsGenerator
from hands.gqd_hands_generator import GqdHandsGenerator
from hands.hand_eval import AntipodalParams, HandGeometry
from hands.ma_hands_generator import MaHandsGenerator
from hands.qd_hands_generator import QdHandsGenerator
from proposal import sampleFromCube
from scene import SingleObjectScene, calcLookAtPose
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.hands_plot import HandsPlot
from utils.open3d_plot import plotSamplesAsBoxes

cube_size = 1.0
grasp_img_shape = (60, 60, 4)

grey = [0.4, 0.4, 0.4]
cyan = [0.0, 1.0, 0.0]
orange = [1.0, 0.6, 0.0]
yellow = [1.0, 1.0, 0.0]
green = [0.0, 1.0, 0.0]
red = [1.0, 0.0, 0.0]
blue = [0.0, 0.0, 1.0]
black = [0.0, 0.0, 0.0]
purple = [0.6, 0.15, 0.9]

np.set_printoptions(precision=3, suppress=True)
np.random.seed(0)


def createArgParser() -> ArgumentParser:
    parser = ArgumentParser(description="Visualize the frames used in our method")
    parser.add_argument("file", help="path to mesh file")
    parser.add_argument("scale", help="scaling factor", type=float)
    parser.add_argument("method", help="grasp pose generator")
    parser.add_argument("num_samples", help="number of samples", type=int)

    return parser


def createHandsGenerator(method, hand_geom, antip_prms):
    if method == "gpd":
        gen = GpdHandsGenerator(hand_geom, antip_prms)
    elif method == "ma":
        # gen = MaHandsGenerator(hand_geom, antip_prms,
        # use_open_gl_frame=True, num_anchor_rots=3, num_diagonal_anchors=1)
        gen = MaHandsGenerator(hand_geom, antip_prms, use_open_gl_frame=True)
    elif method == "qd":
        # gen = QdHandsGenerator(hand_geom, antip_prms, 1, 1, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 1, 2)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 3, 2, 4)
        # gen = QdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = QdHandsGenerator(hand_geom, antip_prms, num_approaches=1)
        gen = QdHandsGenerator(hand_geom, antip_prms)
    elif method == "gqd":
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 2, 5, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 1)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 2)
        # gen = GqdHandsGenerator(hand_geom, antip_prms, 9, 9, 4)
        gen = GqdHandsGenerator(hand_geom, antip_prms)
    return gen


def main() -> None:
    draw_geoms = partial(o3d.visualization.draw_geometries, width=640, height=480)

    # Read command line arguments.
    parser = createArgParser()
    args = parser.parse_args()

    # data = createPointCloudFromMesh(args.file, args.scale)
    scene = SingleObjectScene(args.file)
    scene.resetToScale(args.scale)
    mesh, views_cloud = scene.createCompletePointCloud()

    camera_pose = scene.scene.get_pose(scene.camera_node)
    # camera_pose[:3, 3] -= 0.02 * camera_pose[:3, 2]
    # camera_pose = calcLookAtPose([1.0, 0.0, 0.0], np.zeros(3))
    # camera_pose = calcLookAtPose([1.0, 0.0, 1.0], np.zeros(3))
    camera_pose = calcLookAtPose([0.5, 0.0, 0.5], np.zeros(3))
    # cloud = scene.createPointCloudFromView(camera_pose)
    # cloud = scene.createProcessedPointCloudFromView(camera_pose)
    cloud = scene.createPointCloudFromView(camera_pose)
    cloud = estimateNormals(cloud, camera_pose[:3, 3])
    tree = o3d.geometry.KDTreeFlann(cloud)

    # draw_geoms([mesh, views_cloud],
    # "Multiview point cloud and view points", 640, 480)
    # draw_geoms([mesh], "Surface normals", 640, 480)

    # Draw sample points from the point cloud.
    samples = sampleFromCube(cloud, cube_size, args.num_samples)
    # samples[0, 2] += 0.02
    pts = np.asarray(cloud.points)
    mins, maxs = np.min(pts, 0), np.max(pts, 0)
    print(mins, maxs)
    # samples[0] = samples[np.argmax(samples[:,2])]
    # samples[0, 0] = mins[0] + 0.5 * (maxs[0] - mins[0])
    # samples[0] = mins + 0.5 * (maxs - mins)
    # samples[0,1] = samples[1,1]
    # print(f"samples: {samples}")
    # plotSamplesAsBoxes(mesh, samples, "Samples")
    plotSamplesAsBoxes(cloud, samples, "Samples")
    print(f"samples[0]: {samples[0]}")
    # exit()

    # Create the hands generator.
    hand_geom = HandGeometry(**hand_params)
    antipodal_params = {
        "extremal_thresh": 0.003,
        "friction_coeff": 30,
        "min_viable": 6,
        "checks_overlap": True,
    }
    antip_prms = AntipodalParams(**antipodal_params)
    gen = createHandsGenerator(args.method, hand_geom, antip_prms)

    # Generate the grasp poses.
    t = time()
    camera_pose = scene.scene.get_pose(scene.camera_node)
    hands, frames = gen.generateHands(cloud, samples, camera_pose=camera_pose)
    t = time() - t
    print(f"Created {len(hands)} grasp poses in {t:.4f}s.")

    # camera_mesh = o3d.io.read_triangle_mesh(
    # "/home/andreas/data/camera_simple.ply")
    camera_body = o3d.geometry.TriangleMesh.create_box(depth=2.0)
    camera_head = o3d.geometry.TriangleMesh.create_cone(radius=0.5, height=1.0)
    camera_body = camera_body.translate([-0.5, -0.5, 0.4])
    camera_mesh = camera_body + camera_head
    # camera_mesh = camera_mesh.scale(0.02, camera_mesh.get_center())
    camera_mesh = camera_mesh.transform(camera_pose)
    # camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.86 * camera_pose[:3, 2])
    camera_mesh = camera_mesh.translate(camera_pose[:3, 3] - 0.875 * camera_pose[:3, 2])
    # camera_cloud = camera_mesh.sample_points_poisson_disk(1000)
    camera_cloud = camera_mesh.sample_points_uniformly(10000)

    Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0]))
    camera_frame = deepcopy(camera_pose)
    camera_frame[:3, :3] = np.dot(camera_frame[:3, :3], Rx.as_matrix())
    print(camera_frame)

    # Calculate the grasp poses.
    idxs = np.vstack(
        (np.zeros(gen.num_hand_orientations), np.arange(gen.num_hand_orientations))
    ).astype(int)
    poses = gen.indicesToPoses(
        cloud, tree, samples, {"indices": idxs, "frames": frames}
    )

    samples_cloud = numpyToCloud(samples)

    sphere_pts = np.zeros((len(poses), 3))
    sphere_normals = np.zeros((len(poses), 3))
    for i, X in enumerate(poses):
        # sphere_pts[i] = samples[0] - 0.15 * X[:3, 0]
        # sphere_normals[i] = X[:3, 0]
        sphere_pts[i] = samples[0]
        sphere_normals[i] = -X[:3, 0]
    print(sphere_normals)
    sphere_cloud = numpyToCloud(sphere_pts, sphere_normals)
    print(f"sphere_normals: {len(sphere_normals)}")
    # draw_geoms(geometry_list=[sphere_cloud], window_name="sphere cloud")

    # p = HandsPlot(hand_geom)
    # p.plotNormals(sphere_pts, sphere_normals)
    # p.plotSphere(samples[0], 0.1, [0.0, 1.0, 0.0], 0.5)
    # p.show()

    # Plot all possible grasp poses for a single sample position.
    # sphere_colors = np.random.rand(len(poses),3)
    sphere_colors = np.zeros((len(poses), 3))
    # sphere_colors[:, 1] = np.random.rand(len(poses))
    gray = np.random.rand(len(poses))
    sphere_colors[:,0] = gray
    sphere_colors[:,1] = gray
    # sphere_colors[:,2] = gray
    # purple = [0.97,0.13,1.00]
    # sphere_colors[:] = purple
    c1 = [1.00,0.73,0.43]
    sphere_colors[:] = c1
    for sample in samples:
        sphere_pts = np.zeros((len(poses), 3))
        sphere_normals = np.zeros((len(poses), 3))
        for i, pose in enumerate(poses):
            sphere_pts[i] = sample
            sphere_normals[i] = -pose[:3, 0]
        print(sphere_normals)
        sphere_cloud = numpyToCloud(sphere_pts, sphere_normals)
        print(f"sphere_normals: {len(sphere_normals)}")
        p = HandsPlot(hand_geom)
        p.plotCloud(mesh, [0.5, 0.0, 0.0], 0.4)
        p.plotCloud(cloud, [0.0, 0.0, 1.0])
        # p.plotCloud(camera_cloud, [0.5, 0.5, 0.5])
        # p.plotHands(poses, [1.0, 1.0, 0.0], scale=0.1)
        # p.plotCloud(sphere_cloud, [0.2, 0.2, 0.2])
        # p.plotNormals(sphere_pts, sphere_normals)
        # p.plotNormals(sphere_pts, sphere_normals, sphere_colors)
        p.plotNormals(sphere_pts, sphere_normals, sphere_colors, 0.06)
        # p.plotSphere(samples[0], 0.1, [0.0, 1.0, 0.0], 0.5)
        # p.plotArrow(
        # sphere_pts[mid], sphere_pts[mid] + 0.25 * sphere_normals[mid], color=red
        # )
        # p.plotArrow(
        # sphere_pts[mid + 3],
        # sphere_pts[mid + 3] + 0.25 * sphere_normals[mid + 3],
        # color=green,
        # )
        p.plotAxes(camera_frame)
        # p.plotAxes(np.eye(4), [0.2, 0.2, 0.2])
        p.setTitle("all possible hand orientations at a single position")
        # p.setCameraPose([0, 0, 1.0], [0, 0, 1], [0, 0, 0])
        p.setCameraPose([0.25, -0.25, 1.0], [0, 0, 1], [0, 0, 0])
        p.show()
        p = None

    print(f"samples[0]: {samples[0]}")
    print("DONE!")


if __name__ == "__main__":
    main()

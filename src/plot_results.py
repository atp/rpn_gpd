"""Plot precision and recall against threshold for a given NPY file.
"""

import sys

import matplotlib.pyplot as plt
import numpy as np

# precision = np.hstack((precision, 0.0))
# recall = np.hstack((precision, 1.0))
# precision = np.hstack((precision, 1.0))
# recall = np.hstack((precision, 0.0))

f = np.load(sys.argv[1])
print(f.shape)
avg_precision = 0.0
avg_recall = 0.0

for i, view in enumerate(f):
    threshs = view[:, 0]
    precision = view[:, 1] / (view[:, 1] + view[:, 2])
    recall = view[:, 1] / (view[:, 1] + view[:, 4])
    avg_precision += precision
    avg_recall += recall
    plt.plot(threshs, precision, label="precision")
    plt.plot(threshs, recall, label="recall")
    plt.xlabel("thresholds")
    plt.legend()
    plt.xlim([-0.05, 1.05])
    plt.ylim([-0.05, 1.05])
    plt.title(f"View: {i}")
    plt.show()

avg_precision /= len(f)
avg_recall /= len(f)
plt.plot(threshs, avg_precision, label="precision")
plt.plot(threshs, avg_recall, label="recall")
plt.xlabel("thresholds")
plt.legend()
plt.xlim([-0.05, 1.05])
plt.ylim([-0.05, 1.05])
plt.title(f"Average precision/recall over {len(f)} views")
plt.show()

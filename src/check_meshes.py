"""Check if the meshes in a given directory are watertight.

If it finds a mesh that is not watertight, the program will print a message and exit.

"""

import os
import sys
from glob import glob

import trimesh


def checkMesh(path: str):
    mesh = trimesh.load(path)
    if mesh.is_watertight == False:
        print(f"{path} is not watertight!")
        broken = trimesh.repair.broken_faces(mesh)
        print(f"broken faces: {broken}")
        exit(0)
    else:
        s = f"mesh: {path}, watertight: {mesh.is_watertight}, "
        s += f"#vertices: {len(mesh.vertices)}, #faces: {len(mesh.faces)}"
        print(s)


if len(sys.argv) < 2:
    print("Error: wrong number of input arguments!")
    print("Usage: python check_meshes.py DIRECTORY")
    exit(-1)

in_dir = sys.argv[1]

for d in sorted(os.listdir(in_dir)):
    path = os.path.join(in_dir, d)
    if os.path.isdir(path):
        files = sorted(glob(path + "/*.ply"))
        for f in files:
            checkMesh(f)
    elif os.path.isfile(path):
        checkMesh(path)

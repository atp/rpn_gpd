import numpy as np
from scipy.spatial.transform import Rotation as Rot

from .anchor_hands_generator import AnchorHandsGenerator

np.set_printoptions(precision=4)

# An anchor grasp is defined by a rotation matrix, a set of angles that it can
# be rotated to, and an axis that it is rotated about.


class Anchor:
    def __init__(self, R, angles, axis, name=""):
        self.name = name
        self.R = R
        self.angles = angles
        self.axis = axis

        # Precompute rotations for speed-up.
        self.rotations = np.ones((len(angles), 3, 3))
        for i in range(len(self.rotations)):
            self.rotations[i] = self.calcRotation(i)

    def calcRotation(self, idx):
        rot = Rot.from_rotvec(self.angles[idx] * self.axis)
        R = np.dot(self.R, rot.as_matrix())
        return R

    def __str__(self):
        s = f"name: {self.name}, axis: {self.axis}, #angles: " + \
            f"{len(self.angles)}\n"
        s += f"R:\n{self.R}\n"
        s += f"angles: {self.angles}\n"
        s += f"angles: {np.rad2deg(self.angles)}\n"
        return s


class MaHandsGenerator(AnchorHandsGenerator):
    def __init__(
        self,
        hand_geom,
        antipodal_params,
        use_open_gl_frame,
        num_anchor_rots=9,
        num_diagonal_anchors=9,
    ):
        super().__init__(hand_geom, antipodal_params)

        self.name = "MultiAnchorHandsGenerator"

        # Number of hand orientations per anchor grasp
        self.num_anchor_rots = num_anchor_rots

        # Number of diagonal anchor grasps (about y-axis)
        self.num_diagonal_anchors = num_diagonal_anchors

        # Anchor grasp #1: top-down
        self.anchors = []
        R = np.array([[0, 0, 1],
                      [0, 1, 0],
                      [-1, 0, 0]])
        angles = np.linspace(-np.pi / 2.0, np.pi / 2.0, self.num_anchor_rots)
        self.anchors.append(Anchor(R, angles, np.array([1, 0, 0]), "down"))

        # Anchor grasp #2: forward (this anchor depends on the base frame).
        if use_open_gl_frame:
            R = np.array([[-1, 0, 0],
                          [0, 1, 0],
                          [0, 0, -1]])
            rot_y_45deg = Rot.from_rotvec(np.pi / 4 * np.array([0, -1, 0]))
        else:
            R = np.array([[1, 0, 0],
                          [0, 1, 0],
                          [0, 0, 1]])
            rot_y_45deg = Rot.from_rotvec(np.pi / 4 * np.array([0, 1, 0]))
        angles = np.linspace(-np.pi / 2, np.pi / 2, self.num_anchor_rots)
        self.anchors.append(Anchor(R, angles, np.array([0, 0, 1]), "forward"))

        # Anchor grasp #3-k: diagonal (like forward, then rotated by 45deg).
        R_y_45deg = np.dot(rot_y_45deg.as_matrix(), self.anchors[1].R)
        angles = np.linspace(-np.pi / 2, np.pi / 2,
                             self.num_anchor_rots + 1)[:-1]
        z_angles = np.linspace(-np.pi / 2, np.pi / 2,
                               self.num_diagonal_anchors)
        print(f"z_angles for diagonal grasps:", z_angles)
        for i in range(len(z_angles)):
            rot_z = Rot.from_rotvec(z_angles[i] * np.array([0, 0, 1]))
            R = np.dot(rot_z.as_matrix(), R_y_45deg)
            self.anchors.append(
                Anchor(R, angles, np.array([0, 1, 0]), f"diagonal{i+1}")
            )

        self.num_hand_orientations = len(self.anchors) * self.num_anchor_rots

        print("===============================================================")
        print(f"# anchor grasps: {len(self.anchors)}")
        print("Anchor hand orientations:")
        for anchor in self.anchors:
            print(anchor)
        print("num_hand_orientations:", self.num_hand_orientations)
        print("===============================================================")

    def calcRotation(self, idx):
        anchor_idx, rot_idx = self.linearToAnchorRotationIndex(idx)
        R = self.anchors[anchor_idx].rotations[rot_idx]
        return R

    def linearToAnchorRotationIndex(self, idx):
        anchor_idx = int(np.floor(idx / self.num_anchor_rots))
        rot_idx = idx % self.num_anchor_rots
        return anchor_idx, rot_idx

"""Quarterdome (QD) hands generator.

"""

from copy import deepcopy

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from utils.cloud_utils import numpyToCloud

from .anchor_hands_generator import AnchorHandsGenerator

np.set_printoptions(precision=4)
mode = "run"
# mode = "debug"


class QdHandsGenerator(AnchorHandsGenerator):
    def __init__(
        self,
        hand_geom,
        antipodal_params,
        num_longitudes=9,
        num_latitudes=7,
        num_approaches=4,
        hand_eval=None,
    ):
        super().__init__(hand_geom, antipodal_params, hand_eval)

        self.name = "QuarterDomeHandsGenerator"

        self.num_longitudes = num_longitudes
        self.num_latitudes = num_latitudes
        self.num_approaches = num_approaches

        # Transforms from camera to base, and from base to camera.
        self.bXc = []
        self.cXb = []

        # Initial hand orientation. The camera frame has z forward, y down, and x right.
        # The ordering of hand axes is: forward, closing, up.
        roti = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])

        # Space of angles: 7 longitude, 7 latitude, and 4 approach angles.
        half_pi = np.pi / 2.0
        longitudes = np.linspace(-half_pi, half_pi, num_longitudes)[1:-1]
        latitudes = np.linspace(-np.deg2rad(60), np.deg2rad(60), num_latitudes)
        approaches = np.linspace(-half_pi, half_pi, num_approaches + 1)[:-1]
        if num_approaches == 1:
            approaches = [0.0]
        z = np.array([0.0, 0.0, 1.0])
        y = np.array([0.0, 1.0, 0.0])
        x = np.array([1.0, 0.0, 0.0])
        rots_z = [Rot.from_rotvec(r * z).as_matrix() for r in longitudes]
        rots_y = [Rot.from_rotvec(r * y).as_matrix() for r in latitudes]
        rots_x = [Rot.from_rotvec(r * x).as_matrix() for r in approaches]
        print([len(x) for x in [rots_z, rots_y, rots_x]])
        print("det(roti):", np.linalg.det(roti))
        self.rotations = []
        for rotz in rots_z:
            for roty in rots_y:
                for rotx in rots_x:
                    # r = roti
                    # r = np.linalg.multi_dot([roti, rotz])
                    # r = np.linalg.multi_dot([roti, roty])
                    # r = np.linalg.multi_dot([roti, rotx])
                    # r = np.linalg.multi_dot([roti, rotz, roty])
                    r = np.linalg.multi_dot([roti, rotz, roty, rotx])
                    self.rotations.append(r)
        self.num_hand_orientations = len(self.rotations)

        # Ensure that we have valid rotation matrices.
        epsilon = 0.001
        assert np.all(
            1.0 - np.array([np.linalg.det(r) for r in self.rotations]) < epsilon
        )

        print("longitudes:", np.rad2deg(longitudes))
        print("latitudes:", np.rad2deg(latitudes))
        print("approaches:", np.rad2deg(approaches))
        print(
            f"#hand_orientations: {self.num_hand_orientations}, "
            + f"#rotations: {len(self.rotations)}"
        )

    def calcRotation(self, idx):
        return self.rotations[idx]

    def generateHands(self, cloud, samples, tree=None, camera_pose=[]):
        """This method requires the camera pose."""
        if len(camera_pose) == 0:
            print("Error: no camera pose given!")
            exit(-1)
        self.camera_pose = deepcopy(camera_pose)

        # Transform the point cloud and the samples to the camera frame.
        cloud_cam, samples_cam = self.transformToCameraFrame(
            cloud, samples, camera_pose
        )

        # Visualization for debugging
        if mode == "debug":
            return self.generateHandsDebug(cloud_cam, samples_cam)

        return super().generateHands(cloud_cam, samples_cam)

    def indicesToPosesMP(self, cloud, tree, samples, hands, push_forward=True):
        cloud_cam, samples_cam = self.transformToCameraFrame(
            cloud, samples, self.camera_pose
        )
        tree_cam = o3d.geometry.KDTreeFlann(cloud_cam)
        poses = super().indicesToPosesMP(
            cloud_cam, tree_cam, samples_cam, hands, push_forward
        )
        poses = np.asarray([np.dot(self.bXc, p) for p in poses])

        return poses

    def indicesToPoses(self, cloud, tree, samples, hands, push_forward=True):
        cloud_cam, samples_cam = self.transformToCameraFrame(
            cloud, samples, self.camera_pose
        )
        tree_cam = o3d.geometry.KDTreeFlann(cloud_cam)
        poses = super().indicesToPoses(
            cloud_cam, tree_cam, samples_cam, hands, push_forward
        )
        poses = np.asarray([np.dot(self.bXc, p) for p in poses])

        return poses

    def preprocess(self, cloud, samples, camera_pose):
        return self.transformToCameraFrame(cloud, samples, camera_pose)

    def transformToCameraFrame(self, cloud, samples, camera_pose):
        # Transform the point cloud and the samples to the camera frame.
        Rx = Rot.from_rotvec(np.pi * np.array([1, 0, 0]))
        bXc = deepcopy(camera_pose)
        bXc[:3, :3] = np.dot(bXc[:3, :3], Rx.as_matrix())
        cXb = np.linalg.inv(bXc)
        cloud_cam = numpyToCloud(np.asarray(cloud.points), np.asarray(cloud.normals))
        cloud_cam.transform(cXb)

        samples_hom = np.vstack((samples.T, np.ones(len(samples))))
        samples_cam = np.dot(cXb, samples_hom).T[:, :3]

        self.bXc = bXc
        self.cXb = cXb

        return cloud_cam, samples_cam

    def generateHandsDebug(self, cloud, samples):
        import open3d as o3d

        from hand_params import hand_params
        from utils.plot import plotHands

        boxes = []
        for x in samples:
            box = o3d.geometry.TriangleMesh.create_box(0.002, 0.002, 0.002)
            box.translate(x)
            box.paint_uniform_color([1.0, 0.0, 0.0])
            boxes.append(box)
        cloud.paint_uniform_color([0, 0, 1])
        origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.3)
        o3d.visualization.draw_geometries(
            [cloud_cam, origin_frame] + boxes,
            "point cloud and sample points in camera frame",
            640,
            480,
        )

        hands, _ = super().generateHands(cloud, samples)

        tree = o3d.geometry.KDTreeFlann(cloud)
        idxs = np.array(np.nonzero(hands[:, :, 0] == 1))
        idxs = idxs[:, :5]
        # idxs = np.array(np.nonzero(
        # (hands[:, :, 0] == 1) & (hands[:, :, 1] == 1)))
        poses = self.indicesToPoses(
            cloud, tree, samples, {"indices": idxs, "frames": []}
        )
        labels = np.ones(len(poses))
        plotHands(
            [cloud], poses, labels, hand_params, "grasp poses", plot_base_frame=True
        )

        return hands, []

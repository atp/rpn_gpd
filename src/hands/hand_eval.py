import os
from copy import deepcopy
from functools import partial

import numpy as np
import open3d as o3d
import torch.multiprocessing as mp

from geometry.point_cloud import PointCloud
from geometry.rect import Rect
from utils.open3d_plot import plotCloudSubset, plotCloudSubsets, plotFrame, plotPointsSubset, plotSamples


class AntipodalParams:
    def __init__(self, extremal_thresh, friction_coeff, min_viable, checks_overlap):
        self.extremal_thresh = extremal_thresh
        self.friction_coeff = friction_coeff
        self.min_viable = min_viable
        self.checks_overlap = checks_overlap


class HandGeometry:
    def __init__(self, finger_width, outer_diameter, depth, height, init_bite):
        self.finger_width = finger_width
        self.outer_diameter = outer_diameter
        self.depth = depth
        self.height = height
        self.init_bite = init_bite


class HandEval:
    def __init__(self, geom, antipodal_params):
        self.hand_geom = geom
        self.antipodal_params = antipodal_params

    def evalHand(self, cloud, left, right, plots=False):
        """Evaluate a hand placement.

        Args:
            cloud (PointCloud): point cloud in the hand frame.
            left (float): left finger position along the hand closing direction.
            right (float): right finger position along the hand closing direction.

        Returns:
            np.array: 4 x 1 geometric information about the grasp.
        """
        top = self.hand_geom.init_bite
        bottom = top - self.hand_geom.depth
        outer = Rect(bottom, left, top, right)
        info = self.evalClosingRegion(cloud, outer, plots)

        return info

    def calcHandBottom(self, pts, left, right, bottom=None):
        """Calculate the bottom position of the grasp.

        The bottom position of the grasp is defined by the finger length and 
        by the 'bite', i.e., how much of the object should be covered by the 
        fingers.

        Args:
            pts (np.array): 3 x n points.
            left (float): left finger position along the hand closing direction.
            right (float): right finger position along the hand closing direction.

        Returns:
            float: the bottom position of the grasp along the hand forward axis.
        """
        top = self.hand_geom.init_bite
        bottom = top - self.hand_geom.depth

        return bottom


    def evalPosesMP(
        self,
        cloud,
        poses,
        tree=None,
        nn_radius=0.20,
        checks_antipodal=True,
        plots=False,
    ):
        if tree == None:
            tree = o3d.geometry.KDTreeFlann(cloud)
        pts = np.asarray(cloud.points)
        nn_idxs = [
            np.asarray(tree.search_radius_vector_3d(x, nn_radius)[1])
            for x in poses[:, :3, 3]
        ]
        print(f"Found {len(nn_idxs)} point neighborhoods")
        pool = mp.Pool(os.cpu_count())
        func = partial(
            self.evalHandMP,
            pts=pts,
            normals=None,
            nn_indices=nn_idxs,
            poses=poses,
            checks_antipodal=checks_antipodal,
        )
        labels = pool.map(func, np.arange(len(poses)))
        pool.close()
        pool.join()
        labels = []
        poses = np.asarray(labels)
        print("[evalPosesMP] done")

        return labels

    def evalPoses(
        self,
        cloud,
        poses,
        tree=None,
        nn_radius=0.20,
        checks_antipodal=True,
        plots=False,
    ):
        """Evaluate a given set of hand poses.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            poses (np.array): n x 4 x 4 poses in the point cloud frame.
            tree (o3d.geometry.KDTreeFlann, optional): the KDTree for the point cloud.
            nn_radius (float, optional): nearest neighbors search radius.
            checks_antipodal (bool, optional): if antipodal grasp is checked.
            plots (bool, optional): if visualizations are plotted.
        """
        if tree == None:
            tree = o3d.geometry.KDTreeFlann(cloud)
        labels = np.ones((len(poses), 5))
        for i, pose in enumerate(poses):
            labels[i] = self.evalPose(
                cloud, pose, tree, nn_radius, checks_antipodal, plots
            )
        return labels

    def evalPose(
        self, cloud, X, tree=None, nn_radius=0.20, checks_antipodal=True, plots=False
    ):
        """Evaluate a given hand pose.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            X (np.array): 4 x 4 pose in the point cloud frame.
            tree (o3d.geometry.KDTreeFlann, optional): the KDTree for the point cloud.
            nn_radius (float, optional): nearest neighbors search radius.
            checks_antipodal (bool, optional): if antipodal grasp is checked.
            plots (bool, optional): if visualizations are plotted.

        Returns:
            np.array: 5 x 1 geometric information about the grasp (see above).
        """
        if tree == None:
            tree = o3d.geometry.KDTreeFlann(cloud)
        [_, nn_idx, _] = tree.search_radius_vector_3d(X[:3, 3], nn_radius)
        if checks_antipodal:
            nn_normals = np.asarray(cloud.normals)[nn_idx, :]
        else:
            nn_normals = []
        nn_cloud = PointCloud.fromNumpy(np.asarray(cloud.points)[nn_idx, :], nn_normals)

        if plots:
            plotFrame(cloud, X, "hand frame")
            plotCloudSubset(cloud, nn_idx, "point neighborhood")

        return self.evalHandPose(nn_cloud, X, checks_antipodal, plots)

    def evalHandMP(
        self, idx, pts, normals, nn_indices, poses, checks_antipodal=True, plots=False
    ):
        # 1. Transform point neighborhood into the hand frame.
        nn_idx = nn_indices[idx]
        R = poses[idx][:3, :3]
        p = poses[idx][:3, 3]
        pts_nn = np.dot(R.T, (pts[nn_idx, :] - p).T)

        # 2. Crop the points based on the hand height.
        mask = (pts_nn[2, :] > -0.5 * self.hand_geom.height) & (
            pts_nn[2, :] < 0.5 * self.hand_geom.height
        )
        pts_nn = pts_nn[:, mask]

        # 2. Push hand forward as much as possible (if desired).
        left = -0.5 * self.hand_geom.outer_diameter
        right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
        is_free = self.isCollisionFree(pts_nn.T, Rect(0, left, 0, right))

        return is_free

    def evalHandPose(self, cloud, X, checks_antipodal=True, plots=False):
        """Evaluate a given hand pose.

        Check if the hand pose is collision free and antipodal, and calculate the
        number of points within the hand closing region and the two extreme points
        along the hand closing direction. The vector returned by this function
        contains these five values.

        Args:
            pts (np.array): 3 x n points.
            normals (np.array): 3 x n surface normals.
            X (np.array): 4 x 4 pose in the point cloud frame.
            checks_antipodal (bool, optional): if antipodal grasp is checked.
            plots (bool, optional): if visualizations are plotted.

        Returns:
            np.array: 5 x 1 geometric information about the grasp (see above).
        """
        # info: (is_collision_free, is_antipodal, occupancy, pt_left, pt_right)
        info = np.array([0, 0, 0, -1, -1], np.float)

        # 1. Transform the points into the hand frame.
        cloud_frame = cloud.transform(np.linalg.inv(X))

        # 2. Crop the points based on the hand height.
        cloud_frame = cloud_frame.filter(
            2, -0.5 * self.hand_geom.height, 0.5 * self.hand_geom.height
        )
        if len(cloud_frame.points) == 0:
            print("Warning: no points found in hand closing region.")
            return info

        # 3. Check if the robot hand displacement is collision-free.
        half = 0.5 * self.hand_geom.outer_diameter
        outer = Rect(
            0,
            -half,
            self.hand_geom.depth,
            half - self.hand_geom.finger_width,
        )
        info[0] = self.isCollisionFree(np.asarray(cloud_frame.points), outer)

        if plots:
            # plotCloudSubset(cloud.cloud, idxs, "-hand_height < pts < hand_height")
            plotFrame(cloud_frame.cloud, np.eye(4), "pts in hand frame")

        # 4. Calculate geometric information about the grasp.
        if info[0] and checks_antipodal:
            # print("Grasp is collision free.")
            info[1:] = self.evalClosingRegion(cloud_frame, outer, plots)
            # if info[1]:
            # print("Grasp is antipodal.")

        return info

    def evalClosingRegion(self, cloud, bounds, plots=False):
        """Calculate geometric information for a grasp.

        Check if the grasp is antipodal and calculate a geometric information vector
        for the points in the hand closing region.

        Args:
            cloud (geometry.PointCloud): the point cloud.
            bounds (geometry.Rect): the outer bounding box of the hand.
            plots (bool, optional): if visualizations are plotted.

        Returns:
            np.array: 4 x 1 vector that contains the geometric information.
        """
        info = [0, 0, -1, -1]

        # Calculate points inside closing region.
        closing = deepcopy(bounds)
        closing.left += self.hand_geom.finger_width
        idxs = np.nonzero(closing.contains(np.asarray(cloud.points)))[0]
        if len(idxs) == 0:
            return info
        cloud_closing = cloud[idxs]

        if plots:
            plotCloudSubset(cloud.cloud, idxs, "points in closing region")

        # Check if the hand placement is antipodal.
        info[0] = self.isAntipodal(cloud_closing, plots)

        # Calculate geometric info about the hand placement.
        if info[0]:
            info[1:] = self.calcGraspInfo(cloud_closing, closing.left, closing.right)

        return info

    def isCollisionFree(self, pts, bounds):
        """Check if the given points are collision-free.

        Args:
            pts (np.ndarray): n x 3 points to be checked.
            bounds (geometry.Rect): the outer bounds of the hand.

        Returns:
            bool: true if the points are collision-free, false otherwise.
        """
        # assert(pts.shape[1] == 3)
        if bounds.top == bounds.bottom:
            bounds.top = self.hand_geom.init_bite
            bounds.bottom = bounds.top - self.hand_geom.depth

        # 1. Check if the back of the hand collides with <pts>.
        if np.any(pts[:, 0] < bounds.bottom):
            # print("[isCollisionFree] Collision with back of hand.")
            return False

        # 2. Check that there are points in between the fingers.
        idxs = np.where(pts[:, 0] < bounds.top)[0]
        if len(idxs) == 0:
            # print("[isCollisionFree] No points between the fingers.")
            return False

        # 3. Check finger placement.
        pts_hand = pts[idxs, 1]

        return self.isFingerPlacementFree(pts_hand, bounds.left, bounds.right)

    def isFingerPlacementFree(self, pts, left, right):
        """Check if the given finger placement is collision-free.

        Args:
            pts (np.ndarray): The 1xn points matrix to be checked.
            left (float): The left corner of the left finger.
            right(float): The left corner of the right finger.

        Returns:
            bool: true if the finger placement is collision-free, false otherwise.
        """
        # 1. Check left finger.
        in_collision = (pts > left) & (pts < left + self.hand_geom.finger_width)
        if np.any(in_collision):
            return False

        # 2. Check right finger.
        in_collision = (pts > right) & (pts < right + self.hand_geom.finger_width)

        return not (np.any(in_collision))

    def calcGraspInfo(self, cloud, left, right):
        """Calculate geometric information for a grasp.

        Args:
            cloud (geometry.PointCloud): [TODO:description]
            left (float): position of left finger.
            right (float): position of right finger.

        Returns:
            list: the geometric information (number of points in the hand closing
                region, most left point, most right point).
        """
        points = np.asarray(cloud.points)
        center = 0.5 * (left + right)
        pt_left = np.min(points[:, 1] - center)
        pt_right = np.max(points[:, 1] - center)
        return [points.shape[0], pt_left, pt_right]

    def isAntipodal(self, cloud, plots=False):
        """Evaluate if the grasp is antipodal.

        Args:
            cloud (PointCloud): point cloud for hand closing region.

        Returns:
            bool: True if the grasp is antipodal, false otherwise.
        """
        friction = self.antipodal_params.friction_coeff
        extremal_thresh = self.antipodal_params.extremal_thresh
        min_viable = self.antipodal_params.min_viable
        cos_friction = np.cos(friction * np.pi / 180.0)
        pts, normals = np.asarray(cloud.points), np.asarray(cloud.normals)

        # Select points that are extremal.
        extremal_left = pts[:, 1] < np.min(pts[:, 1]) + extremal_thresh
        extremal_right = pts[:, 1] > np.max(pts[:, 1]) - extremal_thresh

        if np.sum(extremal_left) < min_viable or np.sum(extremal_right) < min_viable:
            return False

        if plots:
            threshs = np.array([np.min(pts[:, 1]), np.max(pts[:, 1])])
            threshs += np.array([extremal_thresh, -extremal_thresh])
            plotExtremal(cloud.cloud, [extremal_left, extremal_right], threshs)

        # Select points that have their surface normal within the friction cone of the
        # closing direction.
        l, r = np.array([0, -1, 0]), np.array([0, 1, 0])
        closing_left = np.dot(l, normals[extremal_left].T) > cos_friction
        closing_right = np.dot(r, normals[extremal_right].T) > cos_friction

        if np.sum(closing_left) < min_viable or np.sum(closing_right) < min_viable:
            return False

        if self.antipodal_params.checks_overlap:
            pts_left = pts[extremal_left][closing_left]
            pts_right = pts[extremal_right][closing_right]
            top_x = np.min([np.max(pts_left[:, 0]), np.max(pts_right[:, 0])])
            bottom_x = np.max([np.min(pts_left[:, 0]), np.min(pts_right[:, 0])])
            top_z = np.min([np.max(pts_left[:, 2]), np.max(pts_right[:, 2])])
            bottom_z = np.max([np.min(pts_left[:, 2]), np.min(pts_right[:, 2])])
            for points in [pts_left, pts_right]:
                viable_x = (points[:, 0] >= bottom_x) & (points[:, 0] <= top_x)
                viable_z = (points[:, 2] >= bottom_z) & (points[:, 2] <= top_z)
                if np.sum(viable_x & viable_z) < min_viable:
                    return False

        return True


def plotExtremal(cloud, masks, threshs, title="extremal points"):
    """Plot the points that lie in the extremal regions.

    Used to visualize the points in the extremal regions of the points inside the hand
    closing region. Used as a visualization in `isAntipodal`.

    Args:
        cloud (o3d.geometry.PointCloud): the point cloud in the hand frame.
        masks (list): pair of boolean arrays into the point cloud.
        threshs (list): the extremal coordinates along the hand closing axis.
        title (str): the title of the plot window.
    """
    create_box = o3d.geometry.TriangleMesh.create_box
    colors_array = [[1.0, 0.5, 0], [1.0, 0, 0]]
    cloud.paint_uniform_color(np.array([0.0, 0.0, 1.0]))
    colors = np.asarray(cloud.colors)
    for i, mask in enumerate(masks):
        colors[np.nonzero(mask)] = colors_array[i]
    cloud.colors = o3d.utility.Vector3dVector(colors)
    frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.01)
    minpt = np.min(np.asarray(cloud.points), 0)
    maxpt = np.max(np.asarray(cloud.points), 0)
    dist = 1.1 * (maxpt - minpt + 0.01)
    planel = create_box(dist[0], 0.001, dist[2])
    planel.translate([minpt[0] - 0.01, threshs[0], minpt[2] - 0.01])
    planer = create_box(dist[0], 0.001, dist[2])
    planer.translate([minpt[0] - 0.01, threshs[1], minpt[2] - 0.01])
    o3d.visualization.draw_geometries([cloud, frame, planel, planer], title, 640, 480)

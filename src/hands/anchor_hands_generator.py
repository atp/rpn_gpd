import logging
import os
import sys
from functools import partial
from time import time

import numpy as np
import open3d as o3d
import torch
import torch.multiprocessing as mp

from geometry.point_cloud import PointCloud
from geometry.rect import Rect

from .deep_hand_eval import DeepHandEval
from .hands_generator import HandsGenerator

np.set_printoptions(precision=4)


class AnchorHandsGenerator(HandsGenerator):
    def __init__(self, hand_geom, antipodal_params, hand_eval=None):
        if hand_eval is None:
            hand_eval = DeepHandEval(hand_geom, antipodal_params)
        super().__init__(hand_eval, False, "AnchorHandsGenerator")

    def generateHands(
        self,
        cloud: o3d.geometry.PointCloud,
        samples: np.array,
        tree: o3d.geometry.KDTreeFlann = None,
        camera_pose: np.array = [],
    ):
        # samples x angles x criteria
        hands = np.zeros((len(samples), self.num_hand_orientations, 5))

        if tree == None:
            tree = o3d.geometry.KDTreeFlann(cloud)
        pts = np.asarray(cloud.points)
        normals = np.asarray(cloud.normals)
        pool = mp.Pool(os.cpu_count())

        idxs = [tree.search_radius_vector_3d(x, self.nn_radius)[1] for x in samples]
        pts_nn = [pts[idx] - x for x, idx in zip(samples, idxs)]
        normals_nn = [normals[idx] for idx in idxs]

        n = len(samples)
        m = self.getNumHandsPerSample()
        total = n * m
        func = partial(self.evalHandIdx, pts=pts_nn, normals=normals_nn)
        hands = pool.map(func, np.arange(total))
        pool.close()
        pool.join()
        hands = np.asarray(hands).reshape((n, m, -1))

        return hands, []

    def evalHandIdx(self, idx: int, pts: list, normals: list) -> np.array:
        """Evaluate a hand pose at a given linear index.

        The hand pose is calculated for sample `i` and rotation `j`. These row
        and column indices are calculated from the linear index :param:`idx`.

        Args:
            idx (int): the linear index into the hand poses matrix.
            pts (list[np.array]): list of (n,3) point neighborhoods.
            normals (list[np.array]): list of (n,3) normals for neighborhoods.

        Returns:
            np.array: (5,) geometric grasp information (see below).
        """
        # info: (is_collision_free, is_antipodal, #occupancy, pt_left, pt_right)
        info = np.array([0, 0, 0, -1, -1], np.float)

        rot_idx = idx % self.getNumHandsPerSample()
        sample_idx = idx // self.getNumHandsPerSample()
        pts_nn = pts[sample_idx]
        normals_nn = normals[sample_idx]
        # print(idx, rot_idx, sample_idx)

        # 1. Rotate the points into the hand frame.
        R = self.calcRotation(rot_idx)
        pts_frame = np.dot(R.T, pts_nn.T)

        # 2. Crop the points based on the hand height.
        mask = (pts_frame[2, :] > -0.5 * self.hand_geom.height) & (
            pts_frame[2, :] < 0.5 * self.hand_geom.height
        )
        pts_frame = pts_frame[:, mask]
        if len(pts_frame) == 0:
            return info

        # 3. Check if the initial robot hand displacement is collision-free.
        left = -0.5 * self.hand_geom.outer_diameter
        right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
        info[0] = self.hand_eval.isCollisionFree(pts_frame.T, Rect(0, left, 0, right))

        # 4. Check if the deep robot hand displacement is antipodal.
        if info[0]:
            normals_frame = np.dot(R.T, normals_nn.T[:, mask])
            cloud = PointCloud.fromNumpy(pts_frame.T, normals_frame.T)
            info[1:] = self.hand_eval.evalHand(cloud, left, right)

        return info

    # Evaluate a hand displacement in the hand's frame.
    # Evaluate if the initial hand is collision-free and the deep hand is antipodal.
    # Return geometric info about the hand.
    def evalHand(self, idx, pts, normals):
        # info: (is_collision_free, is_antipodal, occupancy, pt_left, pt_right)
        info = np.array([0, 0, 0, -1, -1], np.float)

        # 1. Rotate the points into the hand frame.
        R = self.calcRotation(idx)
        pts_frame = np.dot(R.T, pts.T)

        # 2. Crop the points based on the hand height.
        mask = (pts_frame[2, :] > -0.5 * self.hand_geom.height) & (
            pts_frame[2, :] < 0.5 * self.hand_geom.height
        )
        pts_frame = pts_frame[:, mask]
        if len(pts_frame) == 0:
            return info

        # 3. Check if the initial robot hand displacement is collision-free.
        left = -0.5 * self.hand_geom.outer_diameter
        right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
        info[0] = self.hand_eval.isCollisionFree(pts_frame.T, Rect(0, left, 0, right))

        # 4. Check if the deep robot hand displacement is antipodal.
        if info[0]:
            normals_frame = np.dot(R.T, normals.T[:, mask])
            cloud = PointCloud.fromNumpy(pts_frame.T, normals_frame.T)
            info[1:] = self.hand_eval.evalHand(cloud, left, right)

        return info

    def getNumHandsPerSample(self):
        return self.num_hand_orientations

    def indicesToPosesGivenScene(self, scene, indices):
        return self.indicesToPoses(
            scene.cloud, scene.cloud_tree, scene.samples, indices
        )

    def indicesToPosesNoCollisionCheck(self, samples, indices):
        print("[indicesToPosesNoCollisionCheck] Not collision checking")
        func = self.indexToPoseNoCollisionChecks
        poses = [func(i, indices, samples) for i in range(indices.shape[1])]
        poses = np.asarray(poses)

        return poses

    def indicesToPosesMP(self, cloud, tree, samples, hands, push_forward=True):
        if push_forward == False:
            return self.indicesToPosesNoCollisionCheck(samples, hands["indices"])

        indices = hands["indices"]
        pts = np.asarray(cloud.points)
        nn_idxs = [
            np.asarray(tree.search_radius_vector_3d(x, self.nn_radius)[1])
            for x in samples
        ]
        print(f"Found {len(nn_idxs)} point neighborhoods")
        pool = mp.Pool(os.cpu_count())
        func = partial(
            self.indexToPoseMP,
            indices=indices,
            nn_indices=nn_idxs,
            pts=pts,
            samples=samples,
            push_forward=push_forward,
        )
        poses = pool.map(func, np.arange(indices.shape[1]))
        pool.close()
        pool.join()
        poses = np.asarray(poses)

        return poses

    # Transform a set of index pairs to a set of SE3 poses.
    def indicesToPoses(self, cloud, tree, samples, hands, push_forward=True):
        indices = hands["indices"]
        poses = np.zeros((0, 4, 4))
        pts = np.asarray(cloud.points)

        for i in range(indices.shape[1]):
            sample_idx = indices[0, i]
            grid_idx = indices[1, i]
            T = self.indexToPose(sample_idx, grid_idx, pts, tree, samples, push_forward)
            poses = np.vstack((poses, T[np.newaxis]))

        return poses

    def indexToPoseNoCollisionChecks(self, idx, indices, samples):
        bottom = self.hand_geom.init_bite - self.hand_geom.depth
        sample_idx = indices[0, idx]
        grid_idx = indices[1, idx]
        sample = samples[sample_idx]
        R = self.calcRotation(grid_idx)
        T = np.eye(4, 4)
        T[:3, 3] = np.dot(R, np.array([bottom, 0.0, 0.0])) + sample
        T[:3, :3] = R

        return T

    def indexToPoseMP(self, idx, indices, nn_indices, pts, samples, push_forward=True):
        bottom = self.hand_geom.init_bite - self.hand_geom.depth
        sample_idx = indices[0, idx]
        grid_idx = indices[1, idx]
        sample = samples[sample_idx]
        R = self.calcRotation(grid_idx)

        if push_forward:
            # 1. Transform point neighborhood into the hand frame.
            nn_idx = nn_indices[sample_idx]
            pts_nn = self.transformToHandFrame(R, pts[nn_idx], sample)

            # 2. Push hand forward as much as possible (if desired).
            left = -0.5 * self.hand_geom.outer_diameter
            right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
            is_free = self.hand_eval.isCollisionFree(pts_nn.T, Rect(0, left, 0, right))
            if is_free:
                bottom = self.hand_eval.calcHandBottom(pts_nn, left, right)

        # Calculate SE(3) pose.
        T = calcPose(R, sample, bottom)

        return T

    def indexToPose(self, sample_idx, grid_idx, pts, tree, samples, push_forward=True):
        # 1. Find spherical point neighborhood around sample point.
        sample = samples[sample_idx, :]
        R = self.calcRotation(grid_idx)
        [_, nn_idx, _] = tree.search_radius_vector_3d(sample, self.nn_radius)
        pts_nn = self.transformToHandFrame(R, pts[nn_idx], sample)

        # 2. Push hand forward as much as possible.
        left = -0.5 * self.hand_geom.outer_diameter
        right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
        bottom = self.hand_geom.init_bite - self.hand_geom.depth
        is_free = self.hand_eval.isCollisionFree(pts_nn.T, Rect(0, left, 0, right))
        if is_free and push_forward:
            bottom = self.hand_eval.calcHandBottom(pts_nn, left, right)

        # Calculate SE(3) pose.
        T = calcPose(R, sample, bottom)

        return T

    def transformToHandFrame(self, R, pts, sample):
        # 1. Transform point neighborhood into the hand frame.
        pts_nn = np.dot(R.T, (pts - sample).T)

        # 2. Crop the points based on the hand height.
        mask = (pts_nn[2, :] > -0.5 * self.hand_geom.height) & (
            pts_nn[2, :] < 0.5 * self.hand_geom.height
        )
        pts_nn = pts_nn[:, mask]

        return pts_nn

    def calcRotations(self, indices):
        rots = np.zeros((len(indices), 3, 3))
        for i in range(len(indices)):
            rots[i] = self.calcRotation(indices[i])
        return rots

    def calcRotation(self, idx):
        raise NotImplementedError("Please Implement this method")

    def preprocess(self, cloud, samples, camera_pose):
        return cloud, samples


def calcPose(R, sample, bottom):
    T = np.eye(4, 4)
    T[:3, 3] = np.dot(R, np.array([bottom, 0.0, 0.0])) + sample
    T[:3, :3] = R

    return T

"""Module for hand generators.

A hands generator takes a point cloud as input and produces robot hand poses as output. 
The poses are represented by an n x m x k array where n is the number of sample points, 
m is the number of possible grasp orientations, and k is the length of a geometric 
information vector about the grasp. This vector stores if the grasp is collision free, 
antipodal, how many points lie within the hand closing region, and the two most distant 
points along the hand closing direction (left and right).
"""

import numpy as np


class HandsGenerator:
    """Base class for hands generators.
    """

    def __init__(self, hand_eval, has_translation, name):
        self.hand_eval = hand_eval
        self.hand_geom = self.hand_eval.hand_geom
        self.has_translation = has_translation
        self.name = name
        self.nn_radius = np.max(
            [self.hand_geom.depth, self.hand_geom.height,
                self.hand_geom.outer_diameter]
        )

    def generateHands(self, cloud, samples, tree=None, camera_pose=[]):
        """Generate hands.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud for which to generate hands.
            samples (np.array): 3 x n positions of samples.

        Returns:
            np.array: n x m x k array with geometric information about hand poses.
        """
        raise NotImplementedError("Please implement this method")

    def getNumHandsPerSample(self):
        """Return the number of hand poses to be evaluated per sample point.
        """
        raise NotImplementedError("Please implement this method")

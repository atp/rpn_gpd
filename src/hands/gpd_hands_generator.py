from itertools import chain

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from geometry.point_cloud import PointCloud
from geometry.rect import Rect

from .deep_hand_eval import DeepHandEval
from .hands_generator import HandsGenerator

np.set_printoptions(precision=4)


class GpdHandsGenerator(HandsGenerator):
    def __init__(self, hand_geom, antipodal_params, grid_rots=8, grid_trans=10):
        hand_eval = DeepHandEval(hand_geom, antipodal_params)
        super().__init__(hand_eval, True, "GpdHandsGenerator")

        self.grid_rots = grid_rots  # number of rotations about hand rotation axis
        self.grid_trans = grid_trans  # number of translation along hand closing axis
        self.angles = np.linspace(-np.pi / 2.0, np.pi / 2.0, self.grid_rots + 1)
        self.angles = self.angles[:-1]
        print("angles:", self.angles)

        # Calculate finger positions.
        if grid_trans > 1:
            inner = self.hand_geom.outer_diameter - self.hand_geom.finger_width
            fs_half = np.linspace(0, inner, self.grid_trans)
            self.finger_spacing = np.hstack((fs_half - inner, fs_half))
            print("fs_half:", np.array(fs_half))
        elif grid_trans == 1:
            left = -0.5 * self.hand_geom.outer_diameter
            right = 0.5 * self.hand_geom.outer_diameter - self.hand_geom.finger_width
            self.finger_spacing = np.array([left, right])
        print("finger_spacing:", self.finger_spacing)

        self.R_binormal = Rot.from_rotvec(np.pi * np.array([0, 1, 0])).as_matrix()
        print("R_binormal:\n", self.R_binormal)

        # TODO: give this a better name?
        self.num_hand_orientations = self.grid_rots * self.grid_trans

    def getNumHandsPerSample(self):
        return self.grid_rots * self.grid_trans

    def generateHands(self, cloud, samples, check_antipodal, tree=None, camera_pose=[]):
        hands = np.zeros((len(samples), self.grid_rots * self.grid_trans, 5))
        if tree == None:
            tree = o3d.geometry.KDTreeFlann(cloud)
        pts = np.asarray(cloud.points)
        normals = np.asarray(cloud.normals)

        # 1. Estimate local reference frames.
        frames = estimateLocalFrames(tree, normals.T, samples)

        # 2. Evaluate hand displacements using grid search.
        for i in range(len(frames)):
            [_, nn_idx, _] = tree.search_radius_vector_3d(samples[i], self.nn_radius)
            nn_cloud = PointCloud.fromNumpy(
                pts[nn_idx, :] - samples[i], normals[nn_idx, :]
            )
            hands[i] = self.evalHandSet(nn_cloud, frames[i], check_antipodal)

        self.frames = frames

        return hands, frames

    def evalHandSet(self, cloud, F, check_antipodal):
        hand_set = np.zeros((self.grid_rots * self.grid_trans, 5))

        for i in range(self.grid_rots):
            R = self.calcRotation(i, np.array([0, 0, 1]))
            R = np.linalg.multi_dot([F, self.R_binormal, R])
            start = i * self.grid_trans
            stop = start + self.grid_trans
            hand_set[start:stop] = self.evalHandsAtRotation(cloud, R, check_antipodal)

        return hand_set

    def evalHandsAtRotation(self, cloud, R, check_antipodal):
        # info: (collision_free?, antipodal?, occupancy, pt_left, pt_right)
        info = np.array([0, 0, 0, -1, -1], np.float)
        hands = np.tile(info, (self.grid_trans, 1))

        # 1. Rotate the points into the hand frame.
        cloud_frame = cloud.rotate(R.T)

        # 2. Crop the points based on the hand height.
        cloud_frame = cloud_frame.filter(
            2, -0.5 * self.hand_geom.height, 0.5 * self.hand_geom.height
        )
        if len(cloud_frame.points) == 0:
            print("Warning: no points found in hand closing region.")
            return info

        # 3. Check if the back of the hand is in collision.
        pts_frame = np.asarray(cloud_frame.points)
        bottom = self.hand_geom.init_bite - self.hand_geom.depth
        if np.any(pts_frame[:, 0] < bottom):
            return hands

        # 4. Find the points in between bottom and top of the hand.
        top = bottom + self.hand_geom.depth
        mask = pts_frame[:, 0] < top
        if np.count_nonzero(mask) == 0:
            return hands

        # 5. Find collision-free finger placements.
        is_placement_free = self.evalFingersAtTranslations(pts_frame[mask, :])
        # Uncomment the line below to allow non-centered hands.
        hands[:, 0] = is_placement_free

        if check_antipodal and np.any(is_placement_free):
            idxs = np.nonzero(is_placement_free)[0]
            mid = int(len(self.finger_spacing) / 2)
            for i in idxs:
                hands[i, 1:] = self.hand_eval.evalHand(
                    cloud_frame,
                    self.finger_spacing[i],
                    self.finger_spacing[mid + i],
                )

        return hands

    def findProposals(self, hands):
        """Find grasp proposals.

        Grasp proposals for GPD are collision-free, centered hands.

        Args:
            hands ([TODO:type]): n x m x 5 information about grasps.

        Returns:
            np.array: n x m proposals.
        """
        props = np.zeros(hands.shape[:2])
        coll_free = hands[:, :, 0] == 1
        t = self.grid_trans
        r = self.grid_rots
        idxs = [
            [j * t + np.nonzero(coll_free[i, j * t : (j + 1) * t])[0] for j in range(r)]
            for i in range(len(coll_free))
        ]
        mid_idxs = [
            [[i, x[int(np.ceil(len(x) / 2)) - 1]] for x in y if len(x) > 0]
            for i, y in enumerate(idxs)
        ]
        mid_idxs = np.array(list(chain.from_iterable(mid_idxs)))
        props[mid_idxs[:, 0], mid_idxs[:, 1]] = 1

        return props

    def chooseMiddleHandIndex(self, is_placement_free):
        free_idx = np.nonzero(is_placement_free)[0]
        middle_idx = free_idx[int(np.ceil(len(free_idx) / 2)) - 1]

        return middle_idx

    def chooseMiddleHand(self, is_placement_free):
        free_idx = np.nonzero(is_placement_free)[0]
        middle_idx = free_idx[int(np.ceil(len(free_idx) / 2)) - 1]
        mid = int(len(self.finger_spacing) / 2)
        left = self.finger_spacing[middle_idx]
        right = self.finger_spacing[mid + middle_idx]

        return left, right, middle_idx

    def evalFingersAtTranslations(self, pts):
        mid = int(len(self.finger_spacing) / 2)
        is_free = [False] * self.grid_trans

        for i in range(self.grid_trans):
            left = self.finger_spacing[i]
            right = self.finger_spacing[mid + i]
            is_free[i] = self.hand_eval.isFingerPlacementFree(pts, left, right)

        return is_free

    def calcRotation(self, i, axis):
        return Rot.from_rotvec(self.angles[i] * axis).as_matrix()

    def calcRotations(self, indices):
        rots = np.zeros((len(indices), 3, 3))
        for i in range(len(indices)):
            rot_idx = int(np.floor(indices[i] / self.grid_trans))
            rots[i] = self.calcRotation(rot_idx, np.array([0, 0, 1]))
        return rots

    def indicesToPoses(self, cloud, tree, samples, hands):
        frames, indices = hands["frames"], hands["indices"]
        poses = np.zeros((0, 4, 4))
        pts = np.asarray(cloud.points)

        for i in range(indices.shape[1]):
            sample_idx = indices[0, i]
            grid_idx = indices[1, i]
            T = self.indexToPose(
                sample_idx, grid_idx, pts, tree, samples, frames[sample_idx]
            )
            poses = np.vstack((poses, T[np.newaxis]))

        # for p in poses:
        # q = Rot.from_dcm(p[:3,:3]).as_euler('zyx', degrees=True)
        # print(f'(r,t) = ({q}, {p[:3,3]})')

        return poses

    def indexToPose(self, sample_idx, grid_idx, pts, tree, samples, F):
        # 1. Find spherical point neighborhood around sample point.
        sample = samples[sample_idx, :]
        [_, nn_idx, _] = tree.search_radius_vector_3d(sample, self.nn_radius)
        print(f"nn_idx: {nn_idx}")

        # 2. Transform point neighborhood into hand frame.
        rot_idx = int(np.floor(grid_idx / self.grid_trans))
        R = self.calcRotation(rot_idx, np.array([0, 0, 1]))
        R = np.linalg.multi_dot([F, self.R_binormal, R])
        pts_nn = np.dot(R.T, (pts[nn_idx, :] - sample).T)

        # 3. Crop the points based on the hand height.
        mask = (pts_nn[2, :] > -0.5 * self.hand_geom.height) & (
            pts_nn[2, :] < 0.5 * self.hand_geom.height
        )
        pts_nn = pts_nn[:, mask]

        # 2. Calculate the initial hand placement.
        trans_idx = grid_idx % self.grid_trans
        mid = int(len(self.finger_spacing) / 2)
        left = self.finger_spacing[trans_idx]
        right = self.finger_spacing[mid + trans_idx]
        bottom = self.hand_geom.init_bite - self.hand_geom.depth

        # 3. Calculate the deepest hand placement.
        if self.hand_eval.isCollisionFree(pts_nn.T, Rect(0, left, 0, right)):
            bottom = self.hand_eval.calcHandBottom(pts_nn, left, right)
        top = bottom + self.hand_geom.depth

        # Calculate SE(3) pose.
        left += self.hand_geom.finger_width
        center = 0.5 * (left + right)
        T = np.eye(4, 4)
        T[:3, 3] = np.dot(R, np.array([bottom, center, 0.0])) + sample
        T[:3, :3] = R

        return T


def estimateLocalFrames(tree, normals, samples):
    radius = 0.01
    frames = []

    for i in range(len(samples)):
        [_, indices, _] = tree.search_radius_vector_3d(samples[i], radius)
        # print(f'[estimateLocalFrames] sample: {samples[i]}, indices: {len(indices)}')
        frames.append(estimateLocalFrame(normals[:, indices]))

    return frames


def estimateLocalFrame(normals):
    frame = np.zeros((3, 3))

    # 1. Calculate minor principal curvature axis.
    M = np.dot(normals, normals.T)
    w, V = np.linalg.eig(M)
    w = w.real
    V = V.real
    frame[:, 2] = V[:, np.argmin(w)]

    # 2. Calculate normal.
    frame[:, 0] = V[:, np.argmax(w)]

    # 3. Ensure the normal points in the same direction as the other normals.
    avg_normal = np.sum(normals, 1)
    avg_normal /= np.linalg.norm(avg_normal)
    if np.dot(avg_normal, frame[:, 0]) < 0:
        frame[:, 0] *= -1.0

    # 3. Calculate binormal.
    frame[:, 1] = np.cross(frame[:, 2], frame[:, 0])

    return frame

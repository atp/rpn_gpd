import numpy as np

from geometry.point_cloud import PointCloud
from geometry.rect import Rect

from .hand_eval import HandEval


class DeepHandEval(HandEval):
    def evalHand(self, cloud, left, right, plots=False):
        """Evaluate a deep hand placement.

        Args:
            cloud (PointCloud): point cloud in the hand frame.
            left (float): left finger position along the hand closing direction.
            right (float): right finger position along the hand closing direction.

        Returns:
            np.array: 4 x 1 geometric information about the grasp.
        """
        # Calculate the deepest collision-free hand placement along the approach axis.
        bottom = self.calcHandBottom(np.asarray(cloud.points).T, left, right)
        top = bottom + self.hand_geom.depth
        outer = Rect(bottom, left, top, right)

        info = super().evalClosingRegion(cloud, outer, plots)

        return info

    def calcHandBottom(self, pts, left, right, bottom=None):
        """Calculate the bottom position of the most deep grasp.

        Args:
            pts (np.array): 3 x n points.
            left (float): left finger position along the hand closing direction.
            right (float): right finger position along the hand closing direction.

        Returns:
            float: the bottom position of the grasp along the hand forward axis.
        """
        offset = 0.005  # offset between hand bottom and object boundary
        finger_width = self.hand_geom.finger_width

        # 1. Find points that lie within the outer hand bounds.
        if bottom == None:
            top = self.hand_geom.depth
            bottom = self.hand_geom.init_bite - self.hand_geom.depth
        else:
            top = bottom + self.hand_geom.depth 
        mask = (pts[0, :] > bottom) & (pts[0, :] < top)
        pts_in = pts[:, mask]

        # 2. Find closest point along forward axis.
        idx = np.argmin(pts_in[0, :])
        closest = pts_in[:, idx]

        # Case #1: closest point lies inside left finger.
        if (closest[1] > left) & (closest[1] < left + finger_width):
            bottom = closest[0] - self.hand_geom.depth - offset
        # Case #2: closest point lies inside right finger.
        elif (closest[1] > right) & (closest[1] < right + finger_width):
            bottom = closest[0] - self.hand_geom.depth - offset
        # Case #3: closest point lies inside hand closing region.
        else:
            bottom = closest[0] - offset
            top = bottom + self.hand_geom.depth

            # Check if the fingers at this new placement are in collision.
            mask = pts[0, :] < top
            if np.any(mask) > 0:
                pts = pts[:, mask]
                l = (pts[1, :] > left) & (pts[1, :] < left + finger_width)
                r = (pts[1, :] > right) & (pts[1, :] < right + finger_width)
                pts = pts[:, np.logical_or(l, r)]
                if len(pts) > 0 and pts.shape[1] > 0:
                    bottom = np.min(pts[0, :]) - self.hand_geom.depth - offset

        return bottom

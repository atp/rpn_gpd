from copy import deepcopy

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from utils.cloud_utils import numpyToCloud

from .anchor_hands_generator import AnchorHandsGenerator

np.set_printoptions(precision=4)


class GqdHandsGenerator(AnchorHandsGenerator):
    def __init__(self, hand_geom, antipodal_params, longitudes=9, latitudes=5, approaches=4):
        super().__init__(hand_geom, antipodal_params)

        self.name = "QuarterDomeHandsGenerator"
        self.num_hand_orientations = longitudes * latitudes * approaches

        self.num_longitudes = longitudes
        self.num_latitudes = latitudes
        self.num_approaches = approaches

        # Transforms from camera to base, and from base to camera.
        self.bXc = []
        self.cXb = []

        # Initial hand orientation. The camera frame has z forward, y down, and x right.
        roti = np.array([[-1, 0, 0],
                         [0, 1, 0],
                         [0, 0, -1]])
        half_pi = np.pi / 2.0
        longitude_angles = np.linspace(-half_pi, half_pi, longitudes)
        latitude_angles = np.linspace(0.0, half_pi, latitudes)
        approach_angles = np.linspace(-half_pi, half_pi, approaches + 1)[:-1]
        if approaches == 1:
            approach_angles = [0]
        rots_z = [Rot.from_rotvec(r * np.array([0.0, 0.0, 1.0])).as_matrix()
                  for r in longitude_angles]
        rots_y = [Rot.from_rotvec(r * np.array([0.0, -1.0, 0.0])).as_matrix()
                  for r in latitude_angles]
        rots_x = [Rot.from_rotvec(r * np.array([1.0, 0.0, 0.0])).as_matrix()
                  for r in approach_angles]
        print([len(x) for x in [rots_z, rots_y, rots_x]])
        print("det(roti):", np.linalg.det(roti))
        self.rotations = []
        # self.rotations = [np.linalg.multi_dot([roti, rotz]) for rotz in rots_z]
        # self.rotations = [np.linalg.multi_dot([roti, rotz]) for rotz in rots_z]
        for rotz in rots_z:
            for roty in rots_y:
                for rotx in rots_x:
                    # r = roti
                    # r = np.linalg.multi_dot([roti, rotz])
                    # r = np.linalg.multi_dot([roti, roty])
                    # r = np.linalg.multi_dot([roti, rotx])
                    # r = np.linalg.multi_dot([roti, rotz, roty])
                    r = np.linalg.multi_dot([roti, rotz, roty, rotx])
                    self.rotations.append(r)
        self.num_hand_orientations = len(self.rotations)
        print("longitude_angles:", [np.rad2deg(x) for x in longitude_angles])
        print("latitude_angles:", [np.rad2deg(x) for x in latitude_angles])
        print("approach_angles:", [np.rad2deg(x) for x in approach_angles])
        print("num_hand_orientations:", self.num_hand_orientations,
              "#rotations:", len(self.rotations))

    def calcRotation(self, idx):
        return self.rotations[idx]

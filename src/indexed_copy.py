"""Copy every k-th column of a given database to a new database.
"""

import os
import sys

import numpy as np
import tables as tab

os.environ["BLOSC_NTHREADS"] = "8"
filters = tab.Filters(complevel=9, complib="blosc:lz4")

np.set_printoptions(precision=3)

if len(sys.argv) < 4:
    print("Error: not enough input arguments!")
    print("Usage: python shrink_columns.py DB_IN DB_OUT STEP")
    exit(-1)

with tab.open_file(sys.argv[1], "r") as f:
    labels = np.asarray(f.root.labels)
    if labels.ndim != 2:
        print(f"Error: <labels> node has the wrong dimensions: {labels.shape}")
        exit(-1)

    # Remove the columns up to <index> from the labels node.
    step = int(sys.argv[3])
    # labels_out = labels[:, ::step]
    labels_out = labels[:, :step]
    images_out = np.array(f.root.images)

    # Store the remaining columns in a new database.
    with tab.open_file(sys.argv[2], "w") as fout:
        labels_dset = fout.create_carray("/", "labels", obj=labels_out, filters=filters)
        print(f"Created dataset: {labels_dset}")
        images_dset = fout.create_carray(
            "/", "images", tab.UInt8Atom(), obj=images_out, filters=filters
        )
        print(f"Created dataset: {images_dset}")

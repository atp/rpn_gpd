"""Visual unit test for antipodal evaluation.
TODO: adjust for new hand evaluator
"""

import numpy as np
import open3d as o3d
from scipy.spatial.transform import Rotation as Rot

from hands.hand_eval import evalAntipodalCloud
from utils.cloud_utils import estimateNormals, numpyToCloud
from utils.open3d_plot import plotCloudSubset

origin_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.02)
cylinder = o3d.geometry.TriangleMesh.create_cylinder(radius=0.02, height=0.10)
cylinder.translate([0.2, 0, 0])
cloud = cylinder.sample_points_uniformly(5000)
# cloud = estimateNormals(cloud)
cloud = estimateNormals(cloud, [0.2, 0, 0], True)
tree = o3d.geometry.KDTreeFlann(cloud)

R = np.array([[0, 0, 1], [0, 1, 0], [-1, 0, 0]])
p = np.array([0.2, 0, 0.1])
T = np.eye(4)
T[:3, :3] = R
T[:3, 3] = p
hand_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(0.02)
hand_frame.transform(T)
# hand_frame.translate(p)
# hand_frame.rotate(R)
# hand_frame.translate([0.2, 0, 0.1])

o3d.visualization.draw_geometries(
    [cloud, origin_frame, hand_frame], width=640, height=480
)

# x = np.array([0.2, 0, 0.05])
# x = np.array([0.2, 0, 0.07])
x = np.array([0.2, 0, 0.09])
[_, nn_idxs, _] = tree.search_radius_vector_3d(x, 0.05)
plotCloudSubset(cloud, nn_idxs, "point neighborhood")

# neighbors_cloud.transform(T.T)
X, N = np.asarray(cloud.points)[nn_idxs], np.asarray(cloud.normals)[nn_idxs]
neighbors_cloud = numpyToCloud(X, N)
T[:3, 3] = x
neighbors_cloud.transform(np.linalg.inv(T))
o3d.visualization.draw_geometries(
    [neighbors_cloud, origin_frame], width=640, height=480
)

is_antipodal = evalAntipodalCloud(neighbors_cloud, True)
print(f"is_antipodal: {is_antipodal}")

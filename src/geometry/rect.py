class Rect:
    def __init__(self, bottom, left, top, right):
        self.bottom = bottom
        self.left = left
        self.top = top
        self.right = right

    def contains(self, pts):
        return (
            (pts[:, 0] > self.bottom)
            & (pts[:, 0] < self.top)
            & (pts[:, 1] > self.left)
            & (pts[:, 1] < self.right)
        )

    def __str__(self):
        s = f"bottom: {self.bottom:.3f}"
        s += f" left: {self.left:.3f}"
        s += f" top: {self.top:.3f}"
        s += f" right: {self.right:.3f}"
        return s

"""A point cloud that can be sliced like a numpy array.
"""

from __future__ import annotations

from copy import deepcopy
from typing import Iterable

import numpy as np
import open3d as o3d

# from ..utils.cloud_utils import numpyToCloud # this does not work


def numpyToCloud(points, normals=[], colors=[]):
    """Create an open3d point cloud from a numpy array.

    Args:
        pts (np.array): 3 x n the points to be stored in the point cloud.
        normals (np.array, optional): 3 x n the surface normals.
        colors (np.array, optional): 3 x n the colors.

    Returns:
        o3d.geometry.PointCloud: the point cloud.
    """
    cloud = o3d.geometry.PointCloud()
    cloud.points = o3d.utility.Vector3dVector(points)
    if len(normals) > 0:
        cloud.normals = o3d.utility.Vector3dVector(normals)
    if len(colors) > 0:
        cloud.colors = o3d.utility.Vector3dVector(colors)

    return cloud


class PointCloud:
    """A point cloud wrapper.

    The main purpose of this class is to provide point clouds that can be sliced like 
    regular numpy arrays. The attributes :attribute:`points`, :attribute:`normals`, 
    and :attribute:`colors` are references to the corresponding attributes of 
    :attribute:`cloud`.

    Because nearest neighbor searches are frequently performed, this class can 
    optionally store a KDTree for the point cloud.

    Attributes:
        cloud (o3d.geometry.PointCloud): the point cloud.
        points (o3d.utility.Vector3dVector): the points stored in the point cloud.
        normals (o3d.utility.Vector3dVector): the surface normals stored in the
            point cloud.
        colors (o3d.utility.Vector3dVector): the colors stored in the point cloud.
    """

    def __init__(self, cloud: o3d.geometry.PointCloud):
        self.cloud = cloud
        self.points = self.cloud.points
        self.normals = self.cloud.normals
        self.colors = self.cloud.colors
        self.tree = o3d.geometry.KDTreeFlann(self.cloud)

    @staticmethod
    def fromNumpy(points: np.array, normals: np.array = [], colors: np.array = []):
        """Create a point cloud from numpy arrays.

        Args:
            points (np.array): the points to be stored in the point cloud.
            normals (np.array, optional): the surface normals to be stored in
                the point cloud.
            colors (np.array, optional): the colors to be stored in the point cloud.
        """
        cloud = numpyToCloud(points, normals, colors)

        return PointCloud(cloud)

    def transform(self, X: np.array) -> PointCloud:
        """Apply a rigid transformation to the point cloud.

        Args:
            X (np.array): 4x4 transformation matrix.

        Returns:
            PointCloud: the transformed point cloud.
        """
        cloud = deepcopy(self.cloud)
        cloud.transform(X)

        return PointCloud(cloud)

    def rotate(self, R: np.array) -> PointCloud:
        """Apply a rotation to the point cloud.

        Args:
            R (np.array): 3x3 rotation matrix.

        Returns:
            PointCloud: the rotated point cloud.
        """
        cloud = deepcopy(self.cloud)
        # cloud.rotate(R) # this does not work
        X = np.eye(4)
        X[:3, :3] = R
        cloud.transform(X)

        return PointCloud(cloud)

    def filter(self, axis: int, min_val: float, max_val: float) -> PointCloud:
        """Filter out points outside of the given bounds.

        Args:
            axis (int): the axis along which the points are filtered out.
            min_val (float): the value below which points are filtered out.
            max_val (float): the value above which points are filtered out.

        Returns:
            PointCloud: the reduced point cloud.
        """
        points = np.asarray(self.points)
        mask = (points[:, axis] > min_val) & (points[:, axis] < max_val)

        return self[np.nonzero(mask)[0]]

    def crop(self, mins: Iterable[float], maxs: Iterable[float]) -> PointCloud:
        """Crop points from this point cloud into a new one.

        Args:
            mins (Iterable[float]): the min values of the crop.
            maxs (Iterable[float]): the max values of the crop.

        Returns:
            PointCloud: the reduced point cloud.
        """
        points = np.asarray(self.points)
        mask = (
            (points[:, 0] > mins[0])
            & (points[:, 1] > mins[1])
            & (points[:, 2] > mins[2])
            & (points[:, 0] < maxs[0])
            & (points[:, 1] < maxs[1])
            & (points[:, 2] < maxs[2])
        )

        return self[np.nonzero(mask)[0]]

    def __getitem__(self, key):
        points, normals, colors = [], [], []
        if len(self.cloud.points) > 0:
            points = np.atleast_2d(np.asarray(self.cloud.points)[key])
        if len(self.cloud.normals) > 0:
            normals = np.atleast_2d(np.asarray(self.cloud.normals)[key])
        if len(self.cloud.colors) > 0:
            colors = np.atleast_2d(np.asarray(self.cloud.colors)[key])
        cloud = numpyToCloud(points, normals, colors)

        return PointCloud(cloud)

    def __str__(self):
        s = f"PointCloud with {len(self.cloud.points)} points, "
        s += f"{len(self.cloud.normals)} normals, and "
        s += f"{len(self.cloud.colors)} colors"
        return s


# from scipy.spatial.transform import Rotation as Rot

# X = np.eye(4)
# X[:3, :3] = Rot.from_rotvec(np.pi / 2 * np.array([1, 0, 0])).as_matrix()
# X[:3, 3] = [3, 2, 1]
# c = PointCloud(np.random.randn(4, 3), np.random.randn(4, 3))
# d = c.transform(X)
# e = c.filter(2, 0, 1)
# print(np.asarray(c.points))
# print(np.asarray(d.points))
# print(np.asarray(e.points))
# print(np.asarray(c.normals))
# print(np.asarray(d.normals))
# t = c.tree
# print(t)

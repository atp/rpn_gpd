"""A neighborhood of points."""

from __future__ import annotations

from dataclasses import dataclass, field

import numpy as np
import open3d as o3d

from .point_cloud import PointCloud


@dataclass
class Neighborhood:
    sample: np.array = np.empty(3, dtype=float)
    points: np.array = np.empty((0, 3), dtype=float)
    normals: np.array = np.empty((0, 3), dtype=float)

    @staticmethod
    def fromPointCloud(
        cloud: PointCloud, sample: np.array, radius: float
    ) -> Neighborhood:
        [_, idxs, _] = cloud.tree.search_radius_vector_3d(sample, radius)
        nn_cloud = cloud[idxs]
        points = np.asarray(nn_cloud.points) - sample
        normals = np.asarray(nn_cloud.normals) - sample
        neighborhood = Neighborhood(sample, points, normals)

        return neighborhood

    def transform(self, R: np.array) -> Neighborhood:
        points = np.dot(R.T, self.points.T)
        normals = np.dot(R.T, self.normals.T)
        neighborhood = Neighborhood(self.sample, points.T, normals.T)

        return neighborhood

    def crop(self, axis: int, min_val: float, max_val: float) -> Neighborhood:
        mask = (self.points[:, axis] > min_val) & (self.points[:, axis] < max_val)
        points = self.points[mask]
        normals = self.normals[mask]
        neighborhood = Neighborhood(self.sample, points, normals)

        return neighborhood


# c = o3d.io.read_point_cloud("/home/andreas/data/2019-06-06_14_36_39_combined_cloud.pcd")
# c = PointCloud(c)
# sample = np.asarray(c.points)[0]
# n = Neighborhood.fromPointCloud(c, sample, 0.10)
# print(n)


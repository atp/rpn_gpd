from time import time

import cv2
import matplotlib.pyplot as plt
import numpy as np
import open3d as o3d
from scipy.stats import binned_statistic

from utils.cloud_utils import numpyToCloud
from utils.plot_images import plotImageChannels

num_channels = {"occupancy": 3, "depth": 3, "normals": 12}


def projCloudOrth(
    cloud: o3d.geometry.PointCloud,
    cube_size: float = 1.0,
    img_size: int = 600,
    img_type: str = "depth",
):
    axes = ((1, 2), (0, 2), (0, 1))

    cloud = clipCloudToCube(cloud, cube_size)
    pts = np.array(cloud.points)
    ctr = np.min(pts, 0) + 0.5 * (np.max(pts, 0) - np.min(pts, 0))
    projs = projOrth(pts - ctr)
    vert, hort = projToPixel(projs, axes, cube_size, img_size)

    if img_type == "occupancy":
        img = occupancyImage(vert, hort, img_size)
    elif img_type == "depth":
        img = depthImage(pts, vert, hort, img_size)
    elif img_type == "normals":
        normals = np.array(cloud.normals)
        img = normalsImage(pts, normals, vert, hort, img_size)

    # pad = img_size
    # img = np.pad(img, [(pad, pad), (pad, pad), (0, 0)])

    return img


def projOrth(pts):
    """Project points orthographically onto planes x=0, y=0, z=0."""
    x = np.dot(np.diag([0, 1, 1]), pts.T)
    y = np.dot(np.diag([1, 0, 1]), pts.T)
    z = np.dot(np.diag([1, 1, 0]), pts.T)
    return np.array([x.T, y.T, z.T])


def projToPixel(projs, axes, cube_size, img_size):
    cell_size = 1.0 / img_size
    vert, hort = np.empty(projs.shape[:2]), np.empty(projs.shape[:2])
    for i, ax in enumerate(axes):
        unitx = (projs[i, :, ax[0]] + 0.5 * cube_size) / cube_size
        unity = (projs[i, :, ax[1]] + 0.5 * cube_size) / cube_size
        vert[i] = np.minimum(np.floor(unitx / cell_size), img_size - 1)
        hort[i] = np.minimum(np.floor(unity / cell_size), img_size - 1)
    return vert.astype(np.int), hort.astype(np.int)


def clipCloudToCube(cloud, cube_size):
    pts = np.asarray(cloud.points)
    ctr = np.min(pts, 0) + 0.5 * (np.max(pts, 0) - np.min(pts, 0))
    pts_ctr = pts - ctr
    mask = np.all((pts_ctr > -0.5 * cube_size) & (pts_ctr < 0.5 * cube_size), 1)
    cloud_out = numpyToCloud(pts[mask, :])
    return cloud_out


def sampleFromCube(cloud, cube_size, num_samples):
    samples_cloud = clipCloudToCube(cloud, cube_size)
    if num_samples > 0 and num_samples < len(cloud.points):
        idxs = np.random.randint(len(samples_cloud.points), size=num_samples)
    else:
        idxs = np.arange(len(samples_cloud.points))
    samples = np.asarray(samples_cloud.points)[idxs]
    return samples


def downsampleByDistanceThresh(cloud, value, axis, thresh=0.03, prob=0.9):
    points = np.asarray(cloud.points)
    n = len(points)
    idxs = np.where(points[:, axis] - value < thresh)[0]
    probs = np.ones(n)
    probs[idxs] = np.random.rand(len(idxs))
    idxs = np.nonzero(probs > prob)[0]
    v = o3d.__version__
    major = v[v.find(".") + 1 :]
    major = major[: major.find(".")]
    print(v, major)
    if int(major) < 10:
        cloud_out = cloud.select_down_sample(idxs)
    else:
        cloud_out = cloud.select_by_index(idxs)
    m = len(cloud_out.points)
    print(f"Downsampled cloud by distance along axis: {n} --> {m}.")

    return cloud_out


def downsampleByDistance(cloud, min_dist, axis, min_prob=0.1, cut_off=0.02):
    points = np.asarray(cloud.points)
    points = points[points[:, axis] >= min_dist, :]
    n = len(points)
    max_dist = np.max(points[:, axis])
    probs = (points[:, axis] - min_dist) / (max_dist - min_dist)
    probs = np.maximum(min_prob, probs)
    probs[points[:, axis] >= np.min(points[:, axis]) + cut_off] = 1.0
    print(
        "probs:",
        np.min(probs),
        np.max(probs),
        np.mean(probs),
        np.count_nonzero(probs >= np.mean(probs)),
    )
    print(np.min(points[:, axis]))

    colors = np.ones(points.shape) * [1.0, 0.0, 0.0]
    colors[:, 1] = probs
    colors[:, 0] = 1.0 - probs
    cloud.colors = o3d.utility.Vector3dVector(colors)
    origin = o3d.geometry.TriangleMesh.create_coordinate_frame(0.2)
    # o3d.visualization.draw_geometries([cloud, origin], "", 640, 480)

    rand_vals = np.random.rand(len(probs))
    mask = rand_vals < probs
    points = points[mask]
    # print(f"Points downsampled from {n} to {len(points)}")

    return numpyToCloud(points)


def occupancyImage(vert, hort, img_size):
    """Calculate an occupancy image.

    An occupancy image is binary valued. If the point in the point cloud corresponding
    to a pixel is occupied, that pixel is set to 1. Otherwise, it is set to 0.

    Args:
        vert (list): the vertical coordinates of the pixels to be set to 1.
        hort (list): the horizontal coordinates of the pixels to be set to 1.
        img_size (int): the size of the image (assumes width = height).

    Returns:
        np.array: the occupancy image.
    """
    img = np.zeros((img_size, img_size, 3), np.uint8)
    for i in range(3):
        img[vert[i], hort[i], i] = 255
    return img


def calcAverageDepth(pts, idxs, channels):
    return calcAverageDepth2(pts, idxs, channels)


def calcAverageDepthRuntime(pts, idxs, channels):
    n = 100
    t0 = time()
    for i in range(n):
        a = calcAverageDepth1(pts, idxs, channels)
    t0 = time() - t0
    t1 = time()
    for i in range(n):
        b = calcAverageDepth2(pts, idxs, channels)
    t1 = time() - t1
    t2 = time()
    for i in range(n):
        c = calcAverageDepth3(pts, idxs, channels)
    t2 = time() - t2
    t3 = time()
    for i in range(n):
        d = calcAverageDepth4(pts, idxs, channels)
    t3 = time() - t3
    print("calcAverageDepth1:", t0 / n)
    print("calcAverageDepth2:", t1 / n)
    print("calcAverageDepth3:", t2 / n)
    print("calcAverageDepth4:", t3 / n)
    assert np.all()
    exit()


def calcAverageDepth4(pts, idxs, channels):
    a = [
        np.bincount(idxs[i], weights=pts[:, i]) / np.bincount(idxs[i])
        for i in range(channels)
    ]
    a = [x[x > 0] for x in a]
    return a


def calcAverageDepth3(pts, idxs, channels):
    m = np.max(idxs, 1)
    a = [
        binned_statistic(
            idxs[i], pts[:, i], "mean", bins=m[i] + 1, range=(0, m[i])
        ).statistic
        for i in range(channels)
    ]
    return a


def calcAverageDepth2(pts, idxs, channels):
    sums = [np.bincount(idxs[i], weights=pts[:, i]) for i in range(channels)]
    counts = [np.bincount(idxs[i]) for i in range(channels)]
    a = [s[c > 0] / c[c > 0] for s, c in zip(sums, counts)]
    return a


def calcAverageDepth1(pts, idxs, channels):
    counts = [np.bincount(idxs[i]) for i in range(channels)]
    valid = [counts[i] > 0 for i in range(channels)]
    sums = [np.bincount(idxs[i], weights=pts[:, i]) for i in range(channels)]
    avgs = [s[v] / c[v] for s, c, v in zip(sums, counts, valid)]

    return avgs


def depthImage(pts, vert, hort, img_size):
    """Calculate a depth image.

    For the given pixel coordinates, a depth image stores the average depth value of
    the points in the point cloud which correspond to that pixel.

    Args:
        pts (np.array): n x 3 points from the point cloud.
        vert (list): the vertical pixel coordinates.
        hort (list): the horizontal pixel coordinates.
        img_size (int): the size of the image (assumes width = height).

    Returns:
        np.array: the average depth image.
    """
    channels = 3
    tunique = time()
    idxs = (hort + vert * img_size).astype(np.int64)
    tunique = time() - tunique
    print(f"unique time: {tunique:.3f}s")
    unique = [np.unique(idxs[i]) for i in range(channels)]
    tavg = time()
    avgs = calcAverageDepth(pts, idxs, channels)
    tavg = time() - tavg

    timg = time()
    img = np.zeros((img_size * img_size, channels), np.float32)
    for i in range(channels):
        img[unique[i], i] = cv2.normalize(
            avgs[i], None, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F
        ).flatten()
    timg = time() - timg
    print(f"avg depth time: {tavg:.3f}s, image time: {timg:.3f}s")

    timg = time()
    img = img.reshape((img_size, img_size, channels))
    img = np.uint8(img * 255)
    timg = time() - timg
    print(f"image process: {timg:.3f}s")

    # Visualize the image.
    # plotImageChannels([img])

    return img


def normalsImage(pts, normals, vert, hort, img_size):
    """Calculate a normals image.

    For the given pixel coordinates, a normals image stores both the average depth
    value and the average surface normal of the points in the point cloud which
    correspond to that pixel.

    Args:
        pts (np.array): n x 3 points from the point cloud.
        vert (list): the vertical pixel coordinates.
        hort (list): the horizontal pixel coordinates.
        img_size (int): the size of the image (assumes width = height).

    Returns:
        np.array: the average depth image.
    """
    channels = 12
    projections = 3

    img = np.zeros((img_size, img_size, channels), np.float32)

    idxs_list = (hort + vert * img_size).astype(np.int64)
    unique_idxs_list = [np.unique(i) for i in idxs_list]
    unique_coords = [np.unravel_index(u, img.shape[:2]) for u in unique_idxs_list]

    depths = calcAverageDepth(pts, idxs_list, projections)
    depths = [
        cv2.normalize(d, None, 0, 1, cv2.NORM_MINMAX, cv2.CV_32F).flatten()
        for d in depths
    ]

    for i, (idxs, unique_idxs) in enumerate(zip(idxs_list, unique_idxs_list)):
        sub_normals = [normals[idxs == u] for u in unique_idxs]
        sums = [np.sum(np.abs(n), 0) for n in sub_normals]
        counts = [len(x) for x in sub_normals]
        avgs = np.array(sums) / np.array(counts)[:, None]
        v, h = unique_coords[i][0], unique_coords[i][1]
        img[v, h, i] = depths[i]
        img[v, h, (i + 1) * 3 : (i + 2) * 3] = avgs

    return img


def normalsImage1(pts, normals, vert, hort, img_size):
    channels = 12
    projections = 3
    img = np.zeros((img_size, img_size, channels), np.float32)
    img[:, :, :3] = depthImage(pts, vert, hort, img_size)

    idxs = (hort + vert * img_size).astype(np.int64)
    unique_idxs = [np.unique(idxs[i]) for i in range(projections)]
    # sums = [
    # np.array([np.sum(np.abs(normals[idxs == i, :]), 1) for i in u])
    # for u in unique_idxs
    # ]
    # counts = [
    # np.array([np.count_nonzero(normals[idxs == i, :]) for i in u])
    # for u in unique_idxs
    # ]
    # sums = [s / c[:, None] for s, c in zip(sums, counts)]

    for j in range(projections):
        v, h = np.unravel_index(unique_idxs[j], img.shape[:2])
        S = np.array(
            [np.sum(np.abs(normals[idxs[j] == i, :]), 0) for i in unique_idxs[j]]
        )
        C = np.array(
            [np.count_nonzero(normals[idxs[j] == i, :]) for i in unique_idxs[j]]
        )
        S = S / C[:, None]
        img[v, h, (j + 1) * 3 : (j + 2) * 3] = S

    return img


# This function is slower than `depthImage` by a factor of 10.
def depthImageOld(pts, vert, hort, img_size):
    img = np.zeros((img_size, img_size, 3), np.uint8)
    for i in range(3):
        img[:, :, i] = calcDepthChannel(vert[i], hort[i], pts[:, i], img_size)
    return img


def calcDepthChannel(vert, hort, depth, img_size):
    idxs = (hort + vert * img_size).astype(np.int64)

    unique_idxs = np.unique(idxs)
    sums = np.array([np.sum(depth[idxs == i]) for i in unique_idxs])
    counts = np.bincount(idxs)
    counts = counts[counts > 0]
    avgs = sums / counts
    img = np.zeros((img_size, img_size), dtype=np.float32)
    v, h = np.unravel_index(unique_idxs, img.shape[:2])
    img[v, h] = avgs

    # Normalize the image.
    img[v, h] = cv2.normalize(
        img[v, h], None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
    ).flatten()
    img = np.uint8(img * 255)

    # Visualize the image.
    # plt.imshow(img, cmap='gray', vmin=0, vmax=255)
    # plt.show()

    return img


class ProposalImageGenerator:
    """Proposal image generator."""

    def __init__(self, image_type, image_size, visualize=False):
        self.img_size = image_size
        self.img_type = image_type
        self.num_channels = num_channels[image_type]
        self.visualize = visualize

    def createImages(self, cloud, samples):
        """Create proposal images for a cloud and sample points.

        Args:
            cloud (o3d.geometry.PointCloud): the point cloud.
            samples (list): the samples.

        Returns:
            np.array: n x h x w x c proposal images.
        """
        # Create the large image for the complete point cloud.
        # cube_size = 0.5
        # img_size = 300
        cube_size = 1.0
        img_size = 600
        axes = ((1, 2), (0, 2), (0, 1))

        cloud = clipCloudToCube(cloud, cube_size)
        pts = np.array(cloud.points)
        ctr = np.min(pts, 0) + 0.5 * (np.max(pts, 0) - np.min(pts, 0))
        tproj = time()
        projs = projOrth(pts - ctr)
        tproj = time() - tproj
        tpix = time()
        vert, hort = projToPixel(projs, axes, cube_size, img_size)
        tpix = time() - tpix
        print(f"tproj: {tproj:.3f}s, tpix: {tpix:.3f}s")

        cloud_img_time = time()
        if self.img_type == "occupancy":
            cloud_img = occupancyImage(vert, hort, img_size)
        elif self.img_type == "depth":
            cloud_img = depthImage(pts, vert, hort, img_size)
        elif self.img_type == "normals":
            normals = np.array(cloud.normals)
            cloud_img = normalsImage(pts, normals, vert, hort, img_size)
        pad_time = time()
        pad = self.img_size
        cloud_img = np.pad(cloud_img, [(pad, pad), (pad, pad), (0, 0)])
        pad_time = time() - pad_time
        cloud_img_time = time() - cloud_img_time
        print(f"pad_time: {pad_time:.3f}s, cloud_img_time: {cloud_img_time:.3f}s")

        # Project the sample points.
        projs_samples = projOrth(samples - ctr)
        samplesv, samplesh = projToPixel(projs_samples, axes, cube_size, img_size)
        samples_img = occupancyImage(samplesv, samplesh, img_size)

        # plotImageChannels([cloud_img, samples_img])

        # Crop smaller images from the large image.
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        off = int(pad / 2)
        shape = (self.img_size, self.img_size, self.num_channels)
        crops_time = time()
        if self.img_type in ["depth", "occupancy"]:
            imgs = [
                cropThreeChannelsImage(
                    samplesv[:, i], samplesh[:, i], cloud_img, off, pad, shape, kernel
                )
                for i in range(len(samples))
            ]
        elif self.img_type == "normals":
            imgs = [
                cropTwelveChannelsImage(
                    samplesv[:, i], samplesh[:, i], cloud_img, off, pad, shape, kernel
                )
                for i in range(len(samples))
            ]
        crops_time = time() - crops_time
        print(f"cloud_img_time: {cloud_img_time:.6f}s, crops_time: {crops_time:.6f}s")

        # Visualize each proposal image.
        if self.visualize:
            for img in imgs:
                title = f"Proposal image ({self.num_channels} channels)"
                if self.img_type == "normals":
                    prop_img = [
                        img[:, :, i * 3 : (i + 1) * 3] for i in range(len(samplesv) + 1)
                    ]
                else:
                    prop_img = [img]
                plotImageChannels([cloud_img] + prop_img, title)

        return np.stack(imgs, axis=0)


def cropThreeChannelsImage(samplesv, samplesh, cloud_img, off, pad, shape, kernel):
    image = np.zeros(shape, dtype=np.uint8)

    for j in range(samplesv.shape[0]):
        cy, cx = pad + samplesv[j], pad + samplesh[j]
        image[:, :, j] = cloud_img[cy - off : cy + off, cx - off : cx + off, j]

    image = cv2.dilate(image, kernel, iterations=1)

    return image


def cropTwelveChannelsImage(samplesv, samplesh, cloud_img, off, pad, shape, kernel):
    image = np.zeros(shape, dtype=np.float32)

    for j in range(samplesv.shape[0]):
        cy, cx = pad + samplesv[j], pad + samplesh[j]
        ystart, ystop = cy - off, cy + off
        xstart, xstop = cx - off, cx + off
        depth = cloud_img[ystart:ystop, xstart:xstop, j]
        normals_image = cloud_img[ystart:ystop, xstart:xstop, (j + 1) * 3 : (j + 2) * 3]
        image[:, :, j] = depth
        image[:, :, (j + 1) * 3 : (j + 2) * 3] = normals_image
        image[:, :, j] = cv2.normalize(
            image[:, :, j],
            None,
            alpha=0,
            beta=1,
            norm_type=cv2.NORM_MINMAX,
            dtype=cv2.CV_32F,
        )
        image[:, :, (j + 1) * 3 : (j + 2) * 3] = cv2.normalize(
            image[:, :, (j + 1) * 3 : (j + 2) * 3],
            None,
            alpha=0,
            beta=1,
            norm_type=cv2.NORM_MINMAX,
            dtype=cv2.CV_32F,
        )

    # image = cv2.dilate(image, kernel, iterations=1)
    # Normalize the image.
    # norm_image = cv2.normalize(
    # dilation, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
    # )
    image = np.uint8(image * 255)

    return image

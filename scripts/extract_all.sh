#!/bin/bash

DIR=$1
IS_BINARY=1
POSITIVE_CLASS=1

## Create HDF5 files with binary class (one-hot labels).
python ../src/extract_data.py ${DIR}train_derived.h5 ${DIR}train_bin.h5 $IS_BINARY $POSITIVE_CLASS
python ../src/extract_data.py ${DIR}test_derived.h5 ${DIR}test_bin.h5 $IS_BINARY $POSITIVE_CLASS

# Create HDF5s files with rotation as class (multi-hot labels).
python ../src/extract_data.py ${DIR}train_derived.h5 ${DIR}train_pose.h5 0 ${POSITIVE_CLASS}
python ../src/extract_data.py ${DIR}test_derived.h5 ${DIR}test_pose.h5 0 ${POSITIVE_CLASS}

#!/bin/bash

# This script makes meshes watertight.
#
# Given an input folder, it makes each mesh in that folder watertight and stores it in 
# a given output directory.

if [ $# -eq 0 ]; then
    echo "No arguments supplied"
    exit -1
fi

if [ "$#" -ne 3 ]; then
    echo "Illegal number of parameters"
    exit -1
fi

# Read input and output folder location from terminal.
IN=$1
OUT=$2
PROGRAMS_DIR=$3
echo "input dir: $IN"
echo "output dir: $OUT"

for d in $( find ${IN}* ); do
  if [ -d ${d} ]; then
    echo $d
    category="$(basename -- $d)"
    mkdir ${OUT}${category}
    for f in $( find ${d} ); do
      if [ -f ${f} ]; then
        echo "Processing $f"
	instance="$(basename -- $f .ply)"
	fout="${OUT}${category}/${instance}"
	echo "Object instance: ${instance}"

	# Check if the mesh is watertight.
	watertight=$(python ../src/is_watertight.py $f)
	watertight=0
	if [ ${watertight} -eq 1 ]; then
	  echo "${f} is watertight"
	  cp ${f} ${fout}.ply
	  echo "Copied mesh to: ${fout}.ply"
  	else
	  echo "${f} is not watertight"
	  pcl_ply2obj $f tmp.obj
	  ${PROGRAMS_DIR}manifold tmp.obj tmp.obj
	  echo "Created watertight mesh"
	  ${PROGRAMS_DIR}simplify -i tmp.obj -o tmp.obj -m -r 0.02
	  pcl_obj2ply tmp.obj "${fout}.ply"
	  echo "Created watertight, simplified mesh: ${fout}.ply"
	  echo "==============================================================="
	fi
      fi
    done
  fi
done

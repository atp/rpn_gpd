#!/bin/bash

DIR=$1
IS_BINARY=0
POSITIVE_CLASS=1

## Create HDF5 files with binary class (one-hot labels).
python3 ../src/extract_data.py ${DIR}test_merged_derived.h5 ${DIR}test_pose.h5 $IS_BINARY $POSITIVE_CLASS
python3 ../src/extract_data.py ${DIR}train_merged_derived.h5 ${DIR}train_pose.h5 $IS_BINARY $POSITIVE_CLASS

#!/bin/bash

DIR=$1
POSITIVE_CLASS=1

## Create HDF5 files with binary class (one-hot labels).
#IS_BINARY=1
#python3 ../src/extract_data.py ${DIR}test.h5 ${DIR}test_bin.h5 $IS_BINARY $POSITIVE_CLASS
#python3 ../src/extract_data.py ${DIR}train.h5 ${DIR}train_bin.h5 $IS_BINARY $POSITIVE_CLASS

## Create HDF5 files with multiple classes (multi-hot labels).
IS_BINARY=0
python3 ../src/extract_data.py ${DIR}test.h5 ${DIR}test_pose.h5 $IS_BINARY $POSITIVE_CLASS
python3 ../src/extract_data.py ${DIR}train.h5 ${DIR}train_pose.h5 $IS_BINARY $POSITIVE_CLASS

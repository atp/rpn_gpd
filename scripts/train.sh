#!/bin/bash

DIR=$1

# Balances the classes during training by weighted random sampling.
# BALANCE=1
# Balances the classes during training by weighted loss.
BALANCE=2

python3 train_binary.py ${DIR}train_bin.h5 ${DIR}test_bin.h5 ${BALANCE}
python3 train_pose.py ${DIR}train_pose.h5 ${DIR}test_pose.h5 ${BALANCE}

#!/bin/bash

# Directories with HDF5 files for input and output
#INPUT=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/
#INPUT=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/obj_rots_50_10/
#OUTPUT=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/unbalanced/
#OUTPUT=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/unbalanced_reachable/
#OUTPUT=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/obj_rots_50_10/

#INPUT=/home/andreas/data/rpn_gpd2/3dof_hands/new_gt/bigbird/obj_rots_50_10_hand_rots_9/
#OUTPUT=/home/andreas/data/rpn_gpd2/3dof_hands/new_gt/bigbird/obj_rots_50_10_hand_rots_9/

INPUT=/home/andreas/data/rpn_gpd2/3dnet_holdout/relabel_hard_positives_20/
OUTPUT=/home/andreas/data/rpn_gpd2/3dnet_holdout/relabel_hard_positives_20/

# Index of positive class (1: collision-free, 2: antipodal)
POSITIVE_CLASS=1

## Create HDF5 files with binary class (one-hot labels).
python3 extract_data.py ${INPUT}train.h5 ${OUTPUT}train_bin.h5 1 ${POSITIVE_CLASS}
python3 extract_data.py ${INPUT}test.h5 ${OUTPUT}test_bin.h5 1 ${POSITIVE_CLASS}

# Create HDF5s files with rotation as class (multi-hot labels).
python3 extract_data.py ${INPUT}train.h5 ${OUTPUT}train_pose.h5 0 ${POSITIVE_CLASS}
python3 extract_data.py ${INPUT}test.h5 ${OUTPUT}test_pose.h5 0 ${POSITIVE_CLASS}

## Create HDF5 files with binary class (one-hot labels).
#python3 extract_data.py ${INPUT}train.h5 ${OUTPUT}train_bin.h5 1 ${POSITIVE_CLASS}
#python3 extract_data.py ${INPUT}test.h5 ${OUTPUT}test_bin.h5 1 ${POSITIVE_CLASS}
#
## Create HDF5s files with rotation as class (multi-hot labels).
#python3 extract_data.py ${INPUT}train.h5 ${OUTPUT}train_pose.h5 0 ${POSITIVE_CLASS}
#python3 extract_data.py ${INPUT}test.h5 ${OUTPUT}test_pose.h5 0 ${POSITIVE_CLASS}

# Create HDF5 files with binary class (one-hot labels).
#python3 extract_data.py ${INPUT}train_reachable.h5 ${OUTPUT}train_bin.h5 1 ${POSITIVE_CLASS}
#python3 extract_data.py ${INPUT}test_reachable.h5 ${OUTPUT}test_bin.h5 1 ${POSITIVE_CLASS}

# Create HDF5s files with rotation as class (multi-hot labels).
#python3 extract_data.py ${INPUT}train_reachable.h5 ${OUTPUT}train_pose.h5 0 ${POSITIVE_CLASS}
#python3 extract_data.py ${INPUT}test_reachable.h5 ${OUTPUT}test_pose.h5 0 ${POSITIVE_CLASS}

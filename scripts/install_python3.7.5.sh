cd ~/Downloads
wget https://www.python.org/ftp/python/3.7.5/Python-3.7.5.tar.xz
tar -xf Python-3.7.5.tar.xz
cd Python-3.7.5/
./configure --enable-optimizations
make -j
sudo make altinstall

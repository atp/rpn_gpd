#!/bin/bash

# Read input and output folder location from terminal.
IN=$1
OUT=$2
echo "input dir: $IN"
echo "output dir: $OUT"

# 1. Derive a database with binary labels, i.e., f: (n,m,5) --> (n,m,2)
echo $'\e[1;33m'Step 1/2: Deriving databases with binary labels ...$'\e[0m'
python3.7 ../src/derive_database.py ${IN}train_final_0_9.h5 ${OUT}train_derived.h5
echo '========================================================================'
python3.7 ../src/derive_database.py ${IN}test_final_0_9.h5 ${OUT}test_derived.h5
echo '========================================================================'

# 2. Create databases for each classifier.
echo $'\e[1;33m'Step 2/2: Creating databases to train each classifier ...$'\e[0m'
POSITIVE_CLASS=1

# Create HDF5 files with binary class (one-hot labels).
IS_BINARY=1
python3.7 ../src/extract_data.py ${OUT}train_derived.h5 ${OUT}train_bin.h5 \
$IS_BINARY $POSITIVE_CLASS
echo '========================================================================'
python3.7 ../src/extract_data.py ${OUT}test_derived.h5 ${OUT}test_bin.h5 \
$IS_BINARY $POSITIVE_CLASS
echo '========================================================================'

# Create HDF5s files with rotation as class (multi-hot labels).
IS_BINARY=0
python3.7 ../src/extract_data.py ${OUT}train_derived.h5 ${OUT}train_pose.h5 \
$IS_BINARY $POSITIVE_CLASS
echo '========================================================================'
python3.7 ../src/extract_data.py ${OUT}test_derived.h5 ${OUT}test_pose.h5 \
$IS_BINARY $POSITIVE_CLASS
echo '========================================================================'

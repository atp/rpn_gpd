#!/bin/bash

# Directory with HDF5 files
#D=~/data/andreas/rpn_gpd/3dof_hands/views_50train_10test/
#D=~/data/rpn_gpd2/3dof_hands/3dnet/
#D=~/data/rpn_gpd2/3dof_hands/3dnet_newGT/
#D=./data/
D=~/data/rpn_gpd2/3dof_hands/new_gt/bigbird/
OUT_DIR=${D}balanced/

# Index of positive class (1: collision-free, 2: antipodal)
POSITIVE_CLASS=2

# Create HDF5 files with binary class (one-hot labels).
python3 balance_dataset.py ${D}train.h5 ${OUT_DIR}train_bin.h5 1 ${POSITIVE_CLASS}
python3 balance_dataset.py ${D}test.h5 ${OUT_DIR}test_bin.h5 1 ${POSITIVE_CLASS}

# Create HDF5s files with rotation as class (multi-hot labels).
python3 balance_dataset.py ${D}train.h5 ${OUT_DIR}train_pose.h5 0 ${POSITIVE_CLASS}
python3 balance_dataset.py ${D}test.h5 ${OUT_DIR}test_pose.h5 0 ${POSITIVE_CLASS}

